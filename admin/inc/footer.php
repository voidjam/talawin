    </div><!-- page-container -->


    <!-- General loading icon for Ajax requests -->
    <div id="ajax-loading"></div>
    <!-- This script will show the loader at the start of every ajax request and hide it when finished -->
    <script>
        $(document).ready(function() {
            $(document).ajaxStart (function(){
                $('#ajax-loading').fadeIn(100);
            }).ajaxStop( function(){
                $('#ajax-loading').fadeOut(100);
            });
        });
    </script>

</body>

</html>