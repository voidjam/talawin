<?php

// VIEW'S PATHS FOR REDIRECTIONS.
define ('SECTION_LOGIN',                            $rootpath.'admin/login.php');
define ('SECTION_HOME',                             $rootpath.'admin/index.php');
define ('SECTION_PRINTERS',                         $rootpath.'admin/sections/printers/index.php');
define ('SECTION_PRINTERS_3D',                      $rootpath.'admin/sections/printers3d/index.php');
define ('SECTION_BANNERS',                          $rootpath.'admin/sections/banners/index.php');
define ('SECTION_VIDEOS',                           $rootpath.'admin/sections/videos/index.php');
define ('SECTION_CONFIGURATION',                    $rootpath.'admin/sections/configuration/index.php');
define ('SECTION_CLIENTS_LOGOS',                    $rootpath.'admin/sections/clients/index.php');
define ('SECTION_PROVIDERS_LOGOS',                  $rootpath.'admin/sections/providers/index.php');
define ('SECTION_TEAM_MEMBERS',                     $rootpath.'admin/sections/team/index.php');
define ('SECTION_LOGOUT',                           $rootpath.'admin/scripts/loggout.php');




// DATABASE TABLES.
define ('TABLE_PRINTERS',                           'printers');
define ('TABLE_ADMIN_USERS',                        'admin_users');
define ('TABLE_APPLICATIONS',                       'applications');
define ('TABLE_FILES',                              'files');
define ('TABLE_CLIENT_LOGO',                        'client_logos');
define ('TABLE_PROVIDERS_LOGOS',                    'providers_logos');
define ('TABLE_TEAM_MEMBERS',                       'team_members');
define ('TABLE_INK',                                'ink');
define ('TABLE_LANG',                               'lang');
define ('TABLE_MEDIA_TYPE',                         'media_type');
define ('TABLE_PRINTERS_APPLICATIONS',              'printers_applications');
define ('TABLE_PRINTER_BRAND',                      'printer_brand');
define ('TABLE_WHITE_INK',                          'white_ink');


define ('TABLE_PRINTERS3D_PRINTER3D_APPLICATIONS',  'printers3d_printer3d_applications');




// SAVE PRINTER FORM ACTIONS
define ('PRINTER_FORM_ACTION_SAVE_PRINTER',         'save_printer');
define ('PRINTER_FORM_ACTION_DELETE_PRINTER',       'delete_printer');
define ('PRINTER_FORM_ACTION_SET_IS_PUBLISHED',     'set_is_published');
define ('PRINTER_ACTION_SET_NEW_ORDER',             'set_new_order');
// SAVE PRINTER_TRANSLATION FORM ACTIONS
define ('PRINTER_TRANSLATION_FORM_ACTION_SAVE',     'save_printer_translation');
define ('PRINTER_TRANSLATION_FORM_ACTION_DELETE',   'delete_printer_translation');

// SAVE PRINTER 3D FORM ACTIONS
define ('PRINTER3D_FORM_ACTION_SAVE_PRINTER',       'save_printer');
define ('PRINTER3D_FORM_ACTION_DELETE_PRINTER',     'delete_printer');
define ('PRINTER3D_FORM_ACTION_SET_IS_PUBLISHED',   'set_is_published');
define ('PRINTER3D_ACTION_SET_NEW_ORDER',           'set_new_order');
// SAVE PRINTER_TRANSLATION FORM ACTIONS
define ('PRINTER3D_TRANSLATION_FORM_ACTION_SAVE',   'save_printer_translation');
define ('PRINTER3D_TRANSLATION_FORM_ACTION_DELETE', 'delete_printer_translation');



// SAVE ADMIN USERS FORM ACTIONS
define ('ADMIN_USER_FORM_ACTION_SAVE',              'add_new_admin');
define ('ADMIN_USER_FORM_ACTION_UPDATE',            'update_user');
define ('ADMIN_USER_FORM_ACTION_DELETE',            'DELETE');

// SAVE NEW CLIENT LOGO FORM ACTIONS
define ('CLIENT_LOGO_FORM_ACTION_SAVE',             'add_new_client_logo');
define ('CLIENT_LOGO_ACTION_SET_IS_SHOWN',          'set_is_shown');
define ('CLIENT_LOGO_ACTION_DELETE_CLIENT',         'delete_client');
define ('CLIENT_LOGO_ACTION_UPDATE_DISPLAY_ORDER',  'update_display_order');

// SAVE NEW PROVIDER LOGO FORM ACTIONS
define ('PROVIDER_LOGO_FORM_ACTION_SAVE',             'add_new_provider_logo');
define ('PROVIDER_LOGO_ACTION_SET_IS_SHOWN',          'set_is_shown');
define ('PROVIDER_LOGO_ACTION_DELETE_PROVIDER',       'delete_provider');
define ('PROVIDER_LOGO_ACTION_UPDATE_DISPLAY_ORDER',  'update_display_order');

// SAVE NEW TEAM MEMBER FORM ACTIONS
define ('TEAM_MEMBER_FORM_ACTION_SAVE',             'add_new_team_member');
define ('TEAM_MEMBER_ACTION_SET_IS_SHOWN',          'set_is_shown');
define ('TEAM_MEMBER_ACTION_DELETE_MEMBER',         'delete_member');
define ('TEAM_MEMBER_ACTION_UPDATE_DISPLAY_ORDER',  'update_display_order');
define ('TEAM_MEMBER_UPDATE_GRID_CONFIG',           'update_grid_config');

// SAVE NEW HOME BANNER FORM ACTIONS
define ('BANNER_FORM_ACTION_SAVE',                  'add_new_banner');
define ('BANNER_FORM_ACTION_UPDATE',                'update_banner');
define ('BANNER_ACTION_SET_IS_SHOWN',               'set_is_shown');
define ('BANNER_ACTION_DELETE',                     'delete_banner');
define ('BANNER_ACTION_UPDATE_DISPLAY_ORDER',       'update_display_order');

// SAVE NEW HOME VIDEO FORM ACTIONS
define ('HOME_VIDEOS_ACTION_SAVE',                  'add_new_home_video');
define ('HOME_VIDEOS_ACTION_SET_IS_SHOWN',          'set_is_shown');
define ('HOME_VIDEOS_ACTION_DELETE',                'delete_home_video');
define ('HOME_VIDEOS_ACTION_UPDATE_DISPLAY_ORDER',  'update_display_order');



?>