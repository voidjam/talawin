<div id="general-warning-div" class="alert fade show" role="alert">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <p><strong>
    	<?php 
    		echo  ($_SESSION['error']['message']);
    	?>
    </strong></p>
</div>


<!-- Script for authomatically dismiss the alert dialog after 5000 ms -->
<!-- + fadeTo() method syntax -> $(selector).fadeTo(speed,opacity,easing,callback) -->
<!-- + fadeOut() method syntax -> $(selector).fadeOut(speed,easing,callback) -->
<script>

    $(function() {

       $(".alert").fadeTo(5000, 1, "swing").fadeOut(800, function () {
           $(".alert").alert("close");
       });

        let error_level = '<?php echo($_SESSION['error']['level']); ?>';

        if (error_level.localeCompare('info') === 0) {

            $(".alert").addClass("alert-success");

        } else if (error_level.localeCompare('error') === 0) {

            $(".alert").addClass("alert-danger");

        } else if (error_level.localeCompare('warning') === 0) {

            $(".alert").addClass("alert-warning");
        }
    });

</script>

<?php unset ($_SESSION['error']); ?>