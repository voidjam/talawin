<!DOCTYPE html>
<html lang="es">


    <head>

        <!-- For IE: It forces the browser the render at whatever the most recent version's standards are. -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Page charset -->
        <meta charset="utf-8">
        <!-- Page content type -->
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CACHE CONTROL -->
        <!-- Do not store files on cache -->
        <!-- <meta http-equiv="Cache-Control" content="no-cache" />  -->
        <!-- Same for IE -->
        <!-- <meta http-equiv="Pragma" content="no-cache" />  -->
        <!-- Page refreshing delay -->
        <!-- <meta http-equiv="refresh" content="30"> -->
        <!-- Page expiry -->
        <!-- <meta http-equiv="expires" content="Wed, 21 June 2006 14:25:27 GMT">  -->

        <!-- Title -->
        <title>Talawin - Admin</title>
        <!-- <base href="http://132.148.91.154/~talawin/" /> -->

        <!-- Import an icon to represent the document, normally next to page title (favicon) -->
        <link rel="shortcut icon" href="">

        <!-- Page description -->
        <meta name="description" content="Admin page for talwin.es">
        <!-- Provide list of keywords -->
        <meta name="keywords" content="">
        <!-- Author information -->
        <meta name="author" content="Ángel Blanco: UX Designer & Web Developer">

        <!-- Tag to tell robots not to index the content of a page -->
        <meta name="robots" content="noindex, nofollow">

        <!-- It tells the browser to start full-screen mode when the user has added the site to home screen
        on mobile devices -->
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">

        <!-- Data to include when you send a link to the page by any social media -->
        <!-- Open Graph data -->
        <meta property="og:site_name" content="">
        <meta property="og:url" content="">
        <meta property="og:title" content="">
        <meta property="og:description" content="">
        <meta property="og:type" content="website">
        <meta property="og:image" content="">

        <!-- Twitter Card data -->
        <meta name="twitter:site" content="">
        <meta name="twitter:url" content="">
        <meta name="twitter:title" content="">
        <meta name="twitter:description" content="">
        <meta name="twitter:card" content="summary">
        <meta name="twitter:image" content="">


        <!-- Avoid duplicates on search engines by setting which is the version you want to show -->
        <link rel="canonical" href="https://www.talawin.es/">


        <!-- JQuery UI css -->
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"> -->
        <!-- Bootstrap 4.0.0-alpha.6 css -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
              integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

        <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->


        <!-- CDN jQuery + Bootstrap JS -->
        <!-- jQuery first, then Tether, then Bootstrap JS. -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
        <!--  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                 integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
         <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
                 integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
                 crossorigin="anonymous"></script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
        <!-- A client-side library to make absolutely positioned elements attach to elements in the page efficiently.
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script> -->



        <!-- Font Awesome icons set -->
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"> -->

        <!-- CSS FILES -->
        <!-- HTML/Text Editor -->
        <link rel="stylesheet" href="<?php echo $rootpath; ?>admin/lib/trumbowyg/custom-ui/trumbowyg.css" />
        <!-- Fine Uploader: http://docs.fineuploader.com/ -->
        <link href="<?php echo $rootpath; ?>admin/lib/fine-uploader/fine-uploader-gallery.css" rel="stylesheet">
        <link href="<?php echo $rootpath; ?>admin/lib/fine-uploader/fine-uploader-new.css" rel="stylesheet">
        <!-- Data Tables -->
        <link rel="stylesheet" href="<?php echo $rootpath; ?>admin/lib/datatables/datatables.min.css">

        <!-- MY STYLES -->
        <link rel="stylesheet" href="<?php echo $rootpath; ?>admin/css/style.css" />
        <!-- Custom styles for max-width: 992px: Tablets and Mobiles -->
        <link rel="stylesheet" media="screen and (max-width: 992px)" href="<?php echo $rootpath; ?>admin/css/style-tab.css">




		<!-- Fine Uploader: http://docs.fineuploader.com/ -->
		<!-- <link href="<?php echo $rootpath; ?>admin/libs/fine-uploader/fine-uploader-gallery.css" rel="stylesheet"> -->

        <!-- DataTables Sripts & Styles: https://datatables.net/examples/styling/bootstrap.html -->
        <!-- <link rel="stylesheet" href="<?php echo $rootpath; ?>css/dataTables.bootstrap.min.css"> -->
		<!-- <script src="<?php echo $rootpath; ?>js/jquery.dataTables.min.js"></script> -->
		<!-- <script src="<?php echo $rootpath; ?>js/dataTables.bootstrap.min.js"></script> -->
		
	</head>
	<body>
	