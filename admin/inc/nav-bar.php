<!-- ------------------------------------------------------------------------------------ -->
<!-- This is the main navigation bar. It is included using php on the different sections. -->
<!-- Before loading this html we need to:                                                 -->
<!-- + Check that the user is logged in.                                                  -->
<!-- + Include file inc/constants.php                                                     -->
<!-- + Define on PHP variable $rootpath                                                   -->
<!-- + Define on PHP variable $current_uri                                                -->
<!-- ------------------------------------------------------------------------------------ -->

<!-- NavBar -->
<nav id="navbar" class="navbar navbar-expand-lg navbar-custom-style fixed-top navbar-toggleable-md navbar-light bg-light">

    <!-- NAVBAR HEADER: BRAND -->
    <a class="navbar-brand" href="<?php echo SECTION_HOME?>"><img src="<?php echo $rootpath?>admin/img/logo-big.png" class="nav-logo" alt="Talawin, the power of quality"></a>

    <!-- TOGGLE BUTTON: This button will hide/show the content of div .navbar-collapse on mobile devices -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Collapsable items -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <!-- MAIN NAVBAR -->
        <ul id="navbar-main" class="navbar-nav mr-auto ml-auto">
            <!-- Admin Page HOME -->
            <li class="nav-item" data-section="'<?php echo SECTION_HOME?>'" onclick="location.href='<?php echo SECTION_HOME ?>'">
                <a class="nav-link">Panel <span class="sr-only">(current)</span></a>
            </li>
            <!-- Admin Page HOME content -->
            <li class="nav-item dropdown" data-section="<?php echo SECTION_BANNERS ?> OR <?php echo SECTION_VIDEOS ?>">
                <a class="nav-link dropdown-toggle" href="#" id="homeDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Home
                </a>
                <div class="dropdown-menu" aria-labelledby="homeDropdown">
                    <!-- HOME Admin Page BANNERS -->
                    <a class="dropdown-item" onclick="location.href='<?php echo SECTION_BANNERS ?>'">Home Banners</a>
                    <div class="dropdown-divider"></div>
                    <!-- HOME Admin Page VIDEOS -->
                    <a class="dropdown-item" onclick="location.href='<?php echo SECTION_VIDEOS ?>'">Home Vídeos</a>
                </div>
            </li>
            <!-- Admin Page PRINTERS DROP DOWN -->
            <li class="nav-item dropdown" data-section="<?php echo SECTION_PRINTERS ?>?action=new_printers OR <?php echo SECTION_PRINTERS ?>?action=old_printers">
                <a class="nav-link dropdown-toggle" href="#" id="printersDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Impresoras
                </a>
                <div class="dropdown-menu" aria-labelledby="printersDropdown">
                    <!-- Admin Page PRINTERS NEW -->
                    <a class="dropdown-item" onclick="location.href='<?php echo SECTION_PRINTERS ?>?action=new_printers'">Impresoras Nuevas</a>
                    <div class="dropdown-divider"></div>
                    <!-- Admin Page PRINTERS OLD -->
                    <a class="dropdown-item" onclick="location.href='<?php echo SECTION_PRINTERS ?>?action=old_printers'">Impresoras de Ocasión</a>
                </div>
            </li>
            <!-- Admin Page 3D PRINTERS -->
            <li class="nav-item" data-section="<?php echo SECTION_PRINTERS_3D ?>" onclick="location.href='<?php echo SECTION_PRINTERS_3D ?>'">
                <a class="nav-link">Impresoras 3D</a>
            </li>
            <!-- Admin Page ABOUT content -->
            <li class="nav-item dropdown" data-section="<?php echo SECTION_CLIENTS_LOGOS ?> OR <?php echo SECTION_PROVIDERS_LOGOS ?>  OR <?php echo SECTION_TEAM_MEMBERS ?>">
                <a class="nav-link dropdown-toggle" href="#" id="homeDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    About
                </a>
                <div class="dropdown-menu" aria-labelledby="homeDropdown">
                    <!-- ABOUT Admin Page CLIENT LOGOS -->
                    <a class="dropdown-item" onclick="location.href='<?php echo SECTION_CLIENTS_LOGOS ?>'">Logos Clientes</a>
                    <div class="dropdown-divider"></div>
                    <!-- ABOUT Admin Page PROVIDERS LOGOS -->
                    <a class="dropdown-item" onclick="location.href='<?php echo SECTION_PROVIDERS_LOGOS ?>'">Logos Proveedores</a>
                    <div class="dropdown-divider"></div>
                    <!-- ABOUT Admin Page TEAM TALAWIN -->
                    <a class="dropdown-item" onclick="location.href='<?php echo SECTION_TEAM_MEMBERS ?>'">Team Talawin</a>
                </div>
            </li>
        </ul>
        <!-- <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form> -->

        <!-- ADMIN USER DROPDOWN MENU -->
        <ul id="user-menu" class="navbar-nav ml-auto">
            <li class="nav-item dropdown ml-auto"  data-section="<?php echo SECTION_CONFIGURATION?>">
                <!-- If session was started we put the user name on the dropdown menu -->
                <a href="#" class="nav-link dropdown-toggle"  id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo ($_SESSION['adminUserDTO']->getName() )?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <!-- Configuration Link -->
                    <a class="dropdown-item" href="<?php echo SECTION_CONFIGURATION?>">Configuración</a>
                    <div class="dropdown-divider"></div>
                    <!-- Exit Link -->
                    <a class="dropdown-item" href="<?php echo SECTION_LOGOUT?>">Salir</a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<script>
    // This JS sets the active link on the navbar depending on the requested URL
    // First we check if there is no section on URL so we have loaded de Home Panel.
    if ("<?php echo $current_uri?>" === "/admin/")  {
        $("#navbar").find("li.nav-item[data-section=\"'<?php echo SECTION_HOME?>'\"]").addClass("active");
    // If URL contains a section we search every navbar item for matches.
    } else {
        $("#navbar").find("li.nav-item").each(function( index ) {
            if ( ~$(this).attr("data-section").indexOf("<?php echo $current_uri?>"))   {
                $(this).addClass("active");
            }
        });
    }
</script>

<!-- Page Container -->
<div class="page_container" id="page_container">




