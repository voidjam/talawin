<?php



//Definimos la ruta base del plugin
define( 'ROOT_PATH',             $rootpath);

//Definimos las rutas que vamos a utilizar en el plugin, en este caso seguimos un patrón MVC

define( 'AUTOLOADER_DTO_PATH',      ROOT_PATH .'admin/dto' );
define( 'AUTOLOADER_DAO_PATH',  	ROOT_PATH .'admin/dao' );


function wgobd_autoload( $class_name )  {

 // Convert class name to filename format.
 //$class_name = strtolower( $class_name );
 $paths = array(
   AUTOLOADER_DTO_PATH,
   AUTOLOADER_DAO_PATH
 );

 // Buscamos en cada ruta los archivos
 foreach( $paths as $path ) {
  if( file_exists( $path."/".$class_name.".php" ) )
   require_once( $path."/".$class_name.".php" );
 }

}

spl_autoload_register( 'wgobd_autoload' );

?>