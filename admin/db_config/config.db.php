<?php
/**
 * This file sets the configuration for database connection.
 *
 * It initialize the global var $db, that will store the PDO instance that we will use to connect to our database.
 * This object will be used by all the DAOs to get info from database.
 *
 * There are 3 possible database:
 *
 *  + Local database: Stored on our local machine for development purposes.
 *  + Test environment database: On our VPS, 82.223.81.125, for testing purpose. Used by testing site --> test.talawin.es
 *  + Prod environment database: On our VPS, 82.223.81.125. Used by prod site --> www.talawin.es
 *
 */


$local_environment = true;
$test_environment = false;

if ($local_environment)   {

    error_reporting(E_ALL);

    /*
     * DATABASE CONFIGURATION: Localhost
     */
    $username   = "root";
    $password   = "000000"; // In linux password = "toor", in windows "toor1234", in old computer password = ""
    $host       = "localhost";
    $dbname     = "talawin_db";

} else if ($test_environment)   {

    error_reporting(0);

    /*
     * DATABASE CONFIGURATION: TEST.TALAWIN.COM
     */
    $username   = "tsttalaw_web";
    $password   = "oB{ssy1P(A_I";
    $host       = "localhost";   // 82.223.81.125
    $dbname     = "tsttalaw_db";
} else {

    error_reporting(0);

    /*
     * DATABASE CONFIGURATION: Server VPS -> 82.223.81.125
     */
    $username   = "talawin_web";
    $password   = "!ORTH(bA@x+g";
    $host       = "localhost";
    $dbname     = "talawin_db";
}


/*
 * Set characters codifications for the PDO connection.
 */
$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_PERSISTENT => true);

try
{
    $db = new PDO("mysql:host={$host};dbname={$dbname};charset=utf8", $username, $password, $options);
}
catch(PDOException $ex)
{
    die("Failed to connect to the database: " . $ex->getMessage());
}

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);



header('Content-Type: text/html; charset=utf-8');
?>