# noinspection SqlNoDataSourceInspectionForFile

-- Versión del servidor: 5.6.27
-- Versión de PHP: 5.5.30



SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Base de datos: `Talawin`
--

-- --------------------------------------------------------




-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
-- CREATE TABLES -----------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------


-- ------------------------------------------------------------------------
-- STATIC TABLES ----------------------------------------------------------


-- --------------------------------------------------------
-- Table lang:
DROP TABLE IF EXISTS `lang`;
CREATE TABLE IF NOT EXISTS `lang` (
                                      `code`          VARCHAR(2) NOT NULL,
                                      `name`          VARCHAR(50) NOT NULL,
                                      PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------



-- NORMAL PRINTERS:
-- --------------------------------------------------------
-- Table printer_brand
DROP TABLE IF EXISTS `printer_brand`;
CREATE TABLE IF NOT EXISTS `printer_brand` (
                                               id          INT NOT NULL,
                                               label       VARCHAR(30),
                                               PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------


-- --------------------------------------------------------
-- Table printer_sub_brand
DROP TABLE IF EXISTS `printer_sub_brand`;
CREATE TABLE IF NOT EXISTS `printer_sub_brand` (
                                               id          INT NOT NULL,
                                               label       VARCHAR(30),
                                               brand_id    INT NOT NULL,
                                               PRIMARY KEY (id),
                                               CONSTRAINT fk_printer_sub_brand_brand_id FOREIGN KEY (brand_id) REFERENCES printer_brand(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

-- --------------------------------------------------------
-- Table ink
DROP TABLE IF EXISTS `ink`;
CREATE TABLE IF NOT EXISTS `ink` (
                                     id          INT NOT NULL,
                                     label_es    VARCHAR(30),
                                     label_en    VARCHAR(30),
                                     PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

-- --------------------------------------------------------
-- Table white_ink
DROP TABLE IF EXISTS `white_ink`;
CREATE TABLE IF NOT EXISTS `white_ink` (
                                           id          INT NOT NULL,
                                           label_es    VARCHAR(30),
                                           label_en    VARCHAR(30),
                                           PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

-- --------------------------------------------------------
-- Table media_type
DROP TABLE IF EXISTS `media_type`;
CREATE TABLE IF NOT EXISTS `media_type` (
                                            id          INT NOT NULL,
                                            label_es    VARCHAR(40),
                                            label_en    VARCHAR(40),
                                            PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

-- --------------------------------------------------------
-- Table applications
DROP TABLE IF EXISTS `applications`;
CREATE TABLE IF NOT EXISTS `applications` (
                                              id                  INT NOT NULL,
                                              label_es            VARCHAR(50),
                                              label_en            VARCHAR(50),
                                              file_name_prefix    VARCHAR(50) COMMENT 'Prefix for the file name of the pics stored under root/admin/img/apps/$id/',
                                              PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------


-- 3D PRINTERS:
-- --------------------------------------------------------
-- Table printer3d_brand
DROP TABLE IF EXISTS `printer3d_brand`;
CREATE TABLE IF NOT EXISTS `printer3d_brand` (
                                                 id INT NOT NULL,
                                                 label VARCHAR(30),
                                                 PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

-- --------------------------------------------------------
-- Table printer3d_ink
DROP TABLE IF EXISTS `printer3d_ink`;
CREATE TABLE IF NOT EXISTS `printer3d_ink` (
                                               id INT NOT NULL,
                                               label VARCHAR(30),
                                               PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

-- --------------------------------------------------------
-- Table printer3d_media_type
DROP TABLE IF EXISTS `printer3d_media_type`;
CREATE TABLE IF NOT EXISTS `printer3d_media_type` (
                                                      id INT NOT NULL,
                                                      label VARCHAR(40),
                                                      PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

-- --------------------------------------------------------
-- Table printer3d_applications
DROP TABLE IF EXISTS `printer3d_applications`;
CREATE TABLE IF NOT EXISTS `printer3d_applications` (
                                                        id                  INT NOT NULL,
                                                        label_es            VARCHAR(50),
                                                        label_en            VARCHAR(50),
                                                        file_name_prefix    VARCHAR(50) COMMENT 'Prefix for the file name of the pics stored under root/admin/img/apps3d/$id/',
                                                        PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------



-- END STATIC TABLES ------------------------------------------------------
-- ------------------------------------------------------------------------

-- ------------------------------------------------------------------------
-- ADMIN USERS ------------------------------------------------------------

-- --------------------------------------------------------
-- Table admin_users:
DROP TABLE IF EXISTS `admin_users`;
CREATE TABLE admin_users (
                             id        INT NOT NULL AUTO_INCREMENT,
                             email     VARCHAR(80) UNIQUE NOT NULL,
                             password  CHAR(120) NOT NULL,
                             name      VARCHAR(80),
                             updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                             created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             PRIMARY KEY (id),
                             UNIQUE INDEX (email)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

-- END OF ADMIN USERS -----------------------------------------------------
-- ------------------------------------------------------------------------



-- ------------------------------------------------------------------------
-- PRINTERS ---------------------------------------------------------------

-- --------------------------------------------------------
-- Table printers
DROP TABLE IF EXISTS `printers`;
CREATE TABLE printers (

    -- General features.
                          id                INT NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
                          display_order     INT NOT NULL,
                          brand_id          INT NOT NULL,
                          sub_brand_id      INT NOT NULL,
                          model             varchar(80) NOT NULL,
                          is_new            BOOLEAN NOT NULL DEFAULT TRUE COMMENT 'If the printer is new or second hand',
                          is_published      BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'If the printer shall be published on website',
                          is_release        BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'If the printer is a release or not',

    -- Private fields (not to show on website).
                          ref               VARCHAR(30) UNIQUE NOT NULL,
                          price_pvp         numeric(15,2) DEFAULT NULL,
                          price_cost        numeric(15,2) DEFAULT NULL,
                          comments          TEXT DEFAULT NULL,

    -- Specific fields for outlet printers.
                          production_date   DATE DEFAULT '0000-01-01',
                          warranty          INT DEFAULT 0,
                          is_sold           BOOLEAN NOT NULL DEFAULT FALSE,

    -- Technical Specifications.
                          feat_ink_id             INT NOT NULL COMMENT 'Foreign key to ink table',
                          feat_white_ink_id       INT NOT NULL COMMENT 'Foreign key to white_ink table',
                          feat_media_type_id      INT NOT NULL COMMENT 'Foreign key to media_type table',
                          feat_max_print_width    NUMERIC (5,2) NOT NULL DEFAULT 0,
                          feat_max_media_thick    NUMERIC (5,2) NOT NULL DEFAULT 0,
                          feat_drop_size          NUMERIC (5,2) NOT NULL DEFAULT 0,
                          feat_resolution         VARCHAR (12) NOT NULL DEFAULT 0,
                          feat_max_speed          NUMERIC (5,0) NOT NULL DEFAULT 0,

    -- Fields to build the printers html page.
                          title                   TEXT,
                          headline                TEXT,
                          description             TEXT,
                          html_overview           TEXT,
                          html_features           TEXT,
                          html_specs              TEXT,

                          updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                          created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          PRIMARY KEY (id),
                          UNIQUE INDEX (ref),
                          CONSTRAINT fk_printers_brand_id           FOREIGN KEY (brand_id)            REFERENCES printer_brand(id),
                          CONSTRAINT fk_printers_sub_brand_id       FOREIGN KEY (sub_brand_id)        REFERENCES printer_sub_brand(id),
                          CONSTRAINT fk_printers_feat_ink_id        FOREIGN KEY (feat_ink_id)         REFERENCES ink(id),
                          CONSTRAINT fk_printers_feat_white_ink_id  FOREIGN KEY (feat_white_ink_id)   REFERENCES white_ink(id),
                          CONSTRAINT fk_printers_feat_media_type_id FOREIGN KEY (feat_media_type_id)  REFERENCES media_type(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------


-- --------------------------------------------------------
-- Table Files:
DROP TABLE IF EXISTS `files`;
CREATE TABLE files (
                       id            INT NOT NULL AUTO_INCREMENT COMMENT 'Unique id for each file',
                       uuid          VARCHAR(36) UNIQUE NOT NULL COMMENT 'Unique key generated by FineUploader lib.',
                       is_main       BOOLEAN NOT NULL DEFAULT 0 COMMENT 'Whether the picture is the principal.',
                       printer_id    INT NOT NULL COMMENT 'The printer id the file belongs to.',
                       name          VARCHAR(100) NOT NULL COMMENT 'File name',
                       mime_type     VARCHAR(30) NOT NULL COMMENT 'File mime type',
                       path          VARCHAR(140) NOT NULL COMMENT 'File path on server',
                       size          INT NOT NULL COMMENT ' File size',
                       PRIMARY KEY (id),
                       INDEX (uuid),
                       CONSTRAINT fk_files_printer_id FOREIGN KEY (printer_id) REFERENCES printers(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

-- --------------------------------------------------------
-- Table videos:
DROP TABLE IF EXISTS `videos`;
CREATE TABLE videos (
                        id          INT NOT NULL AUTO_INCREMENT COMMENT 'Unique id for each video',
                        youtube_id  VARCHAR(16) NOT NULL COMMENT 'YouTube Id',
                        printer_id  INT NOT NULL COMMENT 'The printer id the video belongs to.',

                        updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                        created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

                        PRIMARY KEY (id),
                        CONSTRAINT fk_videos_printer_id FOREIGN KEY (printer_id) REFERENCES printers(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------



-- --------------------------------------------------------
-- Table printers_translation
DROP TABLE IF EXISTS `printers_translation`;
CREATE TABLE printers_translation (

    -- General features.
                                      id                INT NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
                                      printer_id        INT NOT NULL COMMENT 'Printer ID the translation corresponds to',
                                      lang_code         VARCHAR(2) NOT NULL COMMENT 'Code of the lang table the translation corresponds to',

    -- Fields to build the printers html page.
                                      title                   TEXT,
                                      headline                TEXT,
                                      description             TEXT,
                                      html_overview           TEXT,
                                      html_features           TEXT,
                                      html_specs              TEXT,

                                      updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                      created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                      PRIMARY KEY (id),
                                      CONSTRAINT fk_printers__id           FOREIGN KEY (printer_id)            REFERENCES printers(id),
                                      CONSTRAINT fk_lang_code              FOREIGN KEY (lang_code)             REFERENCES lang(code)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------



-- --------------------------------------------------------
-- RELATIONAL TABLE MANY TO MANY PRINTERS <--> APPLICATIONS
-- Table printers_applications:
DROP TABLE IF EXISTS `printers_applications`;
CREATE TABLE printers_applications (
                                       id              INT NOT NULL AUTO_INCREMENT,
                                       printer_id      INT NOT NULL,
                                       application_id  INT NOT NULL,
                                       PRIMARY KEY (`id`),
                                       CONSTRAINT fk_printers_applications_printer_id     FOREIGN KEY (printer_id)     REFERENCES printers(id),
                                       CONSTRAINT fk_printers_applications_application_id FOREIGN KEY (application_id) REFERENCES applications(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

-- END OF PRINTERS --------------------------------------------------------
-- ------------------------------------------------------------------------












-- ------------------------------------------------------------------------
-- 3D PRINTERS ------------------------------------------------------------

-- --------------------------------------------------------
-- Table printers3d
DROP TABLE IF EXISTS `printers3d`;
CREATE TABLE printers3d (

    -- General features.
                            id                INT NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
                            display_order     INT NOT NULL,
                            brand_id          INT NOT NULL,
                            model             varchar(80) NOT NULL,
                            is_new            BOOLEAN NOT NULL DEFAULT TRUE COMMENT 'If the printer is new or second hand',
                            is_published      BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'If the printer shall be published on website',
                            is_release        BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'If the printer is a release or not',

    -- Private fields (not to show on website).
                            ref               VARCHAR(30) UNIQUE NOT NULL,
                            price_pvp         numeric(15,2) DEFAULT NULL,
                            price_cost        numeric(15,2) DEFAULT NULL,
                            comments          TEXT DEFAULT NULL,

    -- Specific fields for outlet 3d printers.
                            production_date   DATE DEFAULT 0,
                            warranty          INT DEFAULT 0,
                            is_sold           BOOLEAN NOT NULL DEFAULT FALSE,

    -- Technical Specifications.
                            feat_ink_id             INT NOT NULL COMMENT 'Foreign key to ink table',
                            feat_media_type_id      INT NOT NULL COMMENT 'Foreign key to media_type table',
                            feat_num_headers        INT NOT NULL COMMENT 'Foreign key to white_ink table',
                            feat_max_print_vol      VARCHAR (60) NOT NULL DEFAULT '',
                            feat_quality            VARCHAR (60) NOT NULL DEFAULT '',
                            feat_software           VARCHAR (60) NOT NULL DEFAULT '',
                            feat_print_speed        VARCHAR (60) NOT NULL DEFAULT '',

    -- Fields to build the printers html page.
                            title                   TEXT,
                            headline                TEXT,
                            description             TEXT,
                            html_overview           TEXT,
                            html_features           TEXT,
                            html_specs              TEXT,

                            updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                            created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            PRIMARY KEY (id),
                            UNIQUE INDEX (ref),
                            CONSTRAINT fk_printers3d_brand_id           FOREIGN KEY (brand_id)            REFERENCES printer3d_brand(id),
                            CONSTRAINT fk_printers3d_feat_ink_id        FOREIGN KEY (feat_ink_id)         REFERENCES printer3d_ink(id),
                            CONSTRAINT fk_printers3d_feat_media_type_id FOREIGN KEY (feat_media_type_id)  REFERENCES printer3d_media_type(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------


-- --------------------------------------------------------
-- Table Files:
DROP TABLE IF EXISTS `printer3d_files`;
CREATE TABLE printer3d_files (
                                 id            INT NOT NULL AUTO_INCREMENT COMMENT 'Unique id for each file',
                                 uuid          VARCHAR(36) UNIQUE NOT NULL COMMENT 'Unique key generated by FineUploader lib.',
                                 is_main       BOOLEAN NOT NULL DEFAULT 0 COMMENT 'Whether the picture is the principal.',
                                 printer3d_id  INT NOT NULL COMMENT 'The printer id the file belongs to.',
                                 name          VARCHAR(100) NOT NULL COMMENT 'File name',
                                 mime_type     VARCHAR(30) NOT NULL COMMENT 'File mime type',
                                 path          VARCHAR(140) NOT NULL COMMENT 'File path on server',
                                 size          INT NOT NULL COMMENT ' File size',
                                 PRIMARY KEY (id),
                                 INDEX (uuid),
                                 CONSTRAINT fk_printer3d_files_printer3d_id FOREIGN KEY (printer3d_id) REFERENCES printers3d(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

-- --------------------------------------------------------
-- Table videos:
DROP TABLE IF EXISTS `printer3d_videos`;
CREATE TABLE printer3d_videos (
                                  id            INT NOT NULL AUTO_INCREMENT COMMENT 'Unique id for each video',
                                  youtube_id    VARCHAR(16) NOT NULL COMMENT 'YouTube Id',
                                  printer3d_id  INT NOT NULL COMMENT 'The printer id the video belongs to.',

                                  updated_at    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                  created_at    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

                                  PRIMARY KEY (id),
                                  CONSTRAINT fk_printer3d_videos_printer3d_id FOREIGN KEY (printer3d_id) REFERENCES printers3d(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------



-- --------------------------------------------------------
-- Table printers3d_translation
DROP TABLE IF EXISTS `printer3d_translation`;
CREATE TABLE printer3d_translation (

    -- General features.
                                       id                INT NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
                                       printer3d_id      INT NOT NULL COMMENT 'Printer ID the translation corresponds to',
                                       lang_code         VARCHAR(2) NOT NULL COMMENT 'Code of the lang table the translation corresponds to',

    -- Fields to build the printers html page.
                                       title                   TEXT,
                                       headline                TEXT,
                                       description             TEXT,
                                       html_overview           TEXT,
                                       html_features           TEXT,
                                       html_specs              TEXT,

                                       updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                       created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                       PRIMARY KEY (id),
                                       CONSTRAINT fk_printer3d_translation_printer3d_id FOREIGN KEY (printer3d_id) REFERENCES printers3d(id),
                                       CONSTRAINT fk_printer3d_translation_lang_code FOREIGN KEY (lang_code) REFERENCES lang(code)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------


-- --------------------------------------------------------
-- RELATIONAL TABLE MANY TO MANY PRINTERS3D <--> PRINTERS3D_APPLICATIONS
-- Table printers_applications:
DROP TABLE IF EXISTS `printers3d_printer3d_applications`;
CREATE TABLE printers3d_printer3d_applications (
                                                   id                        INT NOT NULL AUTO_INCREMENT,
                                                   printer3d_id              INT NOT NULL,
                                                   printer3d_application_id  INT NOT NULL,
                                                   PRIMARY KEY (`id`),
                                                   CONSTRAINT fk_printers3d_printer3d_applications_printer3d_id     FOREIGN KEY (printer3d_id)     REFERENCES printers3d(id),
                                                   CONSTRAINT fk_printers3d_printer3d_applications_printer3d_application_id FOREIGN KEY (printer3d_application_id) REFERENCES printer3d_applications(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

-- END OF 3D PRINTERS -----------------------------------------------------
-- ------------------------------------------------------------------------







-- --------------------------------------------------------
-- Table home_videos:
DROP TABLE IF EXISTS `home_videos`;
CREATE TABLE home_videos (
                             id                INT NOT NULL AUTO_INCREMENT COMMENT 'Unique id for each video',
                             is_shown          BOOLEAN NOT NULL DEFAULT TRUE,
                             display_order     INT NOT NULL,
                             youtube_id        VARCHAR(16) UNIQUE NOT NULL COMMENT 'YouTube Id',
                             youtube_title     VARCHAR(250) NOT NULL COMMENT 'YouTube Video Title',

                             updated_at      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                             created_at      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

                             PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------


-- --------------------------------------------------------
-- Table client_logos:
DROP TABLE IF EXISTS `client_logos`;
CREATE TABLE client_logos (
                              id            INT NOT NULL AUTO_INCREMENT COMMENT 'Unique id for each client logo',
                              is_shown      BOOLEAN NOT NULL DEFAULT TRUE,
                              display_order INT DEFAULT 0,
                              client_name   VARCHAR(80) NOT NULL COMMENT 'Client enterprise name',
                              file_name     VARCHAR(100) NOT NULL COMMENT 'File name',
                              mime_type     VARCHAR(30) NOT NULL COMMENT 'File mime type',
                              path          VARCHAR(140) NOT NULL COMMENT 'File path on server',
                              size          INT NOT NULL COMMENT ' File size',

                              updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                              created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

                              PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------


-- --------------------------------------------------------
-- Table home_banners:
DROP TABLE IF EXISTS `home_banners`;
CREATE TABLE home_banners (
                              id                   INT NOT NULL AUTO_INCREMENT COMMENT 'Unique id for each banner',
                              is_shown             BOOLEAN NOT NULL DEFAULT TRUE,
                              display_order        INT DEFAULT 0,
                              path                 VARCHAR(180) NOT NULL COMMENT 'File path on server for files related to this banner',

                              updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                              created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

                              PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------



-- --------------------------------------------------------
-- Table messages:
DROP TABLE IF EXISTS `messages`;
CREATE TABLE messages (
                          id          INT NOT NULL AUTO_INCREMENT COMMENT 'Unique id for each message',
                          email       VARCHAR(40) NOT NULL COMMENT 'Clients email',
                          client_name TEXT DEFAULT NULL COMMENT 'Clients name',
                          phone       VARCHAR(20) DEFAULT NULL COMMENT 'Clients phone number',
                          company     TEXT DEFAULT NULL COMMENT 'Clients company',
                          content     TEXT NOT NULL COMMENT 'Message content.',
                          subject     TEXT NOT NULL COMMENT 'Message subject.',

                          updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                          created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

                          PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------




-- --------------------------------------------------------
-- Table client_logos:
DROP TABLE IF EXISTS `providers_logos`;
CREATE TABLE providers_logos (
                                 id            INT NOT NULL AUTO_INCREMENT COMMENT 'Unique id for each provider',
                                 is_shown      BOOLEAN NOT NULL DEFAULT TRUE,
                                 display_order INT DEFAULT 0,
                                 provider_name VARCHAR(80) NOT NULL COMMENT 'Provider enterprise name',
                                 provider_link VARCHAR(120) NOT NULL COMMENT 'Provider enterprise web link',
                                 file_name     VARCHAR(100) NOT NULL COMMENT 'File name',
                                 mime_type     VARCHAR(30) NOT NULL COMMENT 'File mime type',
                                 path          VARCHAR(140) NOT NULL COMMENT 'File path on server',
                                 size          INT NOT NULL COMMENT ' File size',

                                 updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                 created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

                                 PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------



-- --------------------------------------------------------
-- Table client_logos:
DROP TABLE IF EXISTS `team_members`;
CREATE TABLE team_members (
                              id             INT NOT NULL AUTO_INCREMENT COMMENT 'Unique id for each provider',
                              is_shown       BOOLEAN NOT NULL DEFAULT TRUE,
                              display_order  INT DEFAULT 0,
                              member_name    VARCHAR(80) NOT NULL COMMENT 'Team member name to show on front-end',
                              member_role    VARCHAR(80) NOT NULL COMMENT 'Team member position to show on front-end (spanish)',
                              member_role_en VARCHAR(80) NOT NULL COMMENT 'Team member position to show on front-end (english)',
                              file_name      VARCHAR(100) NOT NULL COMMENT 'File name',
                              mime_type      VARCHAR(30) NOT NULL COMMENT 'File mime type',
                              path           VARCHAR(140) NOT NULL COMMENT 'File path on server',
                              size           INT NOT NULL COMMENT ' File size',

                              updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                              created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

                              PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------




-- --------------------------------------------------------
-- Table properties:
DROP TABLE IF EXISTS `properties`;
CREATE TABLE properties (
                            id                INT NOT NULL AUTO_INCREMENT COMMENT 'Unique id for each property',
                            name              VARCHAR(100) NOT NULL COMMENT 'Property Name',
                            value             VARCHAR(100) NOT NULL COMMENT 'Property Value',
                            description       VARCHAR(100) NOT NULL COMMENT 'Property Description',
                            created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                            PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------






-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
-- CREATE TABLES -----------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------





-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
-- ADD TRIGGERS ------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------

-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
-- ADD TRIGGERS ------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------





-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
-- INSERTS -----------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------

-- TABLE ADMIN_USERS:
TRUNCATE TABLE `admin_users`;
-- Insert initial user --> toabm@yahoo.es / 0000
INSERT INTO `admin_users` (`email`, `password`, `name`) VALUES
('toabm@yahoo.es', '$2y$10$2JuYxPn6dJk8OMC5tGFcwuXHxa39fBDmPcivMgapfRUnRqtYOvD2q', 'Ángel Blanco Muñoz' );

-- -----------------------------------------------------
-- STATIC TABLES ---------------------------------------


-- TABLE LANG:
DELETE FROM `lang` WHERE 1;
INSERT INTO `lang` (`code`, `name`) VALUES
('es', 'Español'),
('en', 'English');

-- TABLE PRINTER_BRAND:
DELETE FROM `printer_brand` WHERE 1;
INSERT INTO `printer_brand` (`id`, `label`) VALUES
(0, 'EFI'),
(1, 'LIYU');

-- TABLE PRINTER_SUB_BRAND:
DELETE FROM `printer_sub_brand` WHERE 1;
INSERT INTO `printer_sub_brand` (`id`, `label`, `brand_id`) VALUES
(0, 'EFI',          0),
(1, 'VUTEK',        0),
(2, 'MATAN',        0),
(3, 'PLATINIUM',    1);

-- 	Table printer_sub_brand
DELETE FROM `printer_sub_brand` WHERE 1;
INSERT INTO `printer_sub_brand` (`id`, `label`, `brand_id`) VALUES
    (0, 'EFI', 		0),
    (1, 'VUTEK', 	0),
    (2, 'MATAN', 	0),
    (3, 'PLATINIUM',1);

-- TABLE INK:
DELETE FROM `ink` WHERE 1;
INSERT INTO `ink` (`id`, `label_es`, `label_en`) VALUES
(0, 'UV LED',               'UV LED'),
(1, 'UV Mercurio',          'Mercury'),
(2, 'Sublimación',          'Sublimation'),
(3, 'Eco-Solvente',         'Eco-Solvent'),
(4, 'Solvente',             'Solvent'),
(5, 'UV Termoconformable',  'UV Thermoforming');

-- TABLE WHITE_INK:
DELETE FROM `white_ink` WHERE 1;
INSERT INTO `white_ink` (`id`, `label_es`, `label_en`) VALUES
(0, 'NO',                   'NO'),
(1, 'SI',                   'YES'),
(2, 'OPCIONAL',             'OPTIONAL');

-- TABLE MEDIA_TYPE:
DELETE FROM `media_type` WHERE 1;
INSERT INTO `media_type` (`id`, `label_es`, `label_en`) VALUES
(0, 'Roll-to-Roll',         'Roll-to-Roll'),
(1, 'Híbrida Cama Plana / Roll-to-Roll', 'Hybrid Flatbed /Roll-to-Roll'),
(2, 'Cama Plana',           'Flatbed');


-- TABLE APPLICATIONS:
DELETE FROM `applications` WHERE 1;
INSERT INTO `applications` (`id`, `label_es`, `label_en`, `file_name_prefix`) VALUES

(1,  'Pancartas',                               'Banners',                    'application_banner_bannergraphics_'),
(2,  'Vallas Publicitarias',                    'Billboards',                 'application_banner_billboard_'),
(3,  'Marquesinas para paradas de autobús',     'Bus Shelters',               'application_banner_busshelter_'),
(4,  'Retroiluminación día/noche',              'Day/Night Backlights',       'application_banner_daynightbacklit_'),
(5,  'Gráficos para expositores',               'Graphics for exhibitors',    'application_banner_displaygraphics_'),
(6,  'Gráficos para exposiciones',              'Exhibition graphics',        'application_banner_exhibitiongraphics_'),
(7,  'Banderas publicitarias',                  'Flags',                      'application_banner_flags_'),
(8,  'Gráficos para flotas/vehículos',          'Fleet / Vehicle Graphics',   'application_banner_fleetvehiclegraphics_'),
(9,  'Señalización interior',                   'Indoor signage',             'application_banner_signage_indoor_'),
(10, 'Gráficos para puntos de venta',           'Point-of-Purchase Graphics', 'application_banner_popgraphics_'),
(11, 'Punto de venta',                          'Point-of-Sale',              'application_banner_pospromotional_'),
(12, 'Pósteres',                                'Posters',                    'application_banner_posters_'),
(13, 'Gráficos/carteles para escaparates',      'Window clings / Graphics',   'application_banner_windowclings_'),
(14, 'Gráficos para suelos',                    'Floor Graphics',             'application_banner_floorgraphics_'),
(15, 'Gráficos en piezas de cerámica',          'Ceramic Tile Graphics',      'application_banner_ceramictile_'),
(16, 'Etiquetas',                               'Labels',                     'application_banner_labels_');


-- TABLE PRINTER3D_BRAND:
DELETE FROM `printer3d_brand` WHERE 1;
INSERT INTO `printer3d_brand` (`id`, `label`) VALUES
(0, 'MASSIVIT');

-- TABLE PRINTER3D_INK:
DELETE FROM `printer3d_ink` WHERE 1;
INSERT INTO `printer3d_ink` (`id`, `label`) VALUES
(0, 'UV Dimengel');


-- TABLE PRINTER3D_MEDIA_TYPE:
DELETE FROM `printer3d_media_type` WHERE 1;
INSERT INTO `printer3d_media_type` (`id`, `label`) VALUES
(0, '3D');


-- TABLE PRINTER3D_APPLICATIONS:
DELETE FROM `printer3d_applications` WHERE 1;
INSERT INTO `printer3d_applications` (`id`, `label_es`, `label_en`, `file_name_prefix`) VALUES

(1, 'Torso para tienda',        'Torso For Tie Shop',   '3D_App_01 - Torso For Tie Shop_'),
(2, 'Tacón de aguja',           'Stiletto Heel',        '3D_App_02 - Stiletto Heel_'),
(3, 'Punto Venta Tienda',       'Pop-Up Shop',          '3D_App_03 - Pop-Up Shop_'),
(4, 'Punto Venta Atracciones',  'POP Attractions',      '3D_App_04 - POP Attractions_'),
(5, 'Juegos electrónicos',      'Electronic Games',     '3D_App_05 - Electronic Games_'),
(6, 'Cartón de leche',          'Milk Carton',          '3D_App_06 - Milk Carton_'),
(7, 'Zapato deportivo',         'Sport Shoe',           '3D_App_07 - Sport Shoe_'),
(8, 'Muebles personalizados',   'Custom Furniture',     '3D_App_08 - Custom Furniture_');




-- TABLE PROPERTIES:
INSERT INTO `properties` (`id`, `name`, `value`, `description`, `created_at`, `updated_at`) VALUES
(1, 'laptop_team_members_item_per_row', '4', 'Items per row on section About, Team Members grid for laptop devices.', '2021-06-02 21:01:56', '2021-06-03 21:12:16'),
(2, 'tab_team_members_item_per_row', '3', 'Items per row on section About, Team Members grid for tab devices.', '2021-06-02 21:01:58', '2021-06-03 21:12:16'),
(3, 'mobile_team_members_item_per_row', '1', 'Items per row on section About, Team Members grid for mobile devices.\r\n', '2021-06-03 18:20:25', '2021-06-03 21:12:16');


-- -----------------------------------------------------
-- -----------------------------------------------------


-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
-- INSERTS -----------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------





-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
-- SCRIPT DE ACTUALIZACION DE BBDD: Inclusión del campo sub_brand en las impresoras  ---------------
-- <editor-fold>
-- Table printer_sub_brand
DROP TABLE IF EXISTS `printer_sub_brand`;
CREATE TABLE IF NOT EXISTS `printer_sub_brand`
(	 id          INT NOT NULL,
     label       VARCHAR(30),
     brand_id    INT NOT NULL,
     PRIMARY KEY (id),
     CONSTRAINT fk_printer_sub_brand_brand_id FOREIGN KEY (brand_id)
         REFERENCES printer_brand(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
-- 	Table printer_sub_brand
DELETE FROM `printer_sub_brand` WHERE 1;
INSERT INTO `printer_sub_brand` (`id`, `label`, `brand_id`) VALUES
    (0, 'EFI', 		0),
    (1, 'VUTEK', 	0),
    (2, 'MATAN', 	0),
    (3, 'PLATINIUM',1);

-- Actualizar valores por defecto de production_date / created_at --> 0000-01-01 / CURRENT TIMESTAMP en la tabla printers.


-- Fijar el varlor por defecto actual de las impresoras (aquellas que no se fijo en la creacion)
UPDATE printers SET production_date = '0000-01-01' WHERE production_date LIKE '%0000%';

-- Añadir la columna sub_brand_id a la tabla printers y el correspondiente CONSTRAIN
ALTER TABLE printers ADD COLUMN sub_brand_id INT NOT NULL DEFAULT 0;
ALTER TABLE printers ADD CONSTRAINT fk_printers_sub_brand_id FOREIGN KEY (sub_brand_id)
REFERENCES printer_sub_brand(id);

-- Ahora debemos pasar la columna brand_id a sub_brand_id de la tabla printers.
UPDATE `printers` SET `sub_brand_id` = `brand_id`;

-- Poner la columna brand_id = 0, todas las impresoras actuales son de la marca EFI
UPDATE `printers` SET `brand_id` = 0;

-- Actualizar los valores de PRINTER_BRAND
-- TABLE PRINTER_BRAND:
DELETE FROM `printer_brand` WHERE id != 0 AND ID != 1;
UPDATE `printer_brand` SET label = 'EFI' where id = 0;
UPDATE `printer_brand` SET label = 'LIYU' where id = 1;
-- -------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------- </editor-fold>
-- -------------------------------------------------------------------------------------------------






/*
PROBLEMA CON DEFAULT VALUE AL INSERTAR NULL EN UNA COLUMNA NOT NULL

Descriptción del problema:

Declaración de la tabla:

    -- --------------------------------------------------------
    -- Table messages:
DROP TABLE IF EXISTS `messages`;
CREATE TABLE messages (
                          id          INT NOT NULL AUTO_INCREMENT COMMENT 'Unique id for each message',
                          email       VARCHAR(40) NOT NULL COMMENT 'Clients email',
                          client_name TEXT DEFAULT NULL COMMENT 'Clients name',
                          phone       VARCHAR(20) DEFAULT NULL COMMENT 'Clients phone number',
                          company     TEXT DEFAULT NULL COMMENT 'Clients company',
                          content     TEXT NOT NULL COMMENT 'Message content.',
                          subject     TEXT NOT NULL COMMENT 'Message subject.',

                          updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                          created_at TIMESTAMP NOT NULL DEFAULT 0,

                          PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------


Al lanzar la query en el servidor --> Versión del servidor: 8.0.17 - MySQL Community Server - GPL

insert into messages (`email`, `client_name`, `phone`, `company`, `content`, `subject`, `created_at`) VALUES ('a','a','a','a','a','a',NULL)

    nos da el siguiente mensage de error #1048 - La columna 'created_at' no puede ser nula

Al lanzar la query con MariaDB 10.1.41 o con MySQL 5.6.31 se ejecuta correctamente y toma el valor por defecto al insertar NULL en la columna created_at

insert into messages (`email`, `client_name`, `phone`, `company`, `content`, `subject`, `created_at`) VALUES ('a','a','a','a','a','a',NULL)


    Para solucionar el problema con MySQL 8.0.17 se debe declarar CURRENT_TIMESTAMP como valor por defecto y dejar la columna vacía en la query:

    SOLUCION

-- --------------------------------------------------------
-- Table messages:
DROP TABLE IF EXISTS `messages`;
CREATE TABLE messages (
                          id          INT NOT NULL AUTO_INCREMENT COMMENT 'Unique id for each message',
                          email       VARCHAR(40) NOT NULL COMMENT 'Clients email',
                          client_name TEXT DEFAULT NULL COMMENT 'Clients name',
                          phone       VARCHAR(20) DEFAULT NULL COMMENT 'Clients phone number',
                          company     TEXT DEFAULT NULL COMMENT 'Clients company',
                          content     TEXT NOT NULL COMMENT 'Message content.',
                          subject     TEXT NOT NULL COMMENT 'Message subject.',

                          updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                          created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

                          PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

insert into messages (`email`, `client_name`, `phone`, `company`, `content`, `subject`) VALUES ('a','a','a','a','a','a')

    De esta manera al crear una nueva fila en la tabla, las dos columnas updated_at y created_at tomarian el valor CURRENT_TIMESTAMP y la columna updated_at se actualizará conrrectamente cada vez que se actualice dicha fila.

Esta solucion ha sido probada en MySQL Server 5.6.31, MariaDB 10.1.41, y MySQL 8.0.17 (En este  ultimo caso solo la solución funciona, en los dos anteriores funcionan las dos declaraciones de la tabla y los dos INSERTS independientemente, con el valor NULL u omitiendo el valor de la columna created_at).


*/





