<?php
/**
 * UPLOADSERVER.PHP ENDPOINT:
 *
 * This script receives http POST request form addUpdatePrinter.php
 * It manages the adding/updating of the corresponding printer. It receives #save-printer-form with
 * all the info related to the printer and may perform 2 different actions: save_printer and delete_printer.
 *
 * IT ALSO HANDLES THE DISPLAY ORDER OF THE PRINTERS (display_order column in printers table) REQUESTED FROM
 * DATABLE ON printers/index.html (set_new_order action) and the set_is_published action requested from
 * addUpdatePrinter.php
 *
 *  ACTIONS RELATED TO PRINTERS:
 *
 * + save_printer                 POST      It will perform an insert of a new printer, or an update (if $_POST["printer3d_id"] is set)
 *                                          of an existing one.
 * + delete_printer               POST      It will delete the corresponding printer with id $_POST["printer3d_id"] from the table
 *                                          printers and all the corresponding info about the printer: table printers_applications rows,
 *                                          table files rows, and both documents and images saved on file system.
 * + set_is_published             POST      Sets is_published field to false/true for the corresponding printer $_POST['printer3d_id'] on table printers.
 * + set_new_order                POST      Sets a new display order for printers changing the display_order column on database printer's table.
 *
 *
 * THIS ENDPOINT RECEIVES ALSO REQUEST FROM 2 FINE UPLOADER INSTANCES OF addUpdatePrinter.php.
 * ONE FOR THE PICTURES RELATED TO THE CORRESPONDING PRINTER, AND ANOTHER ONE FOR THE PDFs ASSOCIATED.
 *
 * IT ALSO RECEIVES REQUEST TO SAVE OR DELETE VIDEOS RELATED TO A PRINTER FROM addUpdatePrinter.php.
 *
 * ACTIONS RELATED TO FILES/VIDEOS:
 *
 * + save_file                      POST    For both documents and images: It will save the file on $_POST['dir'] folder.
 * + save_video                     POST    It saves a video row on videos tables related to the corresponding $_POST['printer3d_id'].
 * + delete_video                   POST    It removes the corresponding row on the videos table for $_POST['printer3d_id'].
 * + make_main                      POST    Only for images, sets is_main = true on the corresponding printer with id $_POST['file_id'].
 * + retrieve_initial_pictures      GET     Only for images: Retrieves an array of objects with the images related to the printer if any.
 * + retrieve_initial_document      GET     Only for documents: Retrieves an array of objects with the documents related to the printer if any.
 * + DELETE                         DELETE  For both documents and images: It removes the file from table files and file system.
 *
 */


$rootpath = "./../../../../";

// Get connection to database.
include $rootpath . 'admin/scripts/connect.php';
// Constants file: 
include $rootpath . 'admin/inc/constants.php';
// For thumbnails.
include $rootpath . 'admin/scripts/functions.php';


// Include the upload handler class:
require_once $rootpath . 'admin/sections/printers3d/controllers/Upload3dHandler.php';

// Instantiate the Upload Handler to handle files:
$uploader = new Upload3dHandler();
$uploader->setRootpath($rootpath);
// Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
$uploader->allowedExtensions = array('jpeg', 'jpg', 'gif', 'png','pdf'); // All files types allowed by default
// Specify max file size in bytes.
$uploader->sizeLimit = null;
// Specify the input name set in the javascript.
$uploader->inputName = "qqfile"; // matches Fine Uploader's default inputName value by default


$method = $_SERVER["REQUEST_METHOD"];

/** ********************************************** */
/** ********** PRINTERS RELATED ACTIONS ********** */
/** ********************************************** */

/** ACTION PRINTER_FORM_ACTION_SAVE_PRINTER */
// If we pressed SAVE printer BUTTON:
if ($method == "POST" && isset($_POST['action']) && strcmp( $_POST['action'], PRINTER3D_FORM_ACTION_SAVE_PRINTER) == 0)	{



    //$printerDTO = new PrinterDTO(json_encode($_POST));
    $printer3dDTO = new Printer3dDTO();
    $printer3dDAO = new Printer3dDAO();


    // If id is set, we are UPDATING an existing printer, Printe3drDAO will check it and execute an UPDATE query.
    $printer3dDTO->setId(isset($_POST["printer3d_id"]) && $_POST["printer3d_id"] != 0 ? $_POST["printer3d_id"] : null);


    // brand_id is set and has a int value 0 or bigger.
    $printer3dDTO->setBrandId(array_key_exists(Printer3dDAO::FIELD_BRAND_ID, $_POST) && is_int(intval($_POST[Printer3dDAO::FIELD_BRAND_ID])) && $_POST[Printer3dDAO::FIELD_BRAND_ID] >= 0
        ? intval($_POST[Printer3dDAO::FIELD_BRAND_ID])
        : 0);
    $printer3dDTO->setModel(array_key_exists(Printer3dDAO::FIELD_MODEL, $_POST) && is_string($_POST[Printer3dDAO::FIELD_MODEL])
        ? $_POST[Printer3dDAO::FIELD_MODEL]
        : "0");
    $printer3dDTO->setIsNew(isset($_POST[Printer3dDAO::FIELD_IS_NEW]) && $_POST[Printer3dDAO::FIELD_IS_NEW] === 'on'
        ? 1
        : 0);
    $printer3dDTO->setIsPublished(isset($_POST[Printer3dDAO::FIELD_IS_PUBLISHED]) && $_POST[Printer3dDAO::FIELD_IS_PUBLISHED] === 'on'
        ? 1
        : 0);
    $printer3dDTO->setIsRelease(isset($_POST[Printer3dDAO::FIELD_IS_RELEASE]) && $_POST[Printer3dDAO::FIELD_IS_RELEASE] === 'on'
        ? 1
        : 0);
    $printer3dDTO->setIsSold(isset($_POST[Printer3dDAO::FIELD_IS_SOLD]) && $_POST[Printer3dDAO::FIELD_IS_SOLD] === 'on'
        ? 1
        : 0);



    $printer3dDTO->setRef(isset($_POST[Printer3dDAO::FIELD_REF])
        ? $_POST[Printer3dDAO::FIELD_REF]
        : "0");
    $printer3dDTO->setPricePvp(isset($_POST[Printer3dDAO::FIELD_PRICE_PVP])
        ? $_POST[Printer3dDAO::FIELD_PRICE_PVP]
        : 0);
    $printer3dDTO->setPriceCost(isset($_POST[Printer3dDAO::FIELD_PRICE_COST])
        ? $_POST[Printer3dDAO::FIELD_PRICE_COST]
        : 0);
    $printer3dDTO->setComments(isset($_POST[Printer3dDAO::FIELD_COMMENTS])
        ? $_POST[Printer3dDAO::FIELD_COMMENTS]
        : "0");






    // TECH FEATURES
    $printer3dDTO->setFeatInkId(isset($_POST[Printer3dDAO::FIELD_FEAT_INK_ID]) && is_int(intval($_POST[Printer3dDAO::FIELD_FEAT_INK_ID])) && $_POST[Printer3dDAO::FIELD_FEAT_INK_ID] >= 0
        ? $_POST[Printer3dDAO::FIELD_FEAT_INK_ID]
        : 0);
    $printer3dDTO->setFeatMediaTypeId(isset($_POST[Printer3dDAO::FIELD_FEAT_INK_ID]) && is_int(intval($_POST[Printer3dDAO::FIELD_FEAT_MEDIA_TYPE_ID])) && $_POST[Printer3dDAO::FIELD_FEAT_MEDIA_TYPE_ID] >= 0
        ? $_POST[Printer3dDAO::FIELD_FEAT_MEDIA_TYPE_ID]
        : 0);
    $printer3dDTO->setFeatNumHeaders(array_key_exists(Printer3dDAO::FIELD_FEAT_NUM_HEADERS, $_POST)
        ? $_POST[Printer3dDAO::FIELD_FEAT_NUM_HEADERS]
        : "0");
    $printer3dDTO->setFeatMaxPrintVol(array_key_exists(Printer3dDAO::FIELD_FEAT_MAX_PRINT_VOL, $_POST)
        ? $_POST[Printer3dDAO::FIELD_FEAT_MAX_PRINT_VOL]
        : "0");
    $printer3dDTO->setFeatPrintSpeed(array_key_exists(Printer3dDAO::FIELD_FEAT_PRINT_SPEED, $_POST)
        ? $_POST[Printer3dDAO::FIELD_FEAT_PRINT_SPEED]
        : "0");
    $printer3dDTO->setFeatQuality(array_key_exists(Printer3dDAO::FIELD_FEAT_QUALITY, $_POST)
        ? $_POST[Printer3dDAO::FIELD_FEAT_QUALITY]
        : "0");
    $printer3dDTO->setFeatSoftware(array_key_exists(Printer3dDAO::FIELD_FEAT_SOFTWARE, $_POST)
        ? $_POST[Printer3dDAO::FIELD_FEAT_SOFTWARE]
        : "0");
    $printer3dDTO->setProductionDate(isset($_POST[Printer3dDAO::FIELD_PRODUCTION_DATE])
        ? date_format(date_create($_POST[Printer3dDAO::FIELD_PRODUCTION_DATE]),"Y-m-d" )
        : '0000-00-00');
    $printer3dDTO->setWarranty(isset($_POST[Printer3dDAO::FIELD_WARRANTY])
        ? $_POST[Printer3dDAO::FIELD_WARRANTY]
        : "0");


    // HTML FIELDS:
    $printer3dDTO->setTitle(isset($_POST[Printer3dDAO::FIELD_TITLE]) && !empty($_POST[Printer3dDAO::FIELD_TITLE])
        ? $_POST[Printer3dDAO::FIELD_TITLE]
        : "0");
    $printer3dDTO->setHeadline(isset($_POST[Printer3dDAO::FIELD_HEADLINE]) && !empty($_POST[Printer3dDAO::FIELD_HEADLINE])
        ? $_POST[Printer3dDAO::FIELD_HEADLINE]
        : "0");
    $printer3dDTO->setDescription(isset($_POST[Printer3dDAO::FIELD_DESCRIPTION]) && !empty($_POST[Printer3dDAO::FIELD_DESCRIPTION])
        ? $_POST[Printer3dDAO::FIELD_DESCRIPTION]
        : "0");
    $printer3dDTO->setHtmlOverview(isset($_POST[Printer3dDAO::FIELD_HTML_OVERVIEW]) && !empty($_POST[Printer3dDAO::FIELD_HTML_OVERVIEW])
        ? $_POST[Printer3dDAO::FIELD_HTML_OVERVIEW]
        : "0");
    $printer3dDTO->setHtmlFeatures(isset($_POST[Printer3dDAO::FIELD_HTML_FEATURES]) && !empty($_POST[Printer3dDAO::FIELD_HTML_FEATURES])
        ? $_POST[Printer3dDAO::FIELD_HTML_FEATURES]
        : "0");
    $printer3dDTO->setHtmlSpecs(isset($_POST[Printer3dDAO::FIELD_HTML_SPECS]) && !empty($_POST[Printer3dDAO::FIELD_HTML_SPECS])
        ? $_POST[Printer3dDAO::FIELD_HTML_SPECS]
        : "0");


    try {
        // Here we save the PrinterDTO using a Printer3dDAO object to connect to database.
        $result = intval($printer3dDAO->savePrinter($printer3dDTO));

        // This will manage possible 'Duplicate Entry' exceptions for the ref column
        // that may occur when trying to save the item on database.
    } catch (Exception $e) {

        $result = $e->getMessage();

    }


    // If there was any error and the printer was not saved: We redirect to the form and
    // inform about the error on a warning box.
    if (!is_int($result) || $result < 0)	{

        $_SESSION['error']['level'] = "error";

        if (is_string($result) && strpos($result, 'Duplicate entry') !== false )	{

            // Save an error to put a warning in the page we are being redirected: addprinter.php
            $_SESSION['error']['message'] = "La Refencia introducida ya existe en la base de datos.";

        } else {

            // Save an error to put a warning in the page we are being redirected: addprinter.php
            $_SESSION['error']['message'] = "Error al guardar el elemento. Inténtelo de nuevo pasados unos minutos.";
        }

        // We store the posted info to go back to the form page.
        $_SESSION["post"] = $_POST;
        header("Location:".$rootpath."admin/sections/printers3d/addUpdatePrinter.php");
        exit;

        // The printer was correctly added/updated to database.
    } else if (is_int($result) || $result >= 0)	{

        // Add selected applications to printers.
        $apps_array = array_keys($_POST['apps']);
        // If we are UPDATING a printer we reset the applications and add the new ones.
        isset($_POST["printer3d_id"]) && $_POST["printer3d_id"] != "" ? $printer3dDAO->resetApplicationsForPrinter3d($_POST["printer3d_id"]) : null;
        foreach ($apps_array as $value) {
            $printer3dDAO->addApplicationToPrinter3d(intval($value),intval($result));
        }



        $_SESSION['error']['level'] = 'info';
        isset($_POST["printer3d_id"]) && $_POST["printer3d_id"] != ""
            ? $_SESSION['error']['message'] = 'La impresora ha sido actualizada correctamente...:)'
            : $_SESSION['error']['message'] = 'La impresora ha sido guardada correctamente...:)';
        $_SESSION['printer3d_id'] = $result;
        header("Location:".$rootpath."admin/sections/printers3d/addUpdatePrinter3d.php");
        exit;
    }

    // If we pressed DELETE BUTTON:
}
/** ACTION PRINTER_FORM_ACTION_DELETE_PRINTER */
else if ($method == "POST" && isset($_POST['action']) && strcmp( $_POST['action'], PRINTER3D_FORM_ACTION_DELETE_PRINTER) == 0)	{

    $printer3d_id = $_POST['printer3d_id'];
    $printer_is_new = $_POST[Printer3dDAO::FIELD_IS_NEW];

    // Creating database connections.
    $printer3dDAO = new Printer3dDAO();
    $printer3dFileDAO = new Printer3dFileDAO();
    $printer3dTransDAO = new Printer3dTransDAO();


    try {

        // Deleting files form database.
        $filesNumber = $printer3dFileDAO->deleteFilesByPrinterId($printer3d_id);
        // Deleting files from file system.
        if ($filesNumber >= 1)  { delete_files($rootpath."admin/img/printers/".$printer3d_id); };
        // Deleting related apps.
        $printer3dDAO->resetApplicationsForPrinter3d($printer3d_id);
        // Delete Translation if exist:
        if ($printer3dDAO->hasTranslation($printer3d_id)) $printer3dTransDAO->deletePrinter3dTransByPrinter3dId($printer3d_id);

        // Here we delete the printer row:
        $result = intval($printer3dDAO->deletePrinter3dById($printer3d_id));

    } catch (Exception $e) {

        $result = $e->getMessage();

    }

    // If there was any error and the printer was not saved: We redirect to the form and
    // inform about the error on a warning box.
    if (!is_int($result) || $result < 0)	{

        $_SESSION['error']['level'] = 'error';
        $_SESSION['error']['message'] = 'La impresora no pudo ser borrada de la base de datos';

    } else 	{

        $_SESSION['error']['level'] = 'info';
        $_SESSION['error']['message'] = 'La impresora se eliminó de la base de datos y los archivos correspondientes han sido borrados del sistema.';

    }

    // $_GET data to go back to printers table.
    header("Location:".$rootpath."admin/sections/printers3d/index.php");
    exit;
}
/** ACTION PRINTER_FORM_ACTION_SET_IS_PUBLISHED */
else if ($method == "POST" && strcmp( $_POST['action'], PRINTER3D_FORM_ACTION_SET_IS_PUBLISHED) == 0)  {
    header("Content-Type: text/plain");

    $result = $uploader->setIsPublished($_POST['is_published'],$_POST['printer3d_id']);
    echo json_encode($result);
}
/** ACTION SET_NEW_ORDER */
else if ($method == "POST" && strcmp( $_POST['action'], PRINTER3D_ACTION_SET_NEW_ORDER) == 0)  {
    header("Content-Type: text/plain");

    $result = $uploader->setNewOrder($_POST['new_order']);
    echo json_encode($result);
}
/** ********************************************** */
/** ********************************************** */


/** ********************************************** */
/** ********** FILES RELATED ACTIONS ************* */
/** ********************************************** */
/** ACTION SAVE_FILE */
// Upload a new file to the server.
else if ($method == "POST" && strcmp( $_POST['action'], 'save_file') == 0) {
    header("Content-Type: text/plain");

    // Handles upload requests:
    // Call handleUpload() with the path of the folder to save the file in.
    $result = $uploader->handleUpload($_POST['dir']);
    // To return a name used for uploaded file you can use the following line.
    $result["uploadName"] = $uploader->getUploadName();
    
    echo json_encode($result);
}
/** ACTION SAVE_VIDEO */
// Saves the youtube video on database for the corresponding printer.
else if ($method == "POST" && strcmp( $_POST['action'], 'save_video') == 0)  {

    header("Content-Type: text/plain");

    $result = $uploader->handleSaveVideo();
    echo json_encode($result);


}
/** ACTION MAKE_MAIN */
// Make one of the already uploaded pics the main one:
else if ($method == "POST" && strcmp( $_POST['action'], 'make_main') == 0)  {

    header("Content-Type: text/plain");

    $result = $uploader->handleMakeMain();
    echo json_encode($result);


}
/** ACTION RETRIEVE_INITIAL_PICTURES */
// Retrieve existing pictures of the selected watch.
else if ($method == "GET" && strcmp( $_GET['action'], 'retrieve_initial_pictures') == 0)  {

    header("Content-Type: text/plain");

    $result = $uploader->retrieveInitialPictures($_GET['printer3d_id']);
    echo json_encode($result);


}
/** ACTION RETRIEVE_INITIAL_DOCUMENTS */
// Retrieve existing pictures of the selected watch.
else if ($method == "GET" && strcmp( $_GET['action'], 'retrieve_initial_documents') == 0)  {

    header("Content-Type: text/plain");

    $result = $uploader->retrieveInitialDocuments($_GET['printer3d_id']);
    echo json_encode($result);

}
/** ACTION DELETE */
// Delete one of the already uploaded files:
else if ($method == "DELETE") {

    // We pass the thumbnails dir to the handler if we want it to be deleted:
    $result = $uploader->handleDelete();
    echo json_encode($result);
}
/** ACTION DELETE_VIDEO */
// Deletes the youtube all video on database for the corresponding printer.
else if ($method == "POST" && strcmp( $_POST['action'], 'delete_video') == 0)  {

    header("Content-Type: text/plain");

    $result = $uploader->handleDeleteVideo();
    echo json_encode($result);


}
/** ********************************************** */
/** ********************************************** */


/** ********************************************** */
/** ********** TRANSLATION RELATED ACTION********* */
/** ********************************************** */
/** PRINTER_TRANSLATION_FORM_ACTION_SAVE */
else if ($method == "POST" && strcmp( $_POST['action'], PRINTER3D_TRANSLATION_FORM_ACTION_SAVE) == 0)  {

    $printer3dTransDAO = new Printer3dTransDAO();
    $printer3dTransDTO = new Printer3dTransDTO(json_encode($_POST));

    $printer3dTransDTO->setId(isset($_POST["printer3d_translation_id"]) && $_POST["printer3d_translation_id"] != 0 ? $_POST["printer3d_translation_id"] : null);

//    $printerTransDTO->setPrinterId(array_key_exists(PrinterTransDAO::FIELD_PRINTER3D_ID, $_POST) && is_int(intval($_POST[PrinterTransDAO::FIELD_PRINTER3D_ID])) && $_POST[PrinterTransDAO::FIELD_printer3d_id] >= 0
//        ? intval($_POST[PrinterTransDAO::FIELD_PRINTER3D_ID])
//        : "");
//    $printerTransDTO->setLangCode(array_key_exists(PrinterTransDAO::FIELD_LANG_CODE, $_POST) && isset($_POST[PrinterTransDAO::FIELD_LANG_CODE]) && !empty($_POST[PrinterTransDAO::FIELD_LANG_CODE])
//        ? strval($_POST[PrinterTransDAO::FIELD_LANG_CODE])
//        : "");

    try {
        // Here we save the PrinterTransDTO using a PrinterTransDAO object to connect to database.
        $printer3dTransID = intval($printer3dTransDAO->savePrinter3dTrans($printer3dTransDTO));
    } catch (Exception $e) {
        $printer3dTransID = $e->getMessage();
    }

    header("Content-Type: text/plain");
    if (is_int($printer3dTransID) || $printer3dTransID >= 0)	{

        $result =  json_encode(array("success" => true,
            "message" => "La traducción ha sido correctamente guardada en base de datos.",
            "printer3d_translation_id" => $printer3dTransID,
            "printer3dTransDTO" => $printer3dTransDTO));
        echo $result;
        exit;
    }
    echo json_encode(array("success" => false,
        "message" => "La traducción no pudo ser guardada. Inténtelo en unos minutos y contacte con su administrador si el problema persiste."));
    exit;
}
/** PRINTER_TRANSLATION_FORM_ACTION_DELETE */
else if ($method == "POST" && strcmp( $_POST['action'], PRINTER3D_TRANSLATION_FORM_ACTION_DELETE) == 0)  {

    $printer3dTransDAO = new Printer3dTransDAO();
    $result = $printer3dTransDAO->deletePrinter3dTransByPrinter3dId($_POST[Printer3dTransDAO::FIELD_PRINTER3D_ID]);

    header("Content-Type: text/plain");
    if (is_int($result) || $result > 0)	{
        $result =  json_encode(array("success" => true,
            "message" => "La traducción ha sido correctamente borrada de la base de datos.",
            "printer3d_translation_id" => false));
        echo $result;
        exit;
    }
    echo json_encode(array("success" => false,
        "message" => "La traducción no pudo ser borrada. Inténtelo en unos minutos y contacte con su administrador si el problema persiste."));
    exit;
}

/** ********************************************** */
/** ********************************************** */


/** ********************************************** */
/** ********** NO METHOD ERROR ******************* */
/** ********************************************** */
else {
    header("HTTP/1.0 405 Method Not Allowed");
}
?>