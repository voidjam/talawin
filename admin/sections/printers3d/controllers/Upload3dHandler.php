<?php
/**
 * Do not use or reference this directly from your client-side code.
 * Instead, this should be required via the endpoint.php uploadServer.php
 * file(s).
 */

class Upload3dHandler {

    // To keep the rootpath value of the calling script.
    public $rootpath;

    // FineUploader variables:
    public $allowedExtensions = array();
    public $sizeLimit = null;
    public $inputName = 'qqfile';
    protected $uploadName;

    /**
     * @return mixed
     */
    public function getRootpath() { return $this->rootpath; }
    /**
     * @param mixed $rootpath
     */
    public function setRootpath($rootpath) { $this->rootpath = $rootpath; }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // FINEUPLOADER METHODS. ///////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Get the original filename
     */
    public function getName(){
        if (isset($_REQUEST['qqfilename']))
            return $_REQUEST['qqfilename'];
        if (isset($_FILES[$this->inputName]))
            return $_FILES[$this->inputName]['name'];
    }
    public function getInitialFiles() {
        $initialFiles = array();
        for ($i = 0; $i < 5000; $i++) {
            array_push($initialFiles, array("name" => "name" + $i, uuid => "uuid" + $i, thumbnailUrl => "/test/dev/handlers/vendor/fineuploader/php-traditional-server/fu.png"));
        }
        return $initialFiles;
    }
    /**
     * Get the name of the uploaded file
     */
    public function getUploadName(){
        return $this->uploadName;
    }
    // FINEUPLOADER METHODS. ///////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // PRINTER3D IMAGES/DOCUMENTS RELATED METHODS //////////////////////////////////////////////////////////////////////
    /**
     * Process the upload: This method will create an entry on database and will
     * save the file and a thumbnail on the file system.
     *
     * @param string $uploadDirectory Target directory to save the pics and th thumbnails.
     * @param string $name Overwrites the name of the file.
     *
     * @return array
     */
    public function handleUpload($uploadDirectory = null, $name = null){

        // Get the paramameters of the request:
        $printer3d_id = $_POST['printer3d_id'];
        
        // Check that the max upload size specified in class configuration does not
        // exceed size allowed by server config
        if ($this->toBytes(ini_get('post_max_size')) < $this->sizeLimit ||
            $this->toBytes(ini_get('upload_max_filesize')) < $this->sizeLimit){
            $neededRequestSize = max(1, $this->sizeLimit / 1024 / 1024) . 'M';
            return array('error'=>"Server error. Increase post_max_size and upload_max_filesize to ".$neededRequestSize);
        }

        //if ($this->isInaccessible($uploadDirectory)){
            //return array('error' => "Server error. Uploads directory isn't writable");
        //}

        $type = $_SERVER['CONTENT_TYPE'];
        if (isset($_SERVER['HTTP_CONTENT_TYPE'])) {
            $type = $_SERVER['HTTP_CONTENT_TYPE'];
        }

        if(!isset($type)) {
            return array('error' => "No files were uploaded.");
        } else if (strpos(strtolower($type), 'multipart/') !== 0){
            return array('error' => "Server error. Not a multipart request. Please set forceMultipart to default value (true).");
        }

        // Get size and name
        $file = $_FILES[$this->inputName];
        $size = $file['size'];
        if (isset($_REQUEST['qqtotalfilesize'])) {
            $size = $_REQUEST['qqtotalfilesize'];
        }
        if ($name === null){
            $name = $this->getName();
        }

        // check file error
        if($file['error']) {
            return array('error' => 'Upload Error #'.$file['error']);
        }


        // Validate name
        if ($name === null || $name === ''){
            return array('error' => 'File name empty.');
        }
        // Validate file size
        if ($size == 0){
            return array('error' => 'File is empty.');
        }
        if (!is_null($this->sizeLimit) && $size > $this->sizeLimit) {
            return array('error' => 'File is too large.', 'preventRetry' => true);
        }

        // Validate file extension
        $pathinfo = pathinfo($name);
        $ext = isset($pathinfo['extension']) ? $pathinfo['extension'] : '';
        if($this->allowedExtensions && !in_array(strtolower($ext), array_map("strtolower", $this->allowedExtensions))){
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
        }

        
        # Upload File:

        // Unic ID generated by FineUploader: We need to save in database.
        $uuid = $_REQUEST['qquuid'];

        // For every file received, PHP fulfils the following fields, that we will need
        // that we will need to store the file on the data base.
        //$fileName = $file['name']; // When using scale options of fineuploader this does not work.
        $fileType = $file['type'];
        $fileSize = $file['size'];



        // Set target: file path + file name
        $target = join(DIRECTORY_SEPARATOR, array($this->rootpath.$uploadDirectory, $name));
        if ($target){

            // First we check that there is not a file with same name on path dir, and will change the
            // name adding a random 2 char string if so.
            if (file_exists($target))   {
                $temp = explode(".", $name);
                $name = reset($temp).'_'.substr(md5(microtime()),rand(0,26),2). '.' . end($temp);
                $target = join(DIRECTORY_SEPARATOR, array($this->rootpath.$uploadDirectory, $name));
            }

            // Create the directory if it does not exist yet (first uploaded file).
            $this->uploadName = basename($target);
            if (!is_dir(dirname($target))){
                mkdir(dirname($target), 0777, true);
            }

            // Now we write the file on file system.
            if (move_uploaded_file($file['tmp_name'], $target)){
                //return array('success'=> true, "uuid" => $uuid);

                // ONLY FOR PICTURES, NOT FOR DOCUMENTS.
                // Before saving the file on database we will construct a thumbnail.
                if (null !== $this->rootpath.$uploadDirectory && strpos($fileType, 'image') !== false)   {
                    createThumbs($target, $this->rootpath.$uploadDirectory, 250, 250, "thum_".$name);
                }

                // Finally we will use the corresponding DAO to save the file on the data base.
                $printer3dFileDAO = new Printer3dFileDAO();
                $insertedFileId = $printer3dFileDAO->saveAttachedFile($uuid, $printer3d_id, 0, $name,  $fileType,  $uploadDirectory, $fileSize);

                // Check if there any main picture for this printer and make this pic the main one if not:
                $is_main = false;
                if ($printer3dFileDAO->checkHasMain($printer3d_id) == 0 && strpos($fileType, 'image') !== false) {
                    $printer3dFileDAO->makeMain($insertedFileId);
                    $is_main = true;
                }

                if ($insertedFileId > 0)   {
                    return array('success'=> true, "uuid" => $uuid, "file_id"=> $insertedFileId, "is_main" => $is_main);
                } else {
                    return array('error'=> 'Could not save uploaded file.' .
                        'The upload was cancelled, or server error encountered');
                }
            }
        }
        return array('error'=> 'Could not save uploaded file.' .
            'The upload was cancelled, or server error encountered');
    }


    /**
     * This method deletes a file from both database and file system.
     * @param null $thumbnailDir
     * @param null $name
     * @return array
     */
    public function handleDelete($thumbnailDir = null, $name=null) {

        // printer3d_id to check if there is at least one main picture.
        $printer3d_id = $_REQUEST['printer3d_id'];

        $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $tokens = explode('/', $url);
        $file_uuid = $tokens[sizeof($tokens)-1];


        $printer3dFileDAO = new Printer3dFileDAO();
        $printer3dFileDTO = $printer3dFileDAO->getFileByUuid($file_uuid);

        // Before deleting the file row we will have to get rid of the picures and its thumbnails on file system,
        // if it is an image.
        if (is_dir($this->rootpath.$printer3dFileDTO->getPath()))   {

            if (strpos($printer3dFileDTO->getMime_type(), 'image') !== false)  {
                unlink($this->rootpath.$printer3dFileDTO->getPath()."thum_".$printer3dFileDTO->getName());
            }
            unlink($this->rootpath.$printer3dFileDTO->getPath().$printer3dFileDTO->getName());
        }

        // Finally we will use the corresponding DAO to delete the file from database.
        $result = $printer3dFileDAO->deleteFileByUuid($file_uuid);

        // If it was an image, once deleted the file we check if it was the main one
        $is_main = false;
        if (strpos($printer3dFileDTO->getMime_type(), 'image') !== false)  {
            if ($printer3dFileDAO->checkHasMain($printer3d_id) == 0) {
                $is_main = true;
            }
        }

        if ($result==1)   {
            return array("success" => true, "uuid" => $file_uuid, "is_main" => $is_main);
        }  else {
            return array("success" => false,
                "error" => "File not found! Unable to delete.".$url,
                "path" => $this->rootpath.$printer3dFileDTO->getPath().$printer3dFileDTO->getName()
            );
        }
    }


    /**
     * Process a make main picture request. Mark the selected picture as the
     * main one and all the rest from the same printer_id as normal ones.
     * The file Id is encapsuled within the $_POST variable.
     *
     */
    public function handleMakeMain () {

        // File id sent within the POST request:
        $file_id = $_POST['file_id'];

        // And we use a FileDAO object to access to database and make it the main pic:
        $printer3dFileDAO = new Printer3dFileDAO();
        $result = $printer3dFileDAO->makeMain($file_id);

        return array("success" => true, "file_id" => $file_id, "result" => $result);
    }

    /**
     * It look in database for the files associated to the corresponding printer3d that match an image extension.
     * It returns a JSON array containing Objects for each file to be fed into the uploader.
     * The following properties are recognised by the FineUploader:
     *       + name: String - Name of the file. --> REQUIRED!
     *       + uuid: String - UUID of the file. --> REQUIRED!
     *       + size: Number - Size of the file, in bytes.
     *       + deleteFileEndpoint: String - Endpoint for the associated delete file request. If omitted, the deleteFile.endpoint is used.
     *       + deleteFileParams: Object - Parameters to send along with the associated delete file request. If omitted, deleteFile.params is used.
     *       + thumbnailUrl: String - URL of an image to display next to the file.
     *
     * @param string $printer3d_id Overwrites the name of the file.
     *
     * @return array Array containing initials files to be loaded by FineUploader.
     */
    public function retrieveInitialPictures($printer3d_id)   {

        $printer3dFileDAO = new Printer3dFileDAO();
        $printer3dFilesArray = $printer3dFileDAO->getImagesByPrinter3dId($printer3d_id);

        if(!is_array($printer3dFilesArray))  { return array("success" => true, "info" => "There are not files associated to this printer (".$printer3d_id.").");}

        // We can send the array of objects AS IS, because the FileDTO objects has
        // both "name" and "uuid" defined, so the FineUploader will understand it.
        // The only thing that is not already set is the "thumbnailUrl", so we have
        // to customize each FileDTO to include this field.
        // In order to be able to directly modify array elements within the loop
        // precede $value with &. In that case the value will be assigned by reference.
        foreach ($printer3dFilesArray as  &$printer3dFileDTO)   {

            // If thumbnail has ben deleted we will create it.
            if (!file_exists($this->rootpath.$printer3dFileDTO->getPath()."thum_".$printer3dFileDTO->getName()))   {

                // Before saving the file on database we will construct a thumbnail:
                if (null !== $this->rootpath.$printer3dFileDTO->getPath())   {
                    createThumbs($this->rootpath.$printer3dFileDTO->getPath().$printer3dFileDTO->getName(), $this->rootpath.$printer3dFileDTO->getPath(), 250, 250, "thum_".$printer3dFileDTO->getName());
                }
            }

            $printer3dFileDTO->setThumbnailUrl("talawin/".$this->rootpath.$printer3dFileDTO->getPath()."thum_".$printer3dFileDTO->getName());
            $printer3dFileDTO = (object)$printer3dFileDTO;
        }

        return $printer3dFilesArray;
    }



    /**
     * It look in database for the files associated to the corresponding printer3d that match a document extension.
     * It returns a JSON array containing Objects for each file to be fed into the uploader.
     * The following properties are recognised by the FineUploader:
     *       + name: String - Name of the file. --> REQUIRED!
     *       + uuid: String - UUID of the file. --> REQUIRED!
     *       + size: Number - Size of the file, in bytes.
     *       + deleteFileEndpoint: String - Endpoint for the associated delete file request. If omitted, the deleteFile.endpoint is used.
     *       + deleteFileParams: Object - Parameters to send along with the associated delete file request. If omitted, deleteFile.params is used.
     *
     * @param string $printer3d_id Overwrites the name of the file.
     *
     * @return array Array containing initials files to be loaded by FineUploader.
     */
    public function retrieveInitialDocuments($printer3d_id)   {

        $printer3dFileDAO = new Printer3dFileDAO();
        $printer3dFilesArray = $printer3dFileDAO->getDocumentsByPrinter3dId($printer3d_id);

        if(!is_array($printer3dFilesArray))  { return array("success" => true, "info" => "There are not files associated to this printer."); }
        return $printer3dFilesArray;
    }


    /**
     * This method is triggered when user publish or un-publish a printer.
     *
     * If it is to publish it ( $is_published = true) it will check first if the printer has associated
     * image files. If not it will return an error.
     * We will also check if there is a translation related to the printer, if not we will warn the user.
     *
     * If it is to un-publish the printer it won't check anything, just set is_published = false on database for the
     * corresponding printer.
     *
     * @param $is_published
     * @param $printer3d_id
     * @return array
     * @internal param $is_publisehd
     */
    public function setIsPublished($is_published, $printer3d_id)   {

        $printer3dFileDAO = new Printer3dFileDAO();
        $printer3dDAO = new Printer3dDAO();


        If ($is_published === "true" && $printer3dFileDAO->hasImages($printer3d_id))  {

            $result = $printer3dDAO->setIsPublish($is_published,$printer3d_id);

            // Check if printer has translation to warn user in case it doesn't.
            $translation_info = "";
            if (!$printer3dDAO->hasTranslation($printer3d_id))   {
                $translation_info = "<span style='color: darkred;'> No ha añadido la traducción.</span>";
            }

            if($result) return array("success" => true, "header" => "Publicada!",
                "info" => "La impresora ha sido publicada.".$translation_info, "is_published" => true);

        } else if ($is_published === "false") {

            $result = $printer3dDAO->setIsPublish($is_published,$printer3d_id);

            if($result) return array("success" => true, "header" => " No Publicada!","info" => "La impresora ya no está publicada.", "is_published" => false);

        } else if (!$printer3dFileDAO->hasImages($printer3d_id))    {

            return array("success" => false, "header" => " Error, No Publicada!", "info" => "La impresora no puede ser publicada hasta que añada imágenes.", "is_published" => false);
        }
    }

    // END OF PRINTER3D IMAGES/DOCUMENTS RELATED METHODS ///////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // PRINTER3D VIDEOS RELATED METHODS/////////////////////////////////////////////////////////////////////////////////

    /**
     * This method uses a VideoDAO object to save the video $_POST['youtube_id']
     * on database and relates it to the corresponding $_POST['printer_id'].
     *
     * @return array
     */
    public function handleSaveVideo() {

        $printer3dVideoDAO = new Printer3dVideoDAO();
        $result = $printer3dVideoDAO->saveVideo(  $_POST['youtube_id'], $_POST['printer3d_id']);

        return array("success" => true, "video_id" => $_POST['youtube_id'], "result" => $result,
            "message" => "Elvídeo fue correctamnte guardado en base de datos.");

    }

    /**
     * This method uses a VideoDAO object to delete all videos related to the corresponding $_POST['printer_id']
     *
     * @return array
     */
    public function handleDeleteVideo()   {

        $printer3dVideoDAO = new Printer3dVideoDAO();
        $result = $printer3dVideoDAO->deletePrinter3dVideosByPrinter3dId($_POST['printer3d_id']);

        return array("success" => true, "video_id" => $_POST['youtube_id'], "result" => $result,
            "message" => "El vídeo fué correctamente borrado de la base de datos.");

    }
    // END OF PRINTER3D VIDEOS RELATED METHODS//////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////






    /**
     * This method receives an array containing pairs [printer_id, new_display_order] and
     * updates de new display_order value on the corresponding printers.
     *
     * @param $new_order
     * @return null
     */
    public function setNewOrder($new_order)   {

        $printer3dDAO = new Printer3dDAO();

        foreach ($new_order as $item) {
            $result = $printer3dDAO->updateDisplayOrder($item[0], $item[1]);
        }

        return array("success" => true, "result" => $result);


    }






    /**
     * Converts a given size with units to bytes.
     * @param string $str
     * @return int|string
     */
    protected function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;
        }
        return $val;
    }
    
    /**
     * Determines is the OS is Windows or not
     *
     * @return boolean
     */
    protected function isWindows() {
    	$isWin = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN');
    	return $isWin;
    }


    /**
     * Determines whether a directory can be accessed.
     *
     * is_executable() is not reliable on Windows prior PHP 5.0.0
     *  (http://www.php.net/manual/en/function.is-executable.php)
     * The following tests if the current OS is Windows and if so, merely
     * checks if the folder is writable;
     * otherwise, it checks additionally for executable status (like before).
     *
     * @param string $directory The target directory to test access
     * @return bool
     */
    protected function isInaccessible($directory) {
        $isWin = $this->isWindows();
        $folderInaccessible = ($isWin) ? !is_writable($directory) : ( !is_writable($directory) && !is_executable($directory) );
        return $folderInaccessible;
    }
}?>