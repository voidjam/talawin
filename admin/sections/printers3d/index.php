<?php

$rootpath = "./../../../";
$current_uri = "$_SERVER[REQUEST_URI]";

// Get connection to database.
include $rootpath . 'admin/scripts/connect.php';
// To get link paths.
include $rootpath . 'admin/inc/constants.php';

$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";


if (!empty($_SESSION["LOGGED"]) && $_SESSION["LOGGED"] == true) {


    // Include Header.
    include $rootpath . 'admin/inc/header.php';
    include $rootpath . 'admin/inc/nav-bar.php';

    // Retrieve all printers3d from database.
    $printer3dDAO = new Printer3dDAO();
    $printer3dFileDAO = new Printer3dFileDAO();
    $arrayPrinter3dDTO = $printer3dDAO->getPrinters();

    // Get the brands labels.
    $arrayBrands = $printer3dDAO->getPrinter3dBrands();


    // Set page title.
    $title = "IMPRESORAS 3D";
    ?>

    <!-- DIV containing the watches table -->
    <div class="printers3d-new-content">
        <!-- DIV header explaining what is ther inside -->
        <div class="table-main-header"><?php echo $title ?></div>



        <!-- ADD PRINTER Button -->
        <button onclick="location.href='<?php echo $rootpath?>admin/sections/printers3d/addUpdatePrinter3d.php'" class="btn btn-success right-top-button" role="button"><strong>Añadir Impresora 3D</strong></button>

        <!-- Horizontal separator -->
        <hr />


        <!-- IS_NEW filter input -->
        <select class="form-control" id="filter_new">
            <option value="0">Mostrar Todas</option>
            <option value="1">Mostrar solo Nuevas</option>
            <option value="2">Mostrar solo de Ocasión</option>
        </select>
        <!-- /IS_NEW filter input -->

        <!-- The Table -->
        <div class="table-printers-container ml-md-4 ">

            <table class="table table-striped table-hover table-responsive" id="table-printers3d" width="100%" style="display:none">

                <!-- Table Head -->
                <thead class="table table-striped table-bordered thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>REF</th>
                        <th>MARCA</th>
                        <th>MODELO</th>
                        <th>PUB</th>
                        <th>FOTOS</th>
                        <th>IDIOMAS</th>

                        <th>VENDIDA</th>
                        <th>PRINTER_ID</th>
                        <th>NUEVA</th>

                    </tr>
                </thead>

                <!-- Table Body -->
                <tbody>

                <?php

                // If there are any printers on database we start creating rows:
                if (is_array($arrayPrinter3dDTO) && sizeof($arrayPrinter3dDTO) > 0)	{


                    // Retrieve the main image for every printer object.
                    foreach ($arrayPrinter3dDTO as $index => &$printer3dDTO) {
                        $printer3dDTO->setFilesArray($printer3dFileDAO->getMainImageByPrinter3dId($printer3dDTO->getId()));
                        unset($printer3dDTO);
                    }

                    // We create a row for each Printer3dDTO contained on the array.
                    foreach ($arrayPrinter3dDTO as $key => $printer3dDTO) {

                        // Create a new Row.
                        echo "<tr  title='Haga doble click para editar esta impresora.'>";
                        // Column Number
                        echo "<td scope='row' title='Arrastre para cambiar el orden.'>".($key + 1)."</td>";
                        // Column Reference
                        echo "<td>".$printer3dDTO->getRef()."</td>";
                        // Column Brand
                        echo "<td>".$arrayBrands[$printer3dDTO->getBrandId()]."</td>";
                        // Column Model
                        echo "<td>".$printer3dDTO->getModel()."</td>";
                        // Column IS_PUBLISHED
                        echo "<td>".$printer3dDTO->isPublished()."</td>";
                        // Column FOTOS
                        $fileDTO = $printer3dDTO->getFilesArray();
                        if ($fileDTO instanceof Printer3dFileDTO)    {
                            $main_pic = "<img src='".$rootpath.$printer3dDTO->getFilesArray()->getPath().$printer3dDTO->getFilesArray()->getName()."' class='img-thumbnail'>";
                        } else  {
                            $main_pic =  "<span><strong>No hay fotos</strong></span>";
                        }
                        echo "<td>".$main_pic."</td>";
                        // Column translations
                        echo "<td>".!empty($printer3dDTO->getPrinter3dTransDTO())."</td>";
                        // Column is_sold
                        echo "<td>".$printer3dDTO->isSold()."</td>";
                        // Hidden column to store the corresponding printer3d ID.
                        echo "<td>".$printer3dDTO->getId()."</td>";
                        // Hidden column to store the corresponding printer3d is_new value.
                        echo "<td>".$printer3dDTO->isNew()."</td>";
                        // End of Row
                        echo "</tr>";
                        unset($printer3dDTO);
                    } // End foreach
                } ?>
                </tbody>
            </table>
        </div>
    </div><!-- /printers-new-content -->

    <!-- This form will be used on click on any row to add a dynamic form -->
    <!-- and send the corresponding printer3d id to addUpdatePrinter3d.php    -->
    <div id="update-printer-form"></div>


    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- SECTION PAGE INFO +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="page_section_info unselectable">
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION PAGE INFO TXT +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <span><?php echo $title ?></span>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- /SECTION PAGE INFO TXT ++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    </div>
    <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->




    <!-- Load DataTables JS -->
    <script src="<?php echo $rootpath; ?>admin/lib/datatables/datatables.js"></script>





    <script>


        /**
         * This method appends a warning div to #page_container containing a message for the user.
         *
         * @param message Message to be presented on screen.
         * @param error_level ['info', 'error', 'warning'] Sets the color of the warning div.
         */
        function showWarningMessage(message, error_level) {

            // Check if there is already an allert message on screen and close it.
            if($('.alert').length) { $(".alert").alert("close"); }

            $('#page_container').append('<div id=\"general-warning-div\" class=\"alert fade show\" role=\"alert\">'
                + '<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>'
                + '<p><strong>' + message + '</strong></p></div>');

            $(".alert").fadeTo(5000, 1, "swing").fadeOut(800, function () {
                $(".alert").alert("close");
            });

            if (error_level.localeCompare('info') === 0) {
                $(".alert").addClass("alert-success");
            } else if (error_level.localeCompare('error') === 0) {
                $(".alert").addClass("alert-danger");
            } else if (error_level.localeCompare('warning') === 0) {
                $(".alert").addClass("alert-warning");
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // DOCUENT READY BLOCK /////////////////////////////////////////////////////////////////////////////////////////
        $(function () {

            // Select only new or old printers.
            let filter_new = $('#filter_new').val();;


            /**
             * This method is used to render is_published column.
             *
             * It returns an <img> element with yes.svg or no.svg depending on the data,
             * together with a <span> hidden element used to order the column.
             *
             * @param data  The data for the cell (based on columns.data)
             * @param type  The type call data requested.
             * @param row   The full data source for the row (not based on columns.data)
             * @param meta  An object that contains additional information about the cell being requested.
             * @returns {string}
             */
            let simple_checkbox = function ( data, type, row, meta ) {
                let is_checked = data == true ? "yes" : "no";
                return '<img class="is-published-icon" src="<?php echo $rootpath ?>/admin/img/'+ is_checked +'.svg"/>'
                    + '<span style="display:none">' + is_checked + '</span>';
            }


            /**
             * This method is used to render LANGUAGES column. It returns an img element containing the flags for
             * the corresponding languages that the printer has.
             *
             * @param data
             * @param type
             * @param row
             * @param meta
             * @returns {string}
             */
            let languages = function ( data, type, row, meta ) {
                let flags_div = '<img class="language-icon" src="<?php echo $rootpath ?>/admin/img/es-icon.svg"/>';
                if (data == true)   {
                    flags_div += '<img class="language-icon" src="<?php echo $rootpath ?>/admin/img/en-icon.png"/>';
                }
                return flags_div;
            }


            let new_old = function ( data, type, row, meta ) {
                let flags_div = '<span><strong>OCASIÓN</strong></span>';
                if (data == true)   {
                    flags_div = '<span><strong>NUEVA</strong></span>';
                }
                return flags_div;
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////
            // INIT DATATABLES FOR #table-printers TABLE. //////////////////////////////////////////////////////
            let table = $('#table-printers3d').DataTable( {

                rowReorder: {
                    selector: 'td.reorder',
                    snapX: 30,
                    scrollY: 300,
                    paging: true
                },
                dom:	'<"top row"<"#lenght.col-3"l><"#filter.col-3"f>>' +
                        '<"row"<"col"rt>>' +
                        '<"bottom row"<"#info.col"i><"#pagination.col"p>><"clear">',
                rowId: "7",
                nowrap: true,
                responsive: true,
                autoWidth: true,
                language: {
                    "lengthMenu": "_MENU_ por página.",
                    "zeroRecords": "No hay resultados que correspondan con el texto introducido.",
                    "info": "Del _START_º al _END_º de _TOTAL_ items",
                    "infoEmpty": "No hay información. La Base de Datos está Vacía.",
                    "infoFiltered": "(Filtrado de _MAX_ impresoras)",
                    "search": "_INPUT_",
                    searchPlaceholder: "Buscar...",
                    paginate: {
                        "first":      "Primero",
                        "last":       "Ultimo",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                },

                lengthMenu: [ 1, 5, 10, 15, 20, 25, 50 ],

                pageLength: 5,

                columns: [
                    null,                                                               // Column 0 #
                    null,                                                               // Column 1 REF
                    null,                                                               // Column 2 MARCA
                    null,                                                               // Column 3 MODELO
                    {"render": simple_checkbox, "className": "is-published-column"},    // Column 4 PUB
                    { "className": "main-pic-column" },                                 // Column 5 IMAGES
                    {"render": languages, "className": "languages-column"},             // Column 6 LANGUAGES
                    { "className": "is-sold-column", "render": simple_checkbox },       // Column 7 VENDIDA
                    null,                                                               // Column 8 PRINTER_ID
                    { "className": "is-new-column", "render": new_old }                 // Column 9 IS_NEW                                                      // Column 9 IS_NEW
                ],

                columnDefs: [
                    { "targets": [0], "orderable": true, "className": 'reorder' }, // Column 0: #
                    { "targets": [4], "searchable": false, "type": "html" }, // Column 4: IS PUBLISHED
                    { "targets": [5], "searchable": false, "type": "html", "ordering": false }, // Column 5: IMAGES
                    { "targets": [6], "searchable": false, "type": "html" }, // Column 6: LANGUAGES
                    { "targets": [7], "visible": false, "searchable": false, "ordering": false }, // Column 7: VENDIDA
                    { "targets": [8], "visible": false, "searchable": false, "ordering": false }, // Column 8: PRINTER_ID
                    { "targets": [9], "visible": true, "searchable": true, "ordering": true }, // Column 9: IS_NEW
                    { "width": "1%", "targets": [0,1,2,3,4 ] }
                ]

            });

            // Show table after datatable rendering, it was initially hidden.
            $('#table-printers3d').fadeIn(200);

            // Adjust columns size.
            table.columns.adjust();

            // Listen for responsive resize
            table.on( 'responsive-resize', function ( e, datatable, columns ) {
                let count = columns.reduce( function (a,b) {
                    return b === false ? a+1 : a;
                }, 0 );
                //console.log( count +' column(s) are hidden' );
            } );




            // END OF INIT DATATABLES FOR #table-printers TABLE. ///////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////


            ////////////////////////////////////////////////////////////////////////////////////////////////////
            // ROW REORDER EVENT. //////////////////////////////////////////////////////////////////////////////
            // Listen to Drag&Drop reorder event.
            table.on( 'row-reorder', function ( e, diff, edit ) {

                // Reorder started on row: edit.triggerRow.data()[0];
                // We create an array containing the new positions as pairs [printer_id, new_display_oder]
                // fir every printer that has changed it position.
                let new_order = [];

                // Loop throw all rows that has been affected by the drag&drop action, that is, all printers
                // that have changed its display_order.
                for ( let i=0, ien=diff.length ; i<ien ; i++ ) {

                    // We get the data of the corresponding row.
                    let rowData = table.row( diff[i].node ).data();

                    // Push a new element [printer_id, new_display_oder].
                    new_order.push( [rowData[8], diff[i].newData] );
                }

                // Post to the server to handle the changes
                $.ajax({
                    type: "POST",
                    url: "<?php echo $rootpath?>admin/sections/printers3d/controllers/uploadServer.php",
                    data: {
                        new_order: new_order,
                        action: '<?php echo PRINTER3D_ACTION_SET_NEW_ORDER ?>'
                    },
                    beforeSend: function() {
                        // Disable dragging
                        table.rowReorder.disable();
                    },
                    success: function(html) {
                        // Re-enable dragging
                        table.rowReorder.enable();
                        showWarningMessage("El nuevo orden ha sido correctamente guardado en base de datos.", "info");
                    }
                });
            } );
            // END OF ROW REORDER EVENT. ///////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////



            ////////////////////////////////////////////////////////////////////////////////////////////////////
            // IS_NEW FILTER. //////////////////////////////////////////////////////////////////////////////////
            /**
             * Custom filtering function which will search data in column 9 -> is_new depending on
             * $('#filter_new').val():
             *
             * + value == 0 --> show all.
             * + value == 1 --> show only new.
             * + value == 2 --> show only old.
             *
             * DataTables provides an API method to add your own search functions,
             * $.fn.dataTable.ext.search.
             * This is an array of functions (push your own onto it) which will will
             * be run at table draw time to see if a particular row should be included or not.
             */
            $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex ) {

                    let filter_value = $('#filter_new').val();
                    let is_new = (( data[9] ) == 'NUEVA' ? true : false) || 0; // use data for the age column

                    console.log(data);

                    if (filter_value == 0 ) {
                        return true;
                    } else if (filter_value == 1 && is_new) {
                        return true;
                    } else if (filter_value == 2 && !is_new) {
                        return true;
                    } else {
                        return false;
                    }
                }
            );

            // Event listener to the is_new filtering inputs to redraw on input change.
            $('#filter_new').on('change', function() {
                // Hide column IS_SOLD in case we are showing only new printers.
                if ($(this).val() == 2) {
                    // Toggle the visibility
                    table.column(7).visible( true );
                } else {
                    table.column(7).visible( false );
                }

                table.draw();
            } );
            // END OF IS_NEW FILTER. ///////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////



            ////////////////////////////////////////////////////////////////////////////////////////////////////
            // EDIT PRINTER: DOUBLE CLICK ON ROW ///////////////////////////////////////////////////////////////
            // ON CLICK  on any row we will create dynamically a form
            // that will be submitted to addUpdatePrinter.php and that will take
            // us to the edit page of the corresponding watch.
            $('#table-printers3d').find('tbody').on('dblclick', 'tr', function () {

                // First we take the printer id hidden on the 7th column of the table.
                let data = table.row(this).data();
                let printer3d_id = data[8];

                let $div = $("#update-printer-form");
                let $form = $("<form method='POST'></form>");
                $form.attr("action", '<?php echo $rootpath; ?>admin/sections/printers3d/addUpdatePrinter3d.php' );
                $form.append('<input id="printer3d_id" name="printer3d_id" type=text value="' + printer3d_id + '">');
                $div.append($form);
                $form.submit();
            });
            // END OF EDIT PRINTER: DOUBLE CLICK ON ROW ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////


        }); // End of document ready block.
    </script>

    <?php

    // Check for errors and warnings:
    if(isset($_SESSION['error']) && is_array($_SESSION['error']))   include $rootpath . 'admin/inc/warning.php';

    // Include Footer.
    include $rootpath . 'admin/inc/footer.php';

} else 	{

    header("Location:".SECTION_LOGIN);
    exit;

}