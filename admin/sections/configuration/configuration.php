<?php
/**
 * This is the controler for the confuguration index.php page. It will receive the form and perform the
 * requiered actions.
 */

$rootpath = "./../../../";
$current_uri = "$_SERVER[REQUEST_URI]";

// Get connection to database.
include $rootpath . 'admin/scripts/connect.php';
include $rootpath . 'admin/scripts/functions.php';
include $rootpath . 'admin/inc/constants.php';

if (!empty($_POST) && $_SERVER['REQUEST_METHOD'] == "POST")	{

	// AdminUserDAO to connect with database:
	$adminUserDAO = new AdminUserDAO();
    $loggedAdminUserDTO = $_SESSION['adminUserDTO'];

	// ACTION UPDATE_USER.
	// If we updating the current admin user info:
	if (isset($_POST['action']) && $_POST['action'] == ADMIN_USER_FORM_ACTION_UPDATE)	{


		 $adminUserDTO = new AdminUserDTO();


		 $adminUserDTO->setId(isset($_POST['admin_user_id']) ? $_POST['admin_user_id'] : '');
		 $adminUserDTO->setEmail($_POST['email']);
		 $adminUserDTO->setName($_POST['name']);


		 // If the current password is OK.
		 if ( isset($_POST['old_pass']) && !empty($_POST['old_pass']) && password_verify($_POST['old_pass'], $loggedAdminUserDTO->getPassword()) ) {


             // If a new password was set on the form and they match.
             if (!empty($_POST['new_pass_1']) && !empty($_POST['new_pass_2']) && $_POST['new_pass_1'] === $_POST['new_pass_2']) {
                 // Set the new password before updating the admin user.
                 $adminUserDAO->updateAdminUserPassword($loggedAdminUserDTO->getId(), better_crypt($_POST['new_pass_1']));

             } else if (!isset($_POST['new_pass_1']) || !isset($_POST['new_pass_2']) || $_POST['new_pass_1'] != $_POST['new_pass_2']) {
                 $_SESSION['error']['level'] = "error";
                 $_SESSION['error']['message'] = "Las contraseñas no coinciden...:(";
                 header("Location:" . $rootpath . "admin/sections/configuration/index.php");
                 exit;
             }

             // Now we update the database.
             try {

                 // Here we save the AdminUserDTO using a DAO object to connect to database.
                 $result = intval($adminUserDAO->updateAdminUser($adminUserDTO));

                 // This will manage possible 'Duplicate Entry' exceptions for the ref column
                 // that may occur when trying to save the item on database.
             } catch (Exception $e) {
                 $result = $e->getMessage();
             }

             // If there was any error and the admin user was not saved: We redirect to the conf page and
             // inform about the error on a warning box.
             if (!is_int($result) || $result < 0) {
                 // Save an error to put a warning in the page we are being redirected: addWatch.php
                 $_SESSION['error']['level'] = "error";
                 $_SESSION['error']['message'] = "Error al actualizar el Usuario.<br>Inténtelo de nuevo pasados unos minutos.";
                 header("Location:" . $rootpath . "admin/sections/configuration/index.php");
                 exit;
             // ADMIN USER WAS CORRECTLY UPDATED.
             } else {

                 $_SESSION['error']['level'] = 'info';
                 $_SESSION['error']['message'] = 'El usuario ha sido actualizado.';
                 $_SESSION['adminUserDTO'] = $adminUserDAO->getAdminUserById($adminUserDTO->getId());
                 header("Location:".$rootpath."admin/sections/configuration/index.php");
                 exit;
             }
         // If the current password is KO.
         } else {

             $_SESSION['error']['level']	= "error";
             $_SESSION['error']['message']	= "La contraseña actual no es correcta.";
             header("Location:".$rootpath."admin/sections/configuration/index.php");
             exit;
         }


    // ACTION ADD_NEW_ADMIN
	// If creating a new admin user:
	} elseif (isset($_POST['action']) && $_POST['action'] == ADMIN_USER_FORM_ACTION_SAVE)	{


		$adminUserDTO = new AdminUserDTO();

		$adminUserDTO->setEmail($_POST['email']);
		$adminUserDTO->setName($_POST['name']);
		$adminUserDTO->setPassword(better_crypt($_POST['pass_1']));

		try {
			// Here we save the WatchDTO using a DAO object to connect to database.
			$result = intval($adminUserDAO->saveAdminUser($adminUserDTO));
			
		// This will manage possible 'Duplicate Entry' exceptions for the ref column
		// that may occur when trying to save the item on database.	
		} catch (Exception $e) {

			$result = $e->getMessage();
			
		}

		// If there was any error and the watch was not saved: We redirect to the form and
		// inform about the error on a warning box.
		if (!is_int($result) || $result < 0)	{

			$_SESSION['error']['level'] = 'error';
			$_SESSION['error']['message'] = 'El nuevo usuario no puedo ser creado.'; 
		
		} else 	{

			$_SESSION['error']['level'] = 'info';
			$_SESSION['error']['message'] = 'Nuevo usuario administrador añadido.';

		}

		header("Location:".$rootpath."admin/sections/configuration/index.php");
  		exit;


	}


// DELETE USER
}  elseif ($_SERVER['REQUEST_METHOD'] == ADMIN_USER_FORM_ACTION_DELETE) {

    $adminUserDAO = new AdminUserDAO();

    // Get request parameters.
    parse_str(file_get_contents("php://input"),$del_vars);

    try  {
        // Here we delete the user from database.
        $result = $adminUserDAO->deleteAdminUserById($del_vars["id"]);
    } catch (Exception $e) {
        $result = $e->getMessage();
    }

    // If there was any error and the watch was not saved: We redirect to the form and
    // inform about the error on a warning box.
    if ($result == 1)	{
        $_SESSION['error']['level'] = 'info';
        $_SESSION['error']['message'] = 'Usuario Administrador Borrado.';
    } else 	{
        $_SESSION['error']['level'] = 'error';
        $_SESSION['error']['message'] = 'El nuevo usuario no pudo ser borrado.';
    }

    header("Location:".$rootpath."admin/sections/configuration/index.php");
    exit;


} else 	{

  	header("Location:".$rootpath."admin/sections/configuration/index.php");
  	exit;

} ?>