<?php

$rootpath = "./../../../";
$current_uri = "$_SERVER[REQUEST_URI]";

// Get connection to database.
include $rootpath . 'admin/scripts/connect.php';
include $rootpath . 'admin/inc/constants.php';


$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

// If user is logged.
if (!empty($_SESSION["LOGGED"]) && $_SESSION["LOGGED"] == true)	{
	
  	// Include Header.
  	include $rootpath . 'admin/inc/header.php';
	include $rootpath . 'admin/inc/nav-bar.php';

	// Get all admin users on database. Current logged user is stored on $_SESSION['adminUserDTO'].
	$adminUserDAO = new AdminUserDAO();
	$arrayAdminUser = $adminUserDAO->getAdminUsers();

?>


<!-- DIV containing the configuration options -->
<div class="admin-users-content">

    <!-- ADD Admin User Button -->
    <button class="btn btn-success add-admin-user-button" role="button" data-toggle="modal" data-target="#exampleModal">
        <strong>Añadir Admin User</strong></button>


    <!-- FIRST ROW: MAIN BLOCK -->
    <div class="row">

        <!-- -------------------------------- -->
        <!-- FIRST COLUMN: CURRENT ADMIN INFO -->
        <div class="current-admin-column col-sm-6">

            <!-- DIV header explaining what is ther inside -->
            <div class="conf-table-main-header">INFORMACIÓN DEL ADMIN ACTUAL</div>

            <!-- Horizontal separator -->
            <br><hr/>

            <!-- ---------------------------------- -->
            <!-- FORM CONTAINING CURRENT ADMIN INFO -->
            <form id="update-admin-user-form" class="p-4" action="<?php echo $rootpath?>admin/sections/configuration/configuration.php" method="post">

                <!-- EMAIL -->
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input required type="email" class="input-lg form-control" name="email" id="email" placeholder="Email" value="<?php echo ($_SESSION['adminUserDTO']->getEmail())?>">
                    </div>
                </div>

                <!-- NAME -->
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Nombre</label>
                    <div class="col-sm-10">
                        <input required type="text" class="input-lg form-control" name="name" id="name" placeholder="Nombre y Apellidos" value="<?php echo ($_SESSION['adminUserDTO']->getName() )?>">
                    </div>
                </div>

                <!-- OLD PASSWORD -->
                <div class="form-group row">
                    <label for="old_pass" class="col-sm-10 col-form-label">Cambiar Password:</label>
                    <div class="offset-sm-2 col-sm-10">
                        <input required type="password" class="input-lg form-control" name="old_pass" id="old_pass" placeholder="Password Actual">
                    </div>
                </div>

                <!-- NEW PASSWORD -->
                <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                        <input type="password" class="input-lg form-control" name="new_pass_1" id="new_pass_1" placeholder="Password Nuevo" autocomplete="off">
                    </div>
                </div>

                <!-- NEW PASSWORD REPEAT -->
                <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                        <input type="password" class="input-lg form-control" name="new_pass_2" id="new_pass_2" placeholder="Repita el Password Nuevo" autocomplete="off">
                    </div>
                </div>

                <!-- ACTION UPDATE_USER -->
                <input hidden name="action" id="action" value="<?php echo (ADMIN_USER_FORM_ACTION_UPDATE)?>">

                <!-- ADMIN USER ID -->
                <input hidden name="admin_user_id" id="admin_user_id" value="<?php echo ($_SESSION['adminUserDTO']->getId() )?>">

                <!-- SUBMIT BUTTON -->
                <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                        <button type="submit" class="btn btn-primary btn-block">Guardar Cambios</button>
                    </div>
                </div>
            </form>

        </div><!-- /FIRST COLUMN: CURRENT ADMIN INFO -->

        <!-- -------------------------- -->
        <!-- SECOND COLUMN: ADMIN CARDS -->
        <div class="col-sm-6">

            <!-- DIV header explaining what is ther inside -->
            <div class="conf-table-main-header">OTROS ADMINISTRADORES</div>

            <!-- Horizontal separator -->
            <br><hr/>


            <!-- GENERATE ADMIN USER CARDS -->
            <div class="cards-container row p-4">

                <?php foreach ($arrayAdminUser as $index => $adminUserDTO)  {

                    echo '<div class="col-sm-12 col-md-6 col-lg-6">';
                        echo '<div class="card">';
                            echo '<div class="container-card-img-top">';
                                echo '<img class="card-img-top" src="'.$rootpath.'/admin/img/robot.png">';
                            echo '</div>';
                            echo '<div class="card-block">';
                                echo ' <figure class="profile">';
                                    echo '<img src="'.$rootpath.'/admin/img/name-card-icon.png" class="profile-avatar" alt="">';
                                echo '</figure>';
                                echo '<h4 class="card-title mt-2">'.$adminUserDTO->getName().'</h4>';
                                echo '<div class="meta">';
                                    echo '<a>Super Admin</a>';
                                echo '</div>';
                                echo '<div class="card-text">';
                                    echo '<a href="mailto:'.$adminUserDTO->getEmail().'">'.$adminUserDTO->getEmail().'</a>';
                                echo '</div>';
                            echo '</div>';
                            echo '<div class="card-footer">';
                                echo '<small>Desde '.$adminUserDTO->getCreatedAt().'</small>';
                                echo '<button class="btn btn-secondary float-right btn-sm delete-user" data-id="'.$adminUserDTO->getId().'"><img src="' . $rootpath . './admin/img/trash.svg"></button>';
                            echo '</div>';
                        echo '</div>';
                    echo '</div>';

                    unset($adminUserDTO);
                }?>

            </div><!-- /cards-container -->
        </div><!-- /SECOND COLUMN: ADMIN CARDS -->
    </div><!-- /FIRST ROW: MAIN BLOCK -->



    <!-- -------------------- -->
    <!-- NEW ADMIN USER MODAL -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Añadir Usuario Admin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <!-- NEW ADMIN USER FORM -->
                    <form id="new_admin_user_form" class="form-horizontal" action="<?php echo $rootpath?>admin/sections/configuration/configuration.php" method="post" onSubmit="return checkForm(this)">

                        <!--EMAIL -->
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input required type="email" class="input-lg form-control" id="email" name="email" placeholder="Email">
                            </div>
                        </div>
                        <!--NOMBRE -->
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Nombre</label>
                            <div class="col-sm-10">
                                <input required type="text" class="input-lg form-control" id="name" name="name" placeholder="Nombre y Apellidos">
                            </div>
                        </div>
                        <!--PASSWORD 1 -->
                        <div class="form-group">
                            <label for="pass_1" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input required type="password" class="input-lg form-control" id="pass_1" name="pass_1" placeholder="Password">
                            </div>
                        </div>
                        <!--PASSWORD 2 -->
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input required type="password" class="input-lg form-control" name="pass_2" id="pass_2" placeholder="Repita el Password" autocomplete="off">
                            </div>
                        </div>

                        <!-- ACTION FIELD SAVE NEW ADMIN USER -->
                        <input hidden name="action" id="action" name="action" value="<?php echo (ADMIN_USER_FORM_ACTION_SAVE)?>"/>


                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">

                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button onclick="submitNewAdminForm('save_user')" class="btn btn-primary">Añadir nuevo usuario ADMIN</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /NEW ADMIN USER MODAL -->
    <!-- --------------------- -->


    <!-- MatchHeight jQuery Plugin: This pluggin equals the height of the cards no matter the content. -->
    <script src="<?php echo $rootpath; ?>admin/lib/jquery.matchHeight.js" type="text/javascript"></script>

    <script>


        /**
         * This method checks that the 2 password entries from new_admin_user_form are correct.
         * It is triggered by new_admin_user_form onsubmit event.
         * @param form
         * @returns {boolean}
         */
        function checkForm(form) {

            var pass_1 = document.forms["new_admin_user_form"]["pass_1"];
            var pass_2 = document.forms["new_admin_user_form"]["pass_2"];

            if (pass_1.value !== "" && form.pass_1.value === pass_2.value) {
                if (pass_1.value.length < 6) {
                    alert("Error: Password must contain at least six characters!");
                    pass_1.focus();
                    return false;
                }
                if (pass_1.value === form.email.value) {
                    alert("Error: Password must be different from email!");
                    pass_1.focus();
                    return false;
                }
            } else {

                alert("Error: Please check that you've entered and confirmed your password!");
                pass_1.focus();
                return false;
            }
            return true;
        }


        function submitNewAdminForm(action)   {

            var $form = $('form#new_admin_user_form');
            $("#action").attr("value", action);
            $form.submit();
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // DOCUENT READY BLOCK /////////////////////////////////////////////////////////////////////////////////////////
        $(document).ready(function() {

            // update-admin-user compare passwords.
            $("#update-admin-user-form input[type=password]").keyup(function () {

                var ucase = new RegExp("[A-Z]+");
                var lcase = new RegExp("[a-z]+");
                var num = new RegExp("[0-9]+");

                if ($("#new_pass_1").val().length >= 6) {
                    $("#new_pass_1").css("color", "#00A41E");
                } else {
                    $("#new_pass_1").css("color", "#FF0004");
                }

                // Password equals
                if ($("#new_pass_1").val() == $("#new_pass_2").val()) {
                    $("#new_pass_2").css("color", "#00A41E");
                } else {
                    $("#new_pass_2").css("color", "#FF0004");
                }
            });

            // new_admin_user_form compare passwords.
            $("#new_admin_user_form input[type=password]").keyup(function () {

                var ucase = new RegExp("[A-Z]+");
                var lcase = new RegExp("[a-z]+");
                var num = new RegExp("[0-9]+");

                if ($("#pass_1").val().length >= 6) {
                    $("#pass_1").css("color", "#00A41E");
                } else {
                    $("#pass_1").css("color", "#FF0004");
                }

                // Password equals
                if ($("#pass_1").val() == $("#pass_2").val()) {
                    $("#pass_2").css("color", "#00A41E");
                } else {
                    $("#pass_2").css("color", "#FF0004");
                }
            });

            // Delete Admin User functionality
            $(".card .delete-user").on("click", function(e) {

                // Prevent default button click bejaviour.
                e.preventDefault();

                // Forbid to delete the current user.
                if ($(this).data("id") == "<?php echo ($_SESSION['adminUserDTO']->getId() )?>") {
                    alert("No puede borrarse a sí mismo.");
                    return;
                }

                // Request API to delete the user,
                $.ajax({
                    method: "DELETE",
                    url: "<?php echo $rootpath?>admin/sections/configuration/configuration.php",
                    context: document.body,
                    data: { id: $(this).data("id")}
                }).done(function() {
                    window.location.href = "<?php echo $rootpath?>admin/sections/configuration/index.php";
                });
            });


            // Set cards with same height.
            $('.card').matchHeight();


        }); // End of document ready block.

    </script>

<?php
	
	// Check for errors and warnings:
	if(isset($_SESSION['error']) && is_array($_SESSION['error']))   include $rootpath . 'admin/inc/warning.php';
	// Include Footer.
	include $rootpath . 'admin/inc/footer.php';

// If user is not logged -> Back to loggin page.
} else 	{
  	header("Location:".$rootpath."admin/login.php");
  	exit;
}