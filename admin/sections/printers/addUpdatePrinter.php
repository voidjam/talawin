<?php

$rootpath = "./../../../";
$current_uri = "$_SERVER[REQUEST_URI]";

// Get connection to database.
include $rootpath . 'admin/scripts/connect.php';
// To get link paths.
include $rootpath . 'admin/inc/constants.php';

// Check if user is logged into the app.
if (!empty($_SESSION["LOGGED"]) && $_SESSION["LOGGED"] == true) {

    // PrinterDAO object.
    $printerDAO         = new PrinterDAO();

    // Getting static table labels from database.
    $brands             = $printerDAO->getPrinterBrands();
    $subBrands          = $printerDAO->getPrinterSubBrands();
    $inks               = $printerDAO->getPrinterInks();
    $white_inks         = $printerDAO->getPrinterWhiteInks();
    $media_types        = $printerDAO->getPrinterMediaTypes();

    // We UPDATE an existing printer.
    // If ID comes throw post method it means we are coming from clicking a printers table row.
    // If ID comes throw session var, it means it is an response form uploadServer.php
    if (isset($_SESSION['printer_id'])) {$_POST['printer_id'] = $_SESSION['printer_id']; }
    if (isset($_POST['printer_id']))  {

        // SETTING UPDATE MODE.
        $update_mode        = true;

        // Save printer id from session.
        $printer_id         = $_POST['printer_id'];

        // Get printer DTO object.
        $printerDTO         = $printerDAO->getPrinterById($printer_id);
        $preselected_apps   = $printerDTO->getPreselectString();

        // Check if printer has related translation for html fields:
        $hasPrinterTranslation = !empty($printerDTO->getPrinterTransDTO());

        // Convert $printerDTO to array to access field with constants values.
        $printerDTO         = json_decode(json_encode($printerDTO), true);

        // Set update texts.
        $page_title         = "ACTUALIZAR IMPRESORA";
        $send_button        = "ACTUALIZAR INFORMACIÓN";
        $title              = $brands[$printerDTO[PrinterDAO::FIELD_BRAND_ID]]." - ".$subBrands[$printerDTO[PrinterDAO::FIELD_SUB_BRAND_ID]]." - ".$printerDTO[PrinterDAO::FIELD_MODEL];

        // Check if the printer as a video:
        $videoDAO = new VideoDAO();
        $arrayVideoDTO = $videoDAO->getVideosByPrinterId($printer_id);

    // If we are adding a NEW PRINTER.
    } else {

        // If we come form a duplicated entry 'ref' error.
        if (isset($_SESSION["post"])) $printerDTO = json_encode(new PrinterDTO($_SESSION["post"]));

        // SETTING NEW PRINTER MODE.
        $update_mode    = false;

        // GET var to know if we are accessing throw OLD PRINTERS link or NEW PRINTERS link.
        $filter_new = $_GET["action"] === "old_printers" ? false : true;

        // Default values for new printers.
        $printer_id     = null;
        $is_new         = false;

        // Set new printer texts.
        $page_title     = "AÑADIR UNA IMPRESORA";
        $send_button    = "GUARDAR EN BASE DE DATOS";
        $title          = "IMPRESORA X";
    }

    // Include Header.
    include $rootpath . 'admin/inc/header.php';
    // Include Nav Bar.
    include $rootpath . 'admin/inc/nav-bar.php';
    // Include Fine Uploader Template (after nav-bar)
    include $rootpath . 'admin/lib/fine-uploader/templates/gallery.html';
    include $rootpath . 'admin/lib/fine-uploader/templates/simple-thumbnails.html';

    ?>


    <div id="printers-new-content" class="printers-new-content add-printer-div">


        <div class="table-main-header"><?php echo $page_title; ?></div>

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- FORM ERRORS INFO DIV ++++++++++++++++++++++++++++++++++++++++ -->
        <div id="form-error-callout" class="form-error-callout form-error-callout-danger hidden" onClick="(function(){
                $('#form-error-callout').addClass('hidden');
            })();">
            <h4>Errores en el formulario!</h4>
            <p>Debe corregir los siguientes campos:</p>
            <ul></ul>
        </div>
        <!-- /FORM ERRORS INFO DIV +++++++++++++++++++++++++++++++++++++++ -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- FORM HEADER +++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <div class="form-header">
            <h1><strong><span><?php echo $title; ?></span></strong></h1>
        </div>

        <!-- Horizontal separator -->
        <br><hr/>


        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- NEW PRINTER FORM ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- This is the form to create a new item on the printers table. Form will be empty
        except if we tried to save an item and fails, in this case will be fulfilled with
        the form info we entered, stored on $_SESSION['post'] -->
        <form id="save-printer-form" method="POST"  data-parsley-ui-enabled="true">


            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- DELETE BUTTON: DELETE We will only show it if we are updating an existing printer. ++++++++++++++++ -->
            <?php if ($update_mode) {

                echo '<button id="delete-button" title="Eliminar la impresora de la base de datos." type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmBox">';
                echo '<img src="'.$rootpath.'admin/img/trash.svg'.'">';
                echo '</button>';
                echo '<!-- Confirmation Modal: When pressing delete button, a confirmation dialog will show. If we confirm, the -->';
                echo '<!-- form will be submitted using submitForm($printerDTO) function declared at the end of this page ----- -->';
                echo '<div class="modal fade" id="confirmBox" role="dialog">';
                echo '<div class="modal-dialog" role="document">';
                echo '<div class="modal-content">';
                echo '<div class="modal-header">';
                echo '<h5 class="modal-title">Eliminar Impresora</h5>';
                echo '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
                echo '<span aria-hidden="true">&times;</span>';
                echo '</button>';
                echo '</div>';
                echo '<div class="modal-body">';
                echo '<p>Confirme que desea eliminar esta impresora de la base de datos y todos los archivos asociados.</p>';
                echo '</div>';
                echo '<div class="modal-footer">';
                echo '<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="submitForm(\''.PRINTER_FORM_ACTION_DELETE_PRINTER.'\')">Borrar</button>';
                echo '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

            } ?>
            <!-- /DELETE BUTTON+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


            <div id="section-links">
                <a href="#images"><img src="<?php echo $rootpath ?>admin/img/img-icon.png" title="Editar Imágenes"/></a>
                <a href="#documents"><img src="<?php echo $rootpath ?>admin/img/docs.svg" title="Editar Documentos"/></a>
                <a href="#videos"><img src="<?php echo $rootpath ?>admin/img/youtube.svg" title="Editar Vídeo"/></a>
                <a href="#translation_en"><img src="<?php echo $rootpath ?>admin/img/translate.svg" title="Editar Traducción"/></a>

            </div>





            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- 1ST GROUP: INFO DEL MODELO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <div class="odd-div">


                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- FIELD_IS_PUBLISHED ++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- The Publish Printer Toggle -->
                <input type="checkbox" id="<?php echo PrinterDAO::FIELD_IS_PUBLISHED?>"
                       name="<?php echo PrinterDAO::FIELD_IS_PUBLISHED?>"
                       <?php echo isset($printerDTO[PrinterDAO::FIELD_IS_PUBLISHED])
                       && boolval($printerDTO[PrinterDAO::FIELD_IS_PUBLISHED]) == true ? 'checked' : '' ?>
                       <?php if (!$update_mode) {echo 'disabled';} ?>>
                <label for="<?php echo PrinterDAO::FIELD_IS_PUBLISHED?>" data-toggle="tooltip" data-placement="top"
                       title=<?php echo !$update_mode
                                        ? '"Esta impresora no puede ser publicada pues no ha sido guardada aún en la base de datos."'
                                        : ($printerDTO[PrinterDAO::FIELD_IS_PUBLISHED] == false
                                            ? '"Debe publicar la impresora para que aparezca en talawin.es"'
                                            : '"Impresora Publicada. Click para des-publicar."')?>>
                    <i></i>
                </label>


                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- GROUP TITTLE ++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <h2><strong><span>Info del modelo</span></strong></h2>

                <!-- FIRST ROW -->
                <div class="row justify-content-between align-items-end">
                    <!-- FIRST COLUMN -->
                    <div class="col-4 printer-state-checkboxes">
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_IS_NEW ++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row justify-content-end" style="margin-bottom: 0">
                            <label for="cb-value" class="col control-label">IMPRESORA NUEVA</label>
                            <div class="col-sm-4">
                                <div class="state-checkboxes-container">
                                    <input name="<?php echo PrinterDAO::FIELD_IS_NEW?>" id="<?php echo PrinterDAO::FIELD_IS_NEW?>" type="checkbox"
                                        <?php echo $update_mode
                                            ? $printerDTO[PrinterDAO::FIELD_IS_NEW] == true ? 'checked' : ''
                                            : $filter_new ? 'checked' : '' ?> class="cb-value"/>
                                    <label id="label-toggle-btn" class="toggle-btn active"></label>
                                    <span id="round-btn" class="round-btn"></span>
                                </div>
                            </div>
                        </div>



                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_IS_SOLD +++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row justify-content-end" style="margin-bottom: 0">
                            <label for="cb-value" class="col control-label">IMPRESORA VENDIDA</label>
                            <div class="col-sm-4">
                                <div class="state-checkboxes-container">
                                    <input name="<?php echo PrinterDAO::FIELD_IS_SOLD?>" id="<?php echo PrinterDAO::FIELD_IS_SOLD?>" type="checkbox"
                                        <?php echo $update_mode
                                            ? $printerDTO[PrinterDAO::FIELD_IS_SOLD] == true ? 'checked' : ''
                                            : '' ?> class="cb-value"/>
                                    <label id="label-toggle-btn" class="toggle-btn active"></label>
                                    <span id="round-btn" class="round-btn"></span>
                                    <!-- If is_new is checked then we disable this fiedl -->
                                    <script> if ($("#<?php echo PrinterDAO::FIELD_IS_NEW?>").is(':checked')) { $("#<?php echo PrinterDAO::FIELD_IS_SOLD?>").attr('disabled', true); }</script>
                                </div>
                            </div>
                        </div>

                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_IS_RELEASE +++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row justify-content-end" style="margin-bottom: 0">
                            <label for="cb-value" class="col control-label">NOVEDAD</label>
                            <div class="col-sm-4">
                                <div class="state-checkboxes-container">
                                    <input name="<?php echo PrinterDAO::FIELD_IS_RELEASE?>" id="<?php echo PrinterDAO::FIELD_IS_RELEASE?>" type="checkbox"
                                        <?php echo $update_mode
                                            ? $printerDTO[PrinterDAO::FIELD_IS_RELEASE] == true ? 'checked' : ''
                                            : '' ?> class="cb-value"/>
                                    <label id="label-toggle-btn" class="toggle-btn active"></label>
                                    <span id="round-btn" class="round-btn"></span>
                                </div>
                            </div>
                        </div>


                    </div><!-- /FIRST COLUMN -->

                    <!-- SECOND COLUMN -->
                    <div class="col-6">
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_BRAND_ID ++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_BRAND_ID?>" class="col-sm-3 control-label">Marca</label>
                            <div class="col-sm-9">

                                <select name="<?php echo PrinterDAO::FIELD_BRAND_ID?>"
                                        id="<?php echo PrinterDAO::FIELD_BRAND_ID?>"
                                        class="form-control brands-select"
                                        required="" data-parsley-error-message="Por favor introduzca la marca.">
                                    <option value="" disabled selected style="display: none;">Marca de la Impresora</option>
                                    <?php foreach ($brands as $key => $value) { echo "<option value=".$key.">".$value."</option>"; }                                    ?>
                                </select>
                                <script>$('#<?php echo PrinterDAO::FIELD_BRAND_ID?>').val(<?php echo isset($printerDTO[PrinterDAO::FIELD_BRAND_ID]) && !is_null($printerDTO[PrinterDAO::FIELD_BRAND_ID]) ? $printerDTO[PrinterDAO::FIELD_BRAND_ID] : '' ?>);</script>
                            </div>
                        </div>
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_SUB_BRAND_ID ++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_SUB_BRAND_ID?>" class="col-sm-3 control-label">Sub-Marca</label>
                            <div class="col-sm-9">

                                <select name="<?php echo PrinterDAO::FIELD_SUB_BRAND_ID?>"
                                        id="<?php echo PrinterDAO::FIELD_SUB_BRAND_ID?>"
                                        class="form-control brands-select"
                                        required="" data-parsley-error-message="Por favor introduzca la marca.">
                                    <option value="" disabled selected style="display: none;">Marca de la Impresora</option>
                                    <?php foreach ($subBrands as $key => $value) { echo "<option value=".$key.">".$value."</option>"; }                                    ?>
                                </select>
                                <script>$('#<?php echo PrinterDAO::FIELD_SUB_BRAND_ID?>').val(<?php echo isset($printerDTO[PrinterDAO::FIELD_SUB_BRAND_ID]) && !is_null($printerDTO[PrinterDAO::FIELD_SUB_BRAND_ID]) ? $printerDTO[PrinterDAO::FIELD_SUB_BRAND_ID] : '' ?>);</script>
                            </div>
                        </div>
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_MODEL +++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_MODEL?>" class="col-sm-3 control-label">Modelo</label>
                            <div class="col-sm-9">
                                <input name="<?php echo PrinterDAO::FIELD_MODEL?>" type="text" class="form-control"
                                       id="<?php echo PrinterDAO::FIELD_MODEL?>" placeholder="Modelo"
                                       value="<?php echo isset($printerDTO[PrinterDAO::FIELD_MODEL]) ? $printerDTO[PrinterDAO::FIELD_MODEL] : '' ?>"
                                       required="" data-parsley-error-message="Por favor introduzca el modelo.">
                            </div>
                        </div>
                    </div><!-- /SECOND COLUMN -->
                </div><!-- /row -->



                <!-- SECOND ROW -->
                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- PRIVATE FIELDS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <div class="row private-fields align-items-end">

                    <!-- FIRST COLUMN -->
                    <div class="col">
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_COMMENTS ++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group  align-items-end">
                            <label style="margin: 0.5rem 0;" for="<?php echo PrinterDAO::FIELD_COMMENTS?>" class="row pl-3 control-label">Comentarios:</label>
                            <div class="row pl-4 pr-0">
                                <textarea class="form-control"
                                          name="<?php echo PrinterDAO::FIELD_COMMENTS?>"
                                          id="<?php echo PrinterDAO::FIELD_COMMENTS?>" type="text"
                                          placeholder="Añada aquí observaciones sobre la impresora. Este campo es opcional y no se mostrará en la web."
                                          value=""><?php echo isset($printerDTO[PrinterDAO::FIELD_COMMENTS]) ? $printerDTO[PrinterDAO::FIELD_COMMENTS] : '' ?></textarea>
                            </div>
                        </div>
                    </div><!-- /FIRST COLUMN -->

                    <!-- SECOND COLUMN -->
                    <div class="col">
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_REF +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_REF?>" class="col-sm-3 control-label">REF</label>
                            <div class="col-sm-9">
                                <input name="<?php echo PrinterDAO::FIELD_REF?>" type="text" class="form-control" id="<?php echo PrinterDAO::FIELD_REF?>" placeholder="Referencia" required
                                       value="<?php echo isset($printerDTO['ref']) ? $printerDTO['ref'] : '' ?>">
                            </div>
                        </div>
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_PRICE_COST ++++++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_PRICE_COST?>" class="col-sm-3 control-label"> Coste</label>
                            <div class="col-sm-9">
                                <input name="<?php echo PrinterDAO::FIELD_PRICE_COST?>" id="<?php echo PrinterDAO::FIELD_PRICE_COST?>" class="form-control" type="number" min="0.00" step="0.01" max="1000000" placeholder="00.00" value="<?php echo isset($printerDTO[PrinterDAO::FIELD_PRICE_COST]) ? $printerDTO[PrinterDAO::FIELD_PRICE_COST] : '00.00' ?>"/>
                            </div>
                        </div>
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_PRICE_PVP +++++++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_PRICE_PVP?>" class="col-sm-3 control-label">PVP</label>
                            <div class="col-sm-9">
                                <input name="<?php echo PrinterDAO::FIELD_PRICE_PVP?>" id="<?php echo PrinterDAO::FIELD_PRICE_PVP?>" class="form-control" type="number" min="0.00" step="0.01" max="1000000" placeholder="00.00" value="<?php echo isset($printerDTO[PrinterDAO::FIELD_PRICE_PVP]) ? $printerDTO[PrinterDAO::FIELD_PRICE_PVP] : '00.00' ?>"/>
                            </div>
                        </div>
                    </div><!-- /SECOND COLUMN -->

                    <!-- SECOND COLUMN -> col-12 to form a new row. -->
                    <div class="col-12">
                        <h6><span>Estos campos son <strong>privados</strong> y sólo podrán verse desde el panel de administración.</span></h6>
                    </div>

                </div><!-- /row -->
                <!-- END OF PRIVATE FIELDS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            </div><!-- /odd-div -->
            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- END OF 1ST GROUP: INFO DEL MODELO +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- 2ND GROUP: Contenido HTML +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <div class="even-div">

                <!-- Group Contenido HTML -->
                <h2><strong><span style="vertical-align: middle; padding-right: 1rem;">Contenido HTML</span></strong><img style="max-width: 2rem;" src="<?php echo $rootpath ?>/admin/img/es-icon.svg"></h2>

                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- FIELD_TITLE +++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <div class="form-group row">
                    <label for="<?php echo PrinterDAO::FIELD_TITLE?>" class="col-sm-3 control-label">Título</label>
                    <div class="col-sm-9">
                        <textarea name="<?php echo PrinterDAO::FIELD_TITLE?>" id="<?php echo PrinterDAO::FIELD_TITLE?>" class="form-control" type="text" placeholder="" required value=""><?php echo isset($printerDTO[PrinterDAO::FIELD_TITLE]) ? $printerDTO[PrinterDAO::FIELD_TITLE] : '' ?></textarea>
                    </div>
                </div>
                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- FIELD_HEADLINE ++++++++++++++++++++++++++++++++++++++++++++++ -->
                <div class="form-group row">
                    <label for="<?php echo PrinterDAO::FIELD_HEADLINE?>" class="col-sm-3 control-label">Encabezado</label>
                    <div class="col-sm-9">
                        <textarea name="<?php echo PrinterDAO::FIELD_HEADLINE?>" id="<?php echo PrinterDAO::FIELD_HEADLINE?>" class="form-control" type="text" placeholder="" required value=""><?php echo isset($printerDTO[PrinterDAO::FIELD_HEADLINE]) ? $printerDTO[PrinterDAO::FIELD_HEADLINE] : '' ?></textarea>
                    </div>
                </div>
                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- FIELD_DESCRIPTION +++++++++++++++++++++++++++++++++++++++++++ -->
                <div class="form-group row">
                    <label for="<?php echo PrinterDAO::FIELD_DESCRIPTION?>" class="col-sm-3 control-label">Descripción</label>
                    <div class="col-sm-9">
                        <textarea name="<?php echo PrinterDAO::FIELD_DESCRIPTION?>" id="<?php echo PrinterDAO::FIELD_DESCRIPTION?>" class="form-control" type="text" placeholder="" required value=""><?php echo isset($printerDTO[PrinterDAO::FIELD_DESCRIPTION]) ? $printerDTO[PrinterDAO::FIELD_DESCRIPTION] : '' ?></textarea>
                    </div>
                </div>     
            </div><!-- /even-div -->
            <!-- END OF 2ND GROUP: Contenido HTML ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- 3RD GROUP: CARACTERISTICAS TECNICAS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <div id="html-content" class="odd-div">

                <!-- GROUP CARACTERISTICAS TECNICAS -->
                <h2><strong><span>características Técnicas</span></strong></h2>

                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- FIRST ROW: FEATURES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <div class="row">

                    <!-- FIRST COLUMN -->
                    <div class="col">

                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_FEAT_INK ++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_FEAT_INK_ID?>" class="col-sm-4 control-label">Tinta</label>
                            <div class="col-sm-8">
                                <select name="<?php echo PrinterDAO::FIELD_FEAT_INK_ID?>" id="<?php echo PrinterDAO::FIELD_FEAT_INK_ID?>" class="form-control brands-select" required>
                                    <option value="" disabled selected style="display: none;">Tipos de Tinta</option>
                                    <?php foreach ($inks as $key => $value) { echo "<option value=".$key.">".$value['label_es']."</option>"; } ?>
                                </select>
                                <script>$('#<?php echo PrinterDAO::FIELD_FEAT_INK_ID?>').val(<?php echo isset($printerDTO[PrinterDAO::FIELD_FEAT_INK_ID]) && !is_null($printerDTO[PrinterDAO::FIELD_FEAT_INK_ID]) ? $printerDTO[PrinterDAO::FIELD_FEAT_INK_ID] : '' ?>);</script>
                            </div>
                        </div>
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_FEAT_WHITE_INK ++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_FEAT_WHITE_INK_ID?>" class="col-sm-4 control-label">Tinta Blanca</label>
                            <div class="col-sm-8">
                                <select name="<?php echo PrinterDAO::FIELD_FEAT_WHITE_INK_ID?>" id="<?php echo PrinterDAO::FIELD_FEAT_WHITE_INK_ID?>" class="form-control brands-select" required>
                                    <option value="" disabled selected style="display: none;">Acepta Tinta Blanca?</option>
                                    <?php foreach ($white_inks as $key => $value) { echo "<option value=".$key.">".$value['label_es']."</option>"; } ?>
                                </select>
                                <script>$('#<?php echo PrinterDAO::FIELD_FEAT_WHITE_INK_ID?>').val(<?php echo isset($printerDTO[PrinterDAO::FIELD_FEAT_WHITE_INK_ID]) && !is_null($printerDTO[PrinterDAO::FIELD_FEAT_WHITE_INK_ID]) ? $printerDTO[PrinterDAO::FIELD_FEAT_WHITE_INK_ID] : '' ?>);</script>
                            </div>
                        </div>
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_FEAT_MEDIA_TYPE +++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_FEAT_MEDIA_TYPE_ID?>" class="col-sm-4 control-label">Tipo de Medio</label>
                            <div class="col-sm-8">
                                <select name="<?php echo PrinterDAO::FIELD_FEAT_MEDIA_TYPE_ID?>" id="<?php echo PrinterDAO::FIELD_FEAT_MEDIA_TYPE_ID?>" class="form-control brands-select" required>
                                    <option value="" disabled selected style="display: none;">Tipo de Medio</option>
                                    <?php foreach ($media_types as $key => $value) { echo "<option value=".$key.">".$value['label_es']."</option>"; } ?>
                                </select>
                                <script>$('#<?php echo PrinterDAO::FIELD_FEAT_MEDIA_TYPE_ID?>').val(<?php echo isset($printerDTO[PrinterDAO::FIELD_FEAT_MEDIA_TYPE_ID]) && !is_null($printerDTO[PrinterDAO::FIELD_FEAT_MEDIA_TYPE_ID]) ? $printerDTO[PrinterDAO::FIELD_FEAT_MEDIA_TYPE_ID] : '' ?>);</script>
                            </div>
                        </div>
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_FEAT_MAX_PRINT_WIDTH ++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_FEAT_MAX_PRINT_WIDTH?>" class="col-sm-4 control-label">Máx. Ancho Impresión</label>
                            <div class="col-sm-8">
                                <input name="<?php echo PrinterDAO::FIELD_FEAT_MAX_PRINT_WIDTH?>" id="<?php echo PrinterDAO::FIELD_FEAT_MAX_PRINT_WIDTH?>" class="form-control" data-parsley-type="number" min="0.00" step="0.01" max="1000" placeholder="Máxima Anchura de Impresión (cm)." required value="<?php echo isset($printerDTO[PrinterDAO::FIELD_FEAT_MAX_PRINT_WIDTH]) ? $printerDTO[PrinterDAO::FIELD_FEAT_MAX_PRINT_WIDTH]: '' ?>"/>
                            </div>
                        </div>
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_FEAT_MAX_MEDIA_THICK ++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_FEAT_MAX_MEDIA_THICK?>" class="col-sm-4 control-label">Máx. Grosor Material</label>
                            <div class="col-sm-8">
                                <input name="<?php echo PrinterDAO::FIELD_FEAT_MAX_MEDIA_THICK?>" id="<?php echo PrinterDAO::FIELD_FEAT_MAX_MEDIA_THICK?>" class="form-control" data-parsley-type="number" min="0.00" step="0.01" max="1000" placeholder="Máximo Grosor del Medio (mm)." required value="<?php echo isset($printerDTO[PrinterDAO::FIELD_FEAT_MAX_MEDIA_THICK]) ? $printerDTO[PrinterDAO::FIELD_FEAT_MAX_MEDIA_THICK] : '' ?>"/>
                            </div>
                        </div>
                        
                    </div><!-- /FIRST COLUMN -->

                    <!-- SECOND COLUMN -->
                    <div class="col">

                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_FEAT_RESOLUTION +++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_FEAT_RESOLUTION?>" class="col-sm-3 control-label">Resolución</label>
                            <div class="col-sm-9">
                                <input name="<?php echo PrinterDAO::FIELD_FEAT_RESOLUTION?>" id="<?php echo PrinterDAO::FIELD_FEAT_RESOLUTION?>" class="form-control" data-parsley-type="alphanum" data-parsley-error-message="Debe tener el formato 1000x1000." placeholder="Máxima Resolución (dpi)." required value="<?php echo isset($printerDTO[PrinterDAO::FIELD_FEAT_RESOLUTION]) ? $printerDTO[PrinterDAO::FIELD_FEAT_RESOLUTION] : '' ?>"/>
                            </div>
                        </div>
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_FEAT_DROP_SIZE ++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_FEAT_DROP_SIZE?>" class="col-sm-3 control-label">Drop Size</label>
                            <div class="col-sm-9">
                                <input name="<?php echo PrinterDAO::FIELD_FEAT_DROP_SIZE?>" id="<?php echo PrinterDAO::FIELD_FEAT_DROP_SIZE?>" class="form-control" type="text" placeholder="Tamaño de la Gota (pl)." required value="<?php echo isset($printerDTO[PrinterDAO::FIELD_FEAT_DROP_SIZE]) ? $printerDTO[PrinterDAO::FIELD_FEAT_DROP_SIZE] : '' ?>"/>
                            </div>
                        </div>
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_FEAT_MAX_SPEED ++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_FEAT_MAX_SPEED?>" class="col-sm-3 control-label">Max Velocidad</label>
                            <div class="col-sm-9">
                                <input name="<?php echo PrinterDAO::FIELD_FEAT_MAX_SPEED?>" id="<?php echo PrinterDAO::FIELD_FEAT_MAX_SPEED?>" class="form-control" data-parsley-type="integer" min="0" step="1" max="1000" placeholder="Velocidad de Impresión Máxima (m2/hr)" required value="<?php echo isset($printerDTO[PrinterDAO::FIELD_FEAT_MAX_SPEED]) ? $printerDTO[PrinterDAO::FIELD_FEAT_MAX_SPEED] : '' ?>"/>
                            </div>
                        </div>
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_WARRANTY ++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_WARRANTY?>" class="col-sm-3 control-label">Garantía</label>
                            <div class="col-sm-9">
                                <input name="<?php echo PrinterDAO::FIELD_WARRANTY?>" id="<?php echo PrinterDAO::FIELD_WARRANTY?>" class="form-control" data-parsley-type="integer" min="0" step="1" max="100" data-parsley-error-message="Introduzca los meses de garantía." placeholder="Meses de Garantía" required value="<?php echo isset($printerDTO[PrinterDAO::FIELD_WARRANTY]) ? $printerDTO[PrinterDAO::FIELD_WARRANTY] : '' ?>"/>
                            </div>
                        </div>
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_PRODUCTION_DATE +++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <label for="<?php echo PrinterDAO::FIELD_PRODUCTION_DATE?>" class="col-sm-3 control-label">Antigüedad</label>
                            <div class="col-sm-9">
                                <input name="<?php echo PrinterDAO::FIELD_PRODUCTION_DATE?>"
                                       id="<?php echo PrinterDAO::FIELD_PRODUCTION_DATE?>"
                                       class="form-control" type="month" placeholder=""
                                       value="<?php echo isset($printerDTO[PrinterDAO::FIELD_PRODUCTION_DATE]) ? date_format(date_create($printerDTO[PrinterDAO::FIELD_PRODUCTION_DATE]), "Y-m") : '' ?>"/>
                                <!-- If is_new is checked then we disable this field -->
                                <script> if ($("#<?php echo PrinterDAO::FIELD_IS_NEW?>").is(':checked')) { $("#<?php echo PrinterDAO::FIELD_PRODUCTION_DATE?>").attr('disabled', true); }</script>
                            </div>
                        </div>
                    </div><!-- /SECOND COLUMN -->
                </div><!-- /row -->
                <!-- /FIRST ROW: FEATURES ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

                <br><br>

                
                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- TABS: HTML FIELDS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- TABS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <ul class="nav nav-tabs justify-content-start" role="tablist">
                    <!-- OVERVIEW TAB -->
                    <li class="nav-item">
                        <a class="nav-link active" href="#overview" id="overview-tab" role="tab" data-toggle="tab" aria-controls="overview" aria-expanded="true"><span>General</span></a>
                        <label for="<?php echo PrinterDAO::FIELD_HTML_OVERVIEW?>" hidden>General</label>
                    </li>
                    <!-- Features TAB -->
                    <li class="nav-item">
                        <a class="nav-link" href="#features" role="tab" id="features-tab" data-toggle="tab" aria-controls="features"><span>Características</span></a>
                        <label for="<?php echo PrinterDAO::FIELD_HTML_FEATURES?>" hidden>Características</label>
                    </li>
                    <!-- Specifications TAB -->
                    <li class="nav-item">
                        <a class="nav-link" href="#specifications" role="tab" id="specifications-tab" data-toggle="tab" aria-controls="specifications"><span>Especificaciones</span></a>
                        <label for="<?php echo PrinterDAO::FIELD_HTML_SPECS?>" hidden>Especificaciones</label>
                    </li>
                    <!-- Applications TAB -->
                    <li class="nav-item">
                        <a class="nav-link" href="#applications" role="tab" id="applications-tab" data-toggle="tab" aria-controls="applications"><span>Aplicaciones</span></a>
                        <label for="label_apps" hidden>Aplicaciones</label>
                    </li>
                </ul>
                <!-- /TABS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- CONTENT PANELS ++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <div id="html-nav-content" class="tab-content">

                    <!-- APPLICATIONS TAB CONTENT -->
                    <div role="tabpanel" class="tab-pane fade" id="applications" aria-labelledby="applications-tab">
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_APPLICATIONS+++++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <div class="col-sm-12 apps-container d-flex flex-row justify-content-between align-content-around flex-wrap">

                                <?php
                                    $applicationDAO = new ApplicationDAO();
                                    $arrayApplicationDTO = $applicationDAO->getApplications();

                                    // This img elements will be converted into checkbox inputs by imgCheckbox.js plugin.
                                    // When submitting they will belong to an array of inputs called apps[]. The checked
                                    // elements will have the corresponding index with value "on",
                                    // for example apps[$app_id] = "on" where $app_id is the id field of the corresponding
                                    // application on database table 'applications'.
                                    foreach ($arrayApplicationDTO as $key => $value) {
                                        echo "<div class='img-check-container  d-flex flex-column'>";
                                            echo "<img id='apps[".$value->getId()."]' name ='apps[".$value->getId()
                                                ."]'class='img-check' value='".$value->getId()."' src='".$rootpath."admin/img/apps/".$value->getId()."/".$value->getFileNamePrefix()
                                                ."00.jpg'/>";
                                            echo "<span><small>".$value->getLabelEs()."</small></span>";
                                        echo "</div>";
                                    }
                                ?>
                            </div>
                        </div>
                    </div><!-- /APPLICATIONS TAB CONTENT -->

                    <!-- SPECIFICATIONS TAB CONTENT -->
                    <div role="tabpanel" class="tab-pane fade" id="specifications" aria-labelledby="specifications-tab">
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_HTML_SPECS ++++++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <textarea placeholder="Introduzca un SPECIFICATIONS." name="<?php echo PrinterDAO::FIELD_HTML_SPECS?>"
                                          id="<?php echo PrinterDAO::FIELD_HTML_SPECS?>"
                                          class="form-control" value="" required>
                                    <?php echo isset($printerDTO[PrinterDAO::FIELD_HTML_SPECS]) ? $printerDTO[PrinterDAO::FIELD_HTML_SPECS] : '' ?>
                                </textarea>
                            </div>
                        </div>
                    </div><!-- /SPECIFICATIONS TAB CONTENT -->

                    <!-- FEATURES TAB CONTENT -->
                    <div role="tabpanel" class="tab-pane fade" id="features" aria-labelledby="features-tab">
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_HTML_FEATURES +++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <textarea placeholder="Introduzca un FEATURES." name="<?php echo PrinterDAO::FIELD_HTML_FEATURES?>"
                                          id="<?php echo PrinterDAO::FIELD_HTML_FEATURES?>"
                                          class="form-control" value="" required>
                                    <?php echo isset($printerDTO[PrinterDAO::FIELD_HTML_FEATURES]) ? $printerDTO[PrinterDAO::FIELD_HTML_FEATURES] : '' ?>
                                </textarea>
                            </div>
                        </div>
                    </div><!-- /FEATURES TAB CONTENT -->
                    <!-- OVERVIEW TAB CONTENT -->
                    <div role="tabpanel" class="tab-pane fade show active" id="overview" aria-labelledby="overview-tab">
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- FIELD_HTML_OVERVIEW +++++++++++++++++++++++++++++++++++++++++ -->
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <textarea placeholder="Introduzca un overview." name="<?php echo PrinterDAO::FIELD_HTML_OVERVIEW?>"
                                  id="<?php echo PrinterDAO::FIELD_HTML_OVERVIEW?>"
                                  class="form-control" value="" required>
                                    <?php echo isset($printerDTO[PrinterDAO::FIELD_HTML_OVERVIEW]) ? $printerDTO[PrinterDAO::FIELD_HTML_OVERVIEW] : '' ?>
                                </textarea>
                            </div>
                        </div>
                    </div><!-- /OVERVIEW TAB CONTENT -->
                </div>
                <!-- /CONTENTS PANEL +++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- /TABS: HTML FIELDS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- SUBMIT BUTTON +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <div class="printer-form-buttons">
                    <!-- SUBMIT BUTTON: SAVE -->
                    <button type="button" class="btn btn-success right-bottom-button" onclick="submitForm('<?php echo PRINTER_FORM_ACTION_SAVE_PRINTER; ?>')"><?php echo $send_button; ?></button>
                </div><!-- /printer-form-buttons -->
                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


            </div><!-- /odd-div -->
            <!-- END OF 3RD GROUP: CARACTRISTICAS TECNICAS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- HIDDEN INPUTS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- Printer ID -->
            <input name="printer_id" id="printer_id" hidden value="<?php echo $printer_id; ?>"/>

            <!-- Action: SAVE or DELETE -->
            <input name="action" id="action" hidden/>
            <!-- END OF HIDDEN INPUTS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


        </form><!-- /New printer Form -->
        <!-- END OF NEW PRINTER FORM +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->



        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- 4TH GROUP: FOTOS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <div id="images" class="even-div">

            <!-- Group Title -->
            <h2><strong><span>IMÁGENES</span></strong></h2>

            <!-- If we are entering a new item -->
            <?php if (!$update_mode) {?>
                <!-- INFO about uploading pictures -->
                <div class="upload-pics-warning row">
                    <span class="upload-pics-warning-content"> Debe guardar la nueva impresora para poder añadir imágenes.</span>
                </div>
            <!-- If we are edting an existing item -->
            <?php } else { ?>
                <!-- The element where Fine Uploader will exist. -->
                <div class="table-header">AÑADA FOTOS PARA QUE LA IMPRESORA APAREZCA EN CATÁLOGO</div>
                <div id="fine-uploader-pics"></div>

                <!-- Fine Uploader -->
                <script src="<?php echo $rootpath; ?>admin/lib/fine-uploader/fine-uploader.min.js"></script>
                <script type="text/javascript">

                    let uploader_images = new qq.FineUploader({
                        debug: false,
                        element: document.getElementById('fine-uploader-pics'),
                        template: 'qq-template-pics', // Template DIV ID
                        request: {
                            endpoint: '<?php echo $rootpath; ?>admin/sections/printers/controllers/uploadServer.php',
                            params: {
                                'printer_id': '<?php echo $printer_id; ?>',
                                'action': 'save_file',
                                'dir': '<?php echo "admin/img/printers/".$printerDTO[PrinterDAO::FIELD_ID]."/" ?>'
                            }
                        },
                        validation: {
                            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],
                            sizeLimit: 15000000
                        },
                        scaling: {
                            sendOriginal: false,
                            includeExif: true,
                            sizes: [
                                {name: "", maxSize: 1600}
                            ]
                        },
                        deleteFile: {
                            enabled: true,
                            method: 'DELETE',
                            endpoint: '<?php echo $rootpath; ?>admin/sections/printers/controllers/uploadServer.php',
                            forceConfirm: true,
                            confirmMessage: "Confirme que desea eliminar {filename} de la base de datos.",
                            params: {

                                'printer_id': '<?php echo $printer_id; ?>'
                            }
                        },
                        retry: {
                            enableAuto: true
                        },
                        thumbnails: {
                            placeholders: {
                                waitingPath: '<?php echo $rootpath; ?>admin/lib/fine-uploader/placeholders/waiting-generic.png',
                                notAvailablePath: '<?php echo $rootpath; ?>admin/lib/fine-uploader/placeholders/not_available-generic.png'
                            }
                        },

                        callbacks: {
                            onDelete: function(id) {
                            },
                            onDeleteComplete: function(id, xhr, isError) {

                                // Parse json string response into object:
                                obj = JSON && JSON.parse(xhr.response) || $.parseJSON(json);

                                // Print alert informing that main picture was deleted.
                                if (obj['is_main'] == true) {
                                    alert(qq.format("Se borró la foto principal. Por favor elija otra."));
                                }
                            },  // End onComplete callback.
                            onComplete: function (id, name, responseJSON, xhr) {
                                // If after uploading the file it was the main one:
                                if (responseJSON['is_main'] == true) {
                                    $( ".qq-file-id-"+id ).addClass("qq-file-main");    // Main picture style
                                    $(".qq-file-id-" + id + " .btn-success").hide();    // Hide make-main button.
                                }

                                // We add an attribute to relate the button with its picture.
                                $(".qq-file-id-" + id + " .btn-success").attr("file_id", responseJSON['file_id']);
                                // And finally we add an on click listener to the button to make it the pic if is clicked:
                                addMakeMainListener($(".qq-file-id-" + id + " .btn-success"), responseJSON['file_id']);


                            }, // End onComplete callback.
                            onSessionRequestComplete: function (response, success, xhr) {

                                if (Array.isArray(response))   {
                                    response.forEach (function (currentValue, index, array)  {

                                        if (currentValue.is_main == true) {
                                            $( ".qq-file-id-"+index ).addClass("qq-file-main");    // Main picture style
                                            $(".qq-file-id-" + index + " .btn-success").hide();    // Hide make-main button.
                                        }
                                        // We add an attribute to relate the button with its picture.
                                        $(".qq-file-id-" + index + " .btn-success").attr("file_id", currentValue.id);
                                        // And finally we add an on click listener to the button to make it the pic if is clicked:
                                        addMakeMainListener($(".qq-file-id-" + index + " .btn-success"), currentValue.id);
                                    });
                                }
                            },

                            onError: function(id, name, errorReason, xhrOrXdr) {
                                alert(qq.format("Error on file number {} - {}.  Reason: {}", id, name, errorReason));
                            }
                        }, // End callbacks.

                        // Fine Uploader will send a GET request to your endpoint on startup (and optionally when the instance is reset).
                        // The response to this request must be valid JSON. More specifically,
                        // it must be a JSON array containing Objects for each file to be fed into the uploader.
                        // https://docs.fineuploader.com/branch/master/features/session.html
                        session:  {
                            endpoint: '<?php echo $rootpath; ?>admin/sections/printers/controllers/uploadServer.php',
                            params: {
                                'printer_id': '<?php echo $printer_id; ?>',
                                'action': 'retrieve_initial_pictures'
                            }
                        }
                    });<!-- /Fine Uploader Images -->


                    // <!-- This function is called every time after a new picture is successfully uploaded -->
                    // <!-- to the server. It adds a listener so the corresponding picture will become the  -->
                    // <!-- main picture on database. -->
                    function addMakeMainListener( element , file_id)  {

                        // On click on make main picture button: AJAX post request
                        element.click(function() {

                            $.post('<?php echo $rootpath; ?>admin/sections/printers/controllers/uploadServer.php',
                                {
                                    file_id: file_id,
                                    action: 'make_main'
                                },
                                function(data, status){

                                    // Parse json string response into object:
                                    obj = JSON && JSON.parse(data) || $.parseJSON(json);

                                    $(".qq-upload-success").each(function(){

                                        $(this).removeClass("qq-file-main");
                                        $(this).children("button.make-main-btn").show()
                                    });

                                    $("button[file_id=" + obj['file_id'] + "]").parent().addClass("qq-file-main");    // Main picture style
                                    $("button[file_id=" + obj['file_id'] + "]").hide();
                                });
                        });

                    }
                </script>
            <?php } ?>

        </div> <!-- /even-div -->
        <!-- END OF 4TH GROUP: FOTOS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->



        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- 5TH GROUP: DOCUMENTS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <div id="documents" class="odd-div">

            <!-- Group Title -->
            <h2><strong><span>DOCUMENTACIÓN</span></strong></h2>


            <!-- If we are entering a new item -->
            <?php if (!$update_mode) {?>
                <!-- INFO about uploading pictures -->
                <div class="upload-pics-warning row">
                    <span class="upload-pics-warning-content">Debe guardar la nueva impresora para poder añadir archivos.</span>
                </div>
                <!-- If we are edting an existing item -->
            <?php } else { ?>
                <!-- The element where Fine Uploader will exist. -->
                <div class="table-header">AÑADA LA DOCUMENTACIÓN CORRESPONDIENTE EN PDF</div>
                <div id="fine-uploader-files"></div>


                <script type="text/javascript">

                    var uploader_documents = new qq.FineUploader({
                        debug: false,
                        element: document.getElementById('fine-uploader-files'),
                        template: 'qq-simple-thumbnails-template', // List template for documents.
                        request: {
                            endpoint: '<?php echo $rootpath; ?>admin/sections/printers/controllers/uploadServer.php',
                            params: {
                                'printer_id': '<?php echo $printer_id; ?>',
                                'action': 'save_file',
                                'dir': '<?php echo "admin/img/printers/".$printerDTO[PrinterDAO::FIELD_ID]."/docs/" ?>'
                            }
                        },
                        validation: {
                            allowedExtensions: ['pdf'],
                            sizeLimit: 10485760,
                            itemLimit: 8
                        },
                        deleteFile: {
                            enabled: true,
                            method: 'DELETE',
                            endpoint: '<?php echo $rootpath; ?>admin/sections/printers/controllers/uploadServer.php',
                            forceConfirm: true,
                            confirmMessage: "Confirme que desea eliminar {filename} de la base de datos.",
                            params: {
                                'printer_id': '<?php echo $printer_id; ?>'
                            }
                        },
                        retry: {
                            enableAuto: true
                        },
                        thumbnails: {
                            placeholders: {
                                waitingPath: '<?php echo $rootpath; ?>admin/lib/fine-uploader/placeholders/waiting-generic.png',
                                notAvailablePath: '<?php echo $rootpath; ?>admin/lib/fine-uploader/placeholders/document.png'
                            }
                        },
                        callbacks: {
                            onDelete: function(id) {
                            },

                            onDeleteComplete: function(id, xhr, isError) {
                            },  // End onComplete callback.

                            onComplete: function (id, name, responseJSON, xhr) {
                            }, // End onComplete callback.

                            onSessionRequestComplete: function (response, success, xhr) {
                            },
                            onError: function(id, name, errorReason, xhrOrXdr) {
                                alert(qq.format("Error on file number {} - {}.  Reason: {}", id, name, errorReason));
                            }
                        }, // End callbacks.
                        // Fine Uploader will send a GET request to your endpoint on startup (and optionally when the instance is reset).
                        // The response to this request must be valid JSON. More specifically,
                        // it must be a JSON array containing Objects for each file to be fed into the uploader.
                        // https://docs.fineuploader.com/branch/master/features/session.html
                        session:  {
                            endpoint: '<?php echo $rootpath; ?>admin/sections/printers/controllers/uploadServer.php',
                            params: {
                                'printer_id': '<?php echo $printer_id; ?>',
                                'action': 'retrieve_initial_documents'
                            }
                        }
                    });<!-- /Fine Uploader Documents -->
                </script>
            <?php } ?>
        </div><!-- /odd-div -->
        <!-- END OF 5TH GROUP: DOCUMENTS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- 6TH GROUP: VIDEOS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <div id="videos" class="even-div">

            <!-- Group Title -->
            <h2><strong><span>VÍDEOS</span></strong></h2>

            <!-- If we are entering a new item -->
            <?php if (!$update_mode) {?>
                <!-- INFO about uploading videos -->
                <div class="upload-pics-warning row">
                    <span class="upload-pics-warning-content">Debe guardar la nueva impresora para poder añadir vídeos.</span>
                </div>
            <!-- If we are editing an existing printer we will show th video uploader -->
            <?php } else { ?>
                <!-- Explanation Text -->
                <div class="table-header">AÑADA LOS ENLACES A LOS VÍDEOS CORRESPONDIENTES</div>
                <!-- VIDEO UPLOADER -->
                <div id="video-uploader" class="video-uploader row">
                    <!-- To show video thumbnail once the url has been validated -->
                    <div class="youtube-thumb col-sm-3">
                        <div class="loader">
                            <svg class="circular" viewBox="25 25 50 50">
                                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                            </svg>
                        </div>
                        <img id="youtube-thumb-img" src=""/>
                    </div>
                    <!-- Text input to paste the youtube link -->
                    <div class="col-sm-8  align-self-end">
                        <label for="youtube-link" class="control-label">Pegue aquí el enlace a YouTube:</label>
                        <div class="">
                            <input name="youtube-link" class="youtube-link form-control" id="youtube-link" placeholder="Enlace al vídeo">
                        </div>
                    </div>
                    <!-- Upload Video Buttons -->
                    <div class="youtube-buttons">
                        <!-- Delete Button -->
                        <button class="video-delete-button btn btn-danger"
                                title="Eliminar el vídeo de la base de datos." type="button"
                                 data-toggle="modal" data-target="#confirmDeleteVideoBox" disabled>
                            <img src="<?php echo $rootpath ?>admin/img/trash.svg"></button>
                        <!-- Upload Button -->
                        <button class="video-upload-button"  disabled
                                title="Debe introducir una URL válida de youtube para guardar el vídeo en base de datos.">
                            <span>Guardar Vídeo</span></button>
                        <!-- Confirmation Modals -->
                        <!-- Confirmation Modal: When pressing delete button, a confirmation dialog will show. If we confirm, the -->
                        <!-- form will be submitted using submitForm($printerDTO) function declared at the end of this page ----- -->
                        <div class="modal fade" id="confirmDeleteVideoBox" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Eliminar Video</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <div class="modal-body">
                                        <p>Confirme que desea eliminar la referencia al video de la base de datos.</p>
                                        </div>
                                    <div class="modal-footer">
                                        <button id="confirm-video-delete-button" type="button" class="btn btn-danger" data-dismiss="modal"">Borrar</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div><!-- /.youtube-buttons -->
                </div><!-- /#video-uploader -->
            <?php } ?>

        </div><!-- /even-div last -->
        <!-- END OF 6TH GROUP: VIDEO +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->



        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- 7TH GROUP: TRANSLATION ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <div id="translation_en" class="odd-div last">

            <!-- Group Title -->
            <h2><strong><span>TRADUCCIONES</span></strong><img src="<?php echo $rootpath ?>/admin/img/en-icon.png"></h2>


            <!-- If we are entering a new item -->
            <?php if (!$update_mode) {?>
                <!-- INFO about uploading pictures -->
                <div class="upload-pics-warning allow-translation-warning row">
                    <span class="upload-pics-warning-content"> Debe guardar la nueva impresora para poder añadir la traducción.</span>
                </div>
                <!-- If we are edting an existing item -->
            <?php } else { ?>

                <form id="save-translation-form" action="<?php echo $rootpath?>admin/sections/printers/controllers/uploadServer.php" data-parsley-ui-enabled="true">

                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- HIDDEN INPUTS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- Printer ID -->
                    <input name="printer_id" id="printer_id" hidden value="<?php echo $printer_id; ?>"/>
                    <!-- Action: SAVE or UPDATE or DELETE -->
                    <input name="action" id="action" hidden />
                    <!-- printer_translation_id: If the printer has already been translated, so we run a save or an update action -->
                    <input name="printer_translation_id" id="printer_translation_id" hidden value="<?php echo isset($printerDTO["printer_trans_DTO"][PrinterTransDAO::FIELD_ID]) ? $printerDTO["printer_trans_DTO"][PrinterTransDAO::FIELD_ID] : '' ?>"/>
                    <!-- LANGUAGE CODE -->
                    <input name="lang_code" id="lang_code" hidden value="en"/>
                    <!-- END OF HIDDEN INPUTS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- FIELD_TITLE +++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <div class="form-group row">
                        <label for="<?php echo PrinterDAO::FIELD_TITLE?>" class="col-sm-3 control-label">Title</label>
                        <div class="col-sm-9">
                            <textarea name="<?php echo PrinterDAO::FIELD_TITLE?>" id="<?php echo PrinterDAO::FIELD_TITLE?>" class="form-control" type="text" placeholder="" required value=""><?php echo isset($printerDTO["printer_trans_DTO"][PrinterDAO::FIELD_TITLE]) ? $printerDTO["printer_trans_DTO"][PrinterDAO::FIELD_TITLE] : '' ?></textarea>
                        </div>
                    </div>
                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- FIELD_HEADLINE ++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <div class="form-group row">
                        <label for="<?php echo PrinterDAO::FIELD_HEADLINE?>" class="col-sm-3 control-label">Heading</label>
                        <div class="col-sm-9">
                            <textarea name="<?php echo PrinterDAO::FIELD_HEADLINE?>" id="<?php echo PrinterDAO::FIELD_HEADLINE?>" class="form-control" type="text" placeholder="" required value=""><?php echo isset($printerDTO["printer_trans_DTO"][PrinterDAO::FIELD_HEADLINE]) ? $printerDTO["printer_trans_DTO"][PrinterDAO::FIELD_HEADLINE] : '' ?></textarea>
                        </div>
                    </div>
                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- FIELD_DESCRIPTION +++++++++++++++++++++++++++++++++++++++++++ -->
                    <div class="form-group row">
                        <label for="<?php echo PrinterDAO::FIELD_DESCRIPTION?>" class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-9">
                            <textarea name="<?php echo PrinterDAO::FIELD_DESCRIPTION?>" id="<?php echo PrinterDAO::FIELD_DESCRIPTION?>" class="form-control" type="text" placeholder="" required value=""><?php echo isset($printerDTO["printer_trans_DTO"][PrinterDAO::FIELD_DESCRIPTION]) ? $printerDTO["printer_trans_DTO"][PrinterDAO::FIELD_DESCRIPTION] : '' ?></textarea>
                        </div>
                    </div>

                    <br><br>

                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- TABS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- TABS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <ul class="nav nav-tabs justify-content-end" role="tablist">
                        <!-- OVERVIEW TAB -->
                        <li class="nav-item">
                            <a class="nav-link active" href="#overview_en" id="overview_en-tab" role="tab" data-toggle="tab" aria-controls="overview_en" aria-expanded="true"><span>Overview</span></a>
                            <label for="<?php echo PrinterDAO::FIELD_HTML_OVERVIEW?>" hidden>Overview</label>
                        </li>
                        <!-- Features TAB -->
                        <li class="nav-item">
                            <a class="nav-link" href="#features_en" role="tab" id="features_en-tab" data-toggle="tab" aria-controls="features_en"><span>Features</span></a>
                            <label for="<?php echo PrinterDAO::FIELD_HTML_FEATURES?>" hidden>Features</label>
                        </li>
                        <!-- Specifications TAB -->
                        <li class="nav-item">
                            <a class="nav-link" href="#specifications_en" role="tab" id="specifications_en-tab" data-toggle="tab" aria-controls="specifications_en"><span>Specs</span></a>
                            <label for="<?php echo PrinterDAO::FIELD_HTML_SPECS?>" hidden>Specs</label>
                        </li>
                    </ul>
                    <!-- /TABS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- CONTENT PANELS ++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <div id="html-nav-content" class="tab-content">

                        <!-- SPECIFICATIONS TAB CONTENT -->
                        <div role="tabpanel" class="tab-pane fade" id="specifications_en" aria-labelledby="specifications_en-tab">
                            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                            <!-- FIELD_HTML_SPECS ++++++++++++++++++++++++++++++++++++++++++++ -->
                            <div class="form-group row">
                                <div class="col-sm-12">
                                <textarea placeholder="Introduzca un SPECIFICATIONS." name="<?php echo PrinterDAO::FIELD_HTML_SPECS?>"
                                          id="<?php echo PrinterDAO::FIELD_HTML_SPECS?>"
                                          class="form-control" value="" required>
                                    <?php echo isset($printerDTO["printer_trans_DTO"][PrinterDAO::FIELD_HTML_SPECS]) ? $printerDTO["printer_trans_DTO"][PrinterDAO::FIELD_HTML_SPECS] : '' ?>
                                </textarea>
                                </div>
                            </div>
                        </div><!-- /SPECIFICATIONS TAB CONTENT -->

                        <!-- FEATURES TAB CONTENT -->
                        <div role="tabpanel" class="tab-pane fade" id="features_en" aria-labelledby="features_en-tab">
                            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                            <!-- FIELD_HTML_FEATURES +++++++++++++++++++++++++++++++++++++++++ -->
                            <div class="form-group row">
                                <div class="col-sm-12">
                                <textarea placeholder="Introduzca un FEATURES." name="<?php echo PrinterDAO::FIELD_HTML_FEATURES?>"
                                          id="<?php echo PrinterDAO::FIELD_HTML_FEATURES?>"
                                          class="form-control" value="" required>
                                    <?php echo isset($printerDTO["printer_trans_DTO"][PrinterDAO::FIELD_HTML_FEATURES]) ? $printerDTO["printer_trans_DTO"][PrinterDAO::FIELD_HTML_FEATURES] : '' ?>
                                </textarea>
                                </div>
                            </div>
                        </div><!-- /FEATURES TAB CONTENT -->
                        <!-- OVERVIEW TAB CONTENT -->
                        <div role="tabpanel" class="tab-pane fade show active" id="overview_en" aria-labelledby="overview_en-tab">
                            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                            <!-- FIELD_HTML_OVERVIEW +++++++++++++++++++++++++++++++++++++++++ -->
                            <div class="form-group row">
                                <div class="col-sm-12">
                                <textarea placeholder="Introduzca un overview." name="<?php echo PrinterDAO::FIELD_HTML_OVERVIEW?>"
                                          id="<?php echo PrinterDAO::FIELD_HTML_OVERVIEW?>"
                                          class="form-control" value="" required>
                                    <?php echo isset($printerDTO["printer_trans_DTO"][PrinterDAO::FIELD_HTML_OVERVIEW]) ? $printerDTO["printer_trans_DTO"][PrinterDAO::FIELD_HTML_OVERVIEW] : '' ?>
                                </textarea>
                                </div>
                            </div>
                        </div><!-- /OVERVIEW TAB CONTENT -->
                    </div>
                    <!-- /CONTENTS PANEL +++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- /TABS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- SUBMIT BUTTON +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <div class="printer-translation-form-buttons">
                        <!-- SUBMIT BUTTON: SAVE -->
                        <button type="button" id="sbmtTransForm" class="btn btn-success right-bottom-button" title="Guardar la traducción en base de dato">
                            <?php echo $hasPrinterTranslation ? "Actualizar Traducción" : "Guardar Traducción"?>
                        </button>


                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- DELETE TRANSLATION BUTTON: DELETE We will only show it if we are updating an existing printer. ++++ -->
                        <?php if ($update_mode) { ?>

                            <button id="dltTransForm" title="Eliminar la traducción de la base de datos." type="button" class="btn btn-danger"
                                    data-toggle="modal" data-target="#confirmDelTransBox"
                                    <?php echo !$hasPrinterTranslation ? 'disabled' : ''?>>
                                <img src="<?php echo $rootpath ?>admin/img/trash.svg">
                            </button>
                            <!-- Confirmation Modal: When pressing delete button, a confirmation dialog will be shown. -->
                            <div class="modal fade" id="confirmDelTransBox" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Borrar Traduccion</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <span>Confirme que desea <strong>eliminar</strong> la traducción al inglés para esta impresora.<br><br>Los textos apareceran siempre en castellano hasta que añada de nuevo la traduccion.</span>
                                        </div>
                                        <div class="modal-footer">
                                            <button id="dltTransFormConfirm" type="button" class="btn btn-danger" data-dismiss="modal">Borrar</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <!-- /DELETE BUTTON+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->







                    </div><!-- /printer-translation-form-buttons -->
                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


                </form>
            <?php } ?>

        </div>
        <!-- END OF 7TH GROUP: TRANSLATION +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION PAGE INFO +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <div class="page_section_info unselectable">
            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- SECTION PAGE INFO TXT +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <span>AÑADIR IMPRESORA</span>
            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- /SECTION PAGE INFO TXT ++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        </div>
        <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->



    </div> <!-- End of .printers-new-content.add-printer-div -->


    <!-- LOAD AUTOSIZE LIB -->
    <script src="<?php echo $rootpath; ?>admin/lib/autosize.js"></script>
    <!-- LOAD TRUMBOWYG LIB & SPANISH TRANSLATION -->
    <script src="<?php echo $rootpath; ?>admin/lib/trumbowyg/trumbowyg.min.js"></script>
    <script src="<?php echo $rootpath; ?>admin/lib/trumbowyg/langs/es.min.js"></script>
    <!-- LOAD IMGCHECKBOX LIB -->
    <script src="<?php echo $rootpath; ?>admin/lib/jquery.imgcheckbox.js"></script>
    <!-- LOAD PARSLEYJS FORM VALIDATION PLUGGIN -->
    <script src="<?php echo $rootpath; ?>admin/lib/parsleyJS/parsley.min.js"></script>
    <script src="<?php echo $rootpath; ?>admin/lib/parsleyJS/es.js"></script>
    <!-- LOAD DIRRTY LIB -->
    <!-- <script src="<?php echo $rootpath; ?>admin/lib/jquery.dirrty.js"></script> -->
    <!-- <script type="text/javascript" src="<?php echo $rootpath; ?>admin/lib/jquery.dirtyforms.js"></script> -->



    <!-- This script is always executed, both on update and save mode -->
    <script type="text/javascript">

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // FORM SUBMIT /////////////////////////////////////////////////////////////////////////////////////////////////
        // <!-- This allow us to have two different submit buttons, one for updating/saving and -->
        // <!-- one for deleting. One thing is the form action attribute, and another the server action -->

        // $('form#save-printer-form').on("submit", function () {});
        function submitForm (action)  {
            let $form = $('form#save-printer-form');
            $form.attr("action", "<?php echo $rootpath?>admin/sections/printers/controllers/uploadServer.php");
            $form.find("#action").attr("value", action);
            $form.submit();
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        /**
         * This method appends a warning div to #page_container containing a message for the user.
         *
         * @param message Message to be presented on screen.
         * @param error_level ['info', 'error', 'warning'] Sets the color of the warning div.
         */
        function showWarningMessage(message, error_level) {

            // Check if there is already an allert message on screen and close it.
            if($('.alert').length) { $(".alert").alert("close"); }

            $('#page_container').append('<div id=\"general-warning-div\" class=\"alert fade show unselectable\" role=\"alert\">'
                + '<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>'
                + '<p><strong>' + message + '</strong></p></div>');

            $(".alert").fadeTo(5000, 1, "swing").fadeOut(800, function () {
                $(".alert").alert("close");
            });

            if (error_level.localeCompare('info') === 0) {
                $(".alert").addClass("alert-success");
            } else if (error_level.localeCompare('error') === 0) {
                $(".alert").addClass("alert-danger");
            } else if (error_level.localeCompare('warning') === 0) {
                $(".alert").addClass("alert-warning");
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // DOCUENT READY BLOCK /////////////////////////////////////////////////////////////////////////////////////////
        $(document).ready(function() {

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // SAVE PRINTER FORM JS ////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //<editor-fold desc="SAVE PRINTER FORM JS">

            // To know if user has already tried to send the form.
            let is_printer_form_validated = false;

            //////////////////////////////////////////////////////////////////////////////////////////////
            // INITIALIZE AUTOSIZE ON CORRESPONDING DIVS /////////////////////////////////////////////////
            autosize( $('#<?php echo PrinterDAO::FIELD_COMMENTS?>') );
            autosize( $('#<?php echo PrinterDAO::FIELD_TITLE?>') );
            autosize( $('#<?php echo PrinterDAO::FIELD_HEADLINE?>') );
            autosize( $('#<?php echo PrinterDAO::FIELD_DESCRIPTION?>') );
            //////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////


            //////////////////////////////////////////////////////////////////////////////////////////////
            // INITIALIZE TRUMBOWYG ON CORRESPONDING DIVS ////////////////////////////////////////////////
            let trumbowyg_props =  {
                autogrow: true,
                semantic: true,
                lang: 'es',
                btns: [['viewHTML'], ['formatting'], ['strong', 'em'], ['superscript', 'subscript'], ['link'], 'btnGrp-justify', 'btnGrp-lists', ['horizontalRule'], ['removeformat'], ['fullscreen']]
            };

            // SET TRUMBOWIG EVENTS LISTENERS.
            $('#save-printer-form #<?php echo PrinterDAO::FIELD_HTML_OVERVIEW?>')
                .trumbowyg(trumbowyg_props)
                .on('tbwfocus' , function(){  }) // Listen for `tbwfocus` event
                .on('tbwblur'  , function(){  }) // Listen for `tbwblur` event
                .on('tbwpaste',  function(){  is_printer_form_validated ? $(this).parsley().validate() : null })
                .on('tbwchange', function(){ is_printer_form_validated ? $(this).parsley().validate() : null });

            $('#save-printer-form #<?php echo PrinterDAO::FIELD_HTML_FEATURES?>')
                .trumbowyg(trumbowyg_props)
                .on('tbwfocus' , function(){  }) // Listen for `tbwfocus` event
                .on('tbwblur'  , function(){ }) // Listen for `tbwblur` event
                .on('tbwpaste',  function(){  is_printer_form_validated ? $(this).parsley().validate() : null })
                .on('tbwchange', function(){  is_printer_form_validated ? $(this).parsley().validate() : null });

            $('#save-printer-form #<?php echo PrinterDAO::FIELD_HTML_SPECS?>')
                .trumbowyg(trumbowyg_props)
                .on('tbwfocus' , function(){  }) // Listen for `tbwfocus` event
                .on('tbwblur'  , function(){  }) // Listen for `tbwblur` event
                .on('tbwpaste',  function(){  is_printer_form_validated ? $(this).parsley().validate() : null })
                .on('tbwchange', function(){  is_printer_form_validated ? $(this).parsley().validate() : null });

            //////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////


            //////////////////////////////////////////////////////////////////////////////////////////////
            // IMG CHECKBOXES ////////////////////////////////////////////////////////////////////////////
            let imgCheckbox_props =  {
                    graySelected: true,
                    showErrors: false,
                    preselect: '<?php echo isset($preselected_apps) ? $preselected_apps : ""?>',
                    onload: function(){
                        //var isChecked = el.hasClass("imgChked");
                        //var imgEl = el.children()[0];  // the img element

                        $("input[name^='apps']").each(function( index ) {

                            if (index === 0)    {
                                $(this).attr("data-parsley-required", true);
                                $(this).attr("data-parsley-mincheck","3");
                                $(this).attr("id","label_apps");
                                $(this).attr("data-parsley-error-message","Debe selecionar al menos 3.");
                            }
                            $(this).attr("data-parsley-multiple","apps_group").parsley();
                        });
                    },
                    onclick: function(el){

                        is_printer_form_validated ? $("input#label_apps").parsley().validate() : null;
                    }

                };
            $(".img-check").imgCheckbox(imgCheckbox_props);
            //////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////





            //////////////////////////////////////////////////////////////////////////////////////////////
            // SAVE PRINTER FORM VALIDATION WITH PARSLEY.JS //////////////////////////////////////////////
            $('form#save-printer-form').parsley()
                // Triggered when a Form is bound for the first time.
                .on('form:init', function() {
                // Triggered when after a form validation succeeds and before the form is actually submitted.
                }).on('form:submit', function() {
                    return true; // Return `false` to interrupt submission.
                // Triggered when a form validation is triggered, before its validation.
                }).on('form:validate', function() {
                    // Empty #form-error-callout to fullfil with new errors.
                    $("#form-error-callout").find("ul").empty();
                // Triggered when a form validation is triggered, after its validation fails.
                }).on('form:error', function() {
                    // Show #form-error-callout .
                    $("#form-error-callout").removeClass("hidden");

                // Triggered when a form validation is triggered, after its validation succeeds.
                }).on('form:success', function () {
                    // Show #form-error-callout .
                    $("#form-error-callout").addClass("hidden");

                // This global callback will be called for any field that fails validation.
                }).on('field:error', function(e) {
                    // Fullfil the #form-error-callout with the error messages.
                    let error_item, attr =  this.$element.attr('data-parsley-error-message');

                    // Check if there was a custom error message for this field.
                    // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
                    if (typeof attr !== typeof undefined && attr !== false) {
                        // Element has this attribute
                        error_item = "<li id ='li_" + this.$element.attr('id') + "'><strong>" + $("form#save-printer-form label[for='" + this.$element.attr('id') + "']").text() + ":</strong> " + this.domOptions.errorMessage + "</li>";
                    // If there is no custom error message set we take the default one.
                    } else {
                        error_item = "<li id ='li_" + this.$element.attr('id') + "'><strong>" + $("form#save-printer-form label[for='" + this.$element.attr('id') + "']").text() + ":</strong> " + this.getErrorsMessages() + "</li>";
                    }

                    // Append error message to the list.
                    $("#form-error-callout").find("ul").append(error_item);

                    // If the error was thrown by some of the 3 html field we have to style its container:
                    if(this.$element.hasClass("trumbowyg-textarea"))    {
                        // Style trumbowig box.
                        this.$element.closest(".trumbowyg-box").removeClass("parsley-success").addClass("parsley-error");
                        // Style nav-link.
                        $("label[for='" + this.$element.attr('id') + "']").prev(".nav-link").removeClass("parsley-success").addClass("parsley-error");
                    // If the success was in the applications chekcboxes (requiered min 3).
                    } else if (this.$element.attr("name").startsWith("apps") )    {

                        $(".apps-container").removeClass("parsley-success").addClass("parsley-error");
                        $("#applications-tab").removeClass("parsley-success").addClass("parsley-error");
                    }
                // Triggered when a field validation succeeds.
                }).on('field:success', function() {

                    // Take error out from errors list.
                    $("li#li_" + this.$element.attr('id')).fadeOut(200).promise().done(function () {
                        $(this).remove();
                    });

                    // If the error was thrown by some of the 3 html field we have to style its container:
                    if(this.$element.hasClass("trumbowyg-textarea"))    {
                        // Style trumbowig box.
                        this.$element.closest(".trumbowyg-box").removeClass("parsley-error").addClass("parsley-success");
                        // Style nav-link.
                        $("label[for='" + this.$element.attr('id') + "']").prev(".nav-link").removeClass("parsley-error").addClass("parsley-success");
                    // If the success was in the applications chekcboxes (requiered min 3).
                    } else if (this.$element.attr("name").startsWith("apps") )    {

                        //$('li#li_label_apps').slice(1).remove();
                        $(".apps-container").removeClass("parsley-error").addClass("parsley-success");
                        $("#applications-tab").removeClass("parsley-error").addClass("parsley-success");
                    }

                // Triggered after a field is validated (with success or with errors).
                }).on('field:validated', function () {

                    is_printer_form_validated = true;

                    // Get rid of possible duplicates on error list.
                    $('#form-error-callout>ul>li[id]').each(function (i) {
                        $('[id="' + this.id + '"]').slice(1).remove();
                    });


                    // Hide the errors div if the form is valid.
                    if( $('#save-printer-form').parsley().isValid()) {
                        $("#form-error-callout").addClass("hidden");
                    }
                });
            //////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////

            //</editor-fold>
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // END OF SAVE PRINTER FORM JS /////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////



            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // SAVE TRANSLATION FORM JS ////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //<editor-fold>

            <?php if ($update_mode) {?>

            let $transForm = $("form#save-translation-form");

            //////////////////////////////////////////////////////////////////////////////////////////////
            // SUBMIT BUTTONS: SAVE AND DELETE /////////////////////////////////////////////////////////////////////////////
            // SET THE SAVE SUBMIT BUTTON CLICK EVENT. Submit will be done and validations automatically triggered.
            $transForm.find("#sbmtTransForm").on("click", function () {
                // Set the action input to indicate the endpoint what action to do.
                $transForm.find("#action").attr("value", "<?php echo PRINTER_TRANSLATION_FORM_ACTION_SAVE; ?>");
                $transForm.submit();
            });
            // SET THE DELETE SUBMIT BUTTON CLICK EVENT.
            function setDeleteListener() {
                $transForm.find("#dltTransFormConfirm").on("click", function () {
                    // Set the action input to indicate the endpoint what action to do.
                    $transForm.find("#action").attr("value", "<?php echo PRINTER_TRANSLATION_FORM_ACTION_DELETE; ?>");
                    $transForm.submit();
                });
            }
            <?php if ($hasPrinterTranslation) {?>    setDeleteListener();       <?php }?>
            //////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////





            //////////////////////////////////////////////////////////////////////////////////////////////
            // SEND TRANSLATION FORM THROW AJAX //////////////////////////////////////////////////////////
            $transForm.on("submit", function (e) {

                // Prevent submitting the form to manage it throw AJAX.
                e.preventDefault();

                // Get the form action.
                let act = $(this).find("#action").attr("value");

                if (act === "<?php echo PRINTER_TRANSLATION_FORM_ACTION_DELETE; ?>") $(this).off('form:validate');

                // Check if validations passed.
                if ( $(this).parsley().isValid() || act === "<?php echo PRINTER_TRANSLATION_FORM_ACTION_DELETE; ?>") {

                    // Send form via AJAX to endpoint uploadServer.php
                    $.ajax({
                        url:"<?php echo $rootpath?>admin/sections/printers/controllers/uploadServer.php",
                        // If we are deleting the element, there is no need to send the textareas within the form.
                        data: act === "<?php echo PRINTER_TRANSLATION_FORM_ACTION_DELETE; ?>"
                                ? $(this).find("input").serialize()
                                : $(this).serialize(),
                                type:'POST',
                        success: function(data, status) {
                            try {
                                // Parse json string response into object:
                                obj = JSON && JSON.parse(data) || $.parseJSON(data);
                                if (obj.success === true)   {
                                    setTimeout(function(){ showWarningMessage(obj.message, "info"); }, 600);
                                    // If the translation was saved or updated
                                    if (typeof obj.printer_translation_id !== typeof undefined && obj.printer_translation_id) {
                                        // Add translation id to form so next saves will resolve in uploads on server.
                                        $transForm.find("#printer_translation_id").val(obj.printer_translation_id);
                                        $transForm.find("#dltTransForm").prop("disabled", false);
                                        setDeleteListener();
                                    // If the translation was deleted.
                                    } else  {
                                        $transForm.find("#dltTransForm").prop("disabled", true);
                                        // Reset the form.
                                        $transForm.trigger("reset");
                                        $transForm.find(".trumbowyg-editor").html("");
                                        $transForm.parsley().reset();
                                    }

                                } else {
                                    showWarningMessage(obj.message, "error");
                                }
                            } catch  (error)   {
                                showWarningMessage("Error: " + error, "error");
                                console.log(error);
                            }
                        },
                        error:function(data){
                            console.log(data);
                        }
                    });
                }
            });
            //////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////


            //////////////////////////////////////////////////////////////////////////////////////////////
            // SAVE TRANSLATION FORM VALIDATION WITH PARSLEY.JS //////////////////////////////////////////
            let is_translation_form_validated = false;
            $transForm.parsley()
                // Triggered when a Form is bound for the first time.
                .on('form:init', function() {
                // Triggered when the form is reset: Manually remove styles from trumbowyg editors.
                }).on('form:reset', function() {
                    $transForm.find(".trumbowyg-textarea").closest(".trumbowyg-box").removeClass("parsley-success parsley-error");
                    $transForm.find(".nav-link").removeClass("parsley-success parsley-error");
                    return true;
                // Triggered when after a form validation succeeds and before the form is actually submitted.
                }).on('form:submit', function() {
                    return false; // Return `false` to interrupt submission.
                // Triggered when a form validation is triggered, before its validation.
                }).on('form:validate', function() {
                    // Empty #form-error-callout to fullfil with new errors.
                    $("#form-error-callout").find("ul").empty();
                // Triggered when a form validation is triggered, after its validation fails.
                }).on('form:error', function() {
                    // Show #form-error-callout .
                    $("#form-error-callout").removeClass("hidden");
                // Triggered when a form validation is triggered, after its validation succeeds.
                }).on('form:success', function () {
                    // Hide #form-error-callout .
                    $("#form-error-callout").addClass("hidden");
                // This global callback will be called for any field that fails validation.
                }).on('field:error', function(e) {
                    // Fullfil the #form-error-callout with the error messages.
                    let error_item, attr =  this.$element.attr('data-parsley-error-message');

                    // Check if there was a custom error message for this field.
                    // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
                    if (typeof attr !== typeof undefined && attr !== false) {
                        // Element has this attribute
                        error_item = "<li id ='li_" + this.$element.attr('id') + "'><strong>" + $("form#save-translation-form label[for='" + this.$element.attr('id') + "']").text() + ":</strong> " + this.domOptions.errorMessage + "</li>";
                        // If there is no custom error message set we take the default one.
                    } else {
                        error_item = "<li id ='li_" + this.$element.attr('id') + "'><strong>" + $("form#save-translation-form label[for='" + this.$element.attr('id') + "']").text() + ":</strong> " + this.getErrorsMessages() + "</li>";
                    }
                    // Append error message to the list.
                    $("#form-error-callout").find("ul").append(error_item);
                    // If the error was thrown by some of the 3 html field we have to style its container:
                    if(this.$element.hasClass("trumbowyg-textarea"))    {
                        // Style trumbowig box.
                        this.$element.closest(".trumbowyg-box").removeClass("parsley-success").addClass("parsley-error");
                        // Style nav-link.
                        $("label[for='" + this.$element.attr('id') + "']").prev(".nav-link").removeClass("parsley-success").addClass("parsley-error");
                        // If the success was in the applications chekcboxes (requiered min 3).
                    } else if (this.$element.attr("name").startsWith("apps") )    {

                        $(".apps-container").removeClass("parsley-success").addClass("parsley-error");
                        $("#applications-tab").removeClass("parsley-success").addClass("parsley-error");
                    }
                // Triggered when a field validation succeeds.
                }).on('field:success', function() {

                    // Take error out from errors list.
                    $("li#li_" + this.$element.attr('id')).fadeOut(200).promise().done(function () {
                        $(this).remove();
                    });

                    // If the error was thrown by some of the 3 html field we have to style its container:
                    if(this.$element.hasClass("trumbowyg-textarea"))    {
                        // Style trumbowig box.
                        this.$element.closest(".trumbowyg-box").removeClass("parsley-error").addClass("parsley-success");
                        // Style nav-link.
                        $("label[for='" + this.$element.attr('id') + "']").prev(".nav-link").removeClass("parsley-error").addClass("parsley-success");
                        // If the success was in the applications chekcboxes (requiered min 3).
                    } else if (this.$element.attr("name").startsWith("apps") )    {

                        //$('li#li_label_apps').slice(1).remove();
                        $(".apps-container").removeClass("parsley-error").addClass("parsley-success");
                        $("#applications-tab").removeClass("parsley-error").addClass("parsley-success");
                    }
                // Triggered after a field is validated (with success or with errors).
                }).on('field:validated', function () {
                    is_translation_form_validated = true;
                    // Get rid of possible duplicates on error list.
                    $('#form-error-callout>ul>li[id]').each(function (i) {
                        $('[id="' + this.id + '"]').slice(1).remove();
                    });
                    // Hide the errors div if the form is valid.
                    if( $transForm.parsley().isValid()) {
                        $("#form-error-callout").addClass("hidden");
                    }
                });
            //////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////



            //////////////////////////////////////////////////////////////////////////////////////////////
            // INITIALIZE AUTOSIZE ON CORRESPONDING DIVS /////////////////////////////////////////////////
            autosize( $('#save-translation-form #<?php echo PrinterDAO::FIELD_TITLE?>') );
            autosize( $('#save-translation-form #<?php echo PrinterDAO::FIELD_HEADLINE?>') );
            autosize( $('#save-translation-form #<?php echo PrinterDAO::FIELD_DESCRIPTION?>') );
            //////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////


            //////////////////////////////////////////////////////////////////////////////////////////////
            // INITIALIZE TRUMBOWYG ON CORRESPONDING DIVS ////////////////////////////////////////////////
            let trumbowyg_props_trans =  {
                autogrow: true,
                semantic: true,
                lang: 'en',
                btns: [['viewHTML'], ['formatting'], ['strong', 'em'], ['superscript', 'subscript'], ['link'], 'btnGrp-justify', 'btnGrp-lists', ['horizontalRule'], ['removeformat'], ['fullscreen']]
            };

            // SET TRUMBOWIG EVENTS LISTENERS.
            $('#save-translation-form #<?php echo PrinterDAO::FIELD_HTML_OVERVIEW?>')
                .trumbowyg(trumbowyg_props_trans)
                .on('tbwfocus' , function(){  }) // Listen for `tbwfocus` event
                .on('tbwblur'  , function(){  }) // Listen for `tbwblur` event
                .on('tbwpaste',  function(){  is_translation_form_validated ? $(this).parsley().validate() : null })
                .on('tbwchange', function(){ is_translation_form_validated ? $(this).parsley().validate() : null });

            $('#save-translation-form #<?php echo PrinterDAO::FIELD_HTML_FEATURES?>')
                .trumbowyg(trumbowyg_props_trans)
                .on('tbwfocus' , function(){  }) // Listen for `tbwfocus` event
                .on('tbwblur'  , function(){ }) // Listen for `tbwblur` event
                .on('tbwpaste',  function(){  is_translation_form_validated ? $(this).parsley().validate() : null })
                .on('tbwchange', function(){  is_translation_form_validated ? $(this).parsley().validate() : null });

            $('#save-translation-form #<?php echo PrinterDAO::FIELD_HTML_SPECS?>')
                .trumbowyg(trumbowyg_props_trans)
                .on('tbwfocus' , function(){  }) // Listen for `tbwfocus` event
                .on('tbwblur'  , function(){  }) // Listen for `tbwblur` event
                .on('tbwpaste',  function(){  is_translation_form_validated ? $(this).parsley().validate() : null })
                .on('tbwchange', function(){  is_translation_form_validated ? $(this).parsley().validate() : null });

            //////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////
            <?php }?>
            //</editor-fold>
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // END OF SAVE TRANSLATION FORM JS /////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////




            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // VIDEO UPLOADER JS ///////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //<editor-fold>
            <?php if ($update_mode) { ?>

            $(".youtube-buttons button.video-upload-button").on("click",uploadVideoLink );
            $(".youtube-buttons button#confirm-video-delete-button").on("click",deleteVideoLink );

            let videoId;

            // If there is a video already saved on database we will load the info.
            let arrayVideoDTO = <?php echo $update_mode ? json_encode($arrayVideoDTO) : null?>;
            if (arrayVideoDTO.length > 0)   {
                videoId = arrayVideoDTO[0].youtube_id;
                uiVideoUploaded($("#video-uploader"));
            }

            /**
             * This method will send an ajax request to uploadServer.php to save
             * the youtube video on database.
             */
            function uploadVideoLink(e)  {


                // The video-upload-button.
                let $this = $(e.target);
                // The current youtube-uploader (in case there are more than one).
                let $videoUploader = $this.parents(".video-uploader");


                $.post('<?php echo $rootpath; ?>admin/sections/printers/controllers/uploadServer.php',
                    {
                        youtube_id: videoId,
                        printer_id: "<?php echo $printer_id; ?>",
                        action: 'save_video'
                    },
                    function(data, status){
                        // Parse json string response into object:
                        obj = JSON && JSON.parse(data) || $.parseJSON(json);

                        if (obj.success)    {
                            uiVideoUploaded($videoUploader);
                            showWarningMessage(obj.message, "info");
                        }
                    });
            }



            /**
             * This method prepares the UI to indicate that a video already exists on database.
             */
            function uiVideoUploaded($videoUploader)  {

                // Disable the upload button and enable the delete button for the video.
                $videoUploader.find(".video-upload-button").prop("disabled", true);
                $videoUploader.find(".video-delete-button").prop("disabled", false);

                // Disable the text input so user cant change the video URL.
                $videoUploader.find(".youtube-link").prop("disabled", true);
                $videoUploader.find(".youtube-link").val("https://www.youtube.com/watch?v="+videoId);

                // Set the thumbnail URL from youtube id:
                $videoUploader.find(".youtube-thumb img").attr("src", "https://i.ytimg.com/vi/" + videoId + "/hqdefault.jpg" );
            }


            /**
             * This method will send an ajax request to uploadServer.php to delete
             * the youtube video on database.
             */
            function deleteVideoLink(e)  {

                // The clicked button.
                let $this = $(e.target);
                // The current youtube-uploader (in case there are more than one).
                let $videoUploader = $this.parents(".video-uploader");

                $.post('<?php echo $rootpath; ?>admin/sections/printers/controllers/uploadServer.php',
                    { youtube_id: videoId,
                        printer_id: "<?php echo $printer_id; ?>",
                        action: 'delete_video' },
                    function(data, status) {
                        // Parse json string response into object:
                        obj = JSON && JSON.parse(data) || $.parseJSON(json);

                        if (obj.success)    {
                            uiVideoDeleted($videoUploader);
                            showWarningMessage(obj.message, "info");
                        }
                    });
            }

            /**
             * This method prepares the UI to indicate that there is no video saved on databse for this printer.
             */
            function uiVideoDeleted($videoUploader)  {

                // Disable the delete button and enable the upload button for the video.
                $videoUploader.find(".video-upload-button").prop("disabled", false);
                $videoUploader.find(".video-delete-button").prop("disabled", true);

                // Delete the text input so user cant change the video URL.
                $videoUploader.find(".youtube-link").prop("disabled", false);
                $videoUploader.find(".youtube-link").val("");
                $videoUploader.find(".youtube-link").trigger("input");
                videoId = null;

            }


            /**
             * Creates virtual A element, then you get access to some helpful DOM methods to parse it's href
             * value like hostname, protocol, pathname, search, hash, etc. Then validate the url parts.
             *
             * Returned values:
             *
             * + false if the URL is not youtube url
             * + empty string if it is youtube url but without video ID
             * + string id if the url is a valid youtube video
             *
             * @param getURL
             * @returns {*}
             */
            function isYoutubeRegEx(getURL){
                if(typeof getURL!=='string') return false;
                let newA = document.createElement('A');
                newA.href = getURL;
                let host = newA.hostname;
                let srch = newA.search;
                let path = newA.pathname;

                if(host.search(/youtube\.com|youtu\.be/)===-1) return false;
                if(host.search(/youtu\.be/)!==-1) return path.slice(1);
                if(path.search(/embed/)!==-1) return /embed\/([A-z0-9]+)(\&|$)/.exec(path)[1];
                let getId = /v=([A-z0-9]+)(\&|$)/.exec(srch);
                if(host.search(/youtube\.com/)!==-1) return !getId ? '':getId[1];
            }

            // On every input change we will check if the given URL correspond with an existing youtube video.
            $("#youtube-link").on('input',function(){


                /**
                 * Sets everything ready to upload de video URL on database. It is called when the .youtube-link
                 * input contains a valid youtube video URL.
                 *
                 * @param $videoUploader
                 */
                function videoFound($videoUploader)   {

                    $videoUploader.find(".youtube-thumb img").attr("src",videoInfo.thumbnail_url );
                    $videoUploader.find(".youtube-link").removeClass("youtube-error").addClass("youtube-success");
                    $videoUploader.find(".video-upload-button").prop("disabled", false);
                    $videoUploader.find(".video-upload-button").prop("title", "Guardar el vídeo en base de datos");
                }

                /**
                 * Sets everything to indicate that the video cannot be uploaded to database. It is called when
                 * the .youtube-link input contains an invalid youtube video URL.
                 *
                 * @param $videoUploader
                 */
                function videoNotFound($videoUploader)    {

                    $videoUploader.find(".youtube-thumb img").attr("src","");
                    $videoUploader.find(".youtube-link").removeClass("youtube-success").addClass("youtube-error");
                    $videoUploader.find(".video-upload-button").prop("disabled", true);
                    $videoUploader.find(".video-upload-button").prop("title", "Debe introducir una URL válida de youtube para guardar el vídeo en base de datos.");

                }

                // The youtube-link input.
                let $this = $(this);
                // The current youtube-uploader (in case there are more than one).
                let $videoUploader = $this.parents(".video-uploader");

                // Get the corresponding video's URL and ID.
                let videoUrl = $(this).val();
                videoId = isYoutubeRegEx($(this).val());
                // Variable to store the video related info from API, such as thumbnails.
                let videoInfo = {};

                // GET VIDEO INFO
                // If the video URL matches de regex so we have a valid video ID we will try to get
                // the video info from youtube API.
                if (videoId && videoId.length > 0){

                    // METHOD I --> YOUTUBE API V3
                    // If we send the ajax request directly to http://www.youtube.com/oembed?url=... we get an
                    // Access-Control-Allow-Origin error, so we will have to your Google Youtube's API v3.
                    // For this we need to enable the YOUTUBE API V3 from our https://console.developers.google.com
                    // and get a KEY for our application to validate the API requests.
                    // We can see how to use the API here: https://developers.google.com/youtube/v3/getting-started

                    // The API KEY. Must be a valid key for our app enabled on our google developers' console.
                    // const YOUTBE_API_KEY = "AIzaSyDTEIMPE0a2WjOQuRTibrGFZwtPhc9RGUE";

                    // Now we'll send an HTTP Request to the API to get the required info.
                    // $.ajax({
                    //     url: "https://www.googleapis.com/youtube/v3/videos?id=" + videoId + "&key=" + YOUTBE_API_KEY +
                    //         "&part=snippet&fields=items(id,snippet)",
                    //     dataType: "json",
                    //     beforeSend: function(){
                    //         // Handle the beforeSend event
                    //         $videoUploader.find(".loader").show();
                    //     },
                    //     complete: function(){
                    //         // Handle the complete event
                    //         $videoUploader.find(".loader").hide(200);
                    //     },
                    //     success: function (data) {
                    //         // console.log(data);
                    //         try { videoInfo.thumbnail_url = data.items[0].snippet.thumbnails.default.url; }
                    //         catch(err) {
                    //             // Video error: Video not found
                    //             videoNotFound($videoUploader);
                    //             return;
                    //         }
                    //         // If we reach here, the video exists and we have its info:
                    //         videoFound($videoUploader);
                    //     },
                    //     error: function (result) {
                    //         alert("No se encontró la información del vídeo.");
                    //     }
                    // });


                    // METHOD II --> USE http://img.youtube.com || http://i.ytimg.com || http://i3.ytimg.com
                    // Since we only need to grab the thumbnail, for UI purposes, to show the user that the video was
                    // found before saving its ID on database, we do not need to use Youtube API, we can just construct
                    // the thumbnail's URL. We will also check that the URL references a valid image file.
                    // There is an added problem, when we set the thumbnail url to an img element, even if the video id
                    // does not exist, the i3.ytimg.com (youtube images) url will return a default NOT_FOUND image in
                    // in the response's body, even if the http status is 404 in the reponse's header, so we
                    // have no way to detect the youtube ID doesn't exist by just checking the thumnail URL, we would
                    // need you use YOUTUBE API V3.
                    // As a workaround we are going set hqdefault.jpg thumbnail, which size should be 480px x 360px,
                    // on a temporal Image element. So if the img retrieved is smaller we will assume that
                    // the video does not exists.
                    videoInfo.thumbnail_url = "http://i3.ytimg.com/vi/" + videoId + "/hqdefault.jpg";
                    let img = new Image();
                    $(img).off("load").on("load", function() {
                        if (this.height < 360) {
                            videoNotFound($videoUploader);
                            return;
                        }
                        videoFound($videoUploader);
                    }).attr("src", videoInfo.thumbnail_url);


                } else  {
                    videoNotFound($videoUploader);
                }
            });


            <?php } ?>
            //</editor-fold>
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // END OF VIDEO UPLOADER JS ////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////



            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // INIT TOOLTIPS //////////////////////////////////////////////////////////////////////////////////////
            // Use tooltip Bootstrap4 to customize the onHover message of the checkbox #is_published.
            $('[data-toggle="tooltip"]').tooltip({
                offset: '0,0',
                delay: { 'show': 100, 'hide': 100 },
                html: true,
                template: '<div class="tooltip tooltip-printers" role="tooltip"><div class="tooltip-inner"></div></div>' // Default template
            });
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////



            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // CUSTOM VALIDATIONS /////////////////////////////////////////////////////////////////////////////////
            //<editor-fold>
            ///////////////////////////////////////////
            // IS_NEW & IS_SOLD CHECK BUTTONS. ////////
            // Set is_sold = false whenever a printer is new. If the printer is old, then it might is_sold = true.
            // Disable both is_sold and production_date when the printer is new.
            $("#<?php echo PrinterDAO::FIELD_IS_NEW?>").change(function () {
                if ($(this).is(':checked'))   {
                    $("#<?php echo PrinterDAO::FIELD_IS_SOLD?>").prop("checked", false);
                    $("#<?php echo PrinterDAO::FIELD_IS_SOLD?>").attr("disabled", true);
                    $("#<?php echo PrinterDAO::FIELD_PRODUCTION_DATE?>").attr("disabled", true);
                } else {
                    $("#<?php echo PrinterDAO::FIELD_IS_SOLD?>").attr("disabled", false);
                    $("#<?php echo PrinterDAO::FIELD_PRODUCTION_DATE?>").attr("disabled", false);
                }

            });

            ///////////////////////////////////////////
            // IS_PUBLISHED CHECK BUTTON //////////////
            // When trying to publish a printer, we check that there are image files related to the printer.
            // If the printer has not been saved on database yet the check button will be disabled,
            $("#<?php echo PrinterDAO::FIELD_IS_PUBLISHED?>").change(function () {

                // We make an AJAX request to publish/un-publish the printer. We get the response
                // from uploadServer.php. When publishing, it will check if the printer has at least
                // 1 image, and returns an error if not.
                $.post('<?php echo $rootpath; ?>admin/sections/printers/controllers/uploadServer.php',
                    {
                        printer_id: '<?php echo $printer_id; ?>',
                        action: 'set_is_published',
                        is_published: $(this).is(':checked')
                    },
                    function(data, status){
                        try {
                            // Show info message to user on UI.
                            obj = JSON && JSON.parse(data) || $.parseJSON(data);
                            if (obj.success === true)   {
                                showWarningMessage(obj.header + '<br>' + obj.info, "info");
                                obj.is_published === true
                                    ? $('[data-toggle="tooltip"]').attr('data-original-title', "Impresora Publicada.\nClick para des-publicar.")
                                    : $('[data-toggle="tooltip"]').attr('data-original-title', "Debe publicar la impresora para que aparezca en talawin.es");
                            } else {
                                showWarningMessage(obj.header + '<br>' + obj.info, "error");
                                $("#<?php echo PrinterDAO::FIELD_IS_PUBLISHED?>").prop('checked', false);
                            }
                        } catch  (error)   {
                            showWarningMessage("Error: " + error, "error");
                            console.log(error);
                        }
                    });
            });
            //</editor-fold>
            // END OF CUSTOM VALIDATIONS //////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////



            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // DIRRTY /////////////////////////////////////////////////////////////////////////////////////////////
            // TODO: If we are UPDATING a printer we will detect if any of the form values have changed
            // before allowing user to send the update request.-->
            <?php if ($update_mode) {?>

//            $('#save-printer-form').dirrty({
//                preventLeaving: true,
//                leavingMessage: "Los cambios no han sido guardados en la base de datos."
//
//                // this function is called when the form.trigger's "dirty"
//            }).on("dirty", function() {
//                console.log("I'm dirty!")
//
//                // this function is called when the form.trigger's "clean"
//            }).on("clean", function() {
//                console.log("I'm clean!")
//            });


            // Enable Debugging (non-minified file only).
            // $('#save-printer-form').dirtyForms({ debug: true });



            <?php }?>
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////



            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // SAVE/UPDATE BUTTONS SCROLLING //////////////////////////////////////////////////////////////////////
            $.fn.followTo = function () {
                let $this = $(this), $window = $(window);
                $window.scroll(function (e) {

                    let scrollBottom = $window.scrollTop() + $window.height();
                    let htmlContentDivBottom = $("#html-content").offset().top + $("#html-content").height() + parseInt($(".printers-new-content").css('margin-top').replace(/[^-\d\.]/g, ''));

                    if (scrollBottom > htmlContentDivBottom) {
                        $this.css({position : 'absolute', 'margin-right': '-2.2rem'});
                    } else {
                        $this.css({position: 'fixed', 'margin-right': 0});
                    }
                });
            };
            $(".printer-form-buttons").followTo();
            // END OF SAVE/UPDATE BUTTONS SCROLLING ///////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////





            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // Smooth scrolling when clicking an anchor link //////////////////////////////////////////////////////
            document.querySelectorAll('a[href^="#"]').forEach(anchor => {
                anchor.addEventListener('click', function (e) {
                    e.preventDefault();
                    document.querySelector(this.getAttribute('href')).scrollIntoView({
                        behavior: 'smooth',
                        block: 'start'
                    });
                });
            });
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////



        }); // END OF DOCUMENT READY JS BLOCK
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    </script>

    <?php

    // Check for errors and warnings:
    if(isset($_SESSION['error']) && is_array($_SESSION['error']))   include $rootpath . 'admin/inc/warning.php';

    // Include Footer.
    include $rootpath . 'admin/inc/footer.php';

    // In case we were arriving from a failed attempt to save a new printer.
    unset ($_SESSION['printer_id']);


} else 	{

    header("Location:".SECTION_LOGIN);
    exit;

}?>