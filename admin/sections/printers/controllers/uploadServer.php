<?php
/**
 * UPLOADSERVER.PHP ENDPOINT:
 *
 * This script receives http POST request form addUpdatePrinter.php
 * It manages the adding/updating of the corresponding printer. It receives #save-printer-form with
 * all the info related to the printer and may perform 2 different actions: save_printer and delete_printer.
 *
 * IT ALSO HANDLES THE DISPLAY ORDER OF THE PRINTERS (display_order column in printers table) REQUESTED FROM
 * DATABLE ON printers/index.html (set_new_order action) and the set_is_published action requested from
 * addUpdatePrinter.php
 *
 *  ACTIONS RELATED TO PRINTERS:
 *
 * + save_printer                 POST      It will perform an insert of a new printer, or an update (if $_POST["printer_id"] is set)
 *                                          of an existing one.
 * + delete_printer               POST      It will delete the corresponding printer with id $_POST["printer_id"] from the table
 *                                          printers and all the corresponding info about the printer: table printers_applications rows,
 *                                          table files rows, and both documents and images saved on file system.
 * + set_is_published             POST      Sets is_published field to false/true for the corresponding printer $_POST['printer_id'] on table printers.
 * + set_new_order                POST      Sets a new display order for printers changing the display_order column on database printer's table.
 *
 *
 * THIS ENDPOINT RECEIVES ALSO REQUEST FROM 2 FINE UPLOADER INSTANCES OF addUpdatePrinter.php.
 * ONE FOR THE PICTURES RELATED TO THE CORRESPONDING PRINTER, AND ANOTHER ONE FOR THE PDFs ASSOCIATED.
 *
 * IT ALSO RECEIVES REQUEST TO SAVE OR DELETE VIDEOS RELATED TO A PRINTER FROM addUpdatePrinter.php.
 *
 * ACTIONS RELATED TO FILES/VIDEOS:
 *
 * + save_file                      POST    For both documents and images: It will save the file on $_POST['dir'] folder.
 * + save_video                     POST    It saves a video row on videos tables related to the corresponding $_POST['printer_id'].
 * + delete_video                   POST    It removes the corresponding row on the videos table for $_POST['printer_id'].
 * + make_main                      POST    Only for images, sets is_main = true on the corresponding printer with id $_POST['file_id'].
 * + retrieve_initial_pictures      GET     Only for images: Retrieves an array of objects with the images related to the printer if any.
 * + retrieve_initial_document      GET     Only for documents: Retrieves an array of objects with the documents related to the printer if any.
 * + DELETE                         DELETE  For both documents and images: It removes the file from table files and file system.
 *
 */


$rootpath = "./../../../../";

// Get connection to database.
include $rootpath . 'admin/scripts/connect.php';
// Constants file: 
include $rootpath . 'admin/inc/constants.php';
// For thumbnails.
include $rootpath . 'admin/scripts/functions.php';


// Include the upload handler class:
require_once $rootpath . 'admin/sections/printers/controllers/UploadHandler.php';

// Instantiate the Upload Handler to handle files:
$uploader = new UploadHandler();
$uploader->setRootpath($rootpath);
// Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
$uploader->allowedExtensions = array('jpeg', 'jpg', 'gif', 'png','pdf'); // All files types allowed by default
// Specify max file size in bytes.
$uploader->sizeLimit = null;
// Specify the input name set in the javascript.
$uploader->inputName = "qqfile"; // matches Fine Uploader's default inputName value by default


$method = $_SERVER["REQUEST_METHOD"];

/** ********************************************** */
/** ********** PRINTERS RELATED ACTIONS ********** */
/** ********************************************** */

/** ACTION PRINTER_FORM_ACTION_SAVE_PRINTER */
// If we pressed SAVE printer BUTTON:
if ($method == "POST" && isset($_POST['action']) && strcmp( $_POST['action'], PRINTER_FORM_ACTION_SAVE_PRINTER) == 0)	{



    //$printerDTO = new PrinterDTO(json_encode($_POST));
    $printerDTO = new PrinterDTO();
    $printerDAO = new PrinterDAO();


    // If id is set, we are UPDATING an existing printer, PrinterDAO will check it and execute an UPDATE query.
    $printerDTO->setId(isset($_POST["printer_id"]) && $_POST["printer_id"] != 0 ? $_POST["printer_id"] : null);


    // brand_id is set and has a int value 0 or bigger.
    $printerDTO->setBrandId(array_key_exists(PrinterDAO::FIELD_BRAND_ID, $_POST) && is_int(intval($_POST[PrinterDAO::FIELD_BRAND_ID])) && $_POST[PrinterDAO::FIELD_BRAND_ID] >= 0
        ? intval($_POST[PrinterDAO::FIELD_BRAND_ID])
        : 0);
    $printerDTO->setSubBrandId(array_key_exists(PrinterDAO::FIELD_SUB_BRAND_ID, $_POST) && is_int(intval($_POST[PrinterDAO::FIELD_SUB_BRAND_ID])) && $_POST[PrinterDAO::FIELD_SUB_BRAND_ID] >= 0
        ? intval($_POST[PrinterDAO::FIELD_SUB_BRAND_ID])
        : 0);
    $printerDTO->setModel(array_key_exists(PrinterDAO::FIELD_MODEL, $_POST) && is_string($_POST[PrinterDAO::FIELD_MODEL])
        ? $_POST[PrinterDAO::FIELD_MODEL]
        : "0");
    $printerDTO->setIsNew(isset($_POST[PrinterDAO::FIELD_IS_NEW]) && $_POST[PrinterDAO::FIELD_IS_NEW] === 'on'
        ? 1
        : 0);
    $printerDTO->setIsPublished(isset($_POST[PrinterDAO::FIELD_IS_PUBLISHED]) && $_POST[PrinterDAO::FIELD_IS_PUBLISHED] === 'on'
        ? 1
        : 0);
    $printerDTO->setIsRelease(isset($_POST[PrinterDAO::FIELD_IS_RELEASE]) && $_POST[PrinterDAO::FIELD_IS_RELEASE] === 'on'
        ? 1
        : 0);


    $printerDTO->setRef(isset($_POST[PrinterDAO::FIELD_REF])
        ? $_POST[PrinterDAO::FIELD_REF]
        : "0");
    $printerDTO->setPricePvp(isset($_POST[PrinterDAO::FIELD_PRICE_PVP])
        ? $_POST[PrinterDAO::FIELD_PRICE_PVP]
        : 0);
    $printerDTO->setPriceCost(isset($_POST[PrinterDAO::FIELD_PRICE_COST])
        ? $_POST[PrinterDAO::FIELD_PRICE_COST]
        : 0);
    $printerDTO->setComments(isset($_POST[PrinterDAO::FIELD_COMMENTS])
        ? $_POST[PrinterDAO::FIELD_COMMENTS]
        : "0");

    $printerDTO->setProductionDate(isset($_POST[PrinterDAO::FIELD_PRODUCTION_DATE])
        ? date_format(date_create($_POST[PrinterDAO::FIELD_PRODUCTION_DATE]),"Y-m-d" )
        : '0000-01-01');
    $printerDTO->setWarranty(isset($_POST[PrinterDAO::FIELD_WARRANTY])
        ? $_POST[PrinterDAO::FIELD_WARRANTY]
        : "0");
    $printerDTO->setIsSold(isset($_POST[PrinterDAO::FIELD_IS_SOLD]) && $_POST[PrinterDAO::FIELD_IS_SOLD] === 'on'
        ? 1
        : 0);


    $printerDTO->setFeatInkId(isset($_POST[PrinterDAO::FIELD_FEAT_INK_ID]) && is_int(intval($_POST[PrinterDAO::FIELD_FEAT_INK_ID])) && $_POST[PrinterDAO::FIELD_FEAT_INK_ID] >= 0
        ? $_POST[PrinterDAO::FIELD_FEAT_INK_ID]
        : 0);
    $printerDTO->setFeatWhiteInkId(isset($_POST[PrinterDAO::FIELD_FEAT_INK_ID]) && is_int(intval($_POST[PrinterDAO::FIELD_FEAT_WHITE_INK_ID])) && $_POST[PrinterDAO::FIELD_FEAT_WHITE_INK_ID] >= 0
        ? $_POST[PrinterDAO::FIELD_FEAT_WHITE_INK_ID]
        : 0);
    $printerDTO->setFeatMediaTypeId(isset($_POST[PrinterDAO::FIELD_FEAT_INK_ID]) && is_int(intval($_POST[PrinterDAO::FIELD_FEAT_MEDIA_TYPE_ID])) && $_POST[PrinterDAO::FIELD_FEAT_MEDIA_TYPE_ID] >= 0
        ? $_POST[PrinterDAO::FIELD_FEAT_MEDIA_TYPE_ID]
        : 0);
    $printerDTO->setFeatMaxPrintWidth(array_key_exists(PrinterDAO::FIELD_FEAT_MAX_PRINT_WIDTH, $_POST)
        ? $_POST[PrinterDAO::FIELD_FEAT_MAX_PRINT_WIDTH]
        : "0");
    $printerDTO->setFeatMaxMediaThick(array_key_exists(PrinterDAO::FIELD_FEAT_MAX_MEDIA_THICK, $_POST)
        ? $_POST[PrinterDAO::FIELD_FEAT_MAX_MEDIA_THICK]
        : "0");
    $printerDTO->setFeatDropSize(array_key_exists(PrinterDAO::FIELD_FEAT_DROP_SIZE, $_POST)
        ? $_POST[PrinterDAO::FIELD_FEAT_DROP_SIZE]
        : "0");
    $printerDTO->setFeatResolution(array_key_exists(PrinterDAO::FIELD_FEAT_RESOLUTION, $_POST)
        ? $_POST[PrinterDAO::FIELD_FEAT_RESOLUTION]
        : "0");
    $printerDTO->setFeatMaxSpeed(array_key_exists(PrinterDAO::FIELD_FEAT_MAX_SPEED, $_POST)
        ? $_POST[PrinterDAO::FIELD_FEAT_MAX_SPEED]
        : "0");


    $printerDTO->setTitle(isset($_POST[PrinterDAO::FIELD_TITLE]) && !empty($_POST[PrinterDAO::FIELD_TITLE])
        ? $_POST[PrinterDAO::FIELD_TITLE]
        : "0");
    $printerDTO->setHeadline(isset($_POST[PrinterDAO::FIELD_HEADLINE]) && !empty($_POST[PrinterDAO::FIELD_HEADLINE])
        ? $_POST[PrinterDAO::FIELD_HEADLINE]
        : "0");
    $printerDTO->setDescription(isset($_POST[PrinterDAO::FIELD_DESCRIPTION]) && !empty($_POST[PrinterDAO::FIELD_DESCRIPTION])
        ? $_POST[PrinterDAO::FIELD_DESCRIPTION]
        : "0");
    $printerDTO->setHtmlOverview(isset($_POST[PrinterDAO::FIELD_HTML_OVERVIEW]) && !empty($_POST[PrinterDAO::FIELD_HTML_OVERVIEW])
        ? $_POST[PrinterDAO::FIELD_HTML_OVERVIEW]
        : "0");
    $printerDTO->setHtmlFeatures(isset($_POST[PrinterDAO::FIELD_HTML_FEATURES]) && !empty($_POST[PrinterDAO::FIELD_HTML_FEATURES])
        ? $_POST[PrinterDAO::FIELD_HTML_FEATURES]
        : "0");
    $printerDTO->setHtmlSpecs(isset($_POST[PrinterDAO::FIELD_HTML_SPECS]) && !empty($_POST[PrinterDAO::FIELD_HTML_SPECS])
        ? $_POST[PrinterDAO::FIELD_HTML_SPECS]
        : "0");


    try {
        // Here we save the PrinterDTO using a PrinterDAO object to connect to database.
        $result = intval($printerDAO->savePrinter($printerDTO));

        // This will manage possible 'Duplicate Entry' exceptions for the ref column
        // that may occur when trying to save the item on database.
    } catch (Exception $e) {
        $result = $e->getMessage();
    }


    // If there was any error and the printer was not saved: We redirect to the form and
    // inform about the error on a warning box.
    if (!is_int($result) || $result < 0)	{

        $_SESSION['error']['level'] = "error";

        if (is_string($result) && strpos($result, 'Duplicate entry') !== false )	{

            // Save an error to put a warning in the page we are being redirected: addprinter.php
            $_SESSION['error']['message'] = "La Refencia introducida ya existe en la base de datos.";

        } else {

            // Save an error to put a warning in the page we are being redirected: addprinter.php
            $_SESSION['error']['message'] = "Error al guardar el elemento. Inténtelo de nuevo pasados unos minutos.";
        }

        // We store the posted info to go back to the form page.
        $_SESSION["post"] = $_POST;
        header("Location:".$rootpath."admin/sections/printers/addUpdatePrinter.php");
        exit;

        // The printer was correctly added/updated to database.
    } else if (is_int($result) || $result >= 0)	{

        // Add selected applications to printers.
        $apps_array = array_keys($_POST['apps']);
        // If we are UPDATING a printer we reset the applications and add the new ones.
        isset($_POST["printer_id"]) && $_POST["printer_id"] != "" ? $printerDAO->resetApplicationsForPrinter($_POST["printer_id"]) : null;
        foreach ($apps_array as $value) {
            $printerDAO->addApplicationToPrinter(intval($value),intval($result));
        }



        $_SESSION['error']['level'] = 'info';
        isset($_POST["printer_id"]) && $_POST["printer_id"] != ""
            ? $_SESSION['error']['message'] = 'La impresora ha sido actualizada correctamente...:)'
            : $_SESSION['error']['message'] = 'La impresora ha sido guardada correctamente...:)';
        $_SESSION['printer_id'] = $result;
        header("Location:".$rootpath."admin/sections/printers/addUpdatePrinter.php");
        exit;
    }

    // If we pressed DELETE BUTTON:
}
/** ACTION PRINTER_FORM_ACTION_DELETE_PRINTER */
else if ($method == "POST" && isset($_POST['action']) && strcmp( $_POST['action'], PRINTER_FORM_ACTION_DELETE_PRINTER) == 0)	{

    $printer_id = $_POST['printer_id'];
    $printer_is_new = $_POST[PrinterDAO::FIELD_IS_NEW];

    // Creating database connections.
    $printerDAO = new PrinterDAO();
    $fileDAO = new FileDAO();
    $printerTransDAO = new PrinterTransDAO();


    try {

        // Deleting files form database.
        $filesNumber = $fileDAO->deleteFilesByPrinterId($printer_id);
        // Deleting files from file system.
        if ($filesNumber >= 1)  { delete_files($rootpath."admin/img/printers/".$printer_id); };
        // Deleting related apps.
        $printerDAO->resetApplicationsForPrinter($printer_id);
        // Delete Translation if exist:
        if ($printerDAO->hasTranslation($printer_id)) $printerTransDAO->deletePrinterTransByPrinterId($printer_id);

        // Here we delete the printer row:
        $result = intval($printerDAO->deletePrinterById($printer_id));

    } catch (Exception $e) {

        $result = $e->getMessage();

    }

    // If there was any error and the printer was not saved: We redirect to the form and
    // inform about the error on a warning box.
    if (!is_int($result) || $result < 0)	{

        $_SESSION['error']['level'] = 'error';
        $_SESSION['error']['message'] = 'La impresora no pudo ser borrada de la base de datos';

    } else 	{

        $_SESSION['error']['level'] = 'info';
        $_SESSION['error']['message'] = 'La impresora se eliminó de la base de datos y los archivos correspondientes han sido borrados del sistema.';

    }

    // $_GET data to go back to printers table.
    $params = $printer_is_new ? "new_printers" : "old_printers" ;
    header("Location:".$rootpath."admin/sections/printers/index.php?action=".$params);
    exit;
}
/** ACTION PRINTER_FORM_ACTION_SET_IS_PUBLISHED */
else if ($method == "POST" && strcmp( $_POST['action'], PRINTER_FORM_ACTION_SET_IS_PUBLISHED) == 0)  {
    header("Content-Type: text/plain");

    $result = $uploader->setIsPublished($_POST['is_published'],$_POST['printer_id']);
    echo json_encode($result);
}
/** ACTION SET_NEW_ORDER */
else if ($method == "POST" && strcmp( $_POST['action'], PRINTER_ACTION_SET_NEW_ORDER) == 0)  {
    header("Content-Type: text/plain");
    $result = $uploader->setNewOrder($_POST['new_order']);
    echo json_encode($result);
}
/** ********************************************** */
/** ********************************************** */


/** ********************************************** */
/** ********** FILES RELATED ACTIONS ************* */
/** ********************************************** */
/** ACTION SAVE_FILE */
// Upload a new file to the server.
else if ($method == "POST" && strcmp( $_POST['action'], 'save_file') == 0) {
    header("Content-Type: text/plain");

    // Handles upload requests:
    // Call handleUpload() with the path of the folder to save the file in.
    $result = $uploader->handleUpload($_POST['dir']);
    // To return a name used for uploaded file you can use the following line.
    $result["uploadName"] = $uploader->getUploadName();
    
    echo json_encode($result);
}
/** ACTION SAVE_VIDEO */
// Saves the youtube video on database for the corresponding printer.
else if ($method == "POST" && strcmp( $_POST['action'], 'save_video') == 0)  {

    header("Content-Type: text/plain");

    $result = $uploader->handleSaveVideo();
    echo json_encode($result);


}
/** ACTION MAKE_MAIN */
// Make one of the already uploaded pics the main one:
else if ($method == "POST" && strcmp( $_POST['action'], 'make_main') == 0)  {

    header("Content-Type: text/plain");

    $result = $uploader->handleMakeMain();
    echo json_encode($result);


}
/** ACTION RETRIEVE_INITIAL_PICTURES */
// Retrieve existing pictures of the selected watch.
else if ($method == "GET" && strcmp( $_GET['action'], 'retrieve_initial_pictures') == 0)  {

    header("Content-Type: text/plain");

    $result = $uploader->retrieveInitialPictures($_GET['printer_id']);
    echo json_encode($result);


}
/** ACTION RETRIEVE_INITIAL_DOCUMENTS */
// Retrieve existing pictures of the selected watch.
else if ($method == "GET" && strcmp( $_GET['action'], 'retrieve_initial_documents') == 0)  {

    header("Content-Type: text/plain");

    $result = $uploader->retrieveInitialDocuments($_GET['printer_id']);
    echo json_encode($result);

}
/** ACTION DELETE */
// Delete one of the already uploaded files:
else if ($method == "DELETE") {

    // We pass the thumbnails dir to the handler if we want it to be deleted:
    $result = $uploader->handleDelete();
    echo json_encode($result);
}
/** ACTION DELETE_VIDEO */
// Deletes the youtube all video on database for the corresponding printer.
else if ($method == "POST" && strcmp( $_POST['action'], 'delete_video') == 0)  {

    header("Content-Type: text/plain");

    $result = $uploader->handleDeleteVideo();
    echo json_encode($result);


}
/** ********************************************** */
/** ********************************************** */


/** ********************************************** */
/** ********** TRANSLATION RELATED ACTION********* */
/** ********************************************** */
/** PRINTER_TRANSLATION_FORM_ACTION_SAVE */
else if ($method == "POST" && strcmp( $_POST['action'], PRINTER_TRANSLATION_FORM_ACTION_SAVE) == 0)  {

    $printerTransDAO = new PrinterTransDAO();
    $printerTransDTO = new PrinterTransDTO(json_encode($_POST));

    $printerTransDTO->setId(isset($_POST["printer_translation_id"]) && $_POST["printer_translation_id"] != 0 ? $_POST["printer_translation_id"] : null);

//    $printerTransDTO->setPrinterId(array_key_exists(PrinterTransDAO::FIELD_PRINTER_ID, $_POST) && is_int(intval($_POST[PrinterTransDAO::FIELD_PRINTER_ID])) && $_POST[PrinterTransDAO::FIELD_PRINTER_ID] >= 0
//        ? intval($_POST[PrinterTransDAO::FIELD_PRINTER_ID])
//        : "");
//    $printerTransDTO->setLangCode(array_key_exists(PrinterTransDAO::FIELD_LANG_CODE, $_POST) && isset($_POST[PrinterTransDAO::FIELD_LANG_CODE]) && !empty($_POST[PrinterTransDAO::FIELD_LANG_CODE])
//        ? strval($_POST[PrinterTransDAO::FIELD_LANG_CODE])
//        : "");

    try {
        // Here we save the PrinterTransDTO using a PrinterTransDAO object to connect to database.
        $printerTransID = intval($printerTransDAO->savePrinterTrans($printerTransDTO));
    } catch (Exception $e) {
        $printerTransID = $e->getMessage();
    }

    header("Content-Type: text/plain");
    if (is_int($printerTransID) || $printerTransID >= 0)	{

        $result =  json_encode(array("success" => true,
            "message" => "La traducción ha sido correctamente guardada en base de datos.",
            "printer_translation_id" => $printerTransID));
        echo $result;
        exit;
    }
    echo json_encode(array("success" => false,
        "message" => "La traducción no pudo ser guardada. Inténtelo en unos minutos y contacte con su administrador si el problema persiste."));
    exit;
}
/** PRINTER_TRANSLATION_FORM_ACTION_DELETE */
else if ($method == "POST" && strcmp( $_POST['action'], PRINTER_TRANSLATION_FORM_ACTION_DELETE) == 0)  {

    $printerTransDAO = new PrinterTransDAO();
    $result = $printerTransDAO->deletePrinterTransByPrinterId($_POST[PrinterTransDAO::FIELD_PRINTER_ID]);

    header("Content-Type: text/plain");
    if (is_int($result) || $result > 0)	{
        $result =  json_encode(array("success" => true,
            "message" => "La traducción ha sido correctamente borrada de la base de datos.",
            "printer_translation_id" => false));
        echo $result;
        exit;
    }
    echo json_encode(array("success" => false,
        "message" => "La traducción no pudo ser borrada. Inténtelo en unos minutos y contacte con su administrador si el problema persiste."));
    exit;
}

/** ********************************************** */
/** ********************************************** */


/** ********************************************** */
/** ********** NO METHOD ERROR ******************* */
/** ********************************************** */
else {
    header("HTTP/1.0 405 Method Not Allowed");
}
?>