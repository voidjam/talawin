<?php


$rootpath = "./../../../";
$current_uri = "$_SERVER[REQUEST_URI]";

// Get connection to database.
include $rootpath . 'admin/scripts/connect.php';
// To get link paths.
include $rootpath . 'admin/inc/constants.php';

$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";


if (!empty($_SESSION["LOGGED"]) && $_SESSION["LOGGED"] == true)	{

    // GET var to know if we are accessing throw OLD PRINTERS link or NEW PRINTERS link. NEW PRINTERS if no GET parameter.
    $filter_new = isset($_GET["action"]) ? ($_GET["action"] === "old_printers" ? false : true) : true;

    // Include Header.
    include $rootpath . 'admin/inc/header.php';
    include $rootpath . 'admin/inc/nav-bar.php';


    // Retrieve all printers from database.
    $printerDAO = new PrinterDAO();
    $fileDAO = new FileDAO();
    $arrayPrinterDTO = $printerDAO->getPrinters();

    // Get the brands labels.
    $arrayBrands = $printerDAO->getPrinterBrands();
    $arraySubBrands = $printerDAO->getPrinterSubBrands();

    // Set page title.
    $title = $filter_new ? "IMPRERORAS NUEVAS" : "IMPRESORAS OCASIÓN";

    ?>

    <!-- DIV containing the watches table -->
    <div class="printers-new-content">
        <!-- DIV header explaining what is ther inside -->
        <div class="table-main-header"><?php echo $title ?></div>



        <!-- ADD PRINTER Button -->
        <button onclick="location.href='<?php echo $rootpath?>admin/sections/printers/addUpdatePrinter.php?action=<?php echo $_GET["action"]?>'" class="btn btn-success right-top-button" role="button"><strong>Añadir Impresora</strong></button>

        <!-- Horizontal separator -->
        <hr />


        <!-- The Table -->
        <div class="table-printers-container ml-md-4 ">
            <table class="table table-striped table-hover table-responsive" id="table-printers" width="100%">

                <!-- Table Head -->
                <thead class="table table-striped table-bordered thead-inverse">
                <tr>
                    <th>#</th>
                    <th>REF</th>
                    <th>MARCA</th>
                    <th>SUBMARCA</th>
                    <th>MODELO</th>
                    <th>PUB</th>
                    <th>FOTOS</th>
                    <th>IDIOMAS</th>
                    <th>VENDIDA</th>
                    <th>PRINTER_ID</th>

                </tr>
                </thead>

                <!-- Table Body -->
                <tbody>
                <?php






                // If there are any printers on database we start creating rows:
                if (is_array($arrayPrinterDTO) && sizeof($arrayPrinterDTO) > 0)	{

                    // Filter array with all printers to take only the new ones or the old ones.
                    $filter_new ? $arrayPrinterDTO = array_values (array_filter($arrayPrinterDTO, function ($item)   { return $item->getIsNew(); }))
                        : $arrayPrinterDTO = array_values (array_filter($arrayPrinterDTO, function ($item)   { return !$item->getIsNew();})) ;
                    // Retrieve the main image for every printer object.
                    foreach ($arrayPrinterDTO as $index => &$printerDTO) {
                        $printerDTO->setFilesArray($fileDAO->getMainImageByPrinterId($printerDTO->getId()));
                        unset($printerDTO);
                    }

                    // We create a row for each WatchDTO contained on the array.
                    foreach ($arrayPrinterDTO as $key => $printerDTO) {



                        // Create a new Row.
                        echo "<tr  title='Haga doble click para editar esta impresora.'>";
                        // Column Number
                        echo "<td scope='row' title='Arrastre para cambiar el orden.'>".($key + 1)."</td>";
                        // Column Reference
                        echo "<td>".$printerDTO->getRef()."</td>";
                        // Column Brand
                        echo "<td>".$arrayBrands[$printerDTO->getBrandId()]."</td>";
                        // Column Sub Brand
                        echo "<td>".$arraySubBrands[$printerDTO->getSubBrandId()]."</td>";
                        // Column Model
                        echo "<td>".$printerDTO->getModel()."</td>";
                        // Column IS_PUBLISHED
                        echo "<td>".$printerDTO->getIsPublished()."</td>";
                        // Column FOTOS
                        $fileDTO = $printerDTO->getFilesArray();
                        if ($fileDTO instanceof FileDTO)    {
                            $main_pic = "<img src='".$rootpath.$printerDTO->getFilesArray()->getPath().$printerDTO->getFilesArray()->getName()."' class='img-thumbnail'>";
                        } else  {
                            $main_pic =  "<span><strong>No hay fotos</strong></span>";
                        }
                        echo "<td>".$main_pic."</td>";
                        // Column translations
                        echo "<td>".!empty($printerDTO->getPrinterTransDTO())."</td>";
                        // Column is_sold
                        echo "<td>".$printerDTO->isSold()."</td>";
                        // Hidden column to store the corresponding printer ID.
                        echo "<td>".$printerDTO->getId()."</td>";
                        // End of Row
                        echo "</tr>";
                        unset($printerDTO);
                    } // End foreach
                } ?>
                </tbody>
            </table>
        </div>
    </div><!-- /printers-new-content -->

    <!-- This form will be used on click on any row to add a dinamic form -->
    <!-- and send the corresponding printer id to addUpdatePrinter.php    -->
    <div id="update-printer-form"></div>


    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- SECTION PAGE INFO +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="page_section_info unselectable">
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION PAGE INFO TXT +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <span><?php echo $title ?></span>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- /SECTION PAGE INFO TXT ++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    </div>
    <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <!-- Load DataTables JS -->
    <script src="<?php echo $rootpath; ?>admin/lib/datatables/datatables.js"></script>


    <script>


        /**
         * This method appends a warning div to #page_container containing a message for the user.
         *
         * @param message Message to be presented on screen.
         * @param error_level ['info', 'error', 'warning'] Sets the color of the warning div.
         */
        function showWarningMessage(message, error_level) {

            // Check if there is already an allert message on screen and close it.
            if($('.alert').length) { $(".alert").alert("close"); }

            $('#page_container').append('<div id=\"general-warning-div\" class=\"alert fade show\" role=\"alert\">'
                + '<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>'
                + '<p><strong>' + message + '</strong></p></div>');

            $(".alert").fadeTo(5000, 1, "swing").fadeOut(800, function () {
                $(".alert").alert("close");
            });

            if (error_level.localeCompare('info') === 0) {
                $(".alert").addClass("alert-success");
            } else if (error_level.localeCompare('error') === 0) {
                $(".alert").addClass("alert-danger");
            } else if (error_level.localeCompare('warning') === 0) {
                $(".alert").addClass("alert-warning");
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // DOCUENT READY BLOCK /////////////////////////////////////////////////////////////////////////////////////////
        $(function () {

            // Select only new or old printers.
            let filter_new = '<?php echo $filter_new ?>';


            /**
             * This method is used to render is_published column.
             *
             * It returns an <img> element with yes.svg or no.svg depending on the data,
             * together with a <span> hidden element used to order the column.
             *
             * @param data  The data for the cell (based on columns.data)
             * @param type  The type call data requested.
             * @param row   The full data source for the row (not based on columns.data)
             * @param meta  An object that contains additional information about the cell being requested.
             * @returns {string}
             */
            let simple_checkbox = function ( data, type, row, meta ) {
                let is_checked = data == true ? "yes" : "no";
                return '<img class="is-published-icon" src="<?php echo $rootpath ?>/admin/img/'+ is_checked +'.svg"/>'
                    + '<span style="display:none">' + is_checked + '</span>';
            }


            /**
             * This method is used to render LANGUAGES column. It returns an img element containing the flags for
             * the corresponding languages that the printer has.
             *
             * @param data
             * @param type
             * @param row
             * @param meta
             * @returns {string}
             */
            let languages = function ( data, type, row, meta ) {
                let flags_div = '<img class="language-icon" src="<?php echo $rootpath ?>/admin/img/es-icon.svg"/>';
                if (data == true)   {
                    flags_div += '<img class="language-icon" src="<?php echo $rootpath ?>/admin/img/en-icon.png"/>';
                }
                return flags_div;
            }




            ////////////////////////////////////////////////////////////////////////////////////////////////////
            // INIT DATATABLES FOR #table-printers TABLE. //////////////////////////////////////////////////////
            let table = $('#table-printers').DataTable( {

                rowReorder: {
                    selector: 'td.reorder',
                    snapX: 30,
                    scrollY: 300,
                    paging: true
                },
                dom:'<"top row"<"#lenght.col align-self-end"l><"#toolbar"><"#filter.col "f>><"row"<"col"rt>><"bottom row"<"#info.col"i><"#pagination.col"p>><"clear">',
                rowId: "8",
                nowrap: true,
                responsive: true,
                autoWidth: true,
                language: {
                    "lengthMenu": "_MENU_ impresoras por página",
                    "zeroRecords": "No hay resultados que correspondan con el texto introducido.",
                    "info": "Del _START_º al _END_º de _TOTAL_ items",
                    "infoEmpty": "No hay información. La Base de Datos está Vacía.",
                    "infoFiltered": "(Filtrado de _MAX_ impresoras)",
                    "search": "_INPUT_",
                    searchPlaceholder: "Buscar...",
                    paginate: {
                        "first":      "Primero",
                        "last":       "Ultimo",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                },
                lengthMenu: [ 1, 5, 10, 15, 20, 25, 50 ],
                pageLength: 5,
                columns: [
                    null,                                                               // Column 0 #
                    null,                                                               // Column 1 REF
                    null,                                                               // Column 2 MARCA
                    null,                                                               // Column 3 SUB MARCA
                    null,                                                               // Column 4 MODELO
                    {"render": simple_checkbox, "className": "is-published-column"},    // Column 5 PUB
                    { "className": "main-pic-column" },                                 // Column 6 IMAGES
                    {"render": languages, "className": "languages-column"},             // Column 7 LANGUAGES
                    { "className": "is-sold-column", "render": simple_checkbox },       // Column 8 VENDIDA
                    null                                                                // Column 9 PRINTER_ID
                ],
                columnDefs: [
                    { "targets": [0], "orderable": true, "className": 'reorder' }, // Column 0: #
                    { "targets": [9], "visible": false, "searchable": false, "ordering": false }, // Column 8: PRINTER_ID
                    { "targets": [6], "searchable": false, "type": "html", "ordering": false }, // Column 5: IMAGES
                    { "targets": [5], "searchable": false, "type": "html" }, // Column 4: IS PUBLISHED
                    { "targets": [7], "searchable": false, "type": "html" }, // Column 6: LANGUAGES
                    { "width": "1%", "targets": [0,2,3,8,6,7,8 ] }
                ],
                initComplete: function( settings, json ) {

                    // Implement select filter to show printers filtering by brand.
                    $("div#toolbar").html('<select id="brandSelect" class="form-control">'
                        + '<option value="" selected>TODAS</option>'
                        + '<?php foreach($arrayBrands as $key => $brand) {
                            echo '<option value="'.$brand.'">'.$brand.'</option>';
                        }?>'
                        + '</select>');
                    $("#toolbar > #brandSelect").on("change", function() {
                        table.columns(2).search($(this).val(), true, false).draw();
                    });
                }

            });


            // Adjust columns size.
            table.columns.adjust();

            // Listen for responsive resize
            table.on( 'responsive-resize', function ( e, datatable, columns ) {
                let count = columns.reduce( function (a,b) {
                    return b === false ? a+1 : a;
                }, 0 );
                //console.log( count +' column(s) are hidden' );
            } );

            // Hide column IS_SOLD in case we are showing only new printers.
            if (filter_new) {
                // Get the column API object
                let column = table.column(7);
                // Toggle the visibility
                column.visible( false );
            }


            // Re-Write the index column: Empty column to store the row number.
            table.on('init.dt order.dt search.dt column-reorder', function () {
                table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, index) {
                    cell.innerHTML = index + 1;
                });
            }).draw();




            // END OF INIT DATATABLES FOR #table-printers TABLE. ///////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////


            ////////////////////////////////////////////////////////////////////////////////////////////////////
            // ROW REORDER EVENT. //////////////////////////////////////////////////////////////////////////////
            // Listen to Drag&Drop reorder event.
            table.on( 'row-reorder', function ( e, diff, edit ) {

                // Reorder started on row: edit.triggerRow.data()[0];
                // We create an array containing the new positions as pairs [printer_id, new_display_oder]
                // for every printer that has changed it position.
                let new_order = [];

                // Loop throw all rows that has been affected by the drag&drop action, that is, all printers
                // that have changed its display_order.
                for ( let i=0, ien=diff.length ; i<ien ; i++ ) {

                    // We get the data of the corresponding row.
                    let rowData = table.row( diff[i].node ).data();

                    // Push a new element [printer_id, new_display_oder].
                    new_order.push( [rowData[9], diff[i].newData] );
                }

                // Post to the server to handle the changes
                $.ajax({
                    type: "POST",
                    url: "<?php echo $rootpath?>admin/sections/printers/controllers/uploadServer.php",
                    data: {
                        new_order: new_order,
                        action: '<?php echo PRINTER_ACTION_SET_NEW_ORDER ?>'
                    },
                    beforeSend: function() {
                        // Disable dragging
                        table.rowReorder.disable();
                    },
                    success: function(html) {
                        // Re-enable dragging
                        table.rowReorder.enable();
                        showWarningMessage("El nuevo orden ha sido correctamente guardado en base de datos.", "info");
                    }
                });
            } );
            // END OF ROW REORDER EVENT. ///////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////


            ////////////////////////////////////////////////////////////////////////////////////////////////////
            // EDIT PRINTER: DOUBLE CLICK ON ROW ///////////////////////////////////////////////////////////////
            // ON CLICK  on any row we will create dynamically a form
            // that will be submitted to addUpdatePrinter.php and that will take
            // us to the edit page of the corresponding watch.
            $('#table-printers').find('tbody').on('dblclick', 'tr', function () {

                // First we take the printer id hidden on the 7th column of the table.
                let data = table.row(this).data();
                let printer_id = data[9];

                let $div = $("#update-printer-form");
                let $form = $("<form method='POST'></form>");
                $form.attr("action", '<?php echo $rootpath; ?>admin/sections/printers/addUpdatePrinter.php' );
                $form.append('<input id="printer_id" name="printer_id" type=text value="' + printer_id + '">');
                $div.append($form);
                $form.submit();
            });
            // END OF EDIT PRINTER: DOUBLE CLICK ON ROW ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////


        }); // End of document ready block.


    </script>

    <?php

    // Check for errors and warnings:
    if(isset($_SESSION['error']) && is_array($_SESSION['error']))   include $rootpath . 'admin/inc/warning.php';

    // Include Footer.
    include $rootpath . 'admin/inc/footer.php';

} else 	{

    header("Location:".SECTION_LOGIN);
    exit;

}