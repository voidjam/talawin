<?php

$rootpath = "./../../../";
$current_uri = "$_SERVER[REQUEST_URI]";

// Get connection to database.
include $rootpath . 'admin/scripts/connect.php';
include $rootpath . 'admin/inc/constants.php';
include $rootpath . 'admin/scripts/functions.php';


$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

// If user is logged.
if (!empty($_SESSION["LOGGED"]) && $_SESSION["LOGGED"] == true)	{

    // Include Header.
    include $rootpath . 'admin/inc/header.php';
    include $rootpath . 'admin/inc/nav-bar.php';

    // Get all client_logos
    $clientLogoDAO = new ClientLogoDAO();
    $arrayClientLogoDTO = $clientLogoDAO->getClientLogos();

    // Set page title:
    $title = "clientes";

    ?>


    <!-- DIV containing the clients configuration options -->
    <div class="client-logo-content">

        <!-- ADD CLIENT Button -->
        <button class="btn btn-success add-client-logo-button" role="button" data-toggle="modal" data-target="#newClientLogoModal"
                title="Añadir un nuevo cliente">
            <span><strong>Añadir Cliente</strong></span></button>

        <!-- ---------------------- -->
        <!-- FIRST ROW: MAIN BLOCK  -->
        <div class="row">

            <!-- --------------------------------- -->
            <!-- /FIRST COLUMN: CLIENTS LOGO CARDS -->
            <div class="col-sm" style="position: relative;">

                <!-- DIV header explaining what is ther inside -->
                <div class="client-table-main-header">CLIENTES ACTUALES</div>

                <!-- Horizontal separator -->
                <br><hr/>

                <!-- ---------------------- -->
                <!-- GRID OF CLIENTS ------ -->
                <section class="grid-logos" data-alignment="left-top-fillgaps">
                    <div class="controls cf">
                        <!-- SEARCH FIELD -->
                        <div class="control search">
                            <div class="control-icon"></div>
                            <input class="control-field search-field form-control" type="text" name="search" placeholder="Search..." />
                        </div>

                    </div>
                    <div class="grid"></div>
                </section>
                <!-- /GRID OF CLIENTS ----- -->
                <!-- ---------------------- -->
            </div>
            <!-- /FIRST COLUMN: CLIENTS LOGO CARDS -->
            <!-- --------------------------------- -->

        </div>
        <!-- /FIRST ROW: MAIN BLOCK -->
        <!-- ---------------------- -->

        <!-- ---------------------- -->
        <!-- NEW CLIENT LOGO MODAL  -->
        <div class="modal fade" id="newClientLogoModal" tabindex="-1" role="dialog" aria-labelledby="newClientLogoModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="newClientLogoModalLabel">Añadir Nuevo Cliente</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <!-- NEW CLIENT LOGO FORM -->
                        <form id="new_client_logo_form" class="form-horizontal" action="<?php echo $rootpath?>admin/sections/clients/uploadClientLogoServer.php" method="post" onSubmit="return checkForm(this)" enctype="multipart/form-data">

                            <!--NAME -->
                            <div class="form-group">
                                <label for="client_name" class="col-sm-6 control-label">Cliente</label>
                                <div class="col-sm-12">
                                    <input required type="text" class="input-lg form-control" id="client_name" name="client_name" placeholder="Nombre de Empresa">
                                </div>
                            </div>
                            <!--IMAGE -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="avatar-upload">
                                        <div class="avatar-edit" title="Seleccionar el logo del cliente.">
                                            <input type='file' id="uploadedImage" name="uploadedImage" accept=".png, .jpg, .jpeg" required/>
                                            <label for="uploadedImage"></label>
                                        </div>
                                        <div class="avatar-preview" title="Seleccionar el logo del cliente.">
                                            <div id="imagePreview">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ACTION FIELD SAVE NEW CLIENT LOGO -->
                            <input hidden name="action" id="action" value="<?php echo (CLIENT_LOGO_FORM_ACTION_SAVE)?>"/>
                            <!-- SUBMIT HIDDEN BUTTON -->
                            <button type="submit" id="submitHiddenButton" style="display: none;"></button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button onclick="submitNewClientForm('<?php echo CLIENT_LOGO_FORM_ACTION_SAVE ?>')" class="btn btn-primary">Añadir el nuevo Cliente</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /NEW CLIENT LOGO MODAL -->
        <!-- ---------------------- -->

        <!-- ---------------------- -->
        <!-- CONFIRM DELETE MODAL - -->
        <div class="modal fade" id="confirmDeleteModal" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="confirmDeleteModalLabel">ELIMINANDO CLIENTE:</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ¿Está seguro de que quiere eliminar el <b>cliente</b>?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
                        <button id="confirmDeleteYesBtn" type="button" class="btn btn-primary">SÍ</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /CONFIRM DELETE MODAL  -->
        <!-- ---------------------- -->


    </div><!-- END OF client-logo-content -->


    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- SECTION PAGE INFO +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="page_section_info unselectable">
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION PAGE INFO TXT +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <span><?php echo $title ?></span>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- /SECTION PAGE INFO TXT ++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    </div>
    <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    <!-- Drag & Drop jQuery pluggin: Muuri.js and its dependencies. -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/web-animations/2.3.2/web-animations.min.js" integrity="sha512-oAY57i8MXmaOP7pAylNLnULAM4QLV3uGnvnXVY4zF229/zFzTvG2/5YIgH8iN8oZR2hnbkiDPd4JCJGaH4oG6g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.js" integrity="sha256-GMd3rFxMDNnM5JQEpiKLLl8kSrDuG5egqchk758z59g=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/muuri/0.9.3/muuri.min.js" integrity="sha512-qKTUVe1eoat7bk2HIJpPFdHaQiJU1p240JFDu40JvzK/3WUa0Y1dLKqETGtvpXQvYiXwphFNafQsTtCZ5rgWYg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <!-- Custom JS for our clients grid -->
    <script src="<?php echo $rootpath; ?>admin/sections/clients/js/logo-grid.js"></script>

    <script>

        /**
         * This method checks that
         *
         * @param form
         * @returns {boolean}
         */
        function checkForm(form) {

            return true;
        }

        /**
         * This method triggers from submit throw JS. Instead of using $form.submit() it
         * triggers a click on submit button so the validations are run before submitting.
         */
        function submitNewClientForm(action)   {

            $("#submitHiddenButton").trigger("click");

            // let $form = $('form#new_client_logo_form');
            // $("#action").attr("value", action);
            // $form.submit();
        }

        /**
         * This method appends a warning div to #page_container containing a message for the user.
         *
         * @param message Message to be presented on screen.
         * @param error_level ['info', 'error', 'warning'] Sets the color of the warning div.
         */
        function showWarningMessage(message, error_level) {

            // Check if there is already an allert message on screen and close it.
            if($('.alert').length) { $(".alert").alert("close"); }

            $('#page_container').append('<div id=\"general-warning-div\" class=\"alert fade show\" role=\"alert\">'
                + '<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>'
                + '<p><strong>' + message + '</strong></p></div>');

            $(".alert").fadeTo(5000, 1, "swing").fadeOut(800, function () {
                $(".alert").alert("close");
            });

            if (error_level.localeCompare('info') === 0) {
                $(".alert").addClass("alert-success");
            } else if (error_level.localeCompare('error') === 0) {
                $(".alert").addClass("alert-danger");
            } else if (error_level.localeCompare('warning') === 0) {
                $(".alert").addClass("alert-warning");
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // DOCUENT READY BLOCK /////////////////////////////////////////////////////////////////////////////////////////
        $(document).ready(function() {

            // Showing logo thumbnail when creating a new client on the #newClientLogoModal dialog.
            function readURL(input) {
                if (input.files && input.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function(e) {
                        $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                        $('#imagePreview').hide();
                        $('#imagePreview').fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#uploadedImage").change(function() {
                readURL(this);
            });
            $("#imagePreview").on("click", function () {
                $("#uploadedImage").click();
            });

            // Start Grid of Logos using muuri.js
            loadLogosGrid((<?php echo json_encode($arrayClientLogoDTO) ?>), "<?php echo $rootpath; ?>admin/sections/clients/uploadClientLogoServer.php", "<?php echo $rootpath; ?>");

            // Prepares the form to prevent onSubmit default and to be sent via ajax.
            $('form#new_client_logo_form')
                .on('submit', function(e) {
                    // Prevent form submission
                    e.preventDefault();

                    // Close the modal dialog:
                    $('#newClientLogoModal').modal('hide')

                    let $form    = $(e.target),
                        formData = new FormData(),
                        params   = $form.serializeArray(),
                        files    = $form.find('[name="uploadedImage"]')[0].files;

                    $.each(files, function(i, file) {
                        // Prefix the name of uploaded files with "uploadedFiles-"
                        // Of course, you can change it to any string
                        formData.append('uploadedImage', file);
                    });

                    $.each(params, function(i, val) {
                        formData.append(val.name, val.value);
                    });

                    $.ajax({
                        url: $form.attr('action'),
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function(data, status) {
                            // Parse json string response into object:
                            try {
                                // Show info message to user on UI.
                                obj = JSON && JSON.parse(data) || $.parseJSON(data);
                                if (obj.success === true)   {
                                    showWarningMessage(obj.message, "info");
                                    // Add element from grid and update indices, both on layout and database.
                                    addItem(obj.newClientLogoDTO);
                                } else {
                                    showWarningMessage(obj.message, "error");
                                }
                                $form.trigger("reset");

                            } catch  (error)   {
                                showWarningMessage("Error: " + error, "error");
                                console.log(error);
                            }
                        }
                    });
                })
                .on("reset", function () {
                });



            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // NEW CLIENT LOGO MODAL EVENTS ///////////////////////////////////////////////////////////////////////
            // Reset #newClientLogoModal form on modal hidden event. //////////////////////////////////////////////
            $('#newClientLogoModal').on('hidden.bs.modal', function () {
                $('#imagePreview')[0].style.backgroundImage = '';
            });
            // Focus text input after showing modal.
            $('#newClientLogoModal').on('shown.bs.modal', function () {
                $(this).find("input#client_name").focus();
            });
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////

        }); // End of document ready block.

    </script>

    <?php

    // Check for errors and warnings:
    if(isset($_SESSION['error']) && is_array($_SESSION['error']))   include $rootpath . 'admin/inc/warning.php';
    // Include Footer.
    include $rootpath . 'admin/inc/footer.php';

// If user is not logged -> Back to loggin page.
} else 	{
    header("Location:".$rootpath."admin/login.php");
    exit;
}