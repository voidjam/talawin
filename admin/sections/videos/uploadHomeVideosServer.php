<?php


/**
 * THIS ENDPOINT RECEIVES REQUEST FROM sections/videos/index.php AND HANDLES THEM.
 *
 * ACTIONS:
 *
 * + VIDEOS_FORM_ACTION_SAVE     --> Adds a new home video to database.
 * + VIDEOS_ACTION_DELETE
 * + VIDEOS_ACTION_SET_IS_SHOWN
 * + VIDEOS_ACTION_UPDATE_DISPLAY_ORDER
 *
 */


$rootpath = "./../../../";

// Get connection to database.
include $rootpath . 'admin/scripts/connect.php';
// Constants file:
include $rootpath . 'admin/inc/constants.php';
// PHP Functions file:
include $rootpath . 'admin/scripts/functions.php';


$method = $_SERVER["REQUEST_METHOD"];


/** ACTION HOME_VIDEOS_ACTION_SAVE */
// Upload a new client logo to the both file system and database..
if ($method == "POST" && strcmp( $_POST['action'], HOME_VIDEOS_ACTION_SAVE) == 0) {

    header("Content-Type: text/plain");

    // First we will save the new HommeVideo on database. We need the HomeVideoDTO id for the folder structure.
    $homeVideoDAO = new HomeVideoDAO();
    $homeVideoDTO = new HomeVideoDTO();

    $homeVideoDTO->setIsShown(true);
    $homeVideoDTO->setDisplayOrder(0); // Default display order: First
    $homeVideoDTO->setYoutubeId($_POST['youtube_id']);
    $homeVideoDTO->setYoutubeTitle($_POST['youtube_title']);

    // Save the HomeVideoDTO on DataBase and store its Id on the HomeVideoDTO instance.
    try {
        $homeVideoDTO->setId($homeVideoDAO->saveHomeVideo($homeVideoDTO));
    } catch(Exception $e)   {
        echo json_encode(array("success" => false, "message" => "Failed: Este vídeo ya está existe en la base de datos!. "));
        //echo 'Message: ' .$e->getMessage();
    }

    $homeVideoDTO->getId() > 0
        ? $result = json_encode(array("success" => true,
        "message" => "Success: El video fue correctamente guardado.",
        "newHomeVideo" => $homeVideoDTO))
        : $result = json_encode(array("success" => false, "message" => "Failed: Error al guardar el vídeo. "));

    echo $result;
    exit;
}
/** ACTION HOME_VIDEOS_ACTION_DELETE */
else if ($method == "POST" && strcmp( $_POST['action'], HOME_VIDEOS_ACTION_DELETE) == 0)  {

    header("Content-Type: text/plain");
    $homeVideoDAO = new HomeVideoDAO();
    if ($homeVideoDAO->deleteHomeVideoById($_POST['video_id']) > 0)    {
        $result = json_encode(array("success" => true,
            "message" => "Success: El video <strong>'".$_POST['video_title']."'</strong> ha sido borrado de la base de datos."));
    } else {
        $result = json_encode(array("success" => false, "message" => "Database could not be updated"));
    }
    echo $result;
    exit;
}
/** ACTION HOME_VIDEOS_ACTION_SET_IS_SHOWN */
else if ($method == "POST" && strcmp( $_POST['action'], HOME_VIDEOS_ACTION_SET_IS_SHOWN) == 0)  {

    header("Content-Type: text/plain");
    $homeVideoDAO = new HomeVideoDAO();
    if ($homeVideoDAO->setIsShown($_POST['data'][0], $_POST['data'][1]) > 0)    {
        $result = json_encode(array("success" => true,
            "message" => "Success: El vídeo '####' ".(($_POST['data'][1] == "true") ? "SÍ" : "NO") ." se mostrará en la sección HOME de la web."));
    } else {
        $result = json_encode(array("success" => false, "message" => "Database could not be updated"));
    }
    echo $result;
    exit;
}
/** ACTION HOME_VIDEOS_ACTION_UPDATE_DISPLAY_ORDER */
else if ($method == "POST" && strcmp( $_POST['action'], HOME_VIDEOS_ACTION_UPDATE_DISPLAY_ORDER) == 0)  {

    header("Content-Type: text/plain");
    $homeVideoDAO = new HomeVideoDAO();

    foreach ($_POST['data'] as $valor) {
        if(!($homeVideoDAO->updateDisplayOrder($valor[0], $valor[1]) > 0))    {
            $result = json_encode(array("success" => false, "message" => "El orden de los elementos no pudo ser actualizado. Por favor, inténtelo de nuevo."));
        }
    }

    $result =  json_encode(array("success" => true,
        "message" => "El nuevo orden de los videos se ha guardado correctamente en base de datos."));
    echo $result;
    exit;
}


else {

    header("HTTP/1.0 405 Method Not Allowed");
}
?>