<?php

$rootpath = "./../../../";
$current_uri = "$_SERVER[REQUEST_URI]";

// Get connection to database.
include $rootpath . 'admin/scripts/connect.php';
include $rootpath . 'admin/inc/constants.php';
include $rootpath . 'admin/scripts/functions.php';


// If user is logged.
if (!empty($_SESSION["LOGGED"]) && $_SESSION["LOGGED"] == true)	{

    // Include Header.
    include $rootpath . 'admin/inc/header.php';
    include $rootpath . 'admin/inc/nav-bar.php';

    // Get all client_logos
    $homeVideoDAO = new HomeVideoDAO();
    $arrayHomeVideoDTO = $homeVideoDAO->getHomeVideos();

    // Set page title:
    $title = "Home Videos";

    ?>


    <!-- DIV containing the clients configuration options -->
    <div class="home-videos-content">

        <!-- ADD CLIENT Button -->
        <button class="btn btn-success add-home-video-button" role="button" data-toggle="modal" data-target="#newVideoModal"
                title="Añadir un nuevo video">
            <span><strong>Añadir Video</strong></span></button>

        <!-- ---------------------- -->
        <!-- FIRST ROW: MAIN BLOCK  -->
        <div class="row">

            <!-- --------------------------------- -->
            <!-- /FIRST COLUMN: CLIENTS LOGO CARDS -->
            <div class="col-sm" style="position: relative;">

                <!-- DIV header explaining what is ther inside -->
                <div class="home-videos-table-main-header">VIDEOS DE LA SECCIÓN HOME</div>

                <!-- Horizontal separator -->
                <br><hr/>

                <!-- ---------------------- -->
                <!-- GRID OF VIDEOS ------- -->
                <section class="videos-grid" data-alignment="left-top-fillgaps">
                    <div class="controls cf">
                        <!-- SEARCH FIELD -->
                        <div class="control search">
                            <div class="control-icon"></div>
                            <input class="control-field search-field form-control" type="text" name="search" placeholder="Search..." />
                        </div>

                    </div>
                    <div class="grid"></div>
                </section>
                <!-- /GRID OF VIDEOS ------ -->
                <!-- ---------------------- -->
            </div>
            <!-- /FIRST COLUMN: CLIENTS LOGO CARDS -->
            <!-- --------------------------------- -->

        </div>
        <!-- /FIRST ROW: MAIN BLOCK -->
        <!-- ---------------------- -->

        <!-- ---------------------- -->
        <!-- NEW BANNERS MODAL ---- -->
        <div class="modal fade" id="newVideoModal" tabindex="-1" role="dialog" aria-labelledby="newVideoModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title unselectable" id="newVideoModalLabel">Añadir Nuevo Video</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Cerrar la ventana...">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">

                        <!-- Explanation Text -->
                        <div class="table-header">AÑADA EL ENLACE AL VÍDEO CORRESPONDIENTE</div>
                        <!-- VIDEO UPLOADER -->
                        <div id="video-uploader" class="video-uploader row">
                            <!-- To show video thumbnail once the url has been validated -->
                            <div class="youtube-thumb col-sm-3">
                                <div class="loader">
                                    <svg class="circular" viewBox="25 25 50 50">
                                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                                    </svg>
                                </div>
                                <img id="youtube-thumb-img" src=""/>
                            </div>
                            <!-- Text input to paste the youtube link -->
                            <div class="col-sm-8  align-self-end">
                                <label for="youtube-link" class="control-label">Pegue aquí el enlace a YouTube:</label>
                                <div class="">
                                    <input name="youtube-link" class="youtube-link form-control" id="youtube-link" placeholder="Enlace al vídeo">
                                </div>
                            </div>

                            <!--  VIDEO TITLE FIELD: Hidden input to save video title -->
                            <!-- BANNER ID FIELD -->
                            <input hidden name="youtube_tile" id="youtube_tile" value=""/>

                        </div><!-- /#video-uploader -->

                    </div>
                    <div class="modal-footer">
                        <!-- Upload Video Buttons -->
                        <div class="youtube-buttons">
                            <!-- Upload Button -->
                            <button id="video-upload-button" class="video-upload-button" disabled data-dismiss="modal"
                                    title="Debe introducir una URL válida de youtube para guardar el vídeo en base de datos.">
                                <span>Guardar Vídeo</span></button>
                        </div><!-- /.youtube-buttons -->

                    </div>
                </div>
            </div>
        </div>
        <!-- /NEW BANNER MODAL ---- -->
        <!-- ---------------------- -->

        <!-- ---------------------- -->
        <!-- CONFIRM DELETE MODAL - -->
        <div class="modal fade" id="confirmDeleteModal" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="confirmDeleteModalLabel">ELIMINANDO BANNER:</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ¿Está seguro de que quiere eliminar el <b>video</b>?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
                        <button id="confirmDeleteYesBtn" type="button" class="btn btn-primary">SÍ</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /CONFIRM DELETE MODAL  -->
        <!-- ---------------------- -->


    </div><!-- END OF home-videos-content -->


    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- SECTION PAGE INFO +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="page_section_info unselectable">
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION PAGE INFO TXT +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <span><?php echo $title ?></span>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- /SECTION PAGE INFO TXT ++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    </div>
    <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    <!-- Drag & Drop jQuery pluggin: Muuri.js and its dependencies. -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/web-animations/2.3.1/web-animations.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.js" integrity="sha256-GMd3rFxMDNnM5JQEpiKLLl8kSrDuG5egqchk758z59g=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/muuri/0.5.4/muuri.js" integrity="sha256-CWldck8RM3iilMsXH/oW9iLdm/TcW3WcpRw09wL6MFg=" crossorigin="anonymous"></script>

    <!-- Custom JS for our clients grid -->
    <script src="<?php echo $rootpath; ?>admin/sections/videos/js/home-videos-grid.js"></script>

    <script>


        /**
         * This method appends a warning div to #page_container containing a message for the user.
         *
         * @param message Message to be presented on screen.
         * @param error_level ['info', 'error', 'warning'] Sets the color of the warning div.
         */
        function showWarningMessage(message, error_level) {

            // Check if there is already an allert message on screen and close it.
            if($('.alert').length) { $(".alert").alert("close"); }

            $('#page_container').append('<div id=\"general-warning-div\" class=\"alert fade show\" role=\"alert\">'
                + '<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>'
                + '<p><strong>' + message + '</strong></p></div>');

            $(".alert").fadeTo(5000, 1, "swing").fadeOut(800, function () {
                $(".alert").alert("close");
            });

            if (error_level.localeCompare('info') === 0) {
                $(".alert").addClass("alert-success");
            } else if (error_level.localeCompare('error') === 0) {
                $(".alert").addClass("alert-danger");
            } else if (error_level.localeCompare('warning') === 0) {
                $(".alert").addClass("alert-warning");
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // DOCUMENT READY BLOCK ////////////////////////////////////////////////////////////////////////////////////////
        $(document).ready(function() {





            // Start Grid of Logos using muuri.js
            loadHomeVideosGrid((<?php echo json_encode($arrayHomeVideoDTO) ?>), "<?php echo $rootpath; ?>admin/sections/videos/uploadHomeVideosServer.php");



            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // NEW VIDEO MODAL EVENTS /////////////////////////////////////////////////////////////////////////////
            // Reset #newVideoModal form on modal hidden event. ///////////////////////////////////////////////////
            $('#newVideoModal').on('hidden.bs.modal', function () {

                $(this).find(".video-upload-button").prop("disabled", false);
                $(this).find(".youtube-link").prop("disabled", false);
                $(this).find(".youtube-link").val("");
                $(this).find(".youtube-link").removeClass("youtube-success youtube-error");
                $(this).find(".youtube-thumb img").attr("src","");
                $(this).find(".video-upload-button").prop("disabled", true);
                $(this).find(".video-upload-button").prop("title", "Debe introducir una URL válida de youtube para guardar el vídeo en base de datos.");
                videoId = null;
            });
            // Focus text input after showing modal.
            $('#newVideoModal').on('shown.bs.modal', function () {
                $(this).find("input#youtube-link").focus();
            });
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////



            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // VIDEO UPLOADER /////////////////////////////////////////////////////////////////////////////////////
            let videoId;

            /**
             * This method will send an ajax request to uploadHomeVideosServer.php to save
             * the youtube video on database.
             */
            function uploadVideoLink(_this)  {

                $.post('<?php echo $rootpath; ?>admin/sections/videos/uploadHomeVideosServer.php',
                    {
                        youtube_id: videoId,
                        youtube_title: $("#youtube_tile").val(),
                        action: '<?php echo HOME_VIDEOS_ACTION_SAVE?>'
                    },
                    function(data, status){
                        // Parse json string response into object:
                        obj = JSON && JSON.parse(data) || $.parseJSON(json);

                        if (obj.success)    {
                            showWarningMessage(obj.message, "info")
                            addItem(obj.newHomeVideo);
                        } else {
                            // Detect possible duplicate entry for youtube_id column.
                            if (obj.message.includes("Integrity constraint violation: 1062")) obj.message = "Video Duplicado: Este video ya existe en la base de datos. El video NO se ha guardado.";
                            showWarningMessage(obj.message, "error")
                        }
                    });
            }



            /**
             * todo: This method prepares the UI to indicate that a video already exists on database.
             */
            function uiVideoAlreadyExists($videoUploader)  {

                // Disable the upload button and enable the delete button for the video.
                $videoUploader.find(".video-upload-button").prop("disabled", true);

                // Disable the text input so user cant change the video URL.
                $videoUploader.find(".youtube-link").prop("disabled", true);
                $videoUploader.find(".youtube-link").val("https://www.youtube.com/watch?v="+videoId);

                // Set the thumbnail URL from youtube id:
                $videoUploader.find(".youtube-thumb img").attr("src", "https://i.ytimg.com/vi/" + videoId + "/hqdefault.jpg" );
            }


            /**
             * This method will send an ajax request to uploadServer.php to delete
             * the youtube video on database.
             */
            function deleteVideoLink(_this)  {

                // The clicked button.
                let $this = $(_this);
                // The current youtube-uploader (in case there are more than one).
                let $videoUploader = $this.parents(".video-uploader");

                $.post({
                    url: '<?php echo $rootpath; ?>admin/sections/videos/uploadHomeVideosServer.php',
                    data: { youtube_id: videoId, action: '<?php echo HOME_VIDEOS_ACTION_DELETE?>'},
                    success: function(data, status) {
                        // Parse json string response into object:
                        obj = JSON && JSON.parse(data) || $.parseJSON(json);

                        if (obj.success)    {

                            // uiVideoDeleted($videoUploader);
                        }
                    }});
            }

            /**
             * This method prepares the UI to indicate that there is no video saved on database for this printer.
             */
            function uiVideoDeleted($videoUploader)  {

                // Disable the delete button and enable the upload button for the video.
                $videoUploader.find(".video-upload-button").prop("disabled", false);

                // Delete the text input so user cant change the video URL.
                $videoUploader.find(".youtube-link").prop("disabled", false);
                $videoUploader.find(".youtube-link").val("");
                $videoUploader.find(".youtube-link").trigger("input");
                videoId = null;

            }


            /**
             * Creates virtual A element, then you get access to some helpful DOM methods to parse it's href
             * value like hostname, protocol, pathname, search, hash, etc. Then validate the url parts.
             * Returns:
             * + false if the URL is not youtube url
             * + empty string if it is youtube url but without video ID
             * + string id if the url is a valid youtube video
             *
             * @param getURL
             * @returns {*}
             */
            function isYoutubeRegEx(getURL){
                if(typeof getURL!=='string') return false;
                let newA = document.createElement('A');
                newA.href = getURL;
                let host = newA.hostname;
                let srch = newA.search;
                let path = newA.pathname;

                if(host.search(/youtube\.com|youtu\.be/)===-1) return false;
                if(host.search(/youtu\.be/)!==-1) return path.slice(1);
                if(path.search(/embed/)!==-1) return /embed\/([A-z0-9]+)(\&|$)/.exec(path)[1];
                let getId = /v=([A-z0-9]+)(\&|$)/.exec(srch);
                if(host.search(/youtube\.com/)!==-1) return !getId ? '':getId[1];
            }

            // On every input change we will check if the given URL correspond with an existing youtube video.
            $("#youtube-link").on('input',function(){


                /**
                 * Sets everything ready to upload de video URL on database. It is called when the .youtube-link
                 * input contains a valid youtube video URL.
                 *
                 * @param $videoUploader
                 */
                function videoFound($videoUploader)   {

                    $videoUploader.find("input#youtube_tile").val(videoInfo.title);
                    $videoUploader.find(".youtube-thumb img").attr("src",videoInfo.thumbnail_url );
                    $videoUploader.find(".youtube-link").removeClass("youtube-error").addClass("youtube-success");
                    $videoUploader.find(".video-upload-button").prop("disabled", false);
                    $videoUploader.find(".video-upload-button").prop("title", "Guardar el vídeo en base de datos");
                }

                /**
                 * Sets everything to indicate that the video cannot be uploaded to database. It is called when
                 * the .youtube-link input contains an invalid youtube video URL.
                 *
                 * @param $videoUploader
                 */
                function videoNotFound($videoUploader)    {

                    $videoUploader.find(".youtube-thumb img").attr("src","");
                    $videoUploader.find(".youtube-link").removeClass("youtube-success").addClass("youtube-error");
                    $videoUploader.find(".video-upload-button").prop("disabled", true);
                    $videoUploader.find(".video-upload-button").prop("title", "Debe introducir una URL válida de youtube para guardar el vídeo en base de datos.");

                }

                // The youtube-link input.
                let $this = $(this);
                // The current youtube-uploader.
                let $videoUploader = $this.parents("#newVideoModal");

                let videoUrl = $(this).val();
                videoId = isYoutubeRegEx($(this).val());
                let videoInfo = {};

                // If the video URL matches de regex so we have a valid video ID we will try to get
                // the video info from youtube API.
                if (videoId && videoId.length > 0){

                    // METHOD I  --> Refer to admin/sections/printers/controllers/addUpdatePrinter.php
                    // METHOD II --> USE http://img.youtube.com || http://i.ytimg.com || http://i3.ytimg.com
                    // Since we only need to grab the thumbnail, for UI purposes, to show the user that the video was
                    // found before saving its ID on database, we do not need to use Youtube API, we can just construct
                    // the thumbnail's URL. We will also check that the URL references a valid image file.
                    // There is an added problem, when we set the thumbnail url to an img element, even if the video id
                    // does not exist, the i3.ytimg.com (youtube images) url will return a default NOT_FOUND image in
                    // in the response's body, even if the http status is 404 in the reponse's header, so we
                    // have no way to detect the youtube ID doesn't exist by just checking the thumnail URL, we would
                    // need you use YOUTUBE API V3.
                    // As a workaround we are going set hqdefault.jpg thumbnail, which size should be 480px x 360px,
                    // on a temporal Image element. So if the img retrieved is smaller we will assume that
                    // the video does not exists.
                    videoInfo.thumbnail_url = "http://i3.ytimg.com/vi/" + videoId + "/hqdefault.jpg";
                    let img = new Image();
                    $(img).off("load").on("load", function() {
                        if (this.height < 360) {
                            videoNotFound($videoUploader);
                            return;
                        }
                        videoFound($videoUploader);
                    }).attr("src", videoInfo.thumbnail_url);

                } else  {
                    videoNotFound($videoUploader);
                }
            });


            // Click listeners
            $("#video-upload-button").on("click", uploadVideoLink);

            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////

        });
        // ENF OF DOCUMENT READY BLOCK /////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    </script>

    <?php

    // Check for errors and warnings:
    if(isset($_SESSION['error']) && is_array($_SESSION['error']))   include $rootpath . 'admin/inc/warning.php';
    // Include Footer.
    include $rootpath . 'admin/inc/footer.php';

// If user is not logged -> Back to loggin page.
} else 	{
    header("Location:".$rootpath."admin/login.php");
    exit;
}