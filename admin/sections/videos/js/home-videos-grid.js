/**
 * Fire it up calling loadBannersGrid() method on document ready!
 */


    //
    // Initialize stuff
    //

    let grid = null;
    let docElem = document.documentElement;
    let demo = document.querySelector('.videos-grid');
    let gridElement = demo.querySelector('.grid');
    let searchField = demo.querySelector('.search-field');
    let characters = 'abcdefghijklmnopqrstuvwxyz';
    let colorsArray = ['#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#16a085', '#27ae60', '#2980b9',
        '#8e44ad', '#2c3e50', '#f1c40f', '#e67e22', '#e74c3c', '#f39c12', '#d35400', '#c0392b', '#7f8c8d'];
    let dragOrder = [];
    let uuid = 0;
    let searchFieldValue;



    let videosArray = null;
    let endPointServer = "";

    //
    // Grid helper functions
    //

    function loadHomeVideosGrid( arrayOfHomeVideos, endPoint) {

        // Get the array of banners to show in the grid.
        videosArray = arrayOfHomeVideos;
        endPointServer = endPoint;

        // Init Muuri grid elemen.
        initGrid();

        // Set layout order preference.
        setLayout(demo.getAttribute("data-alignment"));

        // Reset field values.
        searchField.value = '';

        // Set inital search query, active filter, active sort value and active layout.
        searchFieldValue = searchField.value.toLowerCase();

        // Search field binding.
        searchField.addEventListener('keyup', function () {
            let newSearch = searchField.value.toLowerCase();
            if (searchFieldValue !== newSearch) {
                searchFieldValue = newSearch;
                filter();
            }
        });

        // Remove and IsShown items bindings.
        gridElement.addEventListener('click', function (e) {
            if (elementMatches(e.target, '.card-remove, .card-remove strong')) {
                confirmRemoveItem(e);
            } else if (elementMatches(e.target, '.card-chkbox label'))    {
                setIsShown(e);
            } else if (elementMatches(e.target, '.card-edit, .card-edit strong'))    {

                editItem(e);
            }
        });
    }

    function initGrid() {

        let dragCounter = 0;
        let orderChanged = false;

        grid = new Muuri(gridElement, {
            items: generateElements(videosArray),
            layout: {
                fillGaps: false,
                horizontal: false,
                alignRight: true,
                alignBottom: false,
                rounding: true
            },
            layoutDuration: 400,
            layoutEasing: 'ease',
            dragEnabled: true,
            dragSortInterval: 50,
            dragContainer: document.body,
            // This method executes on the start of every drag event and avoid drag to be fired when we click on
            // any of the controls, like x delete button or the checkbox.
            dragStartPredicate: function (item, event) {
                let isDraggable = true;
                let isRemoveAction = elementMatches(event.target, '.card-remove, .card-remove strong');
                let isCheckBoxAction = elementMatches(event.target, '.card-chkbox label');
                return isDraggable && !isRemoveAction && !isCheckBoxAction ? Muuri.ItemDrag.defaultStartPredicate(item, event) : false;
            },
            dragReleaseDuration: 400,
            dragReleseEasing: 'ease'
        })
            .on('dragStart', function () {
                ++dragCounter;
                // Initially order does not changes, only on move event is fired.
                orderChanged = false;
                docElem.classList.add('dragging');
            })
            .on('dragEnd', function (item, event) {
                if (--dragCounter < 1) docElem.classList.remove('dragging');

                // Update display_order column on database.
                if (orderChanged) updateDisplayOrder();
            })
            .on('move', function (result) {
                orderChanged = (result.fromIndex !== result.toIndex);
                updateIndices();
            })
            .on('sort', updateIndices)
            .on('remove', function (items, indices) {
                //console.log(items, indices);
            });

    }

    /**
     * This method filters the grid elements and removes the items which attribute "title" does not match
     * the content of searchFieldValue.
     */
    function filter() {

        grid.filter(function (item) {
            let element = item.getElement();
            return !searchFieldValue ? true : (element.getAttribute('title') || '').toLowerCase().indexOf(searchFieldValue) > -1;
        });
    }

    function sort() {

        dragOrder = grid.getItems();
        // Sort the items.
        grid.sort(dragOrder);
        // Update indices and active sort value.
        updateIndices();

    }

    /**
     *
     * This method adds a new item to the grid and update indexes, both on layout and database.
     * We add a random string param on the images paths in case we are updating an existing image
     * and the new one has the same name, for example lap_es_file.jpg, if we do not set the random
     * parameter on the url the img will be loaded from cache since it is called on a ajax request
     * and the img wont be changed on the front-end.
     *
     * @param newHomeVideo
     */
    function addItem(newHomeVideo) {

        let newItem = generateElement(
            ++uuid,
            newHomeVideo.id,
            newHomeVideo.youtube_id,
            newHomeVideo.youtube_title,
            newHomeVideo.is_shown,
            getRandomElement(colorsArray)
        );

        // Hide the new element.
        newItem.style.display = 'none';

        window.setTimeout(function () {
            // Add the element to the beginning of the grid.
            grid.add(newItem, {index: newHomeVideo.display_order });
            // Update UI indices.
            updateIndices();
            updateDisplayOrder(false);
            // Finally filter the items.
            filter();
        }, 700);

        return true;
    }


    function confirmRemoveItem(e) {

        let videoToRemove = elementClosest(e.target, '.item');
        let homeVideoIdToRemove = videoToRemove.getAttribute('data-home-video-id');
        let videoTitle = videoToRemove.getAttribute('title');

        $('#confirmDeleteModal').find("#confirmDeleteYesBtn").attr('onclick',
            'removeItem(' + homeVideoIdToRemove +',"' + videoTitle +'")');
        $('#confirmDeleteModal').modal('show');

    }

    /**
     * This method removes a banner both from grid and database.
     *
     * @param bannerId
     */
    function removeItem(videoId, videoTitle)   {

        // Post to the server to handle the changes.
        $.ajax({
            type: "POST",
            url: endPointServer,
            data: {
                video_id: videoId,
                video_title: videoTitle,
                action: 'delete_home_video'
            },
            success: function(data, status) {
                // Parse json string response into object:
                try {
                    // Show info message to user on UI.
                    obj = JSON && JSON.parse(data) || $.parseJSON(data);
                    showWarningMessage(obj.message, "info");

                    // Remove element from grid and update indices, only on layout.
                    grid.hide($(".item.video-item[data-home-video-id=" + videoId + "]" )[0], {onFinish: function (items) {
                            let item = items[0];
                            grid.remove(item, {removeElements: true});
                            updateIndices();
                        }});

                } catch  (error)   {
                    showWarningMessage(data, "error");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                e.preventDefault();
                showWarningMessage(xhr.status, thrownError);
            }
        });
        $('#confirmDeleteModal').modal('hide');
    }




    /**
     * This method is triggered when clicked the checkbox button for a corresponding grid element.
     * Sets the is_shown value for the corresponding banner. It changes the value of the is_shown column on
     * database so the front-end will be updated.
     * @param e The click event.
     */
    function setIsShown(e) {

        let elem = elementClosest(e.target, '.item');
        let homeVideoID = elem.getAttribute("data-home-video-id");
        let homeVideoTitle = elem.getAttribute("title");
        let isShown = !document.getElementById('switch' + homeVideoID).checked;

        // Post to the server to handle the changes.
        $.ajax({
            type: "POST",
            url: endPointServer,
            data: {
                data: [homeVideoID, isShown],
                action: 'set_is_shown'
            },
            beforeSend: function() {
                // Disable dragging
            },
            success: function(data, status) {
                // Parse json string response into object:
                try {
                    obj = JSON && JSON.parse(data) || $.parseJSON(data);
                    showWarningMessage(obj.message.replace("####", homeVideoTitle), "info");
                } catch  (error)   {
                    document.getElementById('switch' + homeVideoID).checked = !isShown;
                    showWarningMessage(data, "error");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                e.preventDefault();
                showWarningMessage(xhr.status, thrownError);
            }
        });
    }


    /**
     * Sets layout distribution of the elements contained by the grid but changing the muuri's grid options.
     * Parameter can take one of the following values:
     * [left-top, left-top-fillgaps, right-top, right-top-fillgaps, left-bottom, left-bottom--fillgaps, ... ]
     */
    function setLayout(layoutFieldValue) {

        grid._settings.layout = {
            horizontal: false,
            alignRight: layoutFieldValue.indexOf('right') > -1,
            alignBottom: layoutFieldValue.indexOf('bottom') > -1,
            fillGaps: layoutFieldValue.indexOf('fillgaps') > -1
        };
        grid.layout();
    }


    //
    // Generic helper functions
    //

    /**
     * This method is used by muuri grid element to init the grid. It takes the bannersArray, an
     * array containing all the HomeBannerDTO as Objects passed from php and generates one card
     * for each element using the method generateElement().
     *
     * @returns {Array}
     */
    function generateElements(videosArray) {

        let ret = [];
        for (let i = 0; i < videosArray.length; i++) {
            let currentItem = videosArray[i];
            ret.push(generateElement(
                ++uuid,
                currentItem.id,
                currentItem.youtube_id,
                currentItem.youtube_title,
                currentItem.is_shown,
                getRandomElement(colorsArray)
            ));
        }
        return ret;
    }

    /**
     * This method receives a single HomeBannerDTO as an js object and creates the html div to show
     * the card on the grid.
     *
     *
     * @param position
     * @param home_video_id
     * @param youtube_id
     * @param youtube_title
     * @param is_shown
     * @param color
     * @param width
     * @param height
     * @returns {Node | null}
     * @returns {Node | null}
     */
    function generateElement(position, home_video_id, youtube_id, youtube_title, is_shown, color, width = null, height = null) {

        let itemElem = document.createElement('div');
        let classNames = 'item video-item';
        let itemTemplate = '' +
            '<div class="' + classNames + '" data-pos="' + position + '" data-home-video-id="' + home_video_id + '"data-color="' + color + '" title="' + youtube_title + '">' +
            '<div class="item-content" style="color:' + color + '">' +
            '<div class="logo-card" style="color:' + color + '">' +
            '<div class="card-id">' + position + '</div>' +
            '<div class="card-video"><img src="https://img.youtube.com/vi/' + youtube_id + '/0.jpg"></img></div>' +
            '<div class="card-video-title"><span>' + youtube_title + '</span></div>' +
            '<div class="card-remove" title="Borrar este video." data-home-video-id="' + home_video_id + '"><strong>&#x1F5D1;</strong></div>' +
            '<div class="card-chkbox" title="Mostrar/Ocultar este video en la seccion home de la Web."><input type="checkbox" id="switch' + home_video_id + '" ' + (is_shown == 1 ? 'checked' : '') + '/><label for="switch' + home_video_id + '"></label></div>' +
            '</div>' +
            '</div>' +
            '</div>';

        itemElem.innerHTML = itemTemplate;

        return itemElem.firstChild;
    }

    /**
     * Gets a random element from a collection of elements.
     * @param collection
     * @returns {*}
     */
    function getRandomElement(collection) {

        return collection[Math.floor(Math.random() * collection.length)];

    }

    /**
     * Updates the <div class="card-id"> content from the grid cards whenever it might have changed.
     *
     * It just takes all the grid items in the current order and sets both data-pos attribute and .card-id html content.
     */
    function updateIndices() {

        grid.getItems().forEach(function (item, i) {
            item.getElement().setAttribute('data-pos', i + 1);
            item.getElement().querySelector('.card-id').innerHTML = i + 1;
        });
    }

    /**
     * This method updates the display_order column of banner table on database to match the grid order.
     * It is fired on the dragEnd event (unless the grid order has not been changed), and after the addItem()
     * method. In this last case with parameter showInfoMsg = false not to show the result in a warning div, since
     * the result is the addItem() result.
     * It is not called after removing elements from the grid, it is not necessary.
     *
     * @param showInfoMsg
     */
    function updateDisplayOrder(showInfoMsg = true)   {

        let newOrder = [];

        grid.getItems().forEach(function (item, i) {
            newOrder.push([item.getElement().getAttribute('data-home-video-id', i + 1), item.getElement().getAttribute('data-pos', i + 1)]);
        });

        // Post to the server to handle the changes.
        $.ajax({
            type: "POST",
            url: endPointServer,
            data: {
                data: newOrder,
                action: 'update_display_order'
            },
            beforeSend: function() {
                // Disable dragging
            },
            success: function(data, status) {
                // Parse json string response into object:
                try {
                    obj = JSON && JSON.parse(data) || $.parseJSON(data);
                    showInfoMsg ? showWarningMessage(obj.message, "info") : null;
                } catch  (error)   {
                    showWarningMessage(data, "error");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                e.preventDefault();
                showWarningMessage(xhr.status, thrownError);
            }
        });

    }



    /**
     * JS method to see if an element matches the corresponding css selector.
     *
     * @param element
     * @param selector
     * @returns {boolean | *}
     */
    function elementMatches(element, selector) {

        var p = Element.prototype;
        return (p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector).call(element, selector);

    }

    /**
     * Finds the closet html element to the given html element that matches the given css selector.
     *
     * @param element
     * @param selector
     * @returns {*}
     */
    function elementClosest(element, selector) {

        if (window.Element && !Element.prototype.closest) {
            var isMatch = elementMatches(element, selector);
            while (!isMatch && element && element !== document) {
                element = element.parentNode;
                isMatch = element && element !== document && elementMatches(element, selector);
            }
            return element && element !== document ? element : null;
        }
        else {
            return element.closest(selector);
        }

    }
