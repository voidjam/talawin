<?php

$rootpath = "./../../../";
$current_uri = "$_SERVER[REQUEST_URI]";

// Get connection to database.
include $rootpath . 'admin/scripts/connect.php';
include $rootpath . 'admin/inc/constants.php';
include $rootpath . 'admin/scripts/functions.php';


$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

// If user is logged.
if (!empty($_SESSION["LOGGED"]) && $_SESSION["LOGGED"] == true)	{

    // Include Header.
    include $rootpath . 'admin/inc/header.php';
    include $rootpath . 'admin/inc/nav-bar.php';

    // Get all provider_logos
    $providerLogoDAO = new ProviderLogoDAO();
    $arrayProviderLogoDTO = $providerLogoDAO->getProvidersLogos();

    // Set page title:
    $title = "proveedores";

    ?>


    <!-- DIV containing the providers configuration options -->
    <div class="client-logo-content">

        <!-- ADD CLIENT Button -->
        <button class="btn btn-success add-client-logo-button" role="button" data-toggle="modal" data-target="#newProviderLogoModal"
                title="Añadir un nuevo proveedor">
            <span><strong>Añadir Proveedor</strong></span></button>

        <!-- ---------------------- -->
        <!-- FIRST ROW: MAIN BLOCK  -->
        <div class="row">

            <!-- ----------------------------------- -->
            <!-- /FIRST COLUMN: PROVIDERS LOGO CARDS -->
            <div class="col-sm" style="position: relative;">

                <!-- DIV header explaining what is ther inside -->
                <div class="client-table-main-header">PROVEEDORES ACTUALES</div>

                <!-- Horizontal separator -->
                <br><hr/>

                <!-- ---------------------- -->
                <!-- GRID OF PROVIDERS ------ -->
                <section class="grid-logos" data-alignment="left-top-fillgaps">
                    <div class="controls cf">
                        <!-- SEARCH FIELD -->
                        <div class="control search">
                            <div class="control-icon"></div>
                            <input class="control-field search-field form-control" type="text" name="search" placeholder="Search..." />
                        </div>

                    </div>
                    <div class="grid"></div>
                </section>
                <!-- /GRID OF PROVIDERS ----- -->
                <!-- ---------------------- -->
            </div>
            <!-- /FIRST COLUMN: PROVIDERS LOGO CARDS -->
            <!-- ----------------------------------- -->

        </div>
        <!-- /FIRST ROW: MAIN BLOCK -->
        <!-- ---------------------- -->

        <!-- ------------------------ -->
        <!-- NEW PROVIDER LOGO MODAL  -->
        <div class="modal fade" id="newProviderLogoModal" tabindex="-1" role="dialog" aria-labelledby="newProviderLogoModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="newProviderLogoModalLabel">Añadir Nuevo Proveedor</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <!-- NEW PROVIDER LOGO FORM -->
                        <form id="new_provider_logo_form" class="form-horizontal" action="<?php echo $rootpath?>admin/sections/providers/uploadProviderLogoServer.php" method="post" onSubmit="return checkForm(this)" enctype="multipart/form-data">

                            <!--NAME -->
                            <div class="form-group">
                                <label for="provider_name" class="col-sm-6 control-label">Proveedor</label>
                                <div class="col-sm-12">
                                    <input required type="text" class="input-lg form-control" id="provider_name" name="provider_name" placeholder="Nombre de Empresa">
                                </div>
                            </div>
                            <!-- WEB LINK -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="input-lg form-control" id="provider_linl" name="provider_link" placeholder="Link a la web">
                                </div>
                            </div>
                            <!--IMAGE -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="avatar-upload">
                                        <div class="avatar-edit" title="Seleccionar el logo del proveedor.">
                                            <input type='file' id="uploadedImage" name="uploadedImage" accept=".png, .jpg, .jpeg" required/>
                                            <label for="uploadedImage"></label>
                                        </div>
                                        <div class="avatar-preview" title="Seleccionar el logo del proveedor.">
                                            <div id="imagePreview">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ACTION FIELD SAVE NEW PROVIDER LOGO -->
                            <input hidden name="action" id="action" value="<?php echo (PROVIDER_LOGO_FORM_ACTION_SAVE)?>"/>
                            <!-- SUBMIT HIDDEN BUTTON -->
                            <button type="submit" id="submitHiddenButton" style="display: none;"></button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button onclick="submitNewProviderForm('<?php echo PROVIDER_LOGO_FORM_ACTION_SAVE ?>')" class="btn btn-primary">Añadir el nuevo Proveedor</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /NEW PROVIDER LOGO MODAL -->
        <!-- ---------------------- -->

        <!-- ---------------------- -->
        <!-- CONFIRM DELETE MODAL - -->
        <div class="modal fade" id="confirmDeleteModal" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                            <h5 class="modal-title" id="confirmDeleteModalLabel">ELIMINANDO PROVEEDOR:</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ¿Está seguro de que quiere eliminar el <b>proveedor</b>?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
                        <button id="confirmDeleteYesBtn" type="button" class="btn btn-primary">SÍ</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /CONFIRM DELETE MODAL  -->
        <!-- ---------------------- -->


    </div><!-- END OF client-logo-content -->


    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- SECTION PAGE INFO +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="page_section_info unselectable">
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION PAGE INFO TXT +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <span><?php echo $title ?></span>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- /SECTION PAGE INFO TXT ++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    </div>
    <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    <!-- Drag & Drop jQuery pluggin: Muuri.js and its dependencies. -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/web-animations/2.3.1/web-animations.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.js" integrity="sha256-GMd3rFxMDNnM5JQEpiKLLl8kSrDuG5egqchk758z59g=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/muuri/0.5.4/muuri.js" integrity="sha256-CWldck8RM3iilMsXH/oW9iLdm/TcW3WcpRw09wL6MFg=" crossorigin="anonymous"></script>

    <!-- Custom JS for our Providers grid -->
    <script src="<?php echo $rootpath; ?>admin/sections/providers/js/logo-grid.js"></script>

    <script>

        /**
         * This method checks that
         *
         * @param form
         * @returns {boolean}
         */
        function checkForm(form) {

            return true;
        }

        /**
         * This method triggers from submit throw JS. Instead of using $form.submit() it
         * triggers a click on submit button so the validations are run before submitting.
         */
        function submitNewProviderForm(action)   {

            $("#submitHiddenButton").trigger("click");

            // let $form = $('form#new_provider_logo_form');
            // $("#action").attr("value", action);
            // $form.submit();
        }

        /**
         * This method appends a warning div to #page_container containing a message for the user.
         *
         * @param message Message to be presented on screen.
         * @param error_level ['info', 'error', 'warning'] Sets the color of the warning div.
         */
        function showWarningMessage(message, error_level) {

            // Check if there is already an allert message on screen and close it.
            if($('.alert').length) { $(".alert").alert("close"); }

            $('#page_container').append('<div id=\"general-warning-div\" class=\"alert fade show\" role=\"alert\">'
                + '<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>'
                + '<p><strong>' + message + '</strong></p></div>');

            $(".alert").fadeTo(5000, 1, "swing").fadeOut(800, function () {
                $(".alert").alert("close");
            });

            if (error_level.localeCompare('info') === 0) {
                $(".alert").addClass("alert-success");
            } else if (error_level.localeCompare('error') === 0) {
                $(".alert").addClass("alert-danger");
            } else if (error_level.localeCompare('warning') === 0) {
                $(".alert").addClass("alert-warning");
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // DOCUENT READY BLOCK /////////////////////////////////////////////////////////////////////////////////////////
        $(document).ready(function() {

            // Showing logo thumbnail when creating a new provider on the #newProviderLogoModal dialog.
            function readURL(input) {
                if (input.files && input.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function(e) {
                        $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                        $('#imagePreview').hide();
                        $('#imagePreview').fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#uploadedImage").change(function() {
                readURL(this);
            });
            $("#imagePreview").on("click", function () {
                $("#uploadedImage").click();
            });

            // Start Grid of Logos using muuri.js
            loadLogosGrid((<?php echo json_encode($arrayProviderLogoDTO) ?>), "<?php echo $rootpath; ?>admin/sections/providers/uploadProviderLogoServer.php", "<?php echo $rootpath; ?>");

            // Prepares the form to prevent onSubmit default and to be sent via ajax.
            $('form#new_provider_logo_form')
                .on('submit', function(e) {
                    // Prevent form submission
                    e.preventDefault();

                    // Close the modal dialog:
                    $('#newProviderLogoModal').modal('hide')

                    let $form    = $(e.target),
                        formData = new FormData(),
                        params   = $form.serializeArray(),
                        files    = $form.find('[name="uploadedImage"]')[0].files;

                    $.each(files, function(i, file) {
                        // Prefix the name of uploaded files with "uploadedFiles-"
                        // Of course, you can change it to any string
                        formData.append('uploadedImage', file);
                    });

                    $.each(params, function(i, val) {
                        formData.append(val.name, val.value);
                    });

                    $.ajax({
                        url: $form.attr('action'),
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function(data, status) {
                            // Parse json string response into object:
                            try {
                                // Show info message to user on UI.
                                obj = JSON && JSON.parse(data) || $.parseJSON(data);
                                if (obj.success === true)   {
                                    showWarningMessage(obj.message, "info");
                                    // Add element from grid and update indices, both on layout and database.
                                    addItem(obj.newProviderLogoDTO);
                                } else {
                                    showWarningMessage(obj.message, "error");
                                }
                                $form.trigger("reset");

                            } catch  (error)   {
                                showWarningMessage("Error: " + error, "error");
                                console.log(error);
                            }
                        }
                    });
                })
                .on("reset", function () {
                });



            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // NEW PROVIDER LOGO MODAL EVENTS ///////////////////////////////////////////////////////////////////////
            // Reset #newProviderLogoModal form on modal hidden event. //////////////////////////////////////////////
            $('#newProviderLogoModal').on('hidden.bs.modal', function () {
                $('#imagePreview')[0].style.backgroundImage = '';
            });
            // Focus text input after showing modal.
            $('#newProviderLogoModal').on('shown.bs.modal', function () {
                $(this).find("input#provider_name").focus();
            });
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////

        }); // End of document ready block.

    </script>

    <?php

    // Check for errors and warnings:
    if(isset($_SESSION['error']) && is_array($_SESSION['error']))   include $rootpath . 'admin/inc/warning.php';
    // Include Footer.
    include $rootpath . 'admin/inc/footer.php';

// If user is not logged -> Back to loggin page.
} else 	{
    header("Location:".$rootpath."admin/login.php");
    exit;
}