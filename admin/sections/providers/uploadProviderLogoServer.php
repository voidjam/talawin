<?php


/**
 * THIS ENDPOINT RECEIVES REQUEST FROM sections/providers/index.php AND HANDLES THEM
 *
 * ACTIONS:PROVIDER_LOGO_FORM_ACTION_SAVE     --> Adds a new provider logo to database and store the logo image file on file system.
 * +
 *
 */


$rootpath = "./../../../";

// Get connection to database.
include $rootpath . 'admin/scripts/connect.php';
// Constants file:
include $rootpath . 'admin/inc/constants.php';


$method = $_SERVER["REQUEST_METHOD"];

/** ACTION PROVIDER_LOGO_FORM_ACTION_SAVE */
// Upload a new provider logo to the both file system and database..
if ($method == "POST" && strcmp( $_POST['action'], PROVIDER_LOGO_FORM_ACTION_SAVE) == 0) {

    $target_dir = $rootpath."admin/img/providers_logos/";
    $target_file = $target_dir . basename($_FILES["uploadedImage"]["name"]);
    $uploadOk = 1;
    $result = "";
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["uploadedImage"]["tmp_name"]);
        if($check !== false) {
            $result.= "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            $result.= "File is not an image.";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        $result.= "Sorry, file already exists. ";
        $uploadOk = 0;
    }
    // Check if file size exceeds 5Mb
    if ($_FILES["uploadedImage"]["size"] > 5000000) {
        $result.= "Sorry, your file is too large. ";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" ) {
        $result.= "Sorry, only JPG, JPEG, PNG files are allowed. ";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $result.= "Sorry, your file was not uploaded. ";
    // if everything is ok, try to upload file
    } else {
        // If the file is correctly uploaded to corresponding folder on server's file system.
        if (move_uploaded_file($_FILES["uploadedImage"]["tmp_name"], $target_file)) {

            // We will save the new provider on database:
            $providerLogoDAO = new ProviderLogoDAO();
            $providerLogoDTO = new ProviderLogoDTO();
            $providerLogoDTO->setProviderName(htmlspecialchars($_POST['provider_name']));
            $providerLogoDTO->setProviderLink(htmlspecialchars($_POST['provider_link']));
            $providerLogoDTO->setIsShown(true);
            $providerLogoDTO->setDisplayOrder(null);
            $providerLogoDTO->setFileName($_FILES["uploadedImage"]["name"]);
            $providerLogoDTO->setMimeType($_FILES["uploadedImage"]['type']);
            $providerLogoDTO->setPath("admin/img/providers_logos/");
            $providerLogoDTO->setSize($_FILES["uploadedImage"]["size"]);

            // If new provider is correctly saved on database.
            if (($insertedProviderLogoId = $providerLogoDAO->saveProviderLogo($providerLogoDTO)) > 0) {

                $uploadOk = 1;
                $providerLogoDTO->setId($insertedProviderLogoId);
                $result.= "The new provider was saved on database and the file "
                    .basename( $_FILES["uploadedImage"]["name"])." has been uploaded.";

            // If there was an error saving on database.
            } else  {

                $uploadOk = 0;
                unlink($target_file);
                $result.= "Sorry, there was a problem with database. Please try again in a few minutes and"
                ." contact Umboweti in case the error persists.";
            }

        // If there was an error moving uploaded file to file system.
        } else {
            $uploadOk = 0;
            $result.= "Sorry, there was an error uploading your file. Please try again in a few minutes and"
                ." contact Umboweti in case the error persists.";
        }
    }


    // Give feedback to calling script and redirect.
    //    $uploadOk == 1
    //        ? $_SESSION['error']['level'] = 'info'
    //        : $_SESSION['error']['level'] = 'error';
    //    $_SESSION['error']['message'] = $result;
    //    header("Location:".$rootpath."admin/sections/providers/index.php");


    $uploadOk == 1
        ? $result = json_encode(array("success" => true,
                    "message" => "Success: ".$result,
                    "newProviderLogoDTO" => $providerLogoDTO))
        : $result = json_encode(array("success" => false, "message" => "Failed: ".$result));

    header("Content-Type: text/plain");
    echo $result;
    exit;
}
/** ACTION PROVIDER_LOGO_ACTION_SET_IS_SHOWN */
else if ($method == "POST" && strcmp( $_POST['action'], PROVIDER_LOGO_ACTION_SET_IS_SHOWN) == 0)  {

    header("Content-Type: text/plain");
    $providerLogoDAO = new ProviderLogoDAO();
    if ($providerLogoDAO->setIsShown($_POST['data'][0], $_POST['data'][1]) > 0)    {
        $result = json_encode(array("success" => true,
                                    "message" => "Success: Provider with id ".$_POST['data'][0]." display property updated to ".$_POST['data'][1]."."));
    } else {
        $result = json_encode(array("success" => false, "message" => "Database could not be updated"));
    }
    echo $result;
    exit;
}

/** ACTION PROVIDER_LOGO_ACTION_DELETE_PROVIDER */
else if ($method == "POST" && strcmp( $_POST['action'], PROVIDER_LOGO_ACTION_DELETE_PROVIDER) == 0)  {

    header("Content-Type: text/plain");
    $providerLogoDAO = new ProviderLogoDAO();
    if ($providerLogoDAO->deleteProviderLogoById($_POST['provider_id']) > 0)    {
        $result = json_encode(array("success" => true,
            "message" => "Success: El proveedor con id ".$_POST['provider_id']." ha sido borrado de la base de datos."));
    } else {
        $result = json_encode(array("success" => false, "message" => "Database could not be updated"));
    }
    echo $result;
    exit;
}
/** ACTION PROVIDER_LOGO_ACTION_UPDATE_DISPLAY_ORDER */
else if ($method == "POST" && strcmp( $_POST['action'], PROVIDER_LOGO_ACTION_UPDATE_DISPLAY_ORDER) == 0)  {

    header("Content-Type: text/plain");
    $providerLogoDAO = new ProviderLogoDAO();

    foreach ($_POST['data'] as $valor) {
        if(!($providerLogoDAO->updateDisplayOrder($valor[0], $valor[1]) > 0))    {
            $result = json_encode(array("success" => false, "message" => "El orden de los elementos no pudo ser actualizado. Por favor, inténtelo de nuevo."));
        }
    }

    $result =  json_encode(array("success" => true,
        "message" => "El nuevo orden de los proveedores se ha guardado correctamente en base de datos."));
    echo $result;
    exit;
}

else {

    header("HTTP/1.0 405 Method Not Allowed");
}
?>