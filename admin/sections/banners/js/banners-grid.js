/**
 * Fire it up calling loadBannersGrid() method on document ready!
 */


    //
    // Initialize stuff
    //

    let grid = null;
    let docElem = document.documentElement;
    let demo = document.querySelector('.banners-grid');
    let gridElement = demo.querySelector('.grid');
    let searchField = demo.querySelector('.search-field');
    let characters = 'abcdefghijklmnopqrstuvwxyz';
    let colorsArray = ['#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#16a085', '#27ae60', '#2980b9',
        '#8e44ad', '#2c3e50', '#f1c40f', '#e67e22', '#e74c3c', '#f39c12', '#d35400', '#c0392b', '#7f8c8d'];
    let dragOrder = [];
    let uuid = 0;
    let searchFieldValue;



    let bannersArray = null;
    let endPointServer = "";
    let globalRootPath = "";


    //
    // Grid helper functions
    //

    function loadBannersGrid( arrayOfBanners, endPoint, rootPath) {

        // Get the array of banners to show in the grid.
        bannersArray = arrayOfBanners;
        endPointServer = endPoint;
        globalRootPath = rootPath;

        // Init Muuri grid elemen.
        initGrid();

        // Set layout order preference.
        setLayout(demo.getAttribute("data-alignment"));

        // Reset field values.
        searchField.value = '';

        // Set inital search query, active filter, active sort value and active layout.
        searchFieldValue = searchField.value.toLowerCase();

        // Search field binding.
        searchField.addEventListener('keyup', function () {
            let newSearch = searchField.value.toLowerCase();
            if (searchFieldValue !== newSearch) {
                searchFieldValue = newSearch;
                filter();
            }
        });

        // Remove and IsShown items bindings.
        gridElement.addEventListener('click', function (e) {
            if (elementMatches(e.target, '.card-remove, .card-remove strong')) {
                confirmRemoveItem(e);
            } else if (elementMatches(e.target, '.card-chkbox label'))    {
                setIsShown(e);
            } else if (elementMatches(e.target, '.card-edit, .card-edit strong'))    {

                editItem(e);
            }
        });
    }

    function initGrid() {

        let dragCounter = 0;
        let orderChanged = false;

        grid = new Muuri(gridElement, {
            items: generateElements(bannersArray),
            layoutDuration: 400,
            layoutEasing: 'ease',
            dragEnabled: true,
            dragSortInterval: 50,
            dragContainer: document.body,
            // This method executes on the start of every drag event and avoid drag to be fired when we click on
            // any of the controls, like x delete button or the checkbox.
            dragStartPredicate: function (item, event) {
                let isDraggable = true;
                let isRemoveAction = elementMatches(event.target, '.card-remove, .card-remove strong');
                let isEditAction = elementMatches(event.target, '.card-edit, .card-edit strong');
                let isCheckBoxAction = elementMatches(event.target, '.card-chkbox label');
                return isDraggable && !isRemoveAction && !isCheckBoxAction && !isEditAction ? Muuri.ItemDrag.defaultStartPredicate(item, event) : false;
            },
            dragReleaseDuration: 400,
            dragReleseEasing: 'ease'
        })
            .on('dragStart', function () {
                ++dragCounter;
                // Initially order does not changes, only on move event is fired.
                orderChanged = false;
                docElem.classList.add('dragging');
            })
            .on('dragEnd', function (item, event) {
                if (--dragCounter < 1) docElem.classList.remove('dragging');

                // Update display_order column on database.
                if (orderChanged) updateDisplayOrder();
            })
            .on('move', function (result) {
                orderChanged = (result.fromIndex !== result.toIndex);
                updateIndices();
            })
            .on('sort', updateIndices)
            .on('remove', function (items, indices) {
                //console.log(items, indices);
            });

    }

    function filter() {

        grid.filter(function (item) {
            let element = item.getElement();
            let isSearchMatch = !searchFieldValue ? true : (element.getAttribute('data-title') || '').toLowerCase().indexOf(searchFieldValue) > -1;
            return isSearchMatch;
        });

    }

    function sort() {

        dragOrder = grid.getItems();
        // Sort the items.
        grid.sort(dragOrder);
        // Update indices and active sort value.
        updateIndices();

    }

    /**
     *
     * This method adds a new item to the grid and update indexes, both on layout and database.
     * We add a random string param on the images paths in case we are updating an existing image
     * and the new one has the same name, for example lap_es_file.jpg, if we do not set the random
     * parameter on the url the img will be loaded from cache since it is called on a ajax request
     * and the img wont be changed on the front-end.
     *
     * @param newHomeBanner
     */
    function addItem(newHomeBanner) {

        let newItem = generateElement(
            ++uuid,
            newHomeBanner.id,
            globalRootPath + newHomeBanner.path + newHomeBanner.lap_es_file_name + "?" +  Math.random().toString(12).substring(2, 8),
            globalRootPath + newHomeBanner.path + newHomeBanner.lap_en_file_name + "?" +  Math.random().toString(12).substring(2, 8),
            globalRootPath + newHomeBanner.path + newHomeBanner.mob_es_file_name + "?" +  Math.random().toString(12).substring(2, 8),
            globalRootPath + newHomeBanner.path + newHomeBanner.mob_en_file_name + "?" +  Math.random().toString(12).substring(2, 8),
            newHomeBanner.is_shown,
            getRandomElement(colorsArray)
        );

        // Hide the new element.
        newItem.style.display = 'none';

        window.setTimeout(function () {
            // Add the element to the beginning of the grid.
            grid.add(newItem, {index: newHomeBanner.display_order - 1 });
            // Update UI indices.
            updateIndices();
            updateDisplayOrder(false);
            // Finally filter the items.
            filter();
        }, 700);

        return true;
    }


    function confirmRemoveItem(e) {

        let bannerToRemove = elementClosest(e.target, '.item');
        let bannerIdToRemove =bannerToRemove.getAttribute('data-banner-id');


        $('#confirmDeleteModal').find("#confirmDeleteYesBtn").attr('onclick', 'removeItem(' + bannerIdToRemove +')');
        $('#confirmDeleteModal').modal('show');

    }

    /**
     * This method removes a banner both from grid and database.
     *
     * @param bannerId
     */
    function removeItem(bannerId)   {

        // Post to the server to handle the changes.
        $.ajax({
            type: "POST",
            url: endPointServer,
            data: {
                banner_id: bannerId,
                action: 'delete_banner'
            },
            success: function(data, status) {
                // Parse json string response into object:
                try {
                    // Show info message to user on UI.
                    obj = JSON && JSON.parse(data) || $.parseJSON(data);
                    showWarningMessage(obj.message, "info");

                    // Remove element from grid and update indices, only on layout.
                    grid.hide($(".item.banner-item[data-banner-id=" + bannerId + "]" )[0], {onFinish: function (items) {
                            let item = items[0];
                            grid.remove(item, {removeElements: true});
                            updateIndices();
                        }});

                } catch  (error)   {
                    showWarningMessage(data, "error");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                e.preventDefault();
                showWarningMessage(xhr.status, thrownError);
            }
        });
        $('#confirmDeleteModal').modal('hide');
    }


    /**
     * This method is triggered when edit button is clicked and will let user edit the 4 images of a banner.
     * It loads uses de newBannerModal and its #new_banner_form and preloads the current banner content, that is,
     * id and the 4 files.
     *
     * @param e
     */
    function editItem(e) {

        // We are updating an existing banner
        $("#newBannerModalLabel").html("Editando el Banner...");
        $("#newBannerModalSubmit").html("Guardar los Cambios");
        $("#newBannerModalSubmit").attr("onclick", "submitNewBannerForm('update_banner')");


        // Get the id of the banner we want to edit
        let bannerToEdit = elementClosest(e.target, '.item.banner-item');
        let bannerIdToEdit = bannerToEdit.getAttribute('data-banner-id');

        // Get current images
        let $cardImg = $(bannerToEdit).find(".card-img");
        let img_lap_es = $cardImg.attr('data-img-lap-es');
        let img_lap_en = $cardImg.attr('data-img-lap-en');
        let img_mob_es = $cardImg.attr('data-img-mob-es');
        let img_mob_en = $cardImg.attr('data-img-mob-en');



        // Get the modal and the form.
        let $newBannerModal = $('#newBannerModal');
        let $newBannerForm = $newBannerModal.find("#new_banner_form");

        // Set the banner id on the hidden input #banner_id.
        $newBannerForm.find("input#banner_id").val(bannerIdToEdit);

        // Set the current images:
        $newBannerForm.find("input#lap_es_file");

        //$('#confirmDeleteModal').find("#confirmDeleteYesBtn").attr('onclick', 'removeItem(' + bannerIdToRemove +')');
        $newBannerModal.modal('show');


        // Set the current images on the background-image of the corresponding .imagePreview
        let $input_lap_es = $("#lap_es_file");
        let $input_lap_en = $("#lap_en_file");
        let $input_mob_es = $("#mob_es_file");
        let $input_mob_en = $("#mob_en_file");
        $input_lap_es.closest(".avatar-upload").find(".imagePreview").css('background-image', 'url(' + img_lap_es + ')');
        $input_lap_en.closest(".avatar-upload").find(".imagePreview").css('background-image', 'url(' + img_lap_en + ')');
        $input_mob_es.closest(".avatar-upload").find(".imagePreview").css('background-image', 'url(' + img_mob_es + ')');
        $input_mob_en.closest(".avatar-upload").find(".imagePreview").css('background-image', 'url(' + img_mob_en + ')');


        // Were we set the info for the size of the current image within the corresponding tooltip.
        [[$input_lap_es, img_lap_es ],[$input_lap_en,img_lap_en],[$input_mob_es,img_mob_es ],[$input_mob_en,img_mob_en]]
            .forEach (function (item, index) {
                let bg = new Image();
                bg.onload = function(){
                    item[0].closest(".avatar-upload").find(".avatar-preview").attr("data-original-title", item[0].closest(".avatar-upload").find(".avatar-preview").attr("data-original-title") + "<added-info><br><strong>Tamaño actual</strong> es: <strong>" + bg.width + '&times;' + bg.height + "px</strong></added-info>");
                    bg = null;
                };
                bg.src = item[1];
        });
    }


    /**
     * This method is triggered when clicked the checkbox button for a corresponding grid element.
     * Sets the is_shown value for the corresponding banner. It changes the value of the is_shown column on
     * database so the front-end will be updated.
     * @param e The click event.
     */
    function setIsShown(e) {

        let elem = elementClosest(e.target, '.item');
        let bannerID = elem.getAttribute("data-banner-id");
        let isShown = !document.getElementById('switch' + bannerID).checked;

        // Post to the server to handle the changes.
        $.ajax({
            type: "POST",
            url: endPointServer,
            data: {
                data: [bannerID, isShown],
                action: 'set_is_shown'
            },
            beforeSend: function() {
                // Disable dragging
            },
            success: function(data, status) {
                // Parse json string response into object:
                try {
                    obj = JSON && JSON.parse(data) || $.parseJSON(data);
                    showWarningMessage(obj.message, "info");
                } catch  (error)   {
                    document.getElementById('switch' + bannerID).checked = !isShown;
                    showWarningMessage(data, "error");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                e.preventDefault();
                showWarningMessage(xhr.status, thrownError);
            }
        });
    }


    /**
     * Sets layout distribution of the elements contained by the grid but changing the muuri's grid options.
     * Parameter can take one of the following values:
     * [left-top, left-top-fillgaps, right-top, right-top-fillgaps, left-bottom, left-bottom--fillgaps, ... ]
     */
    function setLayout(layoutFieldValue) {

        grid._settings.layout = {
            horizontal: false,
            alignRight: layoutFieldValue.indexOf('right') > -1,
            alignBottom: layoutFieldValue.indexOf('bottom') > -1,
            fillGaps: layoutFieldValue.indexOf('fillgaps') > -1
        };
        grid.layout();
    }


    //
    // Generic helper functions
    //

    /**
     * This method is used by muuri grid element to init the grid. It takes the bannersArray, an
     * array containing all the HomeBannerDTO as Objects passed from php and generates one card
     * for each element using the method generateElement().
     *
     * @returns {Array}
     */
    function generateElements(bannersArray) {

        let ret = [];
        for (let i = 0; i < bannersArray.length; i++) {
            let currentItem = bannersArray[i];
            ret.push(generateElement(
                ++uuid,
                currentItem.id,
                globalRootPath + currentItem.path + currentItem.lap_es_file_name,
                globalRootPath + currentItem.path + currentItem.lap_en_file_name,
                globalRootPath + currentItem.path + currentItem.mob_es_file_name,
                globalRootPath + currentItem.path + currentItem.mob_en_file_name,
                currentItem.is_shown,
                getRandomElement(colorsArray)
            ));
        }
        return ret;
    }

    /**
     * This method receives a single HomeBannerDTO as an js object and creates the html div to show
     * the card on the grid.
     *
     *
     * @param id
     * @param banner_id
     * @param img_lap_es
     * @param img_lap_en
     * @param img_mob_es
     * @param img_mob_en
     * @param is_shown
     * @param color
     * @param width
     * @param height
     * @returns {Node | null}
     * @returns {Node | null}
     */
    function generateElement(id, banner_id, img_lap_es, img_lap_en, img_mob_es, img_mob_en, is_shown, color, width = null, height = null) {

        let itemElem = document.createElement('div');
        let classNames = 'item banner-item';
        let itemTemplate = '' +
            '<div class="' + classNames + '" data-pos="' + id + '" data-banner-id="' + banner_id + '"data-color="' + color + '">' +
            '<div class="item-content" style="color:' + color + '">' +
            '<div class="logo-card" style="color:' + color + '">' +
            '<div class="card-id">' + id + '</div>' +
            '<div class="card-img" data-img-lap-es="' + img_lap_es + '" data-img-lap-en="' + img_lap_en +'" data-img-mob-es="' + img_mob_es +'" data-img-mob-en="' + img_mob_en +'"><img src="' + img_lap_es + '"/></div>' +
            '<div class="card-edit" title="Editar este banner." data-banner-id="' + banner_id + '"><strong>&#x1F58D;</strong></div>' +
            '<div class="card-remove" title="Borrar este banner." data-banner-id="' + banner_id + '"><strong>&#x1F5D1;</strong></div>' +
            '<div class="card-chkbox" title="Mostrar/Ocultar este banner en la Web."><input type="checkbox" id="switch' + banner_id + '" ' + (is_shown == 1 ? 'checked' : '') + '/><label for="switch' + banner_id + '"></label></div>' +
            '</div>' +
            '</div>' +
            '</div>';

        itemElem.innerHTML = itemTemplate;

        return itemElem.firstChild;
    }

    /**
     * Gets a random element from a collection of elements.
     * @param collection
     * @returns {*}
     */
    function getRandomElement(collection) {

        return collection[Math.floor(Math.random() * collection.length)];

    }

    /**
     * Updates the <div class="card-id"> content from the grid cards whenever it might have changed.
     *
     * It just takes all the grid items in the current order and sets both data-pos attribute and .card-id html content.
     */
    function updateIndices() {

        grid.getItems().forEach(function (item, i) {
            item.getElement().setAttribute('data-pos', i + 1);
            item.getElement().querySelector('.card-id').innerHTML = i + 1;
        });
    }

    /**
     * This method updates the display_order column of banner table on database to match the grid order.
     * It is fired on the dragEnd event (unless the grid order has not been changed), and after the addItem()
     * method. In this last case with parameter showInfoMsg = false not to show the result in a warning div, since
     * the result is the addItem() result.
     * It is not called after removing elements from the grid, it is not necessary.
     *
     * @param showInfoMsg
     */
    function updateDisplayOrder(showInfoMsg = true)   {

        let newOrder = [];

        grid.getItems().forEach(function (item, i) {
            newOrder.push([item.getElement().getAttribute('data-banner-id', i + 1), item.getElement().getAttribute('data-pos', i + 1)]);
        });

        // Post to the server to handle the changes.
        $.ajax({
            type: "POST",
            url: endPointServer,
            data: {
                data: newOrder,
                action: 'update_display_order'
            },
            beforeSend: function() {
                // Disable dragging
            },
            success: function(data, status) {
                // Parse json string response into object:
                try {
                    obj = JSON && JSON.parse(data) || $.parseJSON(data);
                    showInfoMsg ? showWarningMessage(obj.message, "info") : null;
                } catch  (error)   {
                    showWarningMessage(data, "error");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                e.preventDefault();
                showWarningMessage(xhr.status, thrownError);
            }
        });

    }



    /**
     * JS method to see if an element matches the corresponding css selector.
     *
     * @param element
     * @param selector
     * @returns {boolean | *}
     */
    function elementMatches(element, selector) {

        var p = Element.prototype;
        return (p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector).call(element, selector);

    }

    /**
     * Finds the closet html element to the given html element that matches the given css selector.
     *
     * @param element
     * @param selector
     * @returns {*}
     */
    function elementClosest(element, selector) {

        if (window.Element && !Element.prototype.closest) {
            var isMatch = elementMatches(element, selector);
            while (!isMatch && element && element !== document) {
                element = element.parentNode;
                isMatch = element && element !== document && elementMatches(element, selector);
            }
            return element && element !== document ? element : null;
        }
        else {
            return element.closest(selector);
        }

    }
