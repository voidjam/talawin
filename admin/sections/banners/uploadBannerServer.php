<?php


/**
 * THIS ENDPOINT RECEIVES REQUEST FROM sections/banners/index.php AND HANDLES THEM.
 *
 * ACTIONS:
 *
 * + BANNER_FORM_ACTION_SAVE     --> Adds a new banner to database and store the banner image files on file system.
 * + BANNER_FORM_ACTION_UPDATE
 * + BANNER_ACTION_SET_IS_SHOWN
 * + BANNER_ACTION_DELETE
 * + BANNER_ACTION_UPDATE_DISPLAY_ORDER
 *
 */


$rootpath = "./../../../";

// Get connection to database.
include $rootpath . 'admin/scripts/connect.php';
// Constants file:
include $rootpath . 'admin/inc/constants.php';
// PHP Functions file:
include $rootpath . 'admin/scripts/functions.php';


$method = $_SERVER["REQUEST_METHOD"];


/** ACTION BANNER_FORM_ACTION_SAVE */
// Upload a new client logo to the both file system and database..
if ($method == "POST" && strcmp( $_POST['action'], BANNER_FORM_ACTION_SAVE) == 0) {



    // First we will save the new banner on database. We need the BannerId for the folder structure.
    $homeBannerDAO = new HomeBannerDAO();
    $homeBannerDTO = new HomeBannerDTO();

    $homeBannerDTO->setIsShown(true);
    $homeBannerDTO->setDisplayOrder(2); // Default display order: Second
    $homeBannerDTO->setId($homeBannerDAO->saveHomeBanner($homeBannerDTO));


    // Set path to save the files related to the new home banner.
    $target_dir = $rootpath."admin/img/banners/".$homeBannerDTO->getId()."/";
    $homeBannerDTO->setPath("admin/img/banners/".$homeBannerDTO->getId()."/");
    if (!is_dir($target_dir)){
        mkdir($target_dir, 0777, true);
    }


    // ITERATE ALL 4 FILES TO SAVE THEM BOTH ON FILE SYSTEM AND THE CORRESPONDING INFO ON DATABASE.
    $uploadedFiles = 0;
    foreach ($_FILES as $key => $value) {

        $uploadOk = 1;
        $result = "";
        $imageFileType = strtolower(pathinfo($_FILES[$key]["name"],PATHINFO_EXTENSION));
        $target_file = $target_dir . strval($key) . "." . strval($imageFileType);


        ////////////////////////////////////
        // CHECK POSSIBLE ERRORS ON FILES //
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES[$key]["tmp_name"]);
            if($check !== false) {
                $result.= "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                $result.= "File is not an image.";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            $result.= "Sorry, file already exists. ";
            $uploadOk = 0;
        }
        // Check if file size exceeds 8Mb
        if ($_FILES[$key]["size"] > 8000000) {
            $result.= "Sorry, your file is too large. ";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" ) {
            $result.= "Sorry, only JPG, JPEG, PNG files are allowed. ";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0 || $_FILES[$key]["error"] != UPLOAD_ERR_OK) {
            $result.= "Sorry, your file was not uploaded... ";

        }
        ////////////////////////////////////

        if ($uploadOk == 1 || $_FILES[$key]["error"] == UPLOAD_ERR_OK)  {

            // Here we save all files on file system and update the $homeBannerDTO on database.
            if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_file)) {
                if (strcmp( $key, 'lap_es_file') == 0) {
                    $homeBannerDTO->setLapEsFileName(basename($target_file));
                } elseif (strcmp( $key, 'lap_en_file') == 0) {
                    $homeBannerDTO->setLapEnFileName(basename($target_file));
                } elseif (strcmp( $key, 'mob_es_file') == 0) {
                    $homeBannerDTO->setMobEsFileName(basename($target_file));
                } elseif (strcmp( $key, 'mob_en_file') == 0) {
                    $homeBannerDTO->setMobEnFileName(basename($target_file));
                }
                $uploadedFiles++;
            }
        }
    } // End of foreach.

    $uploadOk == 1 && $homeBannerDAO->saveHomeBanner($homeBannerDTO) > 0 && $uploadedFiles == 4
        ? $result = json_encode(array("success" => true,
        "message" => "Success: El banner fue correctamente guardado.",
        "newHomeBanner" => $homeBannerDTO))
        : $result = json_encode(array("success" => false, "message" => "Failed: ".$result));

    header("Content-Type: text/plain");
    echo $result;
    exit;
}

/** ACTION BANNER_FORM_ACTION_UPDATE */
else if ($method == "POST" && strcmp( $_POST['action'], BANNER_FORM_ACTION_UPDATE) == 0)  {

    // Set path to save the files related to the new home banner.
    $target_dir = $rootpath."admin/img/banners/".$_POST['banner_id']."/";


    // ITERATE ALL FILES TO SAVE THEM BOTH ON FILE SYSTEM AND THE CORRESPONDING INFO ON DATABASE.
    // WHEN UPDATING MAYBE ONLY 1 FILE WAS SENT.
    $uploadedFiles = 0;
    foreach ($_FILES as $key => $value) {

        if ($_FILES[$key]["error"] == UPLOAD_ERR_OK)   {

            $uploadOk = 1;
            $result = "";
            $imageFileType = strtolower(pathinfo($_FILES[$key]["name"],PATHINFO_EXTENSION));
            $target_file = $target_dir . strval($key) . "." . strval($imageFileType);

            ////////////////////////////////////
            // FILES VALIDATIONS ///////////////
            // Check if image file is a actual image or fake image
            if(isset($_POST["submit"])) {
                $check = getimagesize($_FILES[$key]["tmp_name"]);
                if($check !== false) {
                    $result.= "File is an image - " . $check["mime"] . ".";
                    $uploadOk = 1;
                } else {
                    $result.= "File is not an image.";
                    $uploadOk = 0;
                }
            }
            // Check if file size exceeds 8Mb
            if ($_FILES[$key]["size"] > 8000000) {
                $result.= "Sorry, your file is too large. ";
                $uploadOk = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" ) {
                $result.= "Sorry, only JPG, JPEG, PNG files are allowed. ";
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0 || $_FILES[$key]["error"] != UPLOAD_ERR_OK) {
                $result.= "Sorry, your file was not uploaded... ";

            }
            ////////////////////////////////////


            // If everything is ok to upload the image (all validations were passed).
            if ($uploadOk == 1)  {
                // Delete the picture that was already on file system to substitute.
                $file_pattern = $target_dir.$key.".*";
                array_map( "unlink", glob( $file_pattern ) );

                // Here we save all files on file system.
                if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_file)) {
                    ++$uploadedFiles;
                } else {
                    $result.= "Hubo un problema con alguna de las imágenes. Por favor inténtelo de nuevo. ";
                }
            }
        }
    } // End of foreach loop.

    $uploadedFiles > 0
        ? $result = json_encode(array("success" => true,
        "message" => "Success: ". $uploadedFiles ." imágenes fueron correctamente sustituídas.",
        "updatedHomeBanner" => (new HomeBannerDAO())->getHomeBannerById($_POST['banner_id'])))
        : $result = json_encode(array("success" => false, "message" => "Error: ".$result));

    header("Content-Type: text/plain");
    echo $result;
    exit;
}
/** ACTION BANNER_ACTION_SET_IS_SHOWN */
else if ($method == "POST" && strcmp( $_POST['action'], BANNER_ACTION_SET_IS_SHOWN) == 0)  {

    header("Content-Type: text/plain");
    $homeBannerDAO = new HomeBannerDAO();
    if ($homeBannerDAO->setIsShown($_POST['data'][0], $_POST['data'][1]) > 0)    {
        $result = json_encode(array("success" => true,
            "message" => "Success: Banner con id ".$_POST['data'][0]." display actializado a ".$_POST['data'][1]."."));
    } else {
        $result = json_encode(array("success" => false, "message" => "Database could not be updated"));
    }
    echo $result;
    exit;
}

/** ACTION BANNER_ACTION_DELETE */
else if ($method == "POST" && strcmp( $_POST['action'], BANNER_ACTION_DELETE) == 0)  {

    header("Content-Type: text/plain");
    $homeBannerDAO = new HomeBannerDAO();
    if ($homeBannerDAO->deleteBannerById($_POST['banner_id']) > 0)    {
        $result = json_encode(array("success" => true,
            "message" => "Success: El banner con id ".$_POST['banner_id']." ha sido borrado de la base de datos."));
    } else {
        $result = json_encode(array("success" => false, "message" => "Database could not be updated"));
    }
    echo $result;
    exit;
}
/** ACTION BANNER_ACTION_UPDATE_DISPLAY_ORDER */
else if ($method == "POST" && strcmp( $_POST['action'], BANNER_ACTION_UPDATE_DISPLAY_ORDER) == 0)  {

    header("Content-Type: text/plain");
    $homeBannerDAO = new HomeBannerDAO();

    foreach ($_POST['data'] as $valor) {
        if(!($homeBannerDAO->updateDisplayOrder($valor[0], $valor[1]) > 0))    {
            $result = json_encode(array("success" => false, "message" => "El orden de los elementos no pudo ser actualizado. Por favor, inténtelo de nuevo."));
        }
    }

    $result =  json_encode(array("success" => true,
        "message" => "El nuevo orden de los banners se ha guardado correctamente en base de datos."));
    echo $result;
    exit;
}

else {

    header("HTTP/1.0 405 Method Not Allowed");
}
?>