<?php

$rootpath = "./../../../";
$current_uri = "$_SERVER[REQUEST_URI]";

// Get connection to database.
include $rootpath . 'admin/scripts/connect.php';
include $rootpath . 'admin/inc/constants.php';
include $rootpath . 'admin/scripts/functions.php';


// If user is logged.
if (!empty($_SESSION["LOGGED"]) && $_SESSION["LOGGED"] == true)	{

    // Include Header.
    include $rootpath . 'admin/inc/header.php';
    include $rootpath . 'admin/inc/nav-bar.php';

    // Get all client_logos
    $homeBannerDAO = new HomeBannerDAO();
    $arrayHomeBannerDTO = $homeBannerDAO->getHomeBanners();

    // Set page title:
    $title = "Home Banners";

    ?>


    <!-- DIV containing the clients configuration options -->
    <div class="home-banner-content">

        <!-- ADD CLIENT Button -->
        <button class="btn btn-success add-home-banner-button" role="button" data-toggle="modal" data-target="#newBannerModal"
                title="Añadir un nuevo cliente">
            <span><strong>Añadir Banner</strong></span></button>

        <!-- ---------------------- -->
        <!-- FIRST ROW: MAIN BLOCK  -->
        <div class="row">

            <!-- --------------------------------- -->
            <!-- /FIRST COLUMN: CLIENTS LOGO CARDS -->
            <div class="col-sm" style="position: relative;">

                <!-- DIV header explaining what is ther inside -->
                <div class="home-banner-table-main-header">BANNERS DE LA SECCIÓN HOME</div>

                <!-- Horizontal separator -->
                <br><hr/>

                <!-- ---------------------- -->
                <!-- GRID OF BANNERS ------ -->
                <section class="banners-grid" data-alignment="left-top-fillgaps">
                    <div class="controls cf">
                        <!-- SEARCH FIELD -->
                        <div class="control search">
                            <div class="control-icon"></div>
                            <input class="control-field search-field form-control" type="text" name="search" placeholder="Search..." />
                        </div>

                    </div>
                    <div class="grid"></div>
                </section>
                <!-- /GRID OF BANNERS ----- -->
                <!-- ---------------------- -->
            </div>
            <!-- /FIRST COLUMN: CLIENTS LOGO CARDS -->
            <!-- --------------------------------- -->

        </div>
        <!-- /FIRST ROW: MAIN BLOCK -->
        <!-- ---------------------- -->

        <!-- ---------------------- -->
        <!-- NEW BANNERS MODAL ---- -->
        <div class="modal fade" id="newBannerModal" tabindex="-1" role="dialog" aria-labelledby="newBannerModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">

                <input type="checkbox" id="fullScrCheckbox" />
                <label class="unselectable" for="fullScrCheckbox" onclick title="Pantala Completa"><span aria-hidden="true">&#x2948;</span></label>

                <div class="modal-content">


                    <div class="modal-header">
                        <h5 class="modal-title unselectable" id="newBannerModalLabel">Añadir Nuevo Banner</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Cerrar la ventana...">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">

                        <!-- NEW BANNER FORM -->
                        <form id="new_banner_form" class="form-horizontal" action="<?php echo $rootpath?>admin/sections/banners/uploadBannerServer.php" method="post"  enctype="multipart/form-data">

                            <div class="new-banner-img-container">
                                <div class="row">
                                    <!-- LAP ES IMAGE FIELD -->
                                    <div class="form-group col-8">
                                        <div class="avatar-upload">
                                            <div class="avatar-edit">
                                                <input type='file' id="lap_es_file" name="lap_es_file" accept=".png, .jpg, .jpeg" required/>
                                                <label for="lap_es_file"></label>
                                            </div>
                                            <div class="avatar-preview" data-toggle="tooltip"  data-html="true"
                                                 title="Seleccionar la imagen para <b>PC</b> en <b>Español</b>.<br>Formato preferido: <b>16:9</b><br><span>Tamaño preferido: <b>1920x1080px</b></span>">
                                                <div class="imagePreview"></div>
                                                <div class="imageFlag es"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- MOB ES IMAGE FIELD -->
                                    <div class="form-group col-4">
                                        <div class="avatar-upload">
                                            <div class="avatar-edit" title="Seleccionar el logo del cliente.">
                                                <input type='file' id="mob_es_file" name="mob_es_file" accept=".png, .jpg, .jpeg" required/>
                                                <label for="mob_es_file"></label>
                                            </div>
                                            <div class="avatar-preview avatar-preview-mob" data-toggle="tooltip"  data-html="true"
                                                 title="Imagen para <b>Móvil</b> en <b>Español</b>.<br>Formato preferido: <b>9:16</b><br><span>Tamaño preferido: <b>1080x1920px</b></span>">
                                                <div class="imagePreview"></div>
                                                <div class="imageFlag es"></div>
                                            </div>
                                        </div>
                                    </div></div>
                                <div class="row">
                                    <!-- LAP EN IMAGE FIELD -->
                                    <div class="form-group col-8">
                                        <div class="avatar-upload">
                                            <div class="avatar-edit" title="Seleccionar el logo del cliente.">
                                                <input type='file' id="lap_en_file" name="lap_en_file" accept=".png, .jpg, .jpeg" required/>
                                                <label for="lap_en_file"></label>
                                            </div>
                                            <div class="avatar-preview" data-toggle="tooltip" data-html="true"
                                                 title="Seleccionar la imagen para <b>PC</b> en <b>Inglés</b>.<br>Formato preferido: <b>16:9</b><br><span>Tamaño preferido: <b>1920x1080px</b></span>">
                                                <div class="imagePreview"></div>
                                                <div class="imageFlag en"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- MOB EN IMAGE FIELD -->
                                    <div class="form-group col-4">
                                        <div class="avatar-upload">
                                            <div class="avatar-edit" title="Seleccionar el logo del cliente.">
                                                <input type='file' id="mob_en_file" name="mob_en_file" accept=".png, .jpg, .jpeg" required/>
                                                <label for="mob_en_file"></label>
                                            </div>
                                            <div class="avatar-preview avatar-preview-mob" data-toggle="tooltip" data-html="true"
                                                 title="Imagen para <b>Móvil</b> en <b>Inglés</b>.<br>Formato preferido: <b>9:16</b><br><span>Tamaño preferido: <b>1080x1920px</b></span>">
                                                <div class="imagePreview"></div>
                                                <div class="imageFlag en"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div></div>


                            <!-- ACTION FIELD -->
                            <input hidden name="action" id="action" value=""/>
                            <!-- BANNER ID FIELD -->
                            <input hidden name="banner_id" id="banner_id" value=""/>
                            <!-- SUBMIT HIDDEN BUTTON -->
                            <button type="submit" id="submitHiddenButton" style="display: none;"></button>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button id="newBannerModalSubmit" type="button" onclick="submitNewBannerForm('<?php echo BANNER_FORM_ACTION_SAVE ?>')" class="btn btn-primary"><b>Guardar el nuevo Banner</b></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /NEW BANNER MODAL ---- -->
        <!-- ---------------------- -->

        <!-- ---------------------- -->
        <!-- CONFIRM DELETE MODAL - -->
        <div class="modal fade" id="confirmDeleteModal" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="confirmDeleteModalLabel">ELIMINANDO BANNER:</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ¿Está seguro de que quiere eliminar el <b>banner</b>?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
                        <button id="confirmDeleteYesBtn" type="button" class="btn btn-primary">SÍ</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /CONFIRM DELETE MODAL  -->
        <!-- ---------------------- -->


    </div><!-- END OF client-logo-content -->


    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- SECTION PAGE INFO +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="page_section_info unselectable">
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION PAGE INFO TXT +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <span><?php echo $title ?></span>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- /SECTION PAGE INFO TXT ++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    </div>
    <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


    <!-- Drag & Drop jQuery pluggin: Muuri.js and its dependencies. -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/web-animations/2.3.1/web-animations.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.js" integrity="sha256-GMd3rFxMDNnM5JQEpiKLLl8kSrDuG5egqchk758z59g=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/muuri/0.5.4/muuri.js" integrity="sha256-CWldck8RM3iilMsXH/oW9iLdm/TcW3WcpRw09wL6MFg=" crossorigin="anonymous"></script>

    <!-- Custom JS for our clients grid -->
    <script src="<?php echo $rootpath; ?>admin/sections/banners/js/banners-grid.js"></script>

    <script>



        /**
         * This method triggers from submit throw JS. If we are saving a new banner, instead of using
         * $form.submit() it triggers a click on submit button so the validations are run before submitting.
         * If we are updating the pictures of an existing banner, it will only check that at least 1 picture
         * was changed.
         */
        function submitNewBannerForm(action)   {

            $("form#new_banner_form input#action").val(action);
            // If saving a new banner we trigger click on submit hidden button so validations are run.
            if (action === '<?php echo BANNER_FORM_ACTION_SAVE ?>') {
                $("form#new_banner_form #submitHiddenButton").trigger("click");
            // If updating existing banner
            } else if ( action === '<?php echo BANNER_FORM_ACTION_UPDATE ?>')    {

                let arrayOfVals = [];
                // Get the array of files input values to check if any of them was changed
                // and clean the empty elements from the values array.
                $("form#new_banner_form input[type='file']").each(function(){
                    if( $(this).val() &&  $(this).val() != '') {
                        arrayOfVals.push($(this).val());
                    }
                });
                if (arrayOfVals.length > 0) {
                    $('form#new_banner_form').submit();
                } else {
                    showWarningMessage("No se detectó ningún cambio...:)", "info");
                }
            }
        }

        /**
         * This method appends a warning div to #page_container containing a message for the user.
         *
         * @param message Message to be presented on screen.
         * @param error_level ['info', 'error', 'warning'] Sets the color of the warning div.
         */
        function showWarningMessage(message, error_level) {

            // Check if there is already an allert message on screen and close it.
            if($('.alert').length) { $(".alert").alert("close"); }

            $('#page_container').append('<div id=\"general-warning-div\" class=\"alert fade show\" role=\"alert\">'
                + '<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>'
                + '<p><strong>' + message + '</strong></p></div>');

            $(".alert").fadeTo(5000, 1, "swing").fadeOut(800, function () {
                $(".alert").alert("close");
            });

            if (error_level.localeCompare('info') === 0) {
                $(".alert").addClass("alert-success");
            } else if (error_level.localeCompare('error') === 0) {
                $(".alert").addClass("alert-danger");
            } else if (error_level.localeCompare('warning') === 0) {
                $(".alert").addClass("alert-warning");
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // DOCUMENT READY BLOCK ////////////////////////////////////////////////////////////////////////////////////////
        $(document).ready(function() {

            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // MANAGE NEW BANNER FORM IMAGES PREVIEW WHEN IMG IS SELECTED /////////////////////////////////////////
            // Showing logo thumbnail when creating a new banner on the #newBannerModal dialog.
            function readURL(input) {
                if (input.files && input.files[0]) {

                    // Check whether browser fully supports all File API to check file size.
                    if (window.File && window.FileReader && window.FileList && window.Blob) {
                        // Get the file size and file type from file input field
                        let fsize = input.files[0].size;
                        // Do something if file size more than 2 mb (1 mb = 1048576 bytes)
                        if(fsize > 2*1048576) showWarningMessage("File is a bit too heavy for web use: " + bytesToSize(fsize) + "<br>You can still upload it anyway...", "warning") ;
                    }

                    let reader = new FileReader();
                    let $imgPreview = $(input).closest(".avatar-upload").find(".imagePreview");
                    reader.onload = function(e) {

                        $imgPreview.css('background-image', 'url('+ e.target.result  +')');
                        $imgPreview.hide();
                        $imgPreview.fadeIn(250);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(".imagePreview").on("click", function () {
                let $input = $(this).closest(".avatar-upload").find("input");
                $input.trigger("click");
                $input.on("change",function() {
                    readURL(this);
                    $(this).off("change");
                });
            });


            function bytesToSize(bytes) {
                let sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                if (bytes == 0) return '0 Byte';
                let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
            };
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////



            // Start Grid of Logos using muuri.js
            loadBannersGrid((<?php echo json_encode($arrayHomeBannerDTO) ?>), "<?php echo $rootpath; ?>admin/sections/banners/uploadBannerServer.php", "<?php echo $rootpath; ?>");



            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // NEW BANNER FORM AJAX SUBMIT ////////////////////////////////////////////////////////////////////////
            // Prepares the form to prevent onSubmit default and to be sent via ajax.
            $('form#new_banner_form')
                .on('submit', function(e) {
                    // Prevent form submission
                    e.preventDefault();

                    // Close the modal dialog:
                    $('#newBannerModal').modal('hide');

                    let form = $('form#new_banner_form')[0];
                    let data = new FormData(form);

                    $.ajax({
                        url: form.getAttribute("action"),
                        enctype: 'multipart/form-data',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function(data, status) {
                            // Parse json string response into object:
                            try {
                                // Show info message to user on UI.
                                obj = JSON && JSON.parse(data) || $.parseJSON(data);
                                if (obj.success === true)   {
                                    showWarningMessage(obj.message, "info");
                                    // Add element from grid and update indices, both on layout and database.
                                    if (obj.newHomeBanner) addItem(obj.newHomeBanner);
                                    if (obj.updatedHomeBanner) {

                                        // console.log(obj.updatedHomeBanner.display_order);
                                        grid.remove(obj.updatedHomeBanner.display_order - 1, {removeElements: true, layout: 'instant'});
                                        addItem(obj.updatedHomeBanner);
                                    }

                                } else {
                                    showWarningMessage(obj.message, "error");
                                }

                            } catch  (error)   {
                                showWarningMessage("Error: " + error, "error");
                                console.log(error);
                            }
                        }
                    });
                })
                .on("reset", function () {
                });
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////


            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // Reset #newBannerModal form on modal hidden event. //////////////////////////////////////////////////
            $('#newBannerModal').on('hidden.bs.modal', function () {
                $('.imagePreview').css( 'background-image', '');
                $("#newBannerModalLabel").html("Añadir Nuevo Banner");
                $("#newBannerModalSubmit").html("Guardar el Nuevo Banner");
                $("#newBannerModalSubmit").attr("onclick", "submitNewBannerForm('<?php echo BANNER_FORM_ACTION_SAVE ?>')");
                // Reset full-screen mode.
                if ($("#fullScrCheckbox").prop("checked")) $("#fullScrCheckbox").click();
                // Reset the banner immage tooltips if they have been changed after editing a banner.
                $(".avatar-preview").each(function () {
                    let addedInfoPos = $(this).attr("data-original-title").lastIndexOf("<added-info>");
                    if (addedInfoPos > 0) $(this).attr("data-original-title", $(this).attr("data-original-title").substring(0, addedInfoPos));
                });
                // Reset the input values if they have been set.
                $('#newBannerModal').find("input[type=file]").val("");

            });
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////



            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // INIT TOOLTIPS //////////////////////////////////////////////////////////////////////////////////////
            // Use tooltip Bootstrap4 to customize the onHover message of the checkbox #is_published.
            $('[data-toggle="tooltip"]').tooltip({
                offset: '3,-85',
                delay: { 'show': 100, 'hide': 100 },
                html: true,
                template: '<div class="tooltip tooltip-banners" role="tooltip"><div class="tooltip-inner"></div></div>' // Default template
            });
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////


            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // FULL SCREEN MODE BUTTON ////////////////////////////////////////////////////////////////////////////
            $("#fullScrCheckbox").click(function() {
                // When checked.
                if ($(this).prop("checked"))    {
                    $("#newBannerModal").addClass("expanded");
                } else {
                    $("#newBannerModal").removeClass("expanded");
                }
                $('#newBannerModal').modal('handleUpdate');
            });
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////

        });
        // ENF OF DOCUMENT READY BLOCK /////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    </script>

    <?php

    // Check for errors and warnings:
    if(isset($_SESSION['error']) && is_array($_SESSION['error']))   include $rootpath . 'admin/inc/warning.php';
    // Include Footer.
    include $rootpath . 'admin/inc/footer.php';

// If user is not logged -> Back to loggin page.
} else 	{
    header("Location:".$rootpath."admin/login.php");
    exit;
}