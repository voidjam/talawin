<?php



class Printer3dDTO {

    /** @var integer */
    public $id;  /* PRIMARY KEY */

    // General features.
    /** @var string */
    public $display_order;
    /** @var integer */
    public $brand_id; /* FOREIGN KEY */
    /** @var string */
    public $model;
    /** @var bool */
    public $is_new;
    /** @var bool */
    public $is_published;
    /** @var bool */
    public $is_release;


    // Private fields (not to show on website).
    /** @var string */
    public $ref;
    /** @var float */
    public $price_pvp;
    /** @var float */
    public $price_cost;
    /** @var string */
    public $comments;

    // Specific fields for outlet printers.
    /** @var string */
    public $production_date;
    /** @var integer */
    public $warranty;
    /** @var bool */
    public $is_sold;

    // Tech Specifications.
    /** @var integer */
    public $feat_ink_id; /* FOREIGN KEY */
    /** @var integer */
    public $feat_media_type_id; /* FOREIGN KEY */
    /** @var string */
    public $feat_num_headers;
    /** @var string */
    public $feat_max_print_vol;
    /** @var string */
    public $feat_quality;
    /** @var string */
    public $feat_software;
    /** @var string */
    public $feat_print_speed;


    // Fields to build the printers html page.
    /** @var string */
    public $title;
    /** @var string */
    public $headline;
    /** @var string */
    public $description;
    /** @var string */
    public $html_overview;
    /** @var string */
    public $html_features;
    /** @var string */
    public $html_specs;

    /** @var string */
    public $updated_at;
    /** @var string */
    public $created_at;


    /** @var array */
    public $apps_array;
    /** @var array --> An array containing every file related to the printer, ordered by is_main */
    public $files_array;

    /** @var Printer3dTransDTO */
    public $printer3d_trans_DTO;




    /** Class constructor from JSON */
    public function __construct($message_json = false) {
        if ($message_json) $this->set(json_decode($message_json));
        return $this;
    }

    // JSon to this->object Map
    public function set($data) {
        foreach ($data AS $key => $value) {
            /*if (is_array($value)) {
                $sub = new JSONObject();
                $sub->set($value);
                $value = $sub;
            }*/
            // Check if json does not have any property that is not one of the PrinterDTO properties.
            if (property_exists("Printer3dDTO" , $key)) $this->{$key} = $value;
        }
    }

    /**
     * Returns an empty new instance if Printer3dDTO class.
     *
     * @return Printer3dDTO
     */
    public static function createPrinter3dDTO()   {
        $printer3dDTO = new Printer3dDTO();
        return $printer3dDTO;
    }



    /*
     *
     */
    public function addApp($printer3dApplicationDTO)    {
        array_push($this->apps_array, $printer3dApplicationDTO);
    }

    /**
     * This method returns a string containing the ids of the applications within $apps_array formatted for
     * imgCheckBoxes to preselect then. For example, for a Printer3dDTO instance containing applications with
     * ids 1, 2 and 3, it will return the string '[0,1,2]'.
     *
     */
    public function getPreselectAppsString() {

        $apps_ids_array = array_values(array_column(json_decode(json_encode($this->getAppsArray()), true), 'id'));
        foreach ($apps_ids_array as &$valor) { $valor = $valor - 1;}
        return '['.implode(',', $apps_ids_array).']';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDisplayOrder()
    {
        return $this->display_order;
    }

    /**
     * @param string $display_order
     */
    public function setDisplayOrder($display_order)
    {
        $this->display_order = $display_order;
    }

    /**
     * @return int
     */
    public function getBrandId()
    {
        return $this->brand_id;
    }

    /**
     * @param int $brand_id
     */
    public function setBrandId($brand_id)
    {
        $this->brand_id = $brand_id;
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param string $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return $this->is_new;
    }

    /**
     * @param bool $is_new
     */
    public function setIsNew($is_new)
    {
        $this->is_new = $is_new;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->is_published;
    }

    /**
     * @param bool $is_published
     */
    public function setIsPublished($is_published)
    {
        $this->is_published = $is_published;
    }

    /**
     * @return bool
     */
    public function isRelease()
    {
        return $this->is_release;
    }

    /**
     * @param bool $is_release
     */
    public function setIsRelease($is_release)
    {
        $this->is_release = $is_release;
    }

    /**
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param string $ref
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    /**
     * @return float
     */
    public function getPricePvp()
    {
        return $this->price_pvp;
    }

    /**
     * @param float $price_pvp
     */
    public function setPricePvp($price_pvp)
    {
        $this->price_pvp = $price_pvp;
    }

    /**
     * @return float
     */
    public function getPriceCost()
    {
        return $this->price_cost;
    }

    /**
     * @param float $price_cost
     */
    public function setPriceCost($price_cost)
    {
        $this->price_cost = $price_cost;
    }

    /**
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return string
     */
    public function getProductionDate()
    {
        return $this->production_date;
    }

    /**
     * @param string $production_date
     */
    public function setProductionDate($production_date)
    {
        $this->production_date = $production_date;
    }

    /**
     * @return int
     */
    public function getWarranty()
    {
        return $this->warranty;
    }

    /**
     * @param int $warranty
     */
    public function setWarranty($warranty)
    {
        $this->warranty = $warranty;
    }

    /**
     * @return bool
     */
    public function isSold()
    {
        return $this->is_sold;
    }

    /**
     * @param bool $is_sold
     */
    public function setIsSold($is_sold)
    {
        $this->is_sold = $is_sold;
    }

    /**
     * @return int
     */
    public function getFeatInkId()
    {
        return $this->feat_ink_id;
    }

    /**
     * @param int $feat_ink_id
     */
    public function setFeatInkId($feat_ink_id)
    {
        $this->feat_ink_id = $feat_ink_id;
    }

    /**
     * @return int
     */
    public function getFeatMediaTypeId()
    {
        return $this->feat_media_type_id;
    }

    /**
     * @param int $feat_media_type_id
     */
    public function setFeatMediaTypeId($feat_media_type_id)
    {
        $this->feat_media_type_id = $feat_media_type_id;
    }

    /**
     * @return string
     */
    public function getFeatNumHeaders()
    {
        return $this->feat_num_headers;
    }

    /**
     * @param string $feat_num_headers
     */
    public function setFeatNumHeaders($feat_num_headers)
    {
        $this->feat_num_headers = $feat_num_headers;
    }

    /**
     * @return string
     */
    public function getFeatMaxPrintVol()
    {
        return $this->feat_max_print_vol;
    }

    /**
     * @param string $feat_max_print_vol
     */
    public function setFeatMaxPrintVol($feat_max_print_vol)
    {
        $this->feat_max_print_vol = $feat_max_print_vol;
    }

    /**
     * @return string
     */
    public function getFeatQuality()
    {
        return $this->feat_quality;
    }

    /**
     * @param string $feat_quality
     */
    public function setFeatQuality($feat_quality)
    {
        $this->feat_quality = $feat_quality;
    }

    /**
     * @return string
     */
    public function getFeatSoftware()
    {
        return $this->feat_software;
    }

    /**
     * @param string $feat_software
     */
    public function setFeatSoftware($feat_software)
    {
        $this->feat_software = $feat_software;
    }

    /**
     * @return string
     */
    public function getFeatPrintSpeed()
    {
        return $this->feat_print_speed;
    }

    /**
     * @param string $feat_software
     */
    public function setFeatPrintSpeed($feat_print_speed)
    {
        $this->feat_print_speed = $feat_print_speed;
    }



    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getHeadline()
    {
        return $this->headline;
    }

    /**
     * @param string $headline
     */
    public function setHeadline($headline)
    {
        $this->headline = $headline;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getHtmlOverview()
    {
        return $this->html_overview;
    }

    /**
     * @param string $html_overview
     */
    public function setHtmlOverview($html_overview)
    {
        $this->html_overview = $html_overview;
    }

    /**
     * @return string
     */
    public function getHtmlFeatures()
    {
        return $this->html_features;
    }

    /**
     * @param string $html_features
     */
    public function setHtmlFeatures($html_features)
    {
        $this->html_features = $html_features;
    }

    /**
     * @return string
     */
    public function getHtmlSpecs()
    {
        return $this->html_specs;
    }

    /**
     * @param string $html_specs
     */
    public function setHtmlSpecs($html_specs)
    {
        $this->html_specs = $html_specs;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return array
     */
    public function getAppsArray()
    {
        return $this->apps_array;
    }

    /**
     * @param array $apps_array
     */
    public function setAppsArray($apps_array)
    {
        $this->apps_array = $apps_array;
    }

    /**
     * @return array
     */
    public function getFilesArray()
    {
        return $this->files_array;
    }

    /**
     * @param array $files_array
     */
    public function setFilesArray($files_array)
    {
        $this->files_array = $files_array;
    }

    /**
     * @return Printer3dTransDTO
     */
    public function getPrinter3dTransDTO()
    {
        return $this->printer3d_trans_DTO;
    }

    /**
     * @param Printer3dTransDTO $printer3d_trans_DTO
     */
    public function setPrinter3dTransDTO($printer3d_trans_DTO)
    {
        $this->printer3d_trans_DTO = $printer3d_trans_DTO;
    }








}?>