<?php



class AdminUserDTO {


    /** @var integer */
        public $id;

    /** @var string */
        public $email;

    /** @var string */
        public $name;
 
    /** @var string */
        public $password;

    /** @var string */
        public $created_at;




    /** Class constructor from JSON */
    public function __construct($message_json = false) {
        if ($message_json) $this->set(json_decode($message_json));
        return $this;
    }

    // JSon to this->object Map
    public function set($data) {
        foreach ($data AS $key => $value) {
            /*if (is_array($value)) {
                $sub = new JSONObject();
                $sub->set($value);
                $value = $sub;
            }*/
            $this->{$key} = $value;
        }
    }

    public static function createUserAdminDTO($userId, $userEmail, $userName, $userPass)   {

        $userAdminDTO = new UserAdminDTO();
        $userAdminDTO->setId($userId);
        $userAdminDTO->setEmail($userEmail);
        $userAdminDTO->setName($userName);
        $userAdminDTO->setPassword($userPass);
        


        return $userAdminDTO;

    }

    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }

    function setEmail($email) { $this->email = $email; }
    function getEmail() { return $this->email; }

    function setName($name) { $this->name = $name; }
    function getName() { return $this->name; }

    function setPassword($password) { $this->password = $password; }
    function getPassword() { return $this->password; }

    public function getCreatedAt(){return $this->created_at;}
    public function setCreatedAt($created_at){$this->created_at = $created_at;}



}?>