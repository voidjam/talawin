<?php



class Printer3dVideoDTO {


    /** @var integer */
    public $id;

    /** @var string */
    public $youtube_id;

    /** @var string */
    public $printer3d_id;

    /** @var string */
    public $created_at;

    /** @var string */
    public $updated_at;




    /** Class constructor from JSON */
    public function __construct($message_json = false) {
        if ($message_json) $this->set(json_decode($message_json));
        return $this;
    }

    // JSon to this->object Map
    public function set($data) {
        foreach ($data AS $key => $value) {
            /*if (is_array($value)) {
                $sub = new JSONObject();
                $sub->set($value);
                $value = $sub;
            }*/
            $this->{$key} = $value;
        }
    }

    public static function createPrinter3dVideoDTO($videoId, $youtube_id, $printer3d_id)   {

        $printer3dVideoDTO = new Printer3dVideoDTO();
        $printer3dVideoDTO->setId($videoId);
        $printer3dVideoDTO->setYoutubeId($youtube_id);
        $printer3dVideoDTO->setPrinterId($printer3d_id);


        return $printer3dVideoDTO;

    }

    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }

    /**
     * @return string
     */
    public function getYoutubeId()
    {
        return $this->youtube_id;
    }

    /**
     * @param string $youtube_id
     */
    public function setYoutubeId($youtube_id)
    {
        $this->youtube_id = $youtube_id;
    }

    /**
     * @return string
     */
    public function getPrinter3dId()
    {
        return $this->printer3d_id;
    }

    /**
     * @param string $printer3d_id
     */
    public function setPrinter3dId($printer3d_id)
    {
        $this->printer3d_id = $printer3d_id;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }


}?>