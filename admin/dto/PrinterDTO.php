<?php



class PrinterDTO {

    /** @var integer */
    public $id;  /* PRIMARY KEY */

    // General features.
    /** @var integer */
    public $brand_id;/* FOREIGN KEY */
    /** @var integer */
    public $sub_brand_id; /* FOREIGN KEY */
    /** @var string */
    public $display_order;
    /** @var string */
    public $model;
    /** @var bool */
    public $is_new;
    /** @var bool */
    public $is_published;
    /** @var bool */
    public $is_release;


    // Private fields (not to show on website).
    /** @var string */
    public $ref;
    /** @var float */
    public $price_pvp;
    /** @var float */
    public $price_cost;
    /** @var string */
    public $comments;

    // Specific fields for outlet printers.
    /** @var string */
    public $production_date;
    /** @var integer */
    public $warranty;
    /** @var bool */
    public $is_sold;

    // Tech Specifications.
    /** @var integer */
    public $feat_ink_id; /* FOREIGN KEY */
    /** @var integer */
    public $feat_white_ink_id; /* FOREIGN KEY */
    /** @var integer */
    public $feat_media_type_id; /* FOREIGN KEY */
    /** @var float */
    public $feat_max_print_width;
    /** @var float */
    public $feat_max_media_thick;
    /** @var float */
    public $feat_drop_size;
    /** @var float */
    public $feat_resolution;
    /** @var float */
    public $feat_max_speed;

    // Fields to build the printers html page.
    /** @var string */
    public $title;
    /** @var string */
    public $headline;
    /** @var string */
    public $description;
    /** @var string */
    public $html_overview;
    /** @var string */
    public $html_features;
    /** @var string */
    public $html_specs;

    /** @var string */
    public $updated_at;
    /** @var string */
    public $created_at;


    /** @var array */
    public $apps_array;
    /** @var array --> An array containing every file related to the printer, ordered by is_main */
    public $files_array;

    /** @var PrinterTransDTO */
    public $printer_trans_DTO;




    /** Class constructor from JSON */
    public function __construct($message_json = false) {
        if ($message_json) $this->set(json_decode($message_json));
        return $this;
    }

    // JSon to this->object Map
    public function set($data) {
        foreach ($data AS $key => $value) {
            /*if (is_array($value)) {
                $sub = new JSONObject();
                $sub->set($value);
                $value = $sub;
            }*/
            // Check if json does not have any property that is not ont of the PrinterDTO properties.
            if (property_exists("PrinterDTO" , $key)) $this->{$key} = $value;
        }
    }

    /**
     * Returns an empty new instance if PrinterDTO class.
     *
     * @return PrinterDTO
     */
    public static function createPrinterDTO()   {
        $printerDTO = new PrinterDTO();
        return $printerDTO;
    }



    /*
     *
     */
    public function addApp($applicationDTO)    {
        array_push($this->apps_array, $applicationDTO);
    }

    /**
     * This method returns a string containing the ids of the applications within $apps_array formatted for
     * imgCheckBoxes to preselect then. For example, for a PrinterDTO instance containing applications with
     * ids 1, 2 and 3, it will return the string '[0,1,2]'.
     *
     */
    public function getPreselectString() {

        $apps_ids_array = array_values(array_column(json_decode(json_encode($this->getAppsArray()), true), 'id'));
        foreach ($apps_ids_array as &$valor) { $valor = $valor - 1;}
        return '['.implode(',', $apps_ids_array).']';
    }



    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDisplayOrder()
    {
        return $this->display_order;
    }

    /**
     * @param string $display_order
     */
    public function setDisplayOrder($display_order)
    {
        $this->display_order = $display_order;
    }

    /**
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param string $ref
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    /**
     * @return int
     */
    public function getBrandId()
    {
        return $this->brand_id;
    }

    /**
     * @param int $brand_id
     */
    public function setBrandId($brand_id)
    {
        $this->brand_id = $brand_id;
    }

    /**
     * @return int
     */
    public function getSubBrandId()
    {
        return $this->sub_brand_id;
    }

    /**
     * @param int $sub_brand_id
     */
    public function setSubBrandId($sub_brand_id)
    {
        $this->sub_brand_id = $sub_brand_id;
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param string $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return float
     */
    public function getPricePvp()
    {
        return $this->price_pvp;
    }

    /**
     * @param float $price_pvp
     */
    public function setPricePvp($price_pvp)
    {
        $this->price_pvp = $price_pvp;
    }

    /**
     * @return float
     */
    public function getPriceCost()
    {
        return $this->price_cost;
    }

    /**
     * @param float $price_cost
     */
    public function setPriceCost($price_cost)
    {
        $this->price_cost = $price_cost;
    }

    /**
     * @return bool
     */
    public function getIsNew()
    {
        return $this->is_new;
    }

    /**
     * @return string
     */
    public function getProductionDate()
    {
        return $this->production_date;
    }

    /**
     * @param string $production_date
     */
    public function setProductionDate($production_date)
    {
        $this->production_date = $production_date;
    }

    /**
     * @return int
     */
    public function getWarranty()
    {
        return $this->warranty;
    }

    /**
     * @param int $warranty
     */
    public function setWarranty($warranty)
    {
        $this->warranty = $warranty;
    }

    /**
     * @return bool
     */
    public function isSold()
    {
        return $this->is_sold;
    }

    /**
     * @param bool $is_sold
     */
    public function setIsSold($is_sold)
    {
        $this->is_sold = $is_sold;
    }

    /**
     * @param bool $is_new
     */
    public function setIsNew($is_new)
    {
        $this->is_new = $is_new;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getHeadline()
    {
        return $this->headline;
    }

    /**
     * @param string $headline
     */
    public function setHeadline($headline)
    {
        $this->headline = $headline;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return integer
     */
    public function getFeatInkId()
    {
        return $this->feat_ink_id;
    }

    /**
     * @param integer $feat_ink_id
     */
    public function setFeatInkId($feat_ink_id)
    {
        $this->feat_ink_id = $feat_ink_id;
    }

    /**
     * @return integer
     */
    public function getFeatWhiteInkId()
    {
        return $this->feat_white_ink_id;
    }

    /**
     * @param integer $feat_white_ink_id
     */
    public function setFeatWhiteInkId($feat_white_ink_id)
    {
        $this->feat_white_ink_id = $feat_white_ink_id;
    }

    /**
     * @return integer
     */
    public function getFeatMediaTypeId()
    {
        return $this->feat_media_type_id;
    }

    /**
     * @param integer $feat_media_type_id
     */
    public function setFeatMediaTypeId($feat_media_type_id)
    {
        $this->feat_media_type_id = $feat_media_type_id;
    }

    /**
     * @return float
     */
    public function getFeatMaxPrintWidth()
    {
        return $this->feat_max_print_width;
    }

    /**
     * @param float $feat_max_print_width
     */
    public function setFeatMaxPrintWidth($feat_max_print_width)
    {
        $this->feat_max_print_width = $feat_max_print_width;
    }

    /**
     * @return float
     */
    public function getFeatMaxMediaThick()
    {
        return $this->feat_max_media_thick;
    }

    /**
     * @param float $feat_max_media_thick
     */
    public function setFeatMaxMediaThick($feat_max_media_thick)
    {
        $this->feat_max_media_thick = $feat_max_media_thick;
    }

    /**
     * @return float
     */
    public function getFeatDropSize()
    {
        return $this->feat_drop_size;
    }

    /**
     * @param float $feat_drop_size
     */
    public function setFeatDropSize($feat_drop_size)
    {
        $this->feat_drop_size = $feat_drop_size;
    }

    /**
     * @return float
     */
    public function getFeatResolution()
    {
        return $this->feat_resolution;
    }

    /**
     * @param float $feat_resolution
     */
    public function setFeatResolution($feat_resolution)
    {
        $this->feat_resolution = $feat_resolution;
    }

    /**
     * @return float
     */
    public function getFeatMaxSpeed()
    {
        return $this->feat_max_speed;
    }

    /**
     * @param float $feat_max_speed
     */
    public function setFeatMaxSpeed($feat_max_speed)
    {
        $this->feat_max_speed = $feat_max_speed;
    }

    /**
     * @return string
     */
    public function getHtmlOverview()
    {
        return $this->html_overview;
    }

    /**
     * @param string $html_overview
     */
    public function setHtmlOverview($html_overview)
    {
        $this->html_overview = $html_overview;
    }

    /**
     * @return string
     */
    public function getHtmlFeatures()
    {
        return $this->html_features;
    }

    /**
     * @param string $html_features
     */
    public function setHtmlFeatures($html_features)
    {
        $this->html_features = $html_features;
    }

    /**
     * @return string
     */
    public function getHtmlSpecs()
    {
        return $this->html_specs;
    }

    /**
     * @param string $html_specs
     */
    public function setHtmlSpecs($html_specs)
    {
        $this->html_specs = $html_specs;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return bool
     */
    public function getIsPublished()
    {
        return $this->is_published;
    }

    /**
     * @param bool $is_published
     */
    public function setIsPublished($is_published)
    {
        $this->is_published = $is_published;
    }

    /**
     * @return array
     */
    public function getAppsArray()
    {
        return $this->apps_array;
    }

    /**
     * @param array $apps_array
     */
    public function setAppsArray($apps_array)
    {
        $this->apps_array = $apps_array;
    }

    /**
     * @return array
     */
    public function getFilesArray()
    {
        return $this->files_array;
    }

    /**
     * @param array $files_array
     */
    public function setFilesArray($files_array)
    {
        $this->files_array = $files_array;
    }

    /**
     * @return bool
     */
    public function getIsRelease()
    {
        return $this->is_release;
    }

    /**
     * @param bool $is_release
     */
    public function setIsRelease($is_release)
    {
        $this->is_release = $is_release;
    }


    /**
     * @return PrinterTransDTO
     */
    public function getPrinterTransDTO()
    {
        return $this->printer_trans_DTO;
    }

    /**
     * @param PrinterTransDTO $printer_trans_DTO
     */
    public function setPrinterTransDTO($printer_trans_DTO)
    {
        $this->printer_trans_DTO = $printer_trans_DTO;
    }





}?>