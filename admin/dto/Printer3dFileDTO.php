<?php



class Printer3dFileDTO {


    /** @var integer */
    public $id;

    /** @var string */
    public $uuid;

    /** @var string */
    public $printer3d_id;

    /** @var string */
    public $name;

    /** @var string */
    public $mime_type;

    /** @var string */
    public $path;

    /** @var string */
    public $is_main;

    /** @var string */
    public $size;



    /**
     * This field is not on database.
     * @var string
     */
    public $thumbnailUrl;




    /** Class constructor from JSON */
    public function __construct($message_json = false) {
        if ($message_json) $this->set(json_decode($message_json));
        return $this;
    }

    // JSon to this->object Map
    public function set($data) {
        foreach ($data AS $key => $value) {
            /*if (is_array($value)) {
                $sub = new JSONObject();
                $sub->set($value);
                $value = $sub;
            }*/
            $this->{$key} = $value;
        }
    }

    public static function Printer3dFileDTO($fileId, $fileUuid, $fileName, $filePath, $fileType, $messageId, $fileIs_main = null, $fileSize = null)   {

        $printer3dFileDTO = new Printer3dFileDTO();
        $printer3dFileDTO->setId($fileId);
        $printer3dFileDTO->setUuid($fileUuid);
        $printer3dFileDTO->setName($fileName);
        $printer3dFileDTO->setPath($filePath);
        $printer3dFileDTO->setMime_type($fileType);
        $printer3dFileDTO->setPrinter_3d_id($messageId);
        $printer3dFileDTO->setIs_main($fileIs_main);
        $printer3dFileDTO->setSize($fileSize);

        return $printer3dFileDTO;

    }

    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }

    function setUuid($uuid) { $this->uuid = $uuid; }
    function getUuid() { return $this->uuid; }

    function setPrinter_3d_id($printer3d_id) { $this->printer3d_id = $printer3d_id; }
    function getPrinter_3d_id() { return $this->printer3d_id; }

    function setName($name) { $this->name = $name; }
    function getName() { return $this->name; }

    function setMime_type($mime_type) { $this->mime_type = $mime_type; }
    function getMime_type() { return $this->mime_type; }

    function setPath($path) { $this->path = $path; }
    function getPath() { return $this->path; }

    function setIs_main($is_main) { $this->is_main = $is_main; }
    function getIs_main() { return $this->is_main; }

    public function getSize(){return $this->size;}
    public function setSize($size){$this->size = $size;}

    /** @return string */
    public function getThumbnailUrl(){return $this->thumbnailUrl;}
    /** @param string $thumbnailUrl */
    public function setThumbnailUrl($thumbnailUrl){$this->thumbnailUrl = $thumbnailUrl;}

}?>