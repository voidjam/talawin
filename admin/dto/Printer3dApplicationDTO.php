<?php



class Printer3dApplicationDTO{


    /** @var integer */
    public $id;

    /** @var string */
    public $label_es;

    /** @var string */
    public $label_en;

    /** @var string */
    public $file_name_prefix;




    /** Class constructor from JSON */
    public function __construct($message_json = false) {
        if ($message_json) $this->set(json_decode($message_json));
        return $this;
    }

    // JSon to this->object Map
    public function set($data) {
        foreach ($data AS $key => $value) {
            /*if (is_array($value)) {
                $sub = new JSONObject();
                $sub->set($value);
                $value = $sub;
            }*/
            $this->{$key} = $value;
        }
    }

    public static function createPrinter3dApplicationDTO($appId, $appLabelEs, $appLabelEn, $appFileNamePrefix)   {

        $printer3dApplicationDTO = new Printer3dApplicationDTO();
        $printer3dApplicationDTO->setId($appId);
        $printer3dApplicationDTO->setLabelEs($appLabelEs);
        $printer3dApplicationDTO->setLabelEn($appLabelEn);
        $printer3dApplicationDTO->setFileNamePrefix($appFileNamePrefix);

        return $printer3dApplicationDTO;

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLabelEs()
    {
        return $this->label_es;
    }

    /**
     * @param string $label_es
     */
    public function setLabelEs($label_es)
    {
        $this->label_es = $label_es;
    }

    /**
     * @return string
     */
    public function getFileNamePrefix()
    {
        return $this->file_name_prefix;
    }

    /**
     * @param string $file_name_prefix
     */
    public function setFileNamePrefix($file_name_prefix)
    {
        $this->file_name_prefix = $file_name_prefix;
    }

    /**
     * @return string
     */
    public function getLabelEn()
    {
        return $this->label_en;
    }

    /**
     * @param string $label_en
     */
    public function setLabelEn($label_en)
    {
        $this->label_en = $label_en;
    }



}?>