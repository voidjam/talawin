<?php



class ProviderLogoDTO {


    /** @var integer */
    public $id;

    /** @var bool */
    public $is_shown;

    /** @var string */
    public $display_order;

    /** @var string */
    public $provider_name;

    /** @var string */
    public $provider_link;

    /** @var string */
    public $file_name;

    /** @var string */
    public $mime_type;

    /** @var string */
    public $path;

    /** @var string */
    public $size;

    /** @var string */
    public $created_at;

    /** @var string */
    public $updated_at;







    /** Class constructor from JSON */
    public function __construct($message_json = false) {
        if ($message_json) $this->set(json_decode($message_json));
        return $this;
    }

    // JSon to this->object Map
    public function set($data) {
        foreach ($data AS $key => $value) {
            /*if (is_array($value)) {
                $sub = new JSONObject();
                $sub->set($value);
                $value = $sub;
            }*/
            $this->{$key} = $value;
        }
    }

    /**
     * Static method to create a new ClientLogoDTO instance using parameters.
     *
     * @param $providerLogoId
     * @param $providerLogoIsShown
     * @param $displayOrder
     * @param $providerLogoName
     * @param $providerLogoLink
     * @param $fileName
     * @param $filePath
     * @param $fileType
     * @param null $fileSize
     * @return ProviderLogoDTO
     */
    public static function createProviderLogoDTO($providerLogoId, $providerLogoIsShown, $displayOrder, $providerLogoName, $providerLogoLink, $fileName, $filePath, $fileType, $fileSize = null)   {

        $providerLogoDTO = new ProviderLogoDTO();
        $providerLogoDTO->setId($providerLogoId);
        $providerLogoDTO->setIsShown($providerLogoIsShown);
        $providerLogoDTO->setDisplayOrder($displayOrder);
        $providerLogoDTO->setProviderName($providerLogoName);
        $providerLogoDTO->setProviderLink($providerLogoLink);
        $providerLogoDTO->setFileName($fileName);
        $providerLogoDTO->setPath($filePath);
        $providerLogoDTO->setMimeType($fileType);
        $providerLogoDTO->setSize($fileSize);

        return $providerLogoDTO;
    }



    /**
     * @return int
     */
    public function getId(){return $this->id; }
    /**
     * @param int $id
     */
    public function setId($id) {  $this->id = $id; }

    /**
     * @return bool
     */
    public function isShown()   { return $this->is_shown;    }

    /**
     * @param bool $is_shown
     */
    public function setIsShown($is_shown)    { $this->is_shown = $is_shown;    }

    /**
     * @return string
     */
    public function getProviderName()    { return $this->provider_name;    }

    /**
     * @param string $provider_name
     */
    public function setProviderName($provider_name)    { $this->provider_name = $provider_name;    }

    /**
     * @return string
     */
    public function getProviderLink()    { return $this->provider_link;    }

    /**
     * @param string $provider_name
     */
    public function setProviderLink($provider_link)    { $this->provider_link = $provider_link;    }

    /**
     * @return string
     */
    public function getFileName()    { return $this->file_name;    }

    /**
     * @param string $file_name
     */
    public function setFileName($file_name)    { $this->file_name = $file_name;    }

    /**
     * @return string
     */
    public function getMimeType()    { return $this->mime_type;    }

    /**
     * @param string $mime_type
     */
    public function setMimeType($mime_type)    { $this->mime_type = $mime_type;    }

    /**
     * @return string
     */
    public function getPath()    { return $this->path;    }

    /**
     * @param string $path
     */
    public function setPath($path)    { $this->path = $path;    }

    /**
     * @return string
     */
    public function getSize()    { return $this->size;    }

    /**
     * @param string $size
     */
    public function setSize($size)    { $this->size = $size;    }

    /**
     * @return string
     */
    public function getCreatedAt() { return $this->created_at; }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at) { $this->created_at = $created_at; }

    /**
     * @return string
     */
    public function getUpdatedAt() { return $this->updated_at; }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at) { $this->updated_at = $updated_at; }

    /**
     * @return string
     */
    public function getDisplayOrder() { return $this->display_order; }

    /**
     * @param string $display_order
     */
    public function setDisplayOrder($display_order) { $this->display_order = $display_order; }


}?>