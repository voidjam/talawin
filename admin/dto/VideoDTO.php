<?php



class VideoDTO {


    /** @var integer */
    public $id;

    /** @var string */
    public $youtube_id;

    /** @var string */
    public $printer_id;

    /** @var string */
    public $created_at;

    /** @var string */
    public $updated_at;




    /** Class constructor from JSON */
    public function __construct($message_json = false) {
        if ($message_json) $this->set(json_decode($message_json));
        return $this;
    }

    // JSon to this->object Map
    public function set($data) {
        foreach ($data AS $key => $value) {
            /*if (is_array($value)) {
                $sub = new JSONObject();
                $sub->set($value);
                $value = $sub;
            }*/
            $this->{$key} = $value;
        }
    }

    public static function createFileDTO($fileId, $youtube_id, $printer_id)   {

        $videoDTO = new VideoDTO();
        $videoDTO->setId($fileId);
        $videoDTO->setYoutubeId($youtube_id);
        $videoDTO->setPrinterId($printer_id);


        return $videoDTO;

    }

    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }

    /**
     * @return string
     */
    public function getYoutubeId()
    {
        return $this->youtube_id;
    }

    /**
     * @param string $youtube_id
     */
    public function setYoutubeId($youtube_id)
    {
        $this->youtube_id = $youtube_id;
    }

    /**
     * @return string
     */
    public function getPrinterId()
    {
        return $this->printer_id;
    }

    /**
     * @param string $printer_id
     */
    public function setPrinterId($printer_id)
    {
        $this->printer_id = $printer_id;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }


}?>