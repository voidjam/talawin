<?php



class HomeVideoDTO {


    /** @var integer */
    public $id;

    /** @var bool */
    public $is_shown;

    /** @var string */
    public $display_order;

    /** @var string */
    public $youtube_id;

    /** @var string */
    public $youtube_title;





    /** @var string */
    public $created_at;

    /** @var string */
    public $updated_at;




    /** Class constructor from JSON */
    public function __construct($message_json = false) {
        if ($message_json) $this->set(json_decode($message_json));
        return $this;
    }

    // JSon to this->object Map
    public function set($data) {
        foreach ($data AS $key => $value) {
            /*if (is_array($value)) {
                $sub = new JSONObject();
                $sub->set($value);
                $value = $sub;
            }*/
            $this->{$key} = $value;
        }
    }


    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }

    /**
     * @return string
     */
    public function getYoutubeId()
    {
        return $this->youtube_id;
    }

    /**
     * @param string $youtube_id
     */
    public function setYoutubeId($youtube_id)
    {
        $this->youtube_id = $youtube_id;
    }

    /**
     * @return string
     */
    public function getYoutubeTitle()
    {
        return $this->youtube_title;
    }

    /**
     * @param string $youtube_title
     */
    public function setYoutubeTitle($youtube_title)
    {
        $this->youtube_title = $youtube_title;
    }

    /**
     * @return bool
     */
    public function isShown()
    {
        return $this->is_shown;
    }

    /**
     * @param bool $is_shown
     */
    public function setIsShown($is_shown)
    {
        $this->is_shown = $is_shown;
    }

    /**
     * @return string
     */
    public function getDisplayOrder()
    {
        return $this->display_order;
    }

    /**
     * @param string $display_order
     */
    public function setDisplayOrder($display_order)
    {
        $this->display_order = $display_order;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }


}?>