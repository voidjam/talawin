<?php



class HomeBannerDTO {


    /** @var integer */
    public $id;

    /** @var bool */
    public $is_shown;

    /** @var string */
    public $display_order;

    /** @var string */
    public $path;


    /* THIS VARS WILL  BE ON FILE SYSTEM, NOT ON DATABASE. */
    /** @var string */
    public $lap_es_file_name;

    /** @var string */
    public $lap_en_file_name;

    /** @var string */
    public $mob_es_file_name;

    /** @var string */
    public $mob_en_file_name;
    /* THIS VARS WILL  BE ON FILE SYSTEM, NOT ON DATABASE. */



    /** @var string */
    public $created_at;

    /** @var string */
    public $updated_at;




    /** Class constructor from JSON */
    public function __construct($message_json = false) {
        if ($message_json) $this->set(json_decode($message_json));
        return $this;
    }

    // JSon to this->object Map
    public function set($data) {
        foreach ($data AS $key => $value) {
            /*if (is_array($value)) {
                $sub = new JSONObject();
                $sub->set($value);
                $value = $sub;
            }*/
            $this->{$key} = $value;
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isShown()
    {
        return $this->is_shown;
    }

    /**
     * @param bool $is_shown
     */
    public function setIsShown($is_shown)
    {
        $this->is_shown = $is_shown;
    }

    /**
     * @return string
     */
    public function getDisplayOrder()
    {
        return $this->display_order;
    }

    /**
     * @param string $display_order
     */
    public function setDisplayOrder($display_order)
    {
        $this->display_order = $display_order;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getLapEsFileName()
    {
        return $this->lap_es_file_name;
    }

    /**
     * @param string $lap_es_file_name
     */
    public function setLapEsFileName($lap_es_file_name)
    {
        $this->lap_es_file_name = $lap_es_file_name;
    }

    /**
     * @return string
     */
    public function getLapEnFileName()
    {
        return $this->lap_en_file_name;
    }

    /**
     * @param string $lap_en_file_name
     */
    public function setLapEnFileName($lap_en_file_name)
    {
        $this->lap_en_file_name = $lap_en_file_name;
    }

    /**
     * @return string
     */
    public function getMobEsFileName()
    {
        return $this->mob_es_file_name;
    }

    /**
     * @param string $mob_es_file_name
     */
    public function setMobEsFileName($mob_es_file_name)
    {
        $this->mob_es_file_name = $mob_es_file_name;
    }

    /**
     * @return string
     */
    public function getMobEnFileName()
    {
        return $this->mob_en_file_name;
    }

    /**
     * @param string $mob_en_file_name
     */
    public function setMobEnFileName($mob_en_file_name)
    {
        $this->mob_en_file_name = $mob_en_file_name;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }





}?>