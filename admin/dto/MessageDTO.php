<?php



class MessageDTO {


    /** @var integer */
        public $id;

    /** @var string */
        public $email;

    /** @var string */
        public $name;
 
    /** @var string */
        public $phone;

    /** @var string */
        public $company;

    /** @var string */
        public $content;

    /** @var string */
        public $subject;


    /** @var string */
        public $created_at;




    /** Class constructor from JSON */
    public function __construct($message_json = false) {
        if ($message_json) $this->set(json_decode($message_json));
        return $this;
    }

    // JSon to this->object Map
    public function set($data) {
        foreach ($data AS $key => $value) {
            /*if (is_array($value)) {
                $sub = new JSONObject();
                $sub->set($value);
                $value = $sub;
            }*/
            $this->{$key} = $value;
        }
    }

    public static function createMessageDTO($msgId = null, $msgEmail, $msgName, $msgPhone, $msgCompany, $msgContent,$msgSubject = null, $msgCreated = null)   {

        $messageDTO = new MessageDTO();

        $messageDTO->setId($msgId);
        $messageDTO->setEmail($msgEmail);
        $messageDTO->setName($msgName);
        $messageDTO->setPhone($msgPhone);
        $messageDTO->setCompany($msgCompany);
        $messageDTO->setContent($msgContent);
        $messageDTO->setSubject($msgSubject);
        $messageDTO->setCreatedAt($msgCreated);

        return $messageDTO;
    }

    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }

    function setEmail($email) { $this->email = $email; }
    function getEmail() { return $this->email; }

    function setName($name) { $this->name = $name; }
    function getName() { return $this->name; }

    function setPhone($phone) { $this->phone = $phone; }
    function getPhone() { return $this->phone; }

    function setCompany($company) { $this->company = $company; }
    function getCompany() { return $this->company; }

    function setContent($content) { $this->content = $content; }
    function getContent() { return $this->content; }

    function setSubject($subject) { $this->subject = $subject; }
    function getSubject() { return $this->subject; }




    public function getCreatedAt(){return $this->created_at;}
    public function setCreatedAt($created_at){$this->created_at = $created_at;}



}?>