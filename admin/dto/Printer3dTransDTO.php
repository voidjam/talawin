<?php



class Printer3dTransDTO {

    /** @var integer */
    public $id;  /* PRIMARY KEY */

    /** @var string */
    public $printer3d_id;
    /** @var string */
    public $lang_code;


    // Fields to build the printers html page.
    /** @var string */
    public $title;
    /** @var string */
    public $headline;
    /** @var string */
    public $description;
    /** @var string */
    public $html_overview;
    /** @var string */
    public $html_features;
    /** @var string */
    public $html_specs;

    /** @var string */
    public $updated_at;
    /** @var string */
    public $created_at;




    /** Class constructor from JSON */
    public function __construct($message_json = false) {
        if ($message_json) $this->set(json_decode($message_json));
        return $this;
    }

    // JSon to this->object Map
    public function set($data) {
        foreach ($data AS $key => $value) {
            /*if (is_array($value)) {
                $sub = new JSONObject();
                $sub->set($value);
                $value = $sub;
            }*/
            // Check if json does not have any property that is not one of the Printer3dDTO properties.
            if (property_exists("Printer3dTransDTO" , $key)) $this->{$key} = $value;
        }
    }

    /**
     * Returns an empty new instance if PrinterTransDTO class.
     *
     * @return PrinterTransDTO
     */
    public static function createPrinterTransDTO()   {
        $printerTransDTO = new PrinterTransDTO();
        return $printerTransDTO;
    }



    /**
     * @return string
     */
    public function getPrinter3dId()
    {
        return $this->printer3d_id;
    }

    /**
     * @param string $printer3d_id
     */
    public function setPrinter3dId($printer3d_id)
    {
        $this->printer3d_id = $printer3d_id;
    }

    /**
     * @return string
     */
    public function getLangCode()
    {
        return $this->lang_code;
    }

    /**
     * @param string $lang_code
     */
    public function setLangCode($lang_code)
    {
        $this->lang_code = $lang_code;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }



    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getHeadline()
    {
        return $this->headline;
    }

    /**
     * @param string $headline
     */
    public function setHeadline($headline)
    {
        $this->headline = $headline;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


    /**
     * @return string
     */
    public function getHtmlOverview()
    {
        return $this->html_overview;
    }

    /**
     * @param string $html_overview
     */
    public function setHtmlOverview($html_overview)
    {
        $this->html_overview = $html_overview;
    }

    /**
     * @return string
     */
    public function getHtmlFeatures()
    {
        return $this->html_features;
    }

    /**
     * @param string $html_features
     */
    public function setHtmlFeatures($html_features)
    {
        $this->html_features = $html_features;
    }

    /**
     * @return string
     */
    public function getHtmlSpecs()
    {
        return $this->html_specs;
    }

    /**
     * @param string $html_specs
     */
    public function setHtmlSpecs($html_specs)
    {
        $this->html_specs = $html_specs;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }
}?>