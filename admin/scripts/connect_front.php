<?php

    // Database connection and configuration variables.
    require_once($rootpath."admin/db_config/config.db.php");
    // Constant defined to use the database server:
    #require_once($rootpath."public_html/web_service/db_config/constants.php");
    // PHP autoloader: To auto-load classes that we have forgotten.
    require_once($rootpath."admin/inc/autoloader.php");

?>