<?php

$rootpath = "./../../"; 


if (!empty($_POST) && !empty($_POST['inputEmail']) && !empty($_POST['inputPassword']))	{
	
	// Get connection to database.
	include $rootpath.'admin/scripts/connect.php';
	


	$adminUserDAO = new AdminUserDAO();
	$adminUserDTO = $adminUserDAO->getAdminUserByEmail($_POST['inputEmail']);

	// Password is correct
	if(password_verify($_POST['inputPassword'], $adminUserDTO->getPassword())) {
    	
    	unset ($_POST);
    	$_SESSION['LOGGED'] = true;
    	$_SESSION['adminUserDTO'] = $adminUserDTO;
    	header("Location:".$rootpath."admin/index.php");
  		exit; 
  	// Password is incorrect
  	} else {

  		$_SESSION['error']['level']		= "error";
  		$_SESSION['error']['message']	= "La contraseña introducida no es correcta.";
  		header("Location:".$rootpath."admin/login.php");
  		exit; 

  	}

} else 	{

	$_SESSION['error']['level']		= "error";
  	$_SESSION['error']['message']	= "Por favor introduzca ambos campos: Usuario y Contraseña";
  	header("Location:".$rootpath."admin/login.php");
  	exit; 

}

?>