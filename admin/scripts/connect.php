<?php

	// Start session. Inlcude UserDTO because we store a AdminUserDTO object on _SESSION["adminUserDTO"].
    include_once ($rootpath."admin/dto/AdminUserDTO.php");
    session_start(); 

    // Database connection and configuration variables.
    require_once($rootpath."admin/db_config/config.db.php");
    // Constant defined to use the database server:
    #require_once($rootpath."public_html/web_service/db_config/constants.php");
    // PHP autoloader: To auto-load classes that we have forgotten.
    require_once($rootpath."admin/inc/autoloader.php");

?>