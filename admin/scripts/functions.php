<?php


	/**
	 * Function that creates a blowfish hash from the input value using a random salt made up of letters and 
	 * numbers. This will automatically use the new $2y$ salt format for increased security.
	 */
	function better_crypt($input, $rounds = 10)	{
	    $crypt_options = array(
	      'cost' => $rounds
	    );
	    return password_hash($input, PASSWORD_BCRYPT, $crypt_options);
	}


	/**
	 * Function that creates a thumbnail with a fix width of $thumbMinWidth pixels 
	 * from an image file and saves it on $pathToThumbs under the name of $thumbName.
	 */
	function createThumbs( $file, $pathToThumbs, $thumbMinWidth, $thumbMinHeight, $thumbName ) {

			     
	    // Creating the image resource from file path:
	    // For this to work we need up vote to enable the following extensions in php.ini
		// extension = php_mbstring.dll
		// extension = php_exif.dll
		$info   = exif_imagetype($file);

		switch ($info) {

			case IMAGETYPE_JPEG:
				$img = imagecreatefromjpeg( $file );
				break;
			case IMAGETYPE_PNG:
				$img = imagecreatefrompng( $file );
				break;
			case IMAGETYPE_GIF:
				$img = imagecreatefromgif( $file );
				break;

		}

	    $width = imagesx( $img );
	    $height = imagesy( $img );

	    // Calculate thumbnail size
	    $new_width = $thumbMinWidth;
	    $new_height = floor( $height * ( $thumbMinWidth / $width ) );

	    if ($new_height <  $thumbMinHeight)	{
	    	$new_height =  $thumbMinHeight;
	    	$new_width  =  floor( $width * ( $thumbMinHeight / $height ) );
	    }

	    // Create a new temporary image
	    $tmp_img = imagecreatetruecolor( $new_width, $new_height );

	    // Copy and resize old image into new image
	    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

	    // Save thumbnail into a file
	    imagepng( $tmp_img, "{$pathToThumbs}/{$thumbName}" );
			
  }



    /**
     * Delete function that deals with directories recursively: Delets all files containded by any directory
     * and the directory itself.
     */
    function delete_files($target) {
        if (is_dir($target)) {
            $files = glob($target . '*', GLOB_MARK); //GLOB_MARK adds a slash to directories returned

            foreach ($files as $file) {
                delete_files($file);
            }

            rmdir($target);
        } elseif (is_file($target)) {
            unlink($target);
        }
    }


?>