<?php

$rootpath = "./../";
$current_uri = "$_SERVER[REQUEST_URI]";

// Get connection to database.
include $rootpath.'admin/scripts/connect.php';
// To get link paths.
include $rootpath.'admin/inc/constants.php';


if (!empty($_SESSION["LOGGED"]) && $_SESSION["LOGGED"] == true)	{
	
  	// Include Header.
  	include $rootpath.'admin/inc/header.php';
	include $rootpath.'admin/inc/nav-bar.php';

?>


    <div class="index-content">

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- MAIN SECTIONS LINKS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- Horizontal separator -->
        <div class="section-links-container d-flex flex-column flex-sm-row flex-row flex-wrap justify-content-around  align-items-center mr-auto p-2 pl-sm-10">

            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- HOME INDEX ICONS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- BACK ARROW -->
            <div id="back-arrow-section-link" class="section-link section-link-level1 hidden d-flex flex-column align-items-center" title="Volver...">
                <img src="<?php echo $rootpath?>admin/img/back-arrow-icon.svg"/>
                <span>&ensp;<br>&ensp;</span>
            </div>
            <!-- ADMIN HOME -->
            <div id="admin-home-section-link" class="section-link section-link-level0 d-flex flex-column align-items-center">
                <img src="<?php echo $rootpath?>admin/img/home-icon.png"/>
                <span>Admin<br>HOME</span>
            </div>
            <!-- NEW PRINTERS -->
            <div id="new-printers-section-link" class="section-link section-link-level0 d-flex flex-column align-items-center" onclick="location.href='<?php echo SECTION_PRINTERS ?>?action=new_printers'">
                <img src="<?php echo $rootpath?>admin/img/wficon.png"/>
                <span>Impresoras<br>Nuevas</span>
            </div>
            <!-- OLD PRINTERS -->
            <div id="old-printers-section-link" class="section-link section-link-level0 d-flex flex-column align-items-center" onclick="location.href='<?php echo SECTION_PRINTERS ?>?action=old_printers'">
                <img src="<?php echo $rootpath?>admin/img/Printer-icon.png"/>
                <span>Impresoras<br>Ocasión</span>
            </div>
            <!-- 3D PRINTERS -->
            <div id="3d-printers-section-link" class="section-link section-link-level0 d-flex flex-column align-items-center" onclick="location.href='<?php echo SECTION_PRINTERS_3D ?>'">
                <img src="<?php echo $rootpath?>admin/img/3d_2.png"/>
                <span>Impresoras<br>3D</span>
            </div>
            <!-- ADMIN ABOUT -->
            <div id="admin-about-section-link" class="section-link section-link-level0 d-flex flex-column align-items-center">
                <img src="<?php echo $rootpath?>admin/img/about-icon.png"/>
                <span>Admin<br>About</span>
            </div>
            <!-- CONFIGURATION -->
            <div id="config-section-link" class="section-link d-flex section-link-level0 flex-column align-items-center" onclick="location.href='<?php echo SECTION_CONFIGURATION?>'">
                <img src="<?php echo $rootpath?>admin/img/name-card-icon.png"/>
                <span>Usuarios<br>Admin</span>
            </div>
            <!-- /HOME INDEX ICONS +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->



            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- HOME SECTION ICONS ++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- HOME BANNERS -->
            <div id="home-banners-section-link" class="section-link section-link-level1 admin-home hidden d-flex flex-column align-items-center" onclick="location.href='<?php echo SECTION_BANNERS ?>'">
                <img src="<?php echo $rootpath?>admin/img/pc-icon.png"/>
                <span>Home<br>Banners</span>
            </div>
            <!-- HOME VIDEOS -->
            <div id="home-videos-section-link" class="section-link section-link-level1 admin-home hidden d-flex flex-column align-items-center" onclick="location.href='<?php echo SECTION_VIDEOS ?>'">
                <img src="<?php echo $rootpath?>admin/img/videos-icon.png"/>
                <span>Home<br>Videos</span>
            </div>
            <!-- /HOME SECTION ICONS ++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- ABOUT SECTION ICONS +++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <!-- CLIENTS LOGOS -->
            <div id="logo-clients-section-link" class="section-link section-link-level1 admin-about d-flex flex-column align-items-center hidden" onclick="location.href='<?php echo SECTION_CLIENTS_LOGOS?>'">
                <img src="<?php echo $rootpath?>admin/img/clients-icon.svg"/>
                <span>Logos<br>Clientes</span>
            </div>
            <!-- PROVIDERS LOGOS -->
            <div id="team-talawin-section-link" class="section-link section-link-level1 admin-about hidden d-flex flex-column align-items-center" onclick="location.href='<?php echo SECTION_PROVIDERS_LOGOS ?>'">
                <img src="<?php echo $rootpath?>admin/img/providers-icon.svg"/>
                <span>Logos<br>Proveedores</span>
            </div>
            <!-- TALAWIN TEAM -->
            <div id="logo-providers-section-link" class="section-link section-link-level1 admin-about hidden d-flex flex-column align-items-center" onclick="location.href='<?php echo SECTION_TEAM_MEMBERS ?>'">
                <img src="<?php echo $rootpath?>admin/img/team-icon.png"/>
                <span>Equipo<br>Talawin</span>
            </div>
            <!-- /ABOUT SECTION ICONS ++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->



        </div>
        <!-- /MAIN SECTIONS LINKS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->



        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- SECTION PAGE INFO +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <div class="page_section_info unselectable">
            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- SECTION PAGE INFO TXT +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <span>ADMIN PAGE INDEX</span>
            <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <!-- /SECTION PAGE INFO TXT ++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        </div>
        <!-- /SECTION PAGE INFO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    </div><!-- /index-content -->


    <footer>
        <p class="small">2018 Talawin S.L. © Todos los derechos reservados.</p>
    </footer>


    <script>
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // DOCUMENT READY BLOCK ////////////////////////////////////////////////////////////////////////////////////////
        $(document).ready(function() {

            /**
             * This method shows the main page of the admin panel. Icons are divided in several levels,
             * and this method takes away all icons and fades in the level 0 icons.
             */
            function showLevel0()   {

                $(".section-link-level1").fadeOut(300, "swing", function () {
                    $(this).addClass("hidden");
                });
                $(".section-link-level0").fadeIn(300, "swing", function () {
                    $(this).removeClass("hidden");
                });
            }


            /**
             * This methods hides all icons from the main page of the admin panel and shows
             * the icons related to Admin Home, that is HomeVideos and HomeBanners.
             *
             * @param e
             */
            function toggleAdminHomeLinks(e) {

                if ($(this).is("#admin-home-section-link")) {
                    $(".section-link").not("#back-arrow-section-link").not(".admin-home").fadeOut(300, "swing", function () {
                        $(this).addClass("hidden");
                    });
                    $("#back-arrow-section-link").add(".admin-home").fadeIn({duration:300, complete: function () {
                            $(this).removeClass("hidden");
                        }});
                }
            }


            /**
             * This methods hides all icons from the main page of the admin panel and shows
             * the icons related to Admin Home, that is HomeVideos and HomeBanners.
             *
             * @param e
             */
            function toggleAdminAboutLinks(e) {

                if ($(this).is("#admin-about-section-link")) {
                    $(".section-link").not("#back-arrow-section-link").not(".admin-about").fadeOut(300, "swing", function () {
                        $(this).addClass("hidden");
                    });
                    $("#back-arrow-section-link").add(".admin-about").fadeIn({duration:300, complete: function () {
                            $(this).removeClass("hidden");
                        }});
                }
            }


            // Add click listeners to AdminHome icon, AdminAbout icon and BackArrow icon.
            $("#admin-home-section-link").on("click", toggleAdminHomeLinks);
            $("#admin-about-section-link").on("click", toggleAdminAboutLinks);
            $("#back-arrow-section-link").on("click", showLevel0);
        });
        // END OF DOCUMENT READY BLOCK /////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    </script>

<?php
	
	// Check for errors and warnings:
	if(isset($_SESSION['error']) && is_array($_SESSION['error']))   include $rootpath.'admin/inc/warning.php';
	
	// Include Footer.
	include $rootpath.'admin/inc/footer.php';

} else 	{

  	header("Location:".SECTION_LOGIN);
  	exit; 

}