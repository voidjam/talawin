/*!
 Autosize 4.0.0
 license: MIT
 http://www.jacklmoore.com/autosize
 */

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 	BROWSER COMPATIBILITY
//
// 		+--------+---------+----+--------+------------+---------+------------+
// 		| Chrome | Firefox | IE | Safari | iOS Safari | Android | Opera Mini |
// 		+--------+---------+----+--------+------------+---------+------------+
// 		| yes    | yes     | 9  | yes    | yes        | 4       | ?          |
// 		+--------+---------+----+--------+------------+---------+------------+
//
//
//     USAGE
//
// 		The autosize function accepts a single textarea element, or an array or array-like object (such as a NodeList or jQuery collection) of textarea elements.
//
// 		// from a NodeList
// 		autosize(document.querySelectorAll('textarea'));
//
// 		// from a single Node
// 		autosize(document.querySelector('textarea'));
//
// 		// from a jQuery collection
// 		autosize($('textarea'));
//
//
// 	METHODS
//
// 		+----------------------------+----------------------------------------------------------------------+--+--+--+--+--+
// 		| Name                       | Description                                                          |  |  |  |  |  |
// 		+----------------------------+----------------------------------------------------------------------+--+--+--+--+--+
// 		| autosize.update(elements)  | Triggers the height adjustment for an assigned textarea element.     |  |  |  |  |  |
// 		|                            | Autosize will automatically adjust the textarea height on keyboard   |  |  |  |  |  |
// 		|                            | and window resize events. There is no efficient way for Autosize     |  |  |  |  |  |
// 		|                            | to monitor for when another script has changed the textarea value    |  |  |  |  |  |
// 		|                            | or for changes in layout that impact the textarea element.           |  |  |  |  |  |
// 		|                            |                                                                      |  |  |  |  |  |
// 		|                            |     // Init                                                          |  |  |  |  |  |
// 		|                            |     var ta = document.querySelector('textarea');                     |  |  |  |  |  |
// 		|                            |     ta.style.display = 'none';                                       |  |  |  |  |  |
// 		|                            |     autosize(ta);                                                    |  |  |  |  |  |
// 		|                            |                                                                      |  |  |  |  |  |
// 		|                            |     // Change layout                                                 |  |  |  |  |  |
// 		|                            |     ta.style.display = 'block';                                      |  |  |  |  |  |
// 		|                            |     autosize.update(ta);                                             |  |  |  |  |  |
// 		|                            |                                                                      |  |  |  |  |  |
// 		|                            |     // Change value                                                  |  |  |  |  |  |
// 		|                            |     ta.value = "Some new value";                                     |  |  |  |  |  |
// 		|                            |     autosize.update(ta);                                             |  |  |  |  |  |
// 		+----------------------------+----------------------------------------------------------------------+--+--+--+--+--+
// 		| autosize.destroy(elements) | Removes Autosize and reverts any changes it made to the textarea     |  |  |  |  |  |
// 		|                            |                                                                      |  |  |  |  |  |
// 		|                            |     element.autosize.destroy(document.querySelectorAll('textarea')); |  |  |  |  |  |
// 		|                            |                                                                      |  |  |  |  |  |
// 		+----------------------------+----------------------------------------------------------------------+--+--+--+--+--+
//
//
//     LIFE-CYCLE EVENTS
//
//
// 		+------------------+------------------------------------------------------------------------------------------------------------------+--+--+--+--+--+
// 		| Event            | Description                                                                                                      |  |  |  |  |  |
// 		+------------------+------------------------------------------------------------------------------------------------------------------+--+--+--+--+--+
// 		| autosize:resized | This event is fired every time autosize adjusts the textarea height.var ta = document.querySelector('textarea'); |  |  |  |  |  |
// 		|                  |                                                                                                                  |  |  |  |  |  |
// 		|                  | ta.addEventListener('autosize:resized', function(){                                                              |  |  |  |  |  |
// 		|                  |   console.log('textarea height updated');                                                                        |  |  |  |  |  |
// 		|                  | });                                                                                                              |  |  |  |  |  |
// 		|                  | If you are using jQuery, you can use the on/off methods to listen to this event:$('textarea').each(function(){   |  |  |  |  |  |
// 		|                  |   autosize(this);                                                                                                |  |  |  |  |  |
// 		|                  | }).on('autosize:resized', function(){                                                                            |  |  |  |  |  |
// 		|                  |   console.log('textarea height updated');                                                                        |  |  |  |  |  |
// 		|                  | });                                                                                                              |  |  |  |  |  |
// 		+------------------+------------------------------------------------------------------------------------------------------------------+--+--+--+--+--+
// 		| autosize:update  | Dispatch this event on a textarea element as an alternative to using the autosize.update method.                 |  |  |  |  |  |
// 		|                  | Note that Firefox does not dispatch events on disabled elements.var ta = document.querySelector('textarea');     |  |  |  |  |  |
// 		|                  |                                                                                                                  |  |  |  |  |  |
// 		|                  | autosize(ta);                                                                                                    |  |  |  |  |  |
// 		|                  |                                                                                                                  |  |  |  |  |  |
// 		|                  | ta.value = "Some new value";                                                                                     |  |  |  |  |  |
// 		|                  |                                                                                                                  |  |  |  |  |  |
// 		|                  | // Dispatch a 'autosize:update' event to trigger a resize:                                                       |  |  |  |  |  |
// 		|                  | var evt = document.createEvent('Event');                                                                         |  |  |  |  |  |
// 		|                  | evt.initEvent('autosize:update', true, false);                                                                   |  |  |  |  |  |
// 		|                  | ta.dispatchEvent(evt);                                                                                           |  |  |  |  |  |
// 		+------------------+------------------------------------------------------------------------------------------------------------------+--+--+--+--+--+
// 		| autosize:destroy | Dispatch this event on a textarea element as an alternative to using the autosize.destroy method.                |  |  |  |  |  |
// 		|                  | Note that Firefox does not dispatch events on disabled elements.var ta = document.querySelector('textarea');     |  |  |  |  |  |
// 		|                  |                                                                                                                  |  |  |  |  |  |
// 		|                  | // assign autosize to ta                                                                                         |  |  |  |  |  |
// 		|                  | autosize(ta);                                                                                                    |  |  |  |  |  |
// 		|                  |                                                                                                                  |  |  |  |  |  |
// 		|                  | // remove autosize from ta                                                                                       |  |  |  |  |  |
// 		|                  | var evt = document.createEvent('Event');                                                                         |  |  |  |  |  |
// 		|                  | evt.initEvent('autosize:destroy', true, false);                                                                  |  |  |  |  |  |
// 		|                  | ta.dispatchEvent(evt);                                                                                           |  |  |  |  |  |
// 		+------------------+------------------------------------------------------------------------------------------------------------------+--+--+--+--+--+
//
//
//
//     TROUBLE SHOOTING / FAQ
//
//
// 	+ Setting a min-height or max-height
//
// 		Use CSS to specify a min-height and max-height for the textarea element.
// 		Once the height exceeds the max-height, Autosize will re-enable the vertical scrollbar.
//
//     	The rows attribute can also be used to specify a minimum height. The rows attribute has a default value of 2,
// 		so to make the textarea smaller than that you'll need to set the value to 1.
//
// 			Example: <textarea rows='1'></textarea>
//
// 	+ Fixing slow or sluggish performance
//
// 		In Webkit, the default focus style for input and textarea elements includes outline-style:
// 		auto, which has a blurred drop-shadow like appearance. Completely independent of Autosize,
// 		Safari is terrible at animating anything with this blur on it. My recommendation would be
// 		to use a non-blurred outline-style, and to remove any blurred box-shadow or other taxing CSS effect.
//
// 		For example:
//
// 			input:focus, textarea:focus {
// 			outline-style: solid;
// 			outline-width: 2px;
// }
// 	+ Initial height is incorrect
//
// 		Autosize needs to be able to calculate the width of the textarea element when it is assigned.
// 		JavaScript cannot accurately calculate the width of an element that has been detached from
// 		the DOM or had its display set to none. If you want to assign Autosize to a hidden textarea
// 		element that you plan to reveal later, be sure to either specify the pixel width of the element
// 		in your CSS, or use the autosize.update method after you reveal the textarea element.
//
//     	Possible ways to resolve:
//
// 		Specify an exact width for the textarea element in your stylesheet.
//
// 			- Delay assigning Autosize until the textarea has been revealed. Alternatively, wait until the user
// 			focuses the textarea element:
//
// 				var ta = document.querySelector('textarea');
// 				ta.addEventListener('focus', function(){
// 					autosize(ta);
// 				});
//
// 			- Use the autosize.update method (or trigger the autosize:update event) after the element has been revealed.
//
// 				var ta = document.querySelector('textarea');
// 				ta.style.display = 'none';
// 				autosize(ta);
// 				ta.style.display = '';
//
// 				// Call the update method to recalculate the size:
// 				autosize.update(ta);
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



(function (global, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['exports', 'module'], factory);
    } else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
        factory(exports, module);
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, mod);
        global.autosize = mod.exports;
    }
})(this, function (exports, module) {
    'use strict';

    var map = typeof Map === "function" ? new Map() : (function () {
        var keys = [];
        var values = [];

        return {
            has: function has(key) {
                return keys.indexOf(key) > -1;
            },
            get: function get(key) {
                return values[keys.indexOf(key)];
            },
            set: function set(key, value) {
                if (keys.indexOf(key) === -1) {
                    keys.push(key);
                    values.push(value);
                }
            },
            'delete': function _delete(key) {
                var index = keys.indexOf(key);
                if (index > -1) {
                    keys.splice(index, 1);
                    values.splice(index, 1);
                }
            }
        };
    })();

    var createEvent = function createEvent(name) {
        return new Event(name, { bubbles: true });
    };
    try {
        new Event('test');
    } catch (e) {
        // IE does not support `new Event()`
        createEvent = function (name) {
            var evt = document.createEvent('Event');
            evt.initEvent(name, true, false);
            return evt;
        };
    }

    function assign(ta) {
        if (!ta || !ta.nodeName || ta.nodeName !== 'TEXTAREA' || map.has(ta)) return;

        var heightOffset = null;
        var clientWidth = ta.clientWidth;
        var cachedHeight = null;

        function init() {
            var style = window.getComputedStyle(ta, null);

            if (style.resize === 'vertical') {
                ta.style.resize = 'none';
            } else if (style.resize === 'both') {
                ta.style.resize = 'horizontal';
            }

            if (style.boxSizing === 'content-box') {
                heightOffset = -(parseFloat(style.paddingTop) + parseFloat(style.paddingBottom));
            } else {
                heightOffset = parseFloat(style.borderTopWidth) + parseFloat(style.borderBottomWidth);
            }
            // Fix when a textarea is not on document body and heightOffset is Not a Number
            if (isNaN(heightOffset)) {
                heightOffset = 0;
            }

            update();
        }

        function changeOverflow(value) {
            {
                // Chrome/Safari-specific fix:
                // When the textarea y-overflow is hidden, Chrome/Safari do not reflow the text to account for the space
                // made available by removing the scrollbar. The following forces the necessary text reflow.
                var width = ta.style.width;
                ta.style.width = '0px';
                // Force reflow:
				/* jshint ignore:start */
                ta.offsetWidth;
				/* jshint ignore:end */
                ta.style.width = width;
            }

            ta.style.overflowY = value;
        }

        function getParentOverflows(el) {
            var arr = [];

            while (el && el.parentNode && el.parentNode instanceof Element) {
                if (el.parentNode.scrollTop) {
                    arr.push({
                        node: el.parentNode,
                        scrollTop: el.parentNode.scrollTop
                    });
                }
                el = el.parentNode;
            }

            return arr;
        }

        function resize() {
            var originalHeight = ta.style.height;
            var overflows = getParentOverflows(ta);
            var docTop = document.documentElement && document.documentElement.scrollTop; // Needed for Mobile IE (ticket #240)

            ta.style.height = '';

            var endHeight = ta.scrollHeight + heightOffset;

            if (ta.scrollHeight === 0) {
                // If the scrollHeight is 0, then the element probably has display:none or is detached from the DOM.
                ta.style.height = originalHeight;
                return;
            }

            ta.style.height = endHeight + 'px';

            // used to check if an update is actually necessary on window.resize
            clientWidth = ta.clientWidth;

            // prevents scroll-position jumping
            overflows.forEach(function (el) {
                el.node.scrollTop = el.scrollTop;
            });

            if (docTop) {
                document.documentElement.scrollTop = docTop;
            }
        }

        function update() {
            resize();

            var styleHeight = Math.round(parseFloat(ta.style.height));
            var computed = window.getComputedStyle(ta, null);

            // Using offsetHeight as a replacement for computed.height in IE, because IE does not account use of border-box
            var actualHeight = computed.boxSizing === 'content-box' ? Math.round(parseFloat(computed.height)) : ta.offsetHeight;

            // The actual height not matching the style height (set via the resize method) indicates that
            // the max-height has been exceeded, in which case the overflow should be allowed.
            if (actualHeight !== styleHeight) {
                if (computed.overflowY === 'hidden') {
                    changeOverflow('scroll');
                    resize();
                    actualHeight = computed.boxSizing === 'content-box' ? Math.round(parseFloat(window.getComputedStyle(ta, null).height)) : ta.offsetHeight;
                }
            } else {
                // Normally keep overflow set to hidden, to avoid flash of scrollbar as the textarea expands.
                if (computed.overflowY !== 'hidden') {
                    changeOverflow('hidden');
                    resize();
                    actualHeight = computed.boxSizing === 'content-box' ? Math.round(parseFloat(window.getComputedStyle(ta, null).height)) : ta.offsetHeight;
                }
            }

            if (cachedHeight !== actualHeight) {
                cachedHeight = actualHeight;
                var evt = createEvent('autosize:resized');
                try {
                    ta.dispatchEvent(evt);
                } catch (err) {
                    // Firefox will throw an error on dispatchEvent for a detached element
                    // https://bugzilla.mozilla.org/show_bug.cgi?id=889376
                }
            }
        }

        var pageResize = function pageResize() {
            if (ta.clientWidth !== clientWidth) {
                update();
            }
        };

        var destroy = (function (style) {
            window.removeEventListener('resize', pageResize, false);
            ta.removeEventListener('input', update, false);
            ta.removeEventListener('keyup', update, false);
            ta.removeEventListener('autosize:destroy', destroy, false);
            ta.removeEventListener('autosize:update', update, false);

            Object.keys(style).forEach(function (key) {
                ta.style[key] = style[key];
            });

            map['delete'](ta);
        }).bind(ta, {
            height: ta.style.height,
            resize: ta.style.resize,
            overflowY: ta.style.overflowY,
            overflowX: ta.style.overflowX,
            wordWrap: ta.style.wordWrap
        });

        ta.addEventListener('autosize:destroy', destroy, false);

        // IE9 does not fire onpropertychange or oninput for deletions,
        // so binding to onkeyup to catch most of those events.
        // There is no way that I know of to detect something like 'cut' in IE9.
        if ('onpropertychange' in ta && 'oninput' in ta) {
            ta.addEventListener('keyup', update, false);
        }

        window.addEventListener('resize', pageResize, false);
        ta.addEventListener('input', update, false);
        ta.addEventListener('autosize:update', update, false);
        ta.style.overflowX = 'hidden';
        ta.style.wordWrap = 'break-word';

        map.set(ta, {
            destroy: destroy,
            update: update
        });

        init();
    }

    function destroy(ta) {
        var methods = map.get(ta);
        if (methods) {
            methods.destroy();
        }
    }

    function update(ta) {
        var methods = map.get(ta);
        if (methods) {
            methods.update();
        }
    }

    var autosize = null;

    // Do nothing in Node.js environment and IE8 (or lower)
    if (typeof window === 'undefined' || typeof window.getComputedStyle !== 'function') {
        autosize = function (el) {
            return el;
        };
        autosize.destroy = function (el) {
            return el;
        };
        autosize.update = function (el) {
            return el;
        };
    } else {
        autosize = function (el, options) {
            if (el) {
                Array.prototype.forEach.call(el.length ? el : [el], function (x) {
                    return assign(x, options);
                });
            }
            return el;
        };
        autosize.destroy = function (el) {
            if (el) {
                Array.prototype.forEach.call(el.length ? el : [el], destroy);
            }
            return el;
        };
        autosize.update = function (el) {
            if (el) {
                Array.prototype.forEach.call(el.length ? el : [el], update);
            }
            return el;
        };
    }

    module.exports = autosize;
});