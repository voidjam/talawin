<?php

class Printer3dVideoDAO {

    // DB Tables constants:
    const TABLE_PRINTER3D_VIDEOS      = "printer3d_videos";

    // Constants for the form variables from app:
    const FIELD_ID          = 'id';
    const FIELD_YOUTUBE_ID  = 'youtube_id';
    const FIELD_PRINTER3D_ID  = "printer3d_id";

    const FIELD_CREATED_AT  = "created_at";
    const FIELD_UPDATED_AT  = "updated_at";


    // Variables to store the connexion from config.inc.php
    var $_db;

    public function __construct() {

        global $db;
        $this->_db = $db;
    }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert was made correctly or not.
     */
    protected function execute($query, $query_params = null, $fetchResults = true) {

        try {

            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute($query_params);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0 && $fetchResults) {

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                $printer3dVideoDTO[$key] = new Printer3dVideoDTO();

                $printer3dVideoDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);
                $printer3dVideoDTO[$key]->setYoutubeId(array_key_exists(self::FIELD_YOUTUBE_ID, $row) ? $row[self::FIELD_YOUTUBE_ID] : null);
                $printer3dVideoDTO[$key]->setPrinter3dId(array_key_exists(self::FIELD_PRINTER3D_ID, $row) ? $row[self::FIELD_PRINTER3D_ID] : null);

                $printer3dVideoDTO[$key]->setCreatedAt(array_key_exists(self::FIELD_CREATED_AT, $row) ? $row[self::FIELD_CREATED_AT] : null);
                $printer3dVideoDTO[$key]->setUpdatedAt(array_key_exists(self::FIELD_UPDATED_AT, $row) ? $row[self::FIELD_UPDATED_AT] : null);

            }

            return $printer3dVideoDTO;

        } else {

            return $result;
        }
    }


    /**
     * This method saves a video to the database.
     *
     * @param $youtubeId
     * @param $printer3dId

     * @return int Returns the id of the inserted row.
     */
    public function saveVideo($youtubeId, $printer3dId) {

        $query = "INSERT INTO ".self::TABLE_PRINTER3D_VIDEOS." ("
            .self::FIELD_YOUTUBE_ID.", "
            .self::FIELD_PRINTER3D_ID.", "
            .self::FIELD_CREATED_AT
            .") VALUES (
            :".self::FIELD_YOUTUBE_ID.",
            :".self::FIELD_PRINTER3D_ID
            .", null) ";

        $query_params = array(
            ':'.self::FIELD_YOUTUBE_ID    => $youtubeId,
            ':'.self::FIELD_PRINTER3D_ID    => $printer3dId);

        // Execute the query. Third param to false indicates not to fetch results, as
        // an UPDATE or INSERT query wont return any rows.
        $result = $this->execute($query, $query_params, false);

        // If there was 1 role affected, that's there were no errors:
        if ($result = 1)   {
            // It will return the id of the new inserted video:
            $insertedVideoId = $this->_db->lastInsertId();

            return $insertedVideoId;
            // If there was an error we return -1.
        } else {
            return -1;
        }
    }


    public function getVideosByPrinter3dId($printer3d_id)   {

        $query = "SELECT * " .
            "FROM ".self::TABLE_PRINTER3D_VIDEOS." WHERE printer3d_id = '$printer3d_id'";

        $videosArray = $this->execute($query, null, true);

        return (is_array($videosArray) ? $videosArray: 0);
    }





    public function getPrinter3dVideoById($video_id)   {

        $query = "SELECT * " .
            "FROM ".self::TABLE_PRINTER3D_VIDEOS." WHERE ".self::TABLE_PRINTER3D_VIDEOS.".id = '$video_id'";

        $arrayPrinter3dVideoDTO = $this->execute($query, null, true);

        return $arrayPrinter3dVideoDTO[0];
    }






    /**
     * This method delete the video row for the corresponding id column.
     *
     * @param $video_id
     * @return bool
     */
    public function deletePrinter3dVideoById($video_id)   {

        $query = "DELETE " .
            "FROM ".self::TABLE_PRINTER3D_VIDEOS." WHERE ".self::TABLE_PRINTER3D_VIDEOS.".id = '$video_id'";

        return $this->execute($query, null, false);
    }

    /**
     * This method deletes all the videos table rows related to the corresponding printer id.
     *
     * @param $printer3d_id
     * @return bool
     */
    public function deletePrinter3dVideosByPrinter3dId($printer3d_id)   {

        $query = "DELETE " .
            "FROM ".self::TABLE_PRINTER3D_VIDEOS." WHERE ".self::TABLE_PRINTER3D_VIDEOS.".printer3d_id = '$printer3d_id'";

        return $this->execute($query, null, false);
    }


    /**
     * This functions check if there are at least one uploaded video for the corresponding printer_id.
     *
     * @param $printer3d_id
     * @return string
     */
    public function hasPrinter3dVideo($printer3d_id)  {

        $query = "SELECT count(printer3d_videos.id) from printer3d_videos where printer3d_videos.printer3d_id = $printer3d_id";

        $stmt = $this->_db->prepare($query);
        $stmt->execute();
        return (intval($stmt->fetchColumn()) > 0);
    }


}?>