<?php

class TeamMemberDAO {

    // DB Tables constants:
    const TABLE_TEAM_MEMBERS   = "team_members";

    // Constants for the form variables from app:
    const FIELD_ID              = 'id';
    const FIELD_IS_SHOWN        = 'is_shown';
    const FIELD_DISPLAY_ORDER   = 'display_order';
    const FIELD_MEMBER_NAME     = "member_name";
    const FIELD_MEMBER_ROLE     = "member_role";
    const FIELD_MEMBER_ROLE_EN  = "member_role_en";
    const FIELD_FILE_NAME       = "file_name";
    const FIELD_MIME_TYPE       = "mime_type";
    const FIELD_PATH            = "path";
    const FIELD_SIZE            = "size";

    const FIELD_CREATED_AT      = "created_at";
    const FIELD_UPDATED_AT      = "updated_at";


    // Variables to store the connexion from config.inc.php
    var $_db;
    // Variable rootpath taken from object creator.
    var $_rootpath;

    public function __construct() {

        global $db;
        global $rootpath;
        $this->_db = $db;
        $this->_rootpath = $rootpath;
    }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert was made correctly or not.
     *
     * @return bool|array
     */
    protected function execute($query, $query_params = null, $fetchResults = true) {

        try {

            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute($query_params);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0 && $fetchResults) {

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                $arrayTeamMemberDTO[$key] = new TeamMemberDTO();

                $arrayTeamMemberDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);
                $arrayTeamMemberDTO[$key]->setIsShown(array_key_exists(self::FIELD_IS_SHOWN, $row) ? $row[self::FIELD_IS_SHOWN] : null);
                $arrayTeamMemberDTO[$key]->setDisplayOrder(array_key_exists(self::FIELD_DISPLAY_ORDER, $row) ? $row[self::FIELD_DISPLAY_ORDER] : null);
                $arrayTeamMemberDTO[$key]->setMemberName(array_key_exists(self::FIELD_MEMBER_NAME, $row) ? $row[self::FIELD_MEMBER_NAME] : null);
                $arrayTeamMemberDTO[$key]->setMemberRole(array_key_exists(self::FIELD_MEMBER_ROLE, $row) ? $row[self::FIELD_MEMBER_ROLE] : null);
                $arrayTeamMemberDTO[$key]->setMemberRoleEn(array_key_exists(self::FIELD_MEMBER_ROLE_EN, $row) ? $row[self::FIELD_MEMBER_ROLE_EN] : null);
                $arrayTeamMemberDTO[$key]->setFileName(array_key_exists(self::FIELD_FILE_NAME, $row) ? $row[self::FIELD_FILE_NAME] : null);
                $arrayTeamMemberDTO[$key]->setMimeType(array_key_exists(self::FIELD_MIME_TYPE, $row) ? $row[self::FIELD_MIME_TYPE] : null);
                $arrayTeamMemberDTO[$key]->setSize(array_key_exists(self::FIELD_SIZE, $row) ? $row[self::FIELD_SIZE] : null);
                $arrayTeamMemberDTO[$key]->setPath(array_key_exists(self::FIELD_PATH, $row) ? $row[self::FIELD_PATH] : null);
                $arrayTeamMemberDTO[$key]->setCreatedAt(array_key_exists(self::FIELD_CREATED_AT, $row) ? $row[self::FIELD_CREATED_AT] : null);
            }

            return $arrayTeamMemberDTO;

        } else {

            return $result;
        }
    }


    /**
     * This method saves a new Team Member object to the database.
     *
     * Returns the id of the inserted row.
     * @param TeamMemberDTO $teamMemberDTO
     * @return int|string
     */
    public function saveTeamMember(TeamMemberDTO $teamMemberDTO) {

        $query = "INSERT INTO ".self::TABLE_TEAM_MEMBERS." ("
            .self::FIELD_IS_SHOWN.", "
            .self::FIELD_DISPLAY_ORDER.", "
            .self::FIELD_MEMBER_NAME.", "
            .self::FIELD_MEMBER_ROLE.", "
            .self::FIELD_MEMBER_ROLE_EN.", "
            .self::FIELD_FILE_NAME.", "
            .self::FIELD_MIME_TYPE.", "
            .self::FIELD_PATH.", "
            .self::FIELD_SIZE.", "
            .self::FIELD_CREATED_AT
            .") VALUES (
                    :".self::FIELD_IS_SHOWN.",
                    :".self::FIELD_DISPLAY_ORDER.",
                    :".self::FIELD_MEMBER_NAME.",
                    :".self::FIELD_MEMBER_ROLE.",
                    :".self::FIELD_MEMBER_ROLE_EN.",
                    :".self::FIELD_FILE_NAME.",
                    :".self::FIELD_MIME_TYPE.",
                    :".self::FIELD_PATH.",
                    :".self::FIELD_SIZE.",
                    null);";    // By setting the column FIELD_CREATED_AT to null
        // we make sure that both columns FIELD_CREATED_AT and FIELD_UPDATED_AT
        // will take the value CURRENT_TIMESTAMP.

        $query_params = array(
            ':'.self::FIELD_DISPLAY_ORDER               => $teamMemberDTO->getDisplayOrder(),
            ':'.self::FIELD_IS_SHOWN                    => $teamMemberDTO->isShown(),
            ':'.self::FIELD_MEMBER_NAME                 => $teamMemberDTO->getMemberName(),
            ':'.self::FIELD_MEMBER_ROLE                 => $teamMemberDTO->getMemberRole(),
            ':'.self::FIELD_MEMBER_ROLE_EN              => $teamMemberDTO->getMemberRoleEn(),
            ':'.self::FIELD_FILE_NAME                   => $teamMemberDTO->getFileName(),
            ':'.self::FIELD_MIME_TYPE                   => $teamMemberDTO->getMimeType(),
            ':'.self::FIELD_PATH                        => $teamMemberDTO->getPath(),
            ':'.self::FIELD_SIZE                        => $teamMemberDTO->getSize());

        // Execute the query. Third param to false indicates not to fetch results, as
        // an UPDATE or INSERT query wont return any rows.
        $result = $this->execute($query, $query_params, false);

        // If there was 1 role affected, that's there were no errors:
        if ($result == 1)   {
            // It will return the id of the new inserted row:
            $insertedTeamMemberId = $this->_db->lastInsertId();
            return $insertedTeamMemberId;
            // If there was an error we return -1.
        } else {
            return -1;
        }
    }


    /**
     * This method returns an array of TeamMemberDTO containing all rows stored in database
     * on team_members table and sorted by display_order column value.
     *
     * @return array
     */
    public function getTeamMembers() {

        $query = "SELECT * FROM ".self::TABLE_TEAM_MEMBERS
            ." WHERE 1 ORDER BY ".self::FIELD_DISPLAY_ORDER." ASC".", ".self::FIELD_ID." DESC";
        $arrayTeamMemberDTO = $this->execute($query, null, true);
        return $arrayTeamMemberDTO;
    }


    /**
     * Get a team_member logo from data base from its corresponding id.
     * @param $member_id
     * @return mixed
     */
    public function getTeamMemberById($member_id)   {

        $query = "SELECT * FROM ". self::TABLE_TEAM_MEMBERS ." WHERE ". self::TABLE_TEAM_MEMBERS.".".self::FIELD_ID ."= '$member_id'";
        $arrayTeamMemberDTO = $this->execute($query, null, true);
        return $arrayTeamMemberDTO[0];
    }


    /**
     * This method sets the field is_publish according to the parameter.
     *
     * @param $member_id
     * @param $is_shown
     * @return bool
     */
    public function setIsShown($member_id, $is_shown)    {

        $query =    "UPDATE ".self::TABLE_TEAM_MEMBERS
            ." SET `is_shown` = $is_shown"
            ." WHERE `id` = $member_id";

        return $this->execute($query, null, false);
    }


    /**
     * Delete a team member logo from database by its corresponding id.
     *
     * @param $team_member_id
     * @return array|bool
     */
    public function deleteTeamMemberById($team_member_id)  {

        // First we take the whole row to get the information:
        $teamMemberDTO = $this->getTeamMemberById($team_member_id);
        // We have to delete the corresponding image from file system.
        unlink($this->_rootpath.$teamMemberDTO->getPath().$teamMemberDTO->getFileName());

        // Finally delete the corresponding row on team_members table.
        $query = "DELETE FROM " .self::TABLE_TEAM_MEMBERS."  WHERE `id` = $team_member_id";
        return $this->execute($query, null, false);
    }

    /**
     * This method updates the column display_order for the given $member_id.
     *
     * @param $member_id
     * @param $new_display_order
     * @return array|bool
     */
    public function updateDisplayOrder($member_id, $new_display_order)    {

        $query =     "UPDATE ".self::TABLE_TEAM_MEMBERS
            ." SET `".self::FIELD_DISPLAY_ORDER."` = $new_display_order"
            ." WHERE `".self::FIELD_ID."` = $member_id";

        return $this->execute($query, null, false);
    }

}?>