<?php

class HomeBannerDAO {

    // DB Tables constants:
    const TABLE_HOME_BANNERS = "home_banners";

    // Constants for the form variables from app:
    const FIELD_ID                      = 'id';
    const FIELD_IS_SHOWN                = 'is_shown';
    const FIELD_DISPLAY_ORDER           = 'display_order';
    const FIELD_PATH                    = "path";

    const FIELD_CREATED_AT              = "created_at";
    const FIELD_UPDATED_AT              = "updated_at";


    // Variable to store the connexion from config.inc.php
    var $_db;
    // Variable rootpath taken from object creator.
    var $_rootpath;

    public function __construct() {

        global $db;
        global $rootpath;
        $this->_db = $db;
        $this->_rootpath = $rootpath;
    }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert was made correctly or not.
     *
     * @param $query
     * @param null $query_params
     * @param bool $fetchResults
     * @return bool|array
     */
    protected function execute($query, $query_params = null, $fetchResults = true) {

        try {

            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute($query_params);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0 && $fetchResults) {

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                $arrayHomeBannerDTO[$key] = new HomeBannerDTO();

                $arrayHomeBannerDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);
                $arrayHomeBannerDTO[$key]->setIsShown(array_key_exists(self::FIELD_IS_SHOWN, $row) ? $row[self::FIELD_IS_SHOWN] : null);
                $arrayHomeBannerDTO[$key]->setDisplayOrder(array_key_exists(self::FIELD_DISPLAY_ORDER, $row) ? $row[self::FIELD_DISPLAY_ORDER] : null);
                $arrayHomeBannerDTO[$key]->setPath(array_key_exists(self::FIELD_PATH, $row) ? $row[self::FIELD_PATH] : null);

                $arrayHomeBannerDTO[$key]->setCreatedAt(array_key_exists(self::FIELD_CREATED_AT, $row) ? $row[self::FIELD_CREATED_AT] : null);


                // GETTING FILE NAMES FROM FILE SYSTEM.
                // WE have to check if path is set, because we call getHomeBannerById() from method saveHomeBanner,
                // after creating a new HomeBannerDTO to set the path column and save the corresponding files.
                // Now we scan filesystem to fulfill the corresponding file names if the car path has been set.
                $currentPath = $arrayHomeBannerDTO[$key]->getPath();
                if (isset($currentPath) && $currentPath != "")  {
                    // Set rootpath to the file paths:
                    $currentPath = $this->_rootpath.$currentPath;

                    $files = array_filter(scandir($currentPath), function($item) use($currentPath) {
                        return !is_dir($currentPath . $item);
                    });
                    
                    foreach ($files as $index => $file_name)  {
                        $method_name = str_replace("_","",substr($file_name,0, 11));
                        $method_name[0] = strtoupper($method_name[0]);
                        $method_name[3] = strtoupper($method_name[3]);
                        $method_name[5] = strtoupper($method_name[5]);
                        $method_name = "set".$method_name."Name";

                        call_user_func(array($arrayHomeBannerDTO[$key], $method_name),$file_name );
                    }
                }
            }

            return $arrayHomeBannerDTO;

        } else {
            return $result;
        }
    }


    /**
     * This method saves or updates a HomeBannerDTO object to the database.
     *
     * Returns the id of the inserted row.
     * @param HomeBannerDTO $homeBannerDTO
     * @return int|string
     */
    public function saveHomeBanner(HomeBannerDTO $homeBannerDTO) {


        $currHomeBannerDTO = null;

        if($homeBannerDTO->getId() != "") {
            $currHomeBannerDTO = $this->getHomeBannerById($homeBannerDTO->getId());
        }

        // If the query returned a row then update,
        // otherwise insert a new user.
        if(sizeof($currHomeBannerDTO) > 0) {

            $query = "UPDATE ".self::TABLE_HOME_BANNERS
                ." SET "

                .self::FIELD_IS_SHOWN." = :".self::FIELD_IS_SHOWN.", "
                .self::FIELD_DISPLAY_ORDER." = :".self::FIELD_DISPLAY_ORDER.", "
                .self::FIELD_PATH." = :".self::FIELD_PATH

                ." WHERE "
                .self::FIELD_ID." = :".self::FIELD_ID;

            $query_params = array(

                ':'.self::FIELD_IS_SHOWN            =>    $homeBannerDTO->isShown(),
                ':'.self::FIELD_DISPLAY_ORDER       =>    $homeBannerDTO->getDisplayOrder(),
                ':'.self::FIELD_PATH                =>    $homeBannerDTO->getPath(),

                ':'.self::FIELD_ID                  =>    $homeBannerDTO->getId()
            );


            // IF IT IS AN UPDATE: Execute the query. Third param to false indicates not to fetch results, as
            // an UPDATE or INSERT query wont return any rows.
            $result = $this->execute($query,$query_params, false);

            // If there was 1 role affected, that's there were no errors:
            if ($result == 1)   {
                // It will return the id of the updated row:
                return $homeBannerDTO->getId();
                // If there was an error we return -1.
            } else {
                return -1;
            }


        } else {

            $query = "INSERT INTO " . self::TABLE_HOME_BANNERS . " ("
                . self::FIELD_IS_SHOWN . ", "
                . self::FIELD_DISPLAY_ORDER . ", "
                . self::FIELD_PATH .", "

                . self::FIELD_CREATED_AT . ") VALUES (
                    :" . self::FIELD_IS_SHOWN . ",
                    :" . self::FIELD_DISPLAY_ORDER . ",
                    :" . self::FIELD_PATH . ",

                    NULL);";    // By setting the column FIELD_CREATED_AT to null
            // we make sure that both columns FIELD_CREATED_AT and FIELD_UPDATED_AT
            // will take the value CURRENT_TIMESTAMP.

            $query_params = array(

                ':' . self::FIELD_DISPLAY_ORDER => $homeBannerDTO->getDisplayOrder(),
                ':' . self::FIELD_IS_SHOWN => $homeBannerDTO->isShown(),
                ':' . self::FIELD_PATH => $homeBannerDTO->getPath());

            // Execute the query. Third param to false indicates not to fetch results, as
            // an UPDATE or INSERT query wont return any rows.
            $result = $this->execute($query, $query_params, false);

            // If there was 1 role affected, that's there were no errors:
            if ($result == 1) {
                // It will return the id of the new inserted row:
                $insertedHomeBannerId = $this->_db->lastInsertId();
                return $insertedHomeBannerId;
                // If there was an error we return -1.
            } else {
                return -1;
            }
        }
    }


    /**
     * This method returns an array of HomeBannerDTO containing all rows stored in database
     * on home_banners table and sorted by display_order column value.
     *
     * @return array
     */
    public function getHomeBanners() {

        $query = "SELECT * FROM ".self::TABLE_HOME_BANNERS
            ." WHERE 1 ORDER BY ".self::FIELD_DISPLAY_ORDER." ASC".", ".self::FIELD_ID." DESC";
        $arrayHomeBannerDTO = $this->execute($query, null, true);
        return $arrayHomeBannerDTO;
    }

    /**
     * This method returns an array of HomeBannerDTO containing all rows stored in database
     * on home_banners where is_shown == true, and sorted by display_order column value.
     *
     * @return array
     */
    public function getDisplayedHomeBanners() {

        $query = "SELECT * FROM ".self::TABLE_HOME_BANNERS
            ." WHERE ".self::FIELD_IS_SHOWN." = 1 ORDER BY ".self::FIELD_DISPLAY_ORDER." ASC".", ".self::FIELD_ID." DESC";
        $arrayHomeBannerDTO = $this->execute($query, null, true);
        return $arrayHomeBannerDTO;
    }


    public function getHomeBannerById($banner_id)   {

        $query = "SELECT * FROM ". self::TABLE_HOME_BANNERS ." WHERE ". self::TABLE_HOME_BANNERS.".".self::FIELD_ID ."= '$banner_id'";
        $arrayHomeBannerDTO = $this->execute($query, null, true);
        return $arrayHomeBannerDTO[0];
    }


    /**
     * This method sets the field is_publish according to the parameter.
     *
     * @param $banner_id
     * @param $is_shown
     * @return bool
     */
    public function setIsShown($banner_id, $is_shown)    {

        $query =    "UPDATE ".self::TABLE_HOME_BANNERS
            ." SET `is_shown` = $is_shown"
            ." WHERE `id` = $banner_id";

        return $this->execute($query, null, false);
    }


    /**
     * This method deletes a home banner from database and all associated files.
     *
     * @param $banner_id
     * @return array|bool
     */
    public function deleteBannerById($banner_id)  {

        // First we take the whole row to get the information:
        $homeBanerDTO = $this->getHomeBannerById($banner_id);
        // We have to delete the corresponding image from file system.
        delete_files($homeBanerDTO->getPath());

        // Finnally delete the corresponding row on home_banners table.
        $query = "DELETE FROM " .self::TABLE_HOME_BANNERS."  WHERE `id` = $banner_id";
        return $this->execute($query, null, false);
    }

    /**
     * This method updates the column display_order for the given client_id.
     *
     * @param $baner_id
     * @param $new_display_order
     * @return array|bool
     */
    public function updateDisplayOrder($baner_id, $new_display_order)    {

        $query =     "UPDATE ".self::TABLE_HOME_BANNERS
            ." SET `".self::FIELD_DISPLAY_ORDER."` = $new_display_order"
            ." WHERE `".self::FIELD_ID."` = $baner_id";

        return $this->execute($query, null, false);
    }

}?>