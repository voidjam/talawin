<?php

class PrinterTransDAO {

    // DB Tables constants:
    const TABLE_PRINTERS_TRANSLATION    = "printers_translation";

    // Constants for the form variables from app.
    const FIELD_ID                      = 'id';
    const FIELD_PRINTER_ID              = "printer_id";
    const FIELD_LANG_CODE               = "lang_code";


    // Fields to build the printers html page.
    const FIELD_TITLE                   = "title";
    const FIELD_HEADLINE                = "headline";
    const FIELD_DESCRIPTION             = "description";
    const FIELD_HTML_OVERVIEW           = "html_overview";
    const FIELD_HTML_FEATURES           = "html_features";
    const FIELD_HTML_SPECS              = "html_specs";

    const FIELD_UPDATED_AT              = "updated_at";
    const FIELD_CREATED_AT              = "created_at";

    // Variables to store the connexion from config.inc.php
    var $_db;

    public function __construct() {

        // We make sure we are using the global $db and not a local one.
        global $db;
        $this->_db = $db;
    }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert/update was made correctly or not.
     */
    protected function execute($query, $query_params = null, $fetchResults = true) {

        try {

            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute($query_params);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0 && $fetchResults) {

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                $printerTransDTO[$key] = new PrinterTransDTO();

                $printerTransDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);

                $printerTransDTO[$key]->setPrinterId(array_key_exists(self::FIELD_PRINTER_ID, $row) ? $row[self::FIELD_PRINTER_ID] : null);
                $printerTransDTO[$key]->setLangCode(array_key_exists(self::FIELD_LANG_CODE, $row) ? $row[self::FIELD_LANG_CODE] : null);

                $printerTransDTO[$key]->setTitle(array_key_exists(self::FIELD_TITLE, $row) ? $row[self::FIELD_TITLE] : null);
                $printerTransDTO[$key]->setHeadline(array_key_exists(self::FIELD_HEADLINE, $row) ? $row[self::FIELD_HEADLINE] : null);
                $printerTransDTO[$key]->setDescription(array_key_exists(self::FIELD_DESCRIPTION, $row) ? $row[self::FIELD_DESCRIPTION] : null);
                $printerTransDTO[$key]->setHtmlOverview(array_key_exists(self::FIELD_HTML_OVERVIEW, $row) ? $row[self::FIELD_HTML_OVERVIEW] : null);
                $printerTransDTO[$key]->setHtmlFeatures(array_key_exists(self::FIELD_HTML_FEATURES, $row) ? $row[self::FIELD_HTML_FEATURES] : null);
                $printerTransDTO[$key]->setHtmlSpecs(array_key_exists(self::FIELD_HTML_SPECS, $row) ? $row[self::FIELD_HTML_SPECS] : null);

                $printerTransDTO[$key]->setUpdatedAt(array_key_exists(self::FIELD_UPDATED_AT, $row) ? $row[self::FIELD_UPDATED_AT] : null);
                $printerTransDTO[$key]->setCreatedAt(array_key_exists(self::FIELD_CREATED_AT, $row) ? $row[self::FIELD_CREATED_AT] : null);

            }

            return $printerTransDTO;

        } else {

            return $result;
        }
    }

    /**
     * This method saves or updates a printer to the database.
     *
     * Returns the id of the inserted row.
     * @param PrinterTransDTO $printerTransDTO
     * @return int|string
     */
    public function savePrinterTrans(PrinterTransDTO $printerTransDTO) {


        $currPrinterTransDTO = null;

        if($printerTransDTO->getId() != "") {
            $currPrinterTransDTO = $this->getPrinterTransById($printerTransDTO->getId());
        }

        // If the query returned a row then update,
        // otherwise insert a new user.
        if(sizeof($currPrinterTransDTO) > 0) {

            $query = "UPDATE ".self::TABLE_PRINTERS_TRANSLATION
                ." SET "

                .self::FIELD_PRINTER_ID." = :".self::FIELD_PRINTER_ID.", "
                .self::FIELD_LANG_CODE." = :".self::FIELD_LANG_CODE.", "

                .self::FIELD_TITLE." = :".self::FIELD_TITLE.", "
                .self::FIELD_HEADLINE." = :".self::FIELD_HEADLINE.", "
                .self::FIELD_DESCRIPTION." = :".self::FIELD_DESCRIPTION.", "
                .self::FIELD_HTML_OVERVIEW." = :".self::FIELD_HTML_OVERVIEW.", "
                .self::FIELD_HTML_FEATURES." = :".self::FIELD_HTML_FEATURES.", "
                .self::FIELD_HTML_SPECS." = :".self::FIELD_HTML_SPECS

                ." WHERE "
                .self::FIELD_ID." = :".self::FIELD_ID;


            $query_params = array(

                ':'.self::FIELD_PRINTER_ID            =>    $printerTransDTO->getPrinterId(),
                ':'.self::FIELD_LANG_CODE             =>    $printerTransDTO->getLangCode(),

                ':'.self::FIELD_TITLE                 =>    $printerTransDTO->getTitle(),
                ':'.self::FIELD_HEADLINE              =>    $printerTransDTO->getHeadline(),
                ':'.self::FIELD_DESCRIPTION           =>    $printerTransDTO->getDescription(),
                ':'.self::FIELD_HTML_OVERVIEW         =>    $printerTransDTO->getHtmlOverview(),
                ':'.self::FIELD_HTML_FEATURES         =>    $printerTransDTO->getHtmlFeatures(),
                ':'.self::FIELD_HTML_SPECS            =>    $printerTransDTO->getHtmlSpecs(),

                ':'.self::FIELD_ID                    =>    $printerTransDTO->getId()
            );


            // IF IT IS AN UPDATE: Execute the query. Third param to false indicates not to fetch results, as
            // an UPDATE or INSERT query wont return any rows.
            $result = $this->execute($query,$query_params, false);

            // If there was 1 role affected, that's there were no errors:
            if ($result == 1)   {
                // It will return the id of the updated row:
                return $printerTransDTO->getId();
                // If there was an error we return -1.
            } else {
                return -1;
            }


        } else {


            $query = "INSERT INTO ".self::TABLE_PRINTERS_TRANSLATION." ("

                .self::FIELD_PRINTER_ID.", "
                .self::FIELD_LANG_CODE.", "

                .self::FIELD_TITLE.", "
                .self::FIELD_HEADLINE.", "
                .self::FIELD_DESCRIPTION.", "
                .self::FIELD_HTML_OVERVIEW.", "
                .self::FIELD_HTML_FEATURES.", "
                .self::FIELD_HTML_SPECS.", "

                .self::FIELD_CREATED_AT

                .") VALUES (
                        :".self::FIELD_PRINTER_ID.",              
                        :".self::FIELD_LANG_CODE.",
                        
                        :".self::FIELD_TITLE.",                 
                        :".self::FIELD_HEADLINE.",              
                        :".self::FIELD_DESCRIPTION.", 
                        :".self::FIELD_HTML_OVERVIEW.",         
                        :".self::FIELD_HTML_FEATURES.",         
                        :".self::FIELD_HTML_SPECS.",            
                        
                        null);";    // By setting the column FIELD_CREATED_AT to null
            // we make sure that both columns FIELD_CREATED_AT and FIELD_UPDATED_AT
            // will take the value CURRENT_TIMESTAMP.


            $query_params = array(

                ':'.self::FIELD_PRINTER_ID            =>    $printerTransDTO->getPrinterId(),
                ':'.self::FIELD_LANG_CODE             =>    $printerTransDTO->getLangCode(),

                ':'.self::FIELD_TITLE                 =>    $printerTransDTO->getTitle(),
                ':'.self::FIELD_HEADLINE              =>    $printerTransDTO->getHeadline(),
                ':'.self::FIELD_DESCRIPTION           =>    $printerTransDTO->getDescription(),
                ':'.self::FIELD_HTML_OVERVIEW         =>    $printerTransDTO->getHtmlOverview(),
                ':'.self::FIELD_HTML_FEATURES         =>    $printerTransDTO->getHtmlFeatures(),
                ':'.self::FIELD_HTML_SPECS            =>    $printerTransDTO->getHtmlSpecs()
            );

            // Execute the query. Third param to false indicates not to fetch results, as
            // an UPDATE or INSERT query wont return any rows.
            $result = $this->execute($query, $query_params, false);

            // If there was 1 role affected, that's there were no errors:
            if ($result == 1)   {
                // It will return the id of the new inserted file:
                $insertedWatchId = $this->_db->lastInsertId();
                return $insertedWatchId;
                // If there was an error we return -1.
            } else {
                return -1;
            }
        }
    }



    public function getPrinterTransById($printerTransId)   {

        $query = "SELECT * FROM ".self::TABLE_PRINTERS_TRANSLATION
            ." WHERE ".self::TABLE_PRINTERS_TRANSLATION.".".self::FIELD_ID." = '$printerTransId' LIMIT 1";
        $arrayFileDTO = $this->execute($query, null, true);

        return $arrayFileDTO[0];
    }


    /**
     * Retrieves all rows from table printers_translation fetched as PrinterTransDTO instances
     * for a given printer_id.
     *
     * @return bool
     */
    public function getPrintersTransByPrinterId($printerId)   {


        $query = "SELECT * FROM ".self::TABLE_PRINTERS_TRANSLATION
        ." WHERE ".self::TABLE_PRINTERS_TRANSLATION.".".self::FIELD_PRINTER_ID." = '$printerId' LIMIT 1";

        $arrayPrinterTransDTO = $this->execute($query, null, true);

        return $arrayPrinterTransDTO[0];
    }


    /**
     * This method deletes a translation from database table printers_translation for
     * the corresponding printer_id.
     *
     * @param $printerId
     * @return bool
     */
    public function deletePrinterTransByPrinterId($printerId)  {

        // Delete the corresponding row on printers_translation table.
        $query = "DELETE FROM ".self::TABLE_PRINTERS_TRANSLATION
            ." WHERE ".self::TABLE_PRINTERS_TRANSLATION.".".self::FIELD_PRINTER_ID." = '$printerId' LIMIT 1";
        return $this->execute($query, null, false);
    }


}
?>