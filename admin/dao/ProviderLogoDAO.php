<?php

class ProviderLogoDAO {

    // DB Tables constants:
    const TABLE_PROVIDERS_LOGOS = "providers_logos";

    // Constants for the form variables from app:
    const FIELD_ID              = 'id';
    const FIELD_IS_SHOWN        = 'is_shown';
    const FIELD_DISPLAY_ORDER   = 'display_order';
    const FIELD_PROVIDER_NAME   = "provider_name";
    const FIELD_PROVIDER_LINK   = "provider_link";
    const FIELD_FILE_NAME       = "file_name";
    const FIELD_MIME_TYPE       = "mime_type";
    const FIELD_PATH            = "path";
    const FIELD_SIZE            = "size";

    const FIELD_CREATED_AT      = "created_at";
    const FIELD_UPDATED_AT      = "updated_at";


    // Variables to store the connexion from config.inc.php
    var $_db;
    // Variable rootpath taken from object creator.
    var $_rootpath;

    public function __construct() {

        global $db;
        global $rootpath;
        $this->_db = $db;
        $this->_rootpath = $rootpath;
    }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert was made correctly or not.
     *
     * @return bool|array
     */
    protected function execute($query, $query_params = null, $fetchResults = true) {

        try {

            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute($query_params);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0 && $fetchResults) {

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                $arrayProviderLogoDTO[$key] = new ProviderLogoDTO();

                $arrayProviderLogoDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);
                $arrayProviderLogoDTO[$key]->setIsShown(array_key_exists(self::FIELD_IS_SHOWN, $row) ? $row[self::FIELD_IS_SHOWN] : null);
                $arrayProviderLogoDTO[$key]->setDisplayOrder(array_key_exists(self::FIELD_DISPLAY_ORDER, $row) ? $row[self::FIELD_DISPLAY_ORDER] : null);
                $arrayProviderLogoDTO[$key]->setProviderName(array_key_exists(self::FIELD_PROVIDER_NAME, $row) ? $row[self::FIELD_PROVIDER_NAME] : null);
                $arrayProviderLogoDTO[$key]->setProviderLink(array_key_exists(self::FIELD_PROVIDER_LINK, $row) ? $row[self::FIELD_PROVIDER_LINK] : null);
                $arrayProviderLogoDTO[$key]->setFileName(array_key_exists(self::FIELD_FILE_NAME, $row) ? $row[self::FIELD_FILE_NAME] : null);
                $arrayProviderLogoDTO[$key]->setMimeType(array_key_exists(self::FIELD_MIME_TYPE, $row) ? $row[self::FIELD_MIME_TYPE] : null);
                $arrayProviderLogoDTO[$key]->setSize(array_key_exists(self::FIELD_SIZE, $row) ? $row[self::FIELD_SIZE] : null);
                $arrayProviderLogoDTO[$key]->setPath(array_key_exists(self::FIELD_PATH, $row) ? $row[self::FIELD_PATH] : null);
                $arrayProviderLogoDTO[$key]->setCreatedAt(array_key_exists(self::FIELD_CREATED_AT, $row) ? $row[self::FIELD_CREATED_AT] : null);
            }

            return $arrayProviderLogoDTO;

        } else {

            return $result;
        }
    }


    /**
     * This method saves a new Provider Logo object to the database.
     *
     * Returns the id of the inserted row.
     * @param ProviderLogoDTO $providerLogoDTO
     * @return int|string
     */
    public function saveProviderLogo(ProviderLogoDTO $providerLogoDTO) {

        $query = "INSERT INTO ".self::TABLE_PROVIDERS_LOGOS." ("
            .self::FIELD_IS_SHOWN.", "
            .self::FIELD_DISPLAY_ORDER.", "
            .self::FIELD_PROVIDER_NAME.", "
            .self::FIELD_PROVIDER_LINK.", "
            .self::FIELD_FILE_NAME.", "
            .self::FIELD_MIME_TYPE.", "
            .self::FIELD_PATH.", "
            .self::FIELD_SIZE.", "
            .self::FIELD_CREATED_AT
            .") VALUES (
                    :".self::FIELD_IS_SHOWN.",
                    :".self::FIELD_DISPLAY_ORDER.",
                    :".self::FIELD_PROVIDER_NAME.",
                    :".self::FIELD_PROVIDER_LINK.",
                    :".self::FIELD_FILE_NAME.",
                    :".self::FIELD_MIME_TYPE.",
                    :".self::FIELD_PATH.",
                    :".self::FIELD_SIZE.",
                    null);";    // By setting the column FIELD_CREATED_AT to null
        // we make sure that both columns FIELD_CREATED_AT and FIELD_UPDATED_AT
        // will take the value CURRENT_TIMESTAMP.

        $query_params = array(
            ':'.self::FIELD_DISPLAY_ORDER               => $providerLogoDTO->getDisplayOrder(),
            ':'.self::FIELD_IS_SHOWN                    => $providerLogoDTO->isShown(),
            ':'.self::FIELD_PROVIDER_NAME               => $providerLogoDTO->getproviderName(),
            ':'.self::FIELD_PROVIDER_LINK               => $providerLogoDTO->getProviderLink(),
            ':'.self::FIELD_FILE_NAME                   => $providerLogoDTO->getFileName(),
            ':'.self::FIELD_MIME_TYPE                   => $providerLogoDTO->getMimeType(),
            ':'.self::FIELD_PATH                        => $providerLogoDTO->getPath(),
            ':'.self::FIELD_SIZE                        => $providerLogoDTO->getSize());

        // Execute the query. Third param to false indicates not to fetch results, as
        // an UPDATE or INSERT query wont return any rows.
        $result = $this->execute($query, $query_params, false);

        // If there was 1 role affected, that's there were no errors:
        if ($result == 1)   {
            // It will return the id of the new inserted row:
            $insertedProviderLogoId = $this->_db->lastInsertId();
            return $insertedProviderLogoId;
            // If there was an error we return -1.
        } else {
            return -1;
        }
    }


    /**
     * This method returns an array of ProviderLogoDTO containing all rows stored in database
     * on providers_logos table and sorted by display_order column value.
     *
     * @return array
     */
    public function getProvidersLogos() {

        $query = "SELECT * FROM ".self::TABLE_PROVIDERS_LOGOS
            ." WHERE 1 ORDER BY ".self::FIELD_DISPLAY_ORDER." ASC".", ".self::FIELD_ID." DESC";
        $arrayProviderLogoDTO = $this->execute($query, null, true);
        return $arrayProviderLogoDTO;
    }


    /**
     * Get a provider logo from data base from its corresponding id.
     * @param $provider_id
     * @return mixed
     */
    public function getProviderLogoById($provider_id)   {

        $query = "SELECT * FROM ". self::TABLE_PROVIDERS_LOGOS ." WHERE ". self::TABLE_PROVIDERS_LOGOS.".".self::FIELD_ID ."= '$provider_id'";
        $arrayProvidersLogosDTO = $this->execute($query, null, true);
        return $arrayProvidersLogosDTO[0];
    }


    /**
     * This method sets the field is_publish according to the parameter.
     *
     * @param $provider_id
     * @param $is_shown
     * @return bool
     */
    public function setIsShown($provider_id, $is_shown)    {
        $query =    "UPDATE ".self::TABLE_PROVIDERS_LOGOS
            ." SET `is_shown` = $is_shown"
            ." WHERE `id` = $provider_id";

        return $this->execute($query, null, false);
    }


    /**
     * Delete a provider logo from database by its corresponding id.
     *
     * @param $provider_logo_id
     * @return array|bool
     */
    public function deleteProviderLogoById($provider_logo_id)  {

        // First we take the whole row to get the information:
        $providerLogoDTO = $this->getProviderLogoById($provider_logo_id);
        // We have to delete the corresponding image from file system.
        unlink($this->_rootpath.$providerLogoDTO->getPath().$providerLogoDTO->getFileName());

        // Finnally delete the corresponding row on providers_logos table.
        $query = "DELETE FROM " .self::TABLE_PROVIDERS_LOGOS."  WHERE `id` = $provider_logo_id";
        return $this->execute($query, null, false);
    }

    /**
     * This method updates the column display_order for the given provider_id.
     *
     * @param $provider_id
     * @param $new_display_order
     * @return array|bool
     */
    public function updateDisplayOrder($provider_id, $new_display_order)    {

        $query =     "UPDATE ".self::TABLE_PROVIDERS_LOGOS
            ." SET `".self::FIELD_DISPLAY_ORDER."` = $new_display_order"
            ." WHERE `".self::FIELD_ID."` = $provider_id";

        return $this->execute($query, null, false);
    }

}?>