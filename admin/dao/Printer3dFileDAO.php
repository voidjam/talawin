<?php

class Printer3dFileDAO {

    // DB Tables constants:
    const TABLE_FILES= "printer3d_files";

    // Constants for the form variables from app:
    const FIELD_ID              = 'id';
    const FIELD_UUID            = 'uuid';
    const FIELD_IS_MAIN         = "is_main";
    const FIELD_PRINTER3D_ID    = "printer3d_id";
    const FIELD_NAME            = "name";
    const FIELD_MIME_TYPE       = "mime_type";
    const FIELD_PATH            = "path";
    const FIELD_SIZE            = "size";



    // Variables to store the connexion from config.inc.php
    var $_db;

    public function __construct() {

        global $db;
        $this->_db = $db;
    }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert was made correctly or not.
     * @param $query
     * @param null $query_params
     * @param bool $fetchResults
     * @return bool|array
     */
    protected function execute($query, $query_params = null, $fetchResults = true) {

        try {

            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute($query_params);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0 && $fetchResults) {

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                $printer3dFileDTO[$key] = new Printer3dFileDTO();

                $printer3dFileDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);
                $printer3dFileDTO[$key]->setUuid(array_key_exists(self::FIELD_UUID, $row) ? $row[self::FIELD_UUID] : null);
                $printer3dFileDTO[$key]->setPrinter_3d_id(array_key_exists(self::FIELD_PRINTER3D_ID, $row) ? $row[self::FIELD_PRINTER3D_ID] : null);
                $printer3dFileDTO[$key]->setName(array_key_exists(self::FIELD_NAME, $row) ? $row[self::FIELD_NAME] : null);
                $printer3dFileDTO[$key]->setMime_type(array_key_exists(self::FIELD_MIME_TYPE, $row) ? $row[self::FIELD_MIME_TYPE] : null);
                $printer3dFileDTO[$key]->setPath(array_key_exists(self::FIELD_PATH, $row) ? $row[self::FIELD_PATH] : null);
                $printer3dFileDTO[$key]->setIs_main(array_key_exists(self::FIELD_IS_MAIN, $row) ? $row[self::FIELD_IS_MAIN] : null);
                $printer3dFileDTO[$key]->setSize(array_key_exists(self::FIELD_SIZE, $row) ? $row[self::FIELD_SIZE] : null);
            }

            return $printer3dFileDTO;

        } else {

            return $result;
        }
    }


    /**
     * This method saves a file to the database.
     *
     * @param $uuid
     * @param $printerId
     * @param $isMain
     * @param $fileName
     * @param $fileMimeType
     * @param $filePath
     * @return int Returns the id of the inserted row.
     */
    public function saveAttachedFile($uuid, $printerId, $isMain, $fileName, $fileMimeType, $filePath, $fileSize) {

        $query = "INSERT INTO ".self::TABLE_FILES." ("
            .self::FIELD_UUID.", "
            .self::FIELD_PRINTER3D_ID.", "
            .self::FIELD_IS_MAIN.", "
            .self::FIELD_NAME.", "
            .self::FIELD_MIME_TYPE.", "
            .self::FIELD_SIZE.", "
            .self::FIELD_PATH
            .") VALUES (
                    :".self::FIELD_UUID.",
                    :".self::FIELD_PRINTER3D_ID.",
                    :".self::FIELD_IS_MAIN.",
                    :".self::FIELD_NAME.",
                    :".self::FIELD_MIME_TYPE.",
                    :".self::FIELD_SIZE.",
                    :".self::FIELD_PATH
            .") ";

        $query_params = array(':'.self::FIELD_UUID          => $uuid,
            ':'.self::FIELD_PRINTER3D_ID    => $printerId,
            ':'.self::FIELD_IS_MAIN       => $isMain,
            ':'.self::FIELD_NAME 		    => $fileName,
            ':'.self::FIELD_MIME_TYPE	    => $fileMimeType,
            ':'.self::FIELD_SIZE	        => $fileSize,
            ':'.self::FIELD_PATH 	        => $filePath);

        // Execute the query. Third param to false indicates not to fetch results, as
        // an UPDATE or INSERT query wont return any rows.
        $result = $this->execute($query, $query_params, false);

        // If there was 1 role affected, that's there were no errors:
        if ($result = 1)   {
            // It will return the id of the new inserted file:
            $insertedFileId = $this->_db->lastInsertId();

            return $insertedFileId;
            // If there was an error we return -1.
        } else {
            return -1;
        }
    }


    /**
     * @param $printer_id
     * @return array|bool|int
     */
    public function getFilesByPrinter3dId($printer_id)   {

        $query = "SELECT * " .
            "FROM printer3d_files WHERE printer3d_id = '$printer_id' ORDER BY is_main DESC";

        $filesArray = $this->execute($query, null, true);

        return (is_array($filesArray) ? $filesArray: 0);
    }

    public function getImagesByPrinter3dId($printer_id){

        $query = "SELECT * " . "FROM printer3d_files WHERE printer3d_id = '$printer_id' AND `mime_type` LIKE '%image%' ORDER BY is_main DESC, id";

        $filesArray = $this->execute($query, null, true);

        // Order files array by its field name.
        usort($filesArray, array('Printer3dFileDAO', 'sortByFileNAme'));

        return (is_array($filesArray) ? $filesArray : 0);
    }

    /**
     * Returns the main image for the corresponding printer3d.
     *
     * @param $printer_id
     * @return int|mixed
     */
    public function getMainImageByPrinter3dId($printer_id){

        $query = "SELECT * " . "FROM printer3d_files WHERE printer3d_id = '$printer_id' AND `is_main` = 1 AND `mime_type` LIKE '%image%' ORDER BY is_main DESC";

        $filesArray = $this->execute($query, null, true);

        return (is_array($filesArray) ? $filesArray[0] : 0);
    }

    /**
     * getMainImageByPrinter3dId alias.
     *
     * @param $printer_id
     * @return int|mixed
     */
    public function getMainImageByPrinterId($printer_id){

        return $this->getMainImageByPrinter3dId($printer_id);
    }


    public function getDocumentsByPrinter3dId($printer_id)   {

        $query = "SELECT * " .
            "FROM printer3d_files WHERE `printer3d_id` = '$printer_id' && `mime_type` LIKE '%pdf%'";

        $filesArray = $this->execute($query, null, true);

        return (is_array($filesArray) ? $filesArray: 0);
    }



    public function getFileById($file_id)   {

        $query = "SELECT name, mime_type, size, content " .
            "FROM printer3d_files WHERE printer3d_files.id = '$file_id'";

        $arrayFileDTO = $this->execute($query, null, true);

        return $arrayFileDTO[0];
    }


    public function getFileByUuid($uuid)   {

        $query = "SELECT * " .
            "FROM printer3d_files WHERE printer3d_files.uuid = '$uuid'";

        $arrayFileDTO = $this->execute($query, null, true);

        return $arrayFileDTO[0];
    }



    /**
     * This method delete the file row for the corresponding uuid column.
     *
     * @param $file_uuid
     * @return bool
     */
    public function deleteFileByUuid($file_uuid)   {

        $query = "DELETE " .
            "FROM printer3d_files WHERE printer3d_files.uuid = '$file_uuid'";

        return $this->execute($query, null, false);
    }

    /**
     * This method deletes all the files table rows related to the corresponding printer id.
     *
     * @param $printer_id
     * @return bool
     */
    public function deleteFilesByPrinterId($printer_id)   {

        $query = "DELETE " .
            "FROM printer3d_files WHERE printer3d_files.printered_id = '$printer_id'";

        return $this->execute($query, null, false);
    }

    /**
     * This method checks if the corresponding printer has one main picture associated.
     *
     * @param $printer_id The ID of the printer to check.
     * @return bool If the printer has main picture or not.
     */
    public function checkHasMain($printer_id)  {

        $query = "SELECT count(printer3d_files.id) from printer3d_files where printer3d_files.is_main = true AND printer3d_files.printer3d_id = '$printer_id' AND printer3d_files.mime_type LIKE '%image%'";

        $stmt = $this->_db->prepare($query);
        $stmt->execute();
        return $stmt->fetchColumn();
    }

    /**
     * This functions check if there are at least one uploaded image file for the corresponding printer3d_id.
     *
     * @param $printer_id
     * @return string
     */
    public function hasImages($printer_id)  {

        $query = "SELECT count(printer3d_files.id) from printer3d_files where printer3d_files.printer3d_id = $printer_id AND printer3d_files.mime_type LIKE '%image%'";

        $stmt = $this->_db->prepare($query);
        $stmt->execute();
        return (intval($stmt->fetchColumn()) > 0);
    }


    /**
     * This methods make one picture the main one setting its field on
     * database. It ensures that the old main picture is set as a non main picture
     * first.
     * @param string $file_id The id of the file we want to make the main one.
     *
     * @return bool
     */
    public function makeMain($file_id)  {


        $query = " SELECT @PRINTER3D_ID:=printer3d_id FROM printer3d_files WHERE printer3d_files.id='$file_id' LIMIT 1; ";

        $query.= " UPDATE `printer3d_files` SET printer3d_files.is_main = false WHERE printer3d_files.printer3d_id = @PRINTER3D_ID; ";

        $query.= " UPDATE printer3d_files SET printer3d_files.is_main = true WHERE printer3d_files.id = '$file_id'; ";

        return $this->execute($query, null, false);
    }


    /**
     * This method is used to naturally sort 2 FileDTO objects by its field name.
     * If any of the files has is_main == true then this field will be given priority in the soring algorithm.
     *
     * @param $fileDTO_1
     * @param $fileDTO_2
     * @return int
     */
    private static function sortByFileNAme($fileDTO_1, $fileDTO_2)    {

        if ($fileDTO_1->getIs_main() == 1)  return -1;
        if ($fileDTO_2->getIs_main() == 1)  return  1;
        return strnatcmp($fileDTO_1->getName(), $fileDTO_2->getName());
    }

}?>