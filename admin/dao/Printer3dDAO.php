<?php

class Printer3dDAO {

    // DB Tables constants:
    const TABLE_PRINTERS3D              = "printers3d";

    // Constants for the form variables from app.
    const FIELD_ID                      = 'id';

    // General features.
    const FIELD_BRAND_ID                = "brand_id";
    const FIELD_DISPLAY_ORDER           = "display_order";
    const FIELD_MODEL                   = "model";
    const FIELD_IS_NEW                  = "is_new";
    const FIELD_IS_RELEASE              = "is_release";
    const FIELD_IS_PUBLISHED            = "is_published";

    // Private fields.
    const FIELD_REF                     = 'ref';
    const FIELD_PRICE_PVP               = "price_pvp";
    const FIELD_PRICE_COST              = "price_cost";
    const FIELD_COMMENTS                = 'comments';

    // Specific fields for outlet printers.
    const FIELD_PRODUCTION_DATE         = "production_date";
    const FIELD_WARRANTY                = "warranty";
    const FIELD_IS_SOLD                 = "is_sold";

    // Tech Specifications.
    const FIELD_FEAT_INK_ID             = "feat_ink_id";
    const FIELD_FEAT_MEDIA_TYPE_ID      = "feat_media_type_id";
    const FIELD_FEAT_NUM_HEADERS        = "feat_num_headers";

    const FIELD_FEAT_MAX_PRINT_VOL      = "feat_max_print_vol";
    const FIELD_FEAT_QUALITY            = "feat_quality";
    const FIELD_FEAT_SOFTWARE           = "feat_software";
    const FIELD_FEAT_PRINT_SPEED        = "feat_print_speed";



    // Fields to build the printers html page.
    const FIELD_TITLE                   = "title";
    const FIELD_HEADLINE                = "headline";
    const FIELD_DESCRIPTION             = "description";
    const FIELD_HTML_OVERVIEW           = "html_overview";
    const FIELD_HTML_FEATURES           = "html_features";
    const FIELD_HTML_SPECS              = "html_specs";

    const FIELD_UPDATED_AT              = "updated_at";
    const FIELD_CREATED_AT              = "created_at";



    // Variables to store the connexion from config.inc.php
    var $_db;

    public function __construct() {

        // We make sure we are using the global $db and not a local one.
        global $db;
        $this->_db = $db;
    }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert/update was made correctly or not.
     */
    protected function execute($query, $query_params = null, $fetchResults = true) {

        try {

            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute($query_params);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0 && $fetchResults) {

            $printer3dApplicationDAO = new Printer3dApplicationDAO();
            $printer3dTransDAO = new Printer3dTransDAO();
            $printer3dFileDAO = new Printer3dFileDAO();

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                $printer3dDTO[$key] = new Printer3dDTO();

                $printer3dDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);

                $printer3dDTO[$key]->setBrandId(array_key_exists(self::FIELD_BRAND_ID, $row) ? $row[self::FIELD_BRAND_ID] : null);
                $printer3dDTO[$key]->setDisplayOrder(array_key_exists(self::FIELD_DISPLAY_ORDER, $row) ? $row[self::FIELD_DISPLAY_ORDER] : null);
                $printer3dDTO[$key]->setModel(array_key_exists(self::FIELD_MODEL, $row) ? $row[self::FIELD_MODEL] : null);
                $printer3dDTO[$key]->setIsNew(array_key_exists(self::FIELD_IS_NEW, $row) ? $row[self::FIELD_IS_NEW] : null);
                $printer3dDTO[$key]->setIsPublished(array_key_exists(self::FIELD_IS_PUBLISHED, $row) ? $row[self::FIELD_IS_PUBLISHED] : null);
                $printer3dDTO[$key]->setIsRelease(array_key_exists(self::FIELD_IS_RELEASE, $row) ? $row[self::FIELD_IS_RELEASE] : null);


                $printer3dDTO[$key]->setRef(array_key_exists(self::FIELD_REF, $row) ? $row[self::FIELD_REF] : null);
                $printer3dDTO[$key]->setPricePvp(array_key_exists(self::FIELD_PRICE_PVP, $row) ? $row[self::FIELD_PRICE_PVP] : null);
                $printer3dDTO[$key]->setPriceCost(array_key_exists(self::FIELD_PRICE_COST, $row) ? $row[self::FIELD_PRICE_COST] : null);
                $printer3dDTO[$key]->setComments(array_key_exists(self::FIELD_COMMENTS, $row) ? $row[self::FIELD_COMMENTS] : null);

                $printer3dDTO[$key]->setProductionDate(array_key_exists(self::FIELD_PRODUCTION_DATE, $row) ? $row[self::FIELD_PRODUCTION_DATE] : null);
                $printer3dDTO[$key]->setWarranty(array_key_exists(self::FIELD_WARRANTY, $row) ? $row[self::FIELD_WARRANTY] : null);
                $printer3dDTO[$key]->setIsSold(array_key_exists(self::FIELD_IS_SOLD, $row) ? $row[self::FIELD_IS_SOLD] : null);

                $printer3dDTO[$key]->setFeatInkId(array_key_exists(self::FIELD_FEAT_INK_ID, $row) ? $row[self::FIELD_FEAT_INK_ID] : null);
                $printer3dDTO[$key]->setFeatNumHeaders(array_key_exists(self::FIELD_FEAT_NUM_HEADERS, $row) ? $row[self::FIELD_FEAT_NUM_HEADERS] : null);
                $printer3dDTO[$key]->setFeatMediaTypeId(array_key_exists(self::FIELD_FEAT_MEDIA_TYPE_ID, $row) ? $row[self::FIELD_FEAT_MEDIA_TYPE_ID] : null);
                $printer3dDTO[$key]->setFeatMaxPrintVol(array_key_exists(self::FIELD_FEAT_MAX_PRINT_VOL, $row) ? $row[self::FIELD_FEAT_MAX_PRINT_VOL] : null);
                $printer3dDTO[$key]->setFeatQuality(array_key_exists(self::FIELD_FEAT_QUALITY, $row) ? $row[self::FIELD_FEAT_QUALITY] : null);
                $printer3dDTO[$key]->setFeatSoftware(array_key_exists(self::FIELD_FEAT_SOFTWARE, $row) ? $row[self::FIELD_FEAT_SOFTWARE] : null);
                $printer3dDTO[$key]->setFeatPrintSpeed(array_key_exists(self::FIELD_FEAT_PRINT_SPEED, $row) ? $row[self::FIELD_FEAT_PRINT_SPEED] : null);

                $printer3dDTO[$key]->setTitle(array_key_exists(self::FIELD_TITLE, $row) ? $row[self::FIELD_TITLE] : null);
                $printer3dDTO[$key]->setHeadline(array_key_exists(self::FIELD_HEADLINE, $row) ? $row[self::FIELD_HEADLINE] : null);
                $printer3dDTO[$key]->setDescription(array_key_exists(self::FIELD_DESCRIPTION, $row) ? $row[self::FIELD_DESCRIPTION] : null);
                $printer3dDTO[$key]->setHtmlOverview(array_key_exists(self::FIELD_HTML_OVERVIEW, $row) ? $row[self::FIELD_HTML_OVERVIEW] : null);
                $printer3dDTO[$key]->setHtmlFeatures(array_key_exists(self::FIELD_HTML_FEATURES, $row) ? $row[self::FIELD_HTML_FEATURES] : null);
                $printer3dDTO[$key]->setHtmlSpecs(array_key_exists(self::FIELD_HTML_SPECS, $row) ? $row[self::FIELD_HTML_SPECS] : null);

                // Get applications array for each Printer3dDTO object.
                $printer3dDTO[$key]->setAppsArray($printer3dApplicationDAO->getAppsArrayByPrinter3dId($printer3dDTO[$key]->getId()));
                // Get files array for each Printer3dDTO object.
                $printer3dDTO[$key]->setFilesArray($printer3dFileDAO->getFilesByPrinter3dId($printer3dDTO[$key]->getId()));
                // Get the translation for this printer on a Printer3dTransDTO object.
                $printer3dDTO[$key]->setPrinter3dTransDTO($printer3dTransDAO->getPrinters3dTransByPrinter3dId($printer3dDTO[$key]->getId()));
            }

            return $printer3dDTO;

        } else {

            return $result;
        }
    }

    /**
     * This method saves or updates a printer to the database.
     *
     * Returns the id of the inserted row.
     * @param Printer3dDTO $printer3dDTO
     * @return int|string
     */
    public function savePrinter(Printer3dDTO $printer3dDTO) {


        $currPrinter3dDTO = null;

        if($printer3dDTO->getId() != "") {
            $currPrinter3dDTO = $this->getPrinter3dById($printer3dDTO->getId());
        }

        // If the query returned a row then update,
        // otherwise insert a new user.
        if(sizeof($currPrinter3dDTO) > 0) {

            $sql = "UPDATE ".self::TABLE_PRINTERS3D
                ." SET "

                .self::FIELD_BRAND_ID." = :".self::FIELD_BRAND_ID.", "
                .self::FIELD_MODEL." = :".self::FIELD_MODEL.", "
                .self::FIELD_IS_NEW." = :".self::FIELD_IS_NEW.", "
                .self::FIELD_IS_PUBLISHED." = :".self::FIELD_IS_PUBLISHED.", "
                .self::FIELD_IS_RELEASE." = :".self::FIELD_IS_RELEASE.", "

                .self::FIELD_REF." = :".self::FIELD_REF.", "
                .self::FIELD_PRICE_PVP." = :".self::FIELD_PRICE_PVP.", "
                .self::FIELD_PRICE_COST." = :".self::FIELD_PRICE_COST.", "
                .self::FIELD_COMMENTS." = :".self::FIELD_COMMENTS.", "

                .self::FIELD_PRODUCTION_DATE." = :".self::FIELD_PRODUCTION_DATE.", "
                .self::FIELD_WARRANTY." = :".self::FIELD_WARRANTY.", "
                .self::FIELD_IS_SOLD." = :".self::FIELD_IS_SOLD.", "

                .self::FIELD_FEAT_INK_ID." = :".self::FIELD_FEAT_INK_ID.", "
                .self::FIELD_FEAT_NUM_HEADERS." = :".self::FIELD_FEAT_NUM_HEADERS.", "
                .self::FIELD_FEAT_MEDIA_TYPE_ID." = :".self::FIELD_FEAT_MEDIA_TYPE_ID.", "
                .self::FIELD_FEAT_MAX_PRINT_VOL." = :".self::FIELD_FEAT_MAX_PRINT_VOL.", "
                .self::FIELD_FEAT_QUALITY." = :".self::FIELD_FEAT_QUALITY.", "
                .self::FIELD_FEAT_SOFTWARE." = :".self::FIELD_FEAT_SOFTWARE.", "
                .self::FIELD_FEAT_PRINT_SPEED." = :".self::FIELD_FEAT_PRINT_SPEED.", "

                .self::FIELD_TITLE." = :".self::FIELD_TITLE.", "
                .self::FIELD_HEADLINE." = :".self::FIELD_HEADLINE.", "
                .self::FIELD_DESCRIPTION." = :".self::FIELD_DESCRIPTION.", "
                .self::FIELD_HTML_OVERVIEW." = :".self::FIELD_HTML_OVERVIEW.", "
                .self::FIELD_HTML_FEATURES." = :".self::FIELD_HTML_FEATURES.", "
                .self::FIELD_HTML_SPECS." = :".self::FIELD_HTML_SPECS

                ." WHERE "
                .self::FIELD_ID." = :".self::FIELD_ID;


            $query_params = array(

                ':'.self::FIELD_BRAND_ID              =>    $printer3dDTO->getBrandId(),
                ':'.self::FIELD_MODEL                 =>    $printer3dDTO->getModel(),
                ':'.self::FIELD_IS_NEW                =>    $printer3dDTO->isNew(),
                ':'.self::FIELD_IS_PUBLISHED          =>    $printer3dDTO->isPublished(),
                ':'.self::FIELD_IS_RELEASE            =>    $printer3dDTO->isRelease(),

                ':'.self::FIELD_REF                   =>    $printer3dDTO->getRef(),
                ':'.self::FIELD_PRICE_PVP             =>    $printer3dDTO->getPricePvp(),
                ':'.self::FIELD_PRICE_COST            =>    $printer3dDTO->getPriceCost(),
                ':'.self::FIELD_COMMENTS              =>    $printer3dDTO->getComments(),

                ':'.self::FIELD_PRODUCTION_DATE       =>    $printer3dDTO->getProductionDate(),
                ':'.self::FIELD_WARRANTY              =>    $printer3dDTO->getWarranty(),
                ':'.self::FIELD_IS_SOLD               =>    $printer3dDTO->isSold(),

                ':'.self::FIELD_FEAT_INK_ID           =>    $printer3dDTO->getFeatInkId(),
                ':'.self::FIELD_FEAT_NUM_HEADERS      =>    $printer3dDTO->getFeatNumHeaders(),
                ':'.self::FIELD_FEAT_MEDIA_TYPE_ID    =>    $printer3dDTO->getFeatMediaTypeId(),
                ':'.self::FIELD_FEAT_MAX_PRINT_VOL    =>    $printer3dDTO->getFeatMaxPrintVol(),
                ':'.self::FIELD_FEAT_QUALITY          =>    $printer3dDTO->getFeatQuality(),
                ':'.self::FIELD_FEAT_SOFTWARE         =>    $printer3dDTO->getFeatSoftware(),
                ':'.self::FIELD_FEAT_PRINT_SPEED      =>    $printer3dDTO->getFeatPrintSpeed(),



                ':'.self::FIELD_TITLE                 =>    $printer3dDTO->getTitle(),
                ':'.self::FIELD_HEADLINE              =>    $printer3dDTO->getHeadline(),
                ':'.self::FIELD_DESCRIPTION           =>    $printer3dDTO->getDescription(),
                ':'.self::FIELD_HTML_OVERVIEW         =>    $printer3dDTO->getHtmlOverview(),
                ':'.self::FIELD_HTML_FEATURES         =>    $printer3dDTO->getHtmlFeatures(),
                ':'.self::FIELD_HTML_SPECS            =>    $printer3dDTO->getHtmlSpecs(),

                ':'.self::FIELD_ID                    =>    $printer3dDTO->getId()
            );


            // IF IT IS AN UPDATE: Execute the query. Third param to false indicates not to fetch results, as
            // an UPDATE or INSERT query wont return any rows.
            $result = $this->execute($sql,$query_params, false);

            // If there was 1 role affected, that's there were no errors:
            if ($result == 1)   {
                // It will return the id of the updated row:
                return $printer3dDTO->getId();
                // If there was an error we return -1.
            } else {
                return -1;
            }


        } else {


            $sql = "INSERT INTO ".self::TABLE_PRINTERS3D." ("

                .self::FIELD_BRAND_ID.", "
                .self::FIELD_MODEL.", "
                .self::FIELD_IS_NEW.", "
                .self::FIELD_IS_PUBLISHED.", "
                .self::FIELD_IS_RELEASE.", "

                .self::FIELD_REF.", "
                .self::FIELD_PRICE_PVP.", "
                .self::FIELD_PRICE_COST.", "
                .self::FIELD_COMMENTS.", "

                .self::FIELD_PRODUCTION_DATE.", "
                .self::FIELD_WARRANTY.", "
                .self::FIELD_IS_SOLD.", "

                .self::FIELD_FEAT_INK_ID.", "
                .self::FIELD_FEAT_NUM_HEADERS.", "
                .self::FIELD_FEAT_MEDIA_TYPE_ID.", "
                .self::FIELD_FEAT_MAX_PRINT_VOL.", "
                .self::FIELD_FEAT_QUALITY.", "
                .self::FIELD_FEAT_SOFTWARE.", "
                .self::FIELD_FEAT_PRINT_SPEED.", "

                .self::FIELD_TITLE.", "
                .self::FIELD_HEADLINE.", "
                .self::FIELD_DESCRIPTION.", "
                .self::FIELD_HTML_OVERVIEW.", "
                .self::FIELD_HTML_FEATURES.", "
                .self::FIELD_HTML_SPECS.", "

                .self::FIELD_CREATED_AT

                .") VALUES (
   
                        :".self::FIELD_BRAND_ID.",              
                        :".self::FIELD_MODEL.",                 
                        :".self::FIELD_IS_NEW.",  
                        :".self::FIELD_IS_PUBLISHED.",  
                        :".self::FIELD_IS_RELEASE.",  
                                      
                        :".self::FIELD_REF.",
                        :".self::FIELD_PRICE_PVP.",        
                        :".self::FIELD_PRICE_COST.",
                        :".self::FIELD_COMMENTS.",        
                        
                        :".self::FIELD_PRODUCTION_DATE.",                 
                        :".self::FIELD_WARRANTY.",              
                        :".self::FIELD_IS_SOLD.",           
                        
                        :".self::FIELD_FEAT_INK_ID.",              
                        :".self::FIELD_FEAT_NUM_HEADERS.",        
                        :".self::FIELD_FEAT_MEDIA_TYPE_ID.",       
                        :".self::FIELD_FEAT_MAX_PRINT_VOL.",  
                        :".self::FIELD_FEAT_QUALITY.",  
                        :".self::FIELD_FEAT_SOFTWARE.",   
                        :".self::FIELD_FEAT_PRINT_SPEED.",             
                        
                        :".self::FIELD_TITLE.",                 
                        :".self::FIELD_HEADLINE.",              
                        :".self::FIELD_DESCRIPTION.", 
                        :".self::FIELD_HTML_OVERVIEW.",         
                        :".self::FIELD_HTML_FEATURES.",         
                        :".self::FIELD_HTML_SPECS.",            
                        
                        null);";    // By setting the column FIELD_CREATED_AT to null
            // we make sure that both columns FIELD_CREATED_AT and FIELD_UPDATED_AT
            // will take the value CURRENT_TIMESTAMP.


            $query_params = array(

                ':'.self::FIELD_BRAND_ID              =>    $printer3dDTO->getBrandId(),
                ':'.self::FIELD_MODEL                 =>    $printer3dDTO->getModel(),
                ':'.self::FIELD_IS_NEW                =>    $printer3dDTO->isNew(),
                ':'.self::FIELD_IS_PUBLISHED          =>    $printer3dDTO->isPublished(),
                ':'.self::FIELD_IS_RELEASE            =>    $printer3dDTO->isRelease(),

                ':'.self::FIELD_REF                   =>    $printer3dDTO->getRef(),
                ':'.self::FIELD_PRICE_PVP             =>    $printer3dDTO->getPricePvp(),
                ':'.self::FIELD_PRICE_COST            =>    $printer3dDTO->getPriceCost(),
                ':'.self::FIELD_COMMENTS              =>    $printer3dDTO->getComments(),

                ':'.self::FIELD_PRODUCTION_DATE       =>    $printer3dDTO->getProductionDate(),
                ':'.self::FIELD_WARRANTY              =>    $printer3dDTO->getWarranty(),
                ':'.self::FIELD_IS_SOLD               =>    $printer3dDTO->isSold(),

                ':'.self::FIELD_FEAT_INK_ID           =>    $printer3dDTO->getFeatInkId(),
                ':'.self::FIELD_FEAT_NUM_HEADERS      =>    $printer3dDTO->getFeatNumHeaders(),
                ':'.self::FIELD_FEAT_MEDIA_TYPE_ID    =>    $printer3dDTO->getFeatMediaTypeId(),
                ':'.self::FIELD_FEAT_MAX_PRINT_VOL    =>    $printer3dDTO->getFeatMaxPrintVol(),
                ':'.self::FIELD_FEAT_QUALITY          =>    $printer3dDTO->getFeatQuality(),
                ':'.self::FIELD_FEAT_SOFTWARE         =>    $printer3dDTO->getFeatSoftware(),
                ':'.self::FIELD_FEAT_PRINT_SPEED      =>    $printer3dDTO->getFeatPrintSpeed(),

                ':'.self::FIELD_TITLE                 =>    $printer3dDTO->getTitle(),
                ':'.self::FIELD_HEADLINE              =>    $printer3dDTO->getHeadline(),
                ':'.self::FIELD_DESCRIPTION           =>    $printer3dDTO->getDescription(),
                ':'.self::FIELD_HTML_OVERVIEW         =>    $printer3dDTO->getHtmlOverview(),
                ':'.self::FIELD_HTML_FEATURES         =>    $printer3dDTO->getHtmlFeatures(),
                ':'.self::FIELD_HTML_SPECS            =>    $printer3dDTO->getHtmlSpecs()
            );

            // Execute the query. Third param to false indicates not to fetch results, as
            // an UPDATE or INSERT query wont return any rows.
            $result = $this->execute($sql, $query_params, false);

            // If there was 1 role affected, that's there were no errors:
            if ($result == 1)   {
                // It will return the id of the new inserted file:
                $insertedId = $this->_db->lastInsertId();
                return $insertedId;
                // If there was an error we return -1.
            } else {
                return -1;
            }
        }
    }


    /**
     * Retrieves all rows from table printers fetched as PrinterDTO instances
     * ordered by column "display_order".
     *
     * @return bool
     */
    public function getPrinters()   {

        $query = "SELECT * FROM ".self::TABLE_PRINTERS3D
            ." WHERE 1 ORDER BY ".self::FIELD_DISPLAY_ORDER;
        $arrayPrinter3dDTO = $this->execute($query, null, true);

        return $arrayPrinter3dDTO;
    }

    /**
     * This method returns all new releases from printers3d table with is_published set to true.
     *
     * @return bool|array
     */
    public function getReleasedPrinters3d()   {

        $query = "SELECT * FROM ".self::TABLE_PRINTERS3D
            ." WHERE " . self::FIELD_IS_RELEASE . " = '1' AND ". self::FIELD_IS_PUBLISHED." = '1' ORDER BY ".self::FIELD_DISPLAY_ORDER;
        $arrayPrinter3dDTO = $this->execute($query, null, true);

        return $arrayPrinter3dDTO;
    }

    public function getPrinter3dById($printer3dId)   {

        $query = "SELECT * FROM ".self::TABLE_PRINTERS3D
            ." WHERE ".self::TABLE_PRINTERS3D.".id = '$printer3dId'";
        $arrayPrinter3dDTO = $this->execute($query, null, true);

        return $arrayPrinter3dDTO[0];
    }

    /**
     * This method sets the field is_publish according to the parameter.
     *
     * @param $is_published
     * @param $printer3d_id
     * @return bool
     */
    public function setIsPublish($is_published, $printer3d_id)    {

        $query =    "UPDATE ".self::TABLE_PRINTERS3D
            ." SET `is_published` = $is_published"
            ." WHERE `id` = $printer3d_id";

        return $this->execute($query, null, false);
    }

    /**
     * This method associates an application with the corresponding printer3d on database.
     *
     * @param $app_id
     * @param $printer3d_id
     * @return bool
     */
    public function addApplicationToPrinter3d($app_id, $printer3d_id) {

        $query = "INSERT INTO " .TABLE_PRINTERS3D_PRINTER3D_APPLICATIONS
            ." ( `printer3d_id`, `printer3d_application_id`) VALUES ($printer3d_id, $app_id)";


        return $this->execute($query, null, false);
    }

    /**
     * This method deletes all rows from printers_applications table related
     * to the corresponding printer id.
     *
     * @param $printer3d_id
     * @return bool
     */
    public function resetApplicationsForPrinter3d($printer3d_id)    {

        $query = "DELETE FROM " .TABLE_PRINTERS3D_PRINTER3D_APPLICATIONS
            ." WHERE `printer3d_id` = $printer3d_id";

        return $this->execute($query, null, false);

    }

    /**
     * This method deletes a printer from database.
     *
     * @param $printer3d_id
     * @return bool
     */
    public function deletePrinter3dById($printer3d_id)  {

        // Delete the corresponding row on printers table.
        $query = "DELETE FROM " .self::TABLE_PRINTERS3D."  WHERE `id` = $printer3d_id";

        return $this->execute($query, null, false);
    }


    /**
     * This method receives a pair printer_id, new_display_order and updates de new display_order
     * value on the corresponding printer.
     *
     * @param $printer_id
     * @param $new_display__order
     * @return null
     */
    public function updateDisplayOrder($printer_id, $new_display__order)    {

        $query = "UPDATE ".self::TABLE_PRINTERS3D
            ." SET ".self::FIELD_DISPLAY_ORDER." = ". $new_display__order
            ." WHERE ".self::FIELD_ID." = ".$printer_id;

        return $this->execute($query, null, false);

    }


    /**
     * This method check whether a printer has a translation record on printers_translation table
     * and returns true or false.
     *
     * @param $printer3d_id
     * @return bool
     */
    public function hasTranslation($printer3d_id) {

        $printer3dTransDAO = new Printer3dTransDAO();
        return (!empty($printer3dTransDAO->getPrinters3dTransByPrinter3dId($printer3d_id)));

    }


    // STATIC TABLES //

    /**
     * This method access to printer_brand table returning every stored brand.
     */
    public function getPrinter3dBrands() {

        $brands = Array();

        $query = "SELECT label FROM printer3d_brand WHERE 1 ORDER BY id ASC";

        try {
            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute(null);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0) {
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {
                $brands[$key] = (array_key_exists("label", $row) ? $row["label"] : null);
            }
            return $brands;
        } else {
            return $result;
        }
    }


    /**
     * This method access to ink table returning every stored ink.
     */
    public function getPrinter3dInks() {

        $inks = Array();

        $query = "SELECT label FROM printer3d_ink WHERE 1 ORDER BY id ASC";

        try {
            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute(null);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0) {
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {
                $inks[$key] = (array_key_exists("label", $row) ? $row["label"] : null);
            }
            return $inks;
        } else {
            return $result;
        }
    }




    /**
     * This method access to media_type table returning every stored media types.
     */
    public function getPrinter3dMediaTypes() {

        $media_types = Array();

        $query = "SELECT label FROM printer3d_media_type WHERE 1 ORDER BY id ASC";

        try {
            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute(null);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0) {
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {
                $media_types[$key] = (array_key_exists("label", $row) ? $row["label"] : null);
            }
            return $media_types;
        } else {
            return $result;
        }
    }




}
?>