<?php

class ApplicationDAO {

    // DB Tables constants:
    const TABLE_APPLICATIONS             = "applications";

    // Constants for the form variables from app:
    const FIELD_ID                      = 'id';
    const FIELD_LABEL_ES                = 'label_es';
    const FIELD_LABEL_EN                = 'label_en';
    const FIELD_FILE_NAME_PREFIX        = "file_name_prefix";


    // Variables to store the connexion from config.inc.php
    var $_db;

    public function __construct() {

        global $db;
        $this->_db = $db;
     }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert was made correctly or not.
     * @param $query
     * @param null $query_params
     * @param bool $fetchResults
     * @return array|bool
     */
      protected function execute($query, $query_params = null, $fetchResults = true) {

        try {

                $stmt   = $this->_db->prepare($query);
                $result = $stmt->execute($query_params);

          } catch (PDOException $ex) {
                // Connexion failed:
                $response["success"] = 0;
                $response["message"] = "PDOException: ".$ex->getMessage();
                die(json_encode($response));
          }

          $numRows = $stmt->rowCount();

            if($numRows > 0 && $fetchResults) {

                $applicationDTO = [];

                foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                    $applicationDTO[$key] = new ApplicationDTO();

                    $applicationDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);
                    $applicationDTO[$key]->setLabelEs(array_key_exists(self::FIELD_LABEL_ES, $row) ? $row[self::FIELD_LABEL_ES] : null);
                    $applicationDTO[$key]->setLabelEn(array_key_exists(self::FIELD_LABEL_EN, $row) ? $row[self::FIELD_LABEL_EN] : null);
                    $applicationDTO[$key]->setFileNamePrefix(array_key_exists(self::FIELD_FILE_NAME_PREFIX, $row) ? $row[self::FIELD_FILE_NAME_PREFIX] : null);
                }

                return $applicationDTO;

            } else {

                return $result;
            }
        }


    /**
     * This method saves an Application to the database.
     *
     * Returns the id of the inserted row.
     * @param ApplicationDTO $applicationDTO
     * @return int|string
     */
    public function saveApplication(ApplicationDTO $applicationDTO) {

        $query = "INSERT INTO ".self::TABLE_APPLICATIONS." ("
                    .self::FIELD_LABEL_ES.", "
                    .self::FIELD_LABEL_EN.", "
                    .self::FIELD_FILE_NAME_PREFIX
                    .") VALUES (
                    :".self::FIELD_LABEL_ES.",
                    :".self::FIELD_LABEL_EN.",
                    :".self::FIELD_FILE_NAME_PREFIX
                    .") ";

        $query_params = array(':'.self::FIELD_LABEL_ES                 => $applicationDTO->getLabelEs(),
                              ':'.self::FIELD_LABEL_EN                 => $applicationDTO->getLabelEn(),
                              ':'.self::FIELD_FILE_NAME_PREFIX         => $applicationDTO->getFileNamePrefix());

        // Execute the query. Third param to false indicates not to fetch results, as
        // an UPDATE or INSERT query wont return any rows.
        $result = $this->execute($query, $query_params, false);

        // If there was 1 role affected, that's there were no errors:
        if ($result = 1)   {
            // It will return the id of the new inserted file:
            $insertedFileId = $this->_db->lastInsertId();
            return $insertedFileId;
        // If there was an error we return -1.
        } else {
            return -1;
        }
    }



    /**
     * This method returns all the applications stored on database.
     *
     * @return array
     */
    public function getApplications()   {

        $query = "SELECT * FROM ".self::TABLE_APPLICATIONS." WHERE 1 ORDER BY ".self::FIELD_ID." ASC";
        $arrayApplicationDTO = $this->execute($query, null, true);
        return $arrayApplicationDTO;
    }


    public function getApplicationById($app_id)   {

        $query = "SELECT * FROM applications WHERE applications.id = '$app_id'  ORDER BY id ASC";
        $arrayApplicationDTO = $this->execute($query, null, true);
        return $arrayApplicationDTO[0];
    }


    public function getAppsArrayByPrinterId( $printer_id)   {

        // Both querys do the same: Multiple inner joins or nested inner joins.
        // SELECT a.* FROM applications AS a INNER JOIN
        //  ( SELECT pa.application_id FROM printers AS p INNER JOIN printers_applications AS pa ON p.id = pa.printer_id WHERE pa.printer_id = 12 )
        //  AS ids ON a.id = ids.application_id WHERE 1

        $query = "  SELECT a.*
                    FROM applications AS a
                    INNER JOIN printers_applications AS pa
                    ON a.id = pa.application_id
                    INNER JOIN printers AS p
                    ON p.id =pa.printer_id
                    WHERE pa.printer_id = '$printer_id'";

        return $this->execute($query, null, true);

    }

} ?>