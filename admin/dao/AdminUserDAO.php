<?php

class AdminUserDAO {

    // DB Tables constants:
    const TABLE_ADMIN_USERS = "admin_users";

    // Constants for the form variables from app:
    const FIELD_ID          = 'id';
    const FIELD_EMAIL       = 'email';
    const FIELD_NAME        = "name";
    const FIELD_PASSWORD    = "password";
    const FIELD_CREATED_AT  = "created_at";


  // Variables to store the connexion from config.inc.php
    var $_db;

    public function __construct() {

        global $db;
        $this->_db = $db;
     }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert was made correctly or not.
     */
  protected function execute($query, $query_params = null, $fetchResults = true) {

    try {

            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute($query_params);

      } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
      }

      $numRows = $stmt->rowCount();

        if($numRows > 0 && $fetchResults) {

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                $adminUserDTO[$key] = new AdminUserDTO();

                $adminUserDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);
                $adminUserDTO[$key]->setEmail(array_key_exists(self::FIELD_EMAIL, $row) ? $row[self::FIELD_EMAIL] : null);
                $adminUserDTO[$key]->setName(array_key_exists(self::FIELD_NAME, $row) ? $row[self::FIELD_NAME] : null);
                $adminUserDTO[$key]->setPassword(array_key_exists(self::FIELD_PASSWORD, $row) ? $row[self::FIELD_PASSWORD] : null);
                $adminUserDTO[$key]->setCreatedAt(array_key_exists(self::FIELD_CREATED_AT, $row) ? $row[self::FIELD_CREATED_AT] : null);
            }

            return $adminUserDTO;

        } else {

            return $result;
        }
    }


    /**
     * This method saves an Admin User to the database.
     *
     * Returns the id of the inserted row.
     * @param AdminUserDTO $adminUserDTO
     * @return int|string
     */
    public function saveAdminUser(AdminUserDTO $adminUserDTO) {

        $query = "INSERT INTO ".self::TABLE_ADMIN_USERS." ("
                    .self::FIELD_EMAIL.", "
                    .self::FIELD_NAME.", "
                    .self::FIELD_PASSWORD.", "
                    .self::FIELD_CREATED_AT
                    .") VALUES (
                    :".self::FIELD_EMAIL.",
                    :".self::FIELD_NAME.",
                    :".self::FIELD_PASSWORD.",
                    null);";    // By setting the column FIELD_CREATED_AT to null
                                    // we make sure that both columns FIELD_CREATED_AT and FIELD_UPDATED_AT
                                    // will take the value CURRENT_TIMESTAMP.

        $query_params = array(':'.self::FIELD_EMAIL     => $adminUserDTO->getEmail(),
                              ':'.self::FIELD_NAME      => $adminUserDTO->getName(),
                              ':'.self::FIELD_PASSWORD  => $adminUserDTO->getPassword());

        // Execute the query. Third param to false indicates not to fetch results, as
        // an UPDATE or INSERT query wont return any rows.
        $result = $this->execute($query, $query_params, false);

        // If there was 1 role affected, that's there were no errors:
        if ($result == 1)   {
            // It will return the id of the new inserted file:
            $insertedFileId = $this->_db->lastInsertId();
            return $insertedFileId;
        // If there was an error we return -1.
        } else {
            return -1;
        }
    }

    public function updateAdminUser(AdminUserDTO $adminUserDTO) {

         $query = "UPDATE ".self::TABLE_ADMIN_USERS
                   ." SET "
                   .self::FIELD_EMAIL." = :".self::FIELD_EMAIL.", "
                   .self::FIELD_NAME." = :".self::FIELD_NAME
                   ." WHERE "
                   .self::FIELD_ID." = :".self::FIELD_ID;

         $query_params = array(':'.self::FIELD_EMAIL     => $adminUserDTO->getEmail(),
                               ':'.self::FIELD_NAME      => $adminUserDTO->getName(),
                               ':'.self::FIELD_ID        => $adminUserDTO->getId());

         return $this->execute($query, $query_params, false);

    }


    public function updateAdminUserPassword($admin_user_id, $new_password) {

        $query = "UPDATE ".self::TABLE_ADMIN_USERS
            ." SET "
            .self::FIELD_PASSWORD." = :".self::FIELD_PASSWORD
            ." WHERE "
            .self::FIELD_ID." = :".self::FIELD_ID;

        $query_params = array(
            ':'.self::FIELD_ID          => $admin_user_id,
            ':'.self::FIELD_PASSWORD    => $new_password);

        return $this->execute($query, $query_params, false);

    }


    public function deleteAdminUserById($admin_user_id) {

        // Delete the corresponding row on admin_user table.
        $query = "DELETE FROM " .TABLE_ADMIN_USERS."  WHERE `id` = ".$admin_user_id;

        return $this->execute($query, null, false);
    }



    public function getAdminUserByEmail($adminUserEmail)   {

        $query = "SELECT id, email, name, password, created_at " .
                 "FROM admin_users WHERE admin_users.email = '$adminUserEmail'";

        $arrayFileDTO = $this->execute($query, null, true);

        return $arrayFileDTO[0];
    }

    public function getAdminUserById($adminUserId)   {

        $query = "SELECT id, email, name, password, created_at " .
            "FROM admin_users WHERE admin_users.id = '$adminUserId'";

        $arrayFileDTO = $this->execute($query, null, true);

        return $arrayFileDTO[0];
    }


    public function getAdminUsers() {

        $query = "SELECT id, email, name, password, created_at " .
            "FROM admin_users WHERE 1";

        $arrayFileDTO = $this->execute($query, null, true);

        return $arrayFileDTO;
    }

}?>