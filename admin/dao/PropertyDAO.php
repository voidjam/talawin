<?php

class PropertyDAO {

    // DB Tables constants:
    const TABLE_PROPERTIES = "properties";

    // Constants for the form variables from app:
    const FIELD_ID                      = 'id';
    const FIELD_NAME                    = 'name';
    const FIELD_VALUE                   = 'value';
    const FIELD_DESCRIPTION             = "description";

    const FIELD_CREATED_AT              = "created_at";
    const FIELD_UPDATED_AT              = "updated_at";

    // List of stores properties;
    const LAPTOP_TEAM_MEMBERS_ITEM_PER_ROW  = "laptop_team_members_item_per_row";
    const TAB_TEAM_MEMBERS_ITEM_PER_ROW     = "tab_team_members_item_per_row";
    const MOBILE_TEAM_MEMBERS_ITEM_PER_ROW  = "mobile_team_members_item_per_row";

    // Variable to store the connexion from config.inc.php
    var $_db;
    // Variable rootpath taken from object creator.
    var $_rootpath;

    public function __construct() {

        global $db;
        global $rootpath;
        $this->_db = $db;
        $this->_rootpath = $rootpath;
    }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert was made correctly or not.
     *
     * @param $query
     * @param null $query_params
     * @param bool $fetchResults
     * @return bool|array
     */
    protected function execute($query, $query_params = null, $fetchResults = true) {

        try {

            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute($query_params);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0 && $fetchResults) {

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                $arrayPropertyDTO[$key] = new PropertyDTO();

                $arrayPropertyDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);
                $arrayPropertyDTO[$key]->setName(array_key_exists(self::FIELD_NAME, $row) ? $row[self::FIELD_NAME] : null);
                $arrayPropertyDTO[$key]->setValue(array_key_exists(self::FIELD_VALUE, $row) ? $row[self::FIELD_VALUE] : null);
                $arrayPropertyDTO[$key]->setDescription(array_key_exists(self::FIELD_DESCRIPTION, $row) ? $row[self::FIELD_DESCRIPTION] : null);
                $arrayPropertyDTO[$key]->setCreatedAt(array_key_exists(self::FIELD_CREATED_AT, $row) ? $row[self::FIELD_CREATED_AT] : null);
            }

            return $arrayPropertyDTO;

        } else {
            return $result;
        }
    }


    /**
     * This method saves or updates a HomeBannerDTO object to the database.
     *
     * Returns the id of the inserted row.
     * @param PropertyDTO $propertyDTO
     * @return int|string
     */
    public function saveProperty(PropertyDTO $propertyDTO) {


        $currPropertyDTO = null;

        if($propertyDTO->getId() != "") {
            $currPropertyDTO = $this->getPropertyById($propertyDTO->getId());
        }

        // If the query returned a row then update,
        // otherwise insert a new user.
        if(sizeof($currPropertyDTO) > 0) {

            $query = "UPDATE ".self::TABLE_PROPERTIES
                ." SET "

                .self::FIELD_NAME." = :".self::FIELD_NAME.", "
                .self::FIELD_VALUE." = :".self::FIELD_VALUE.", "
                .self::FIELD_DESCRIPTION." = :".self::FIELD_DESCRIPTION

                ." WHERE "
                .self::FIELD_ID." = :".self::FIELD_ID;

            $query_params = array(

                ':'.self::FIELD_NAME                =>    $propertyDTO->getName(),
                ':'.self::FIELD_VALUE               =>    $propertyDTO->getValue(),
                ':'.self::FIELD_DESCRIPTION         =>    $propertyDTO->getDescription(),
                ':'.self::FIELD_ID                  =>    $propertyDTO->getId()
            );


            // IF IT IS AN UPDATE: Execute the query. Third param to false indicates not to fetch results, as
            // an UPDATE or INSERT query wont return any rows.
            $result = $this->execute($query,$query_params, false);

            // If there was 1 role affected, that's there were no errors:
            if ($result == 1)   {
                // It will return the id of the updated row:
                return $propertyDTO->getId();
                // If there was an error we return -1.
            } else {
                return -1;
            }


        } else {

            $query = "INSERT INTO " . self::TABLE_PROPERTIES . " ("
                . self::FIELD_NAME . ", "
                . self::FIELD_VALUE . ", "
                . self::FIELD_DESCRIPTION .", "

                . self::FIELD_CREATED_AT . ") VALUES (
                    :" . self::FIELD_NAME . ",
                    :" . self::FIELD_VALUE . ",
                    :" . self::FIELD_DESCRIPTION . ",

                    NULL);";    // By setting the column FIELD_CREATED_AT to null
            // we make sure that both columns FIELD_CREATED_AT and FIELD_UPDATED_AT
            // will take the value CURRENT_TIMESTAMP.

            $query_params = array(

                ':' . self::FIELD_VALUE => $propertyDTO->getValue(),
                ':' . self::FIELD_NAME => $propertyDTO->getName(),
                ':' . self::FIELD_DESCRIPTION => $propertyDTO->getDescription());

            // Execute the query. Third param to false indicates not to fetch results, as
            // an UPDATE or INSERT query wont return any rows.
            $result = $this->execute($query, $query_params, false);

            // If there was 1 role affected, that's there were no errors:
            if ($result == 1) {
                // It will return the id of the new inserted row:
                $insertedHomeBannerId = $this->_db->lastInsertId();
                return $insertedHomeBannerId;
                // If there was an error we return -1.
            } else {
                return -1;
            }
        }
    }


    /**
     * This method returns an array of HomeBannerDTO containing all rows stored in database
     * on home_banners table and sorted by display_order column value.
     *
     * @return array
     */
    public function getProperties() {

        $query = "SELECT * FROM ".self::TABLE_PROPERTIES
            ." WHERE 1 ORDER BY ".self::FIELD_VALUE." ASC".", ".self::FIELD_ID." DESC";
        $arrayPropertyDTO = $this->execute($query, null, true);
        return $arrayPropertyDTO;
    }



    public function getPropertyById($property_id)   {
        $query = "SELECT * FROM ". self::TABLE_PROPERTIES ." WHERE ". self::TABLE_PROPERTIES.".".self::FIELD_ID ."= '$property_id'";
        $arrayPropertyDTO = $this->execute($query, null, true);
        return $arrayPropertyDTO[0];
    }



    public function getPropertyValueByName($property_name)   {
        $query = "SELECT ".self::FIELD_VALUE ." FROM ". self::TABLE_PROPERTIES ." WHERE ". self::TABLE_PROPERTIES.".".self::FIELD_NAME ." = '$property_name'";
        $arrayPropertyDTO = $this->execute($query, null, true);
        return $arrayPropertyDTO[0]->value;
    }


    public function updatePropertyValueByName($property_name, $new_value)   {
        $query = "UPDATE ".self::TABLE_PROPERTIES." SET ".self::FIELD_VALUE." = ".$new_value." WHERE ".self::FIELD_NAME." = '".$property_name."'";
        return $this->execute($query, null, false);
    }

    /**
     * This method deletes a home banner from database and all associated files.
     *
     * @param $banner_id
     * @return array|bool
     */
    public function deletePropertyId($banner_id)  {
        // Finally delete the corresponding row on home_banners table.
        $query = "DELETE FROM " .self::TABLE_PROPERTIES."  WHERE `id` = $banner_id";
        return $this->execute($query, null, false);
    }


}?>