<?php

class MessageDAO {

    // DB Tables constants:
    const TABLE_MESSAGES    = "messages";

    // Constants for the form variables from app:
    const FIELD_ID          = 'id';
    const FIELD_EMAIL       = 'email';
    const FIELD_NAME        = "client_name";
    const FIELD_PHONE       = "phone";
    const FIELD_COMPANY     = "company";
    const FIELD_CONTENT     = "content";
    const FIELD_SUBJECT     = "subject";

    const FIELD_CREATED_AT  = "created_at";


  // Variables to store the connexion from config.inc.php
    var $_db;

    public function __construct() {

        global $db;
        $this->_db = $db;
     }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert was made correctly or not.
     */
  protected function execute($query, $query_params = null, $fetchResults = true) {

    try {

            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute($query_params);

      } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
      }

      $numRows = $stmt->rowCount();

        if($numRows > 0 && $fetchResults) {

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                $messageDTO[$key] = new MessageDTO();

                $messageDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);
                $messageDTO[$key]->setEmail(array_key_exists(self::FIELD_EMAIL, $row) ? $row[self::FIELD_EMAIL] : null);
                $messageDTO[$key]->setName(array_key_exists(self::FIELD_NAME, $row) ? $row[self::FIELD_NAME] : null);
                $messageDTO[$key]->setPhone(array_key_exists(self::FIELD_PHONE, $row) ? $row[self::FIELD_PHONE] : null);
                $messageDTO[$key]->setCompany(array_key_exists(self::FIELD_COMPANY, $row) ? $row[self::FIELD_COMPANY] : null);
                $messageDTO[$key]->setSubject(array_key_exists(self::FIELD_SUBJECT, $row) ? $row[self::FIELD_SUBJECT] : null);
                $messageDTO[$key]->setContent(array_key_exists(self::FIELD_CONTENT, $row) ? $row[self::FIELD_CONTENT] : null);
                $messageDTO[$key]->setCreatedAt(array_key_exists(self::FIELD_CREATED_AT, $row) ? $row[self::FIELD_CREATED_AT] : null);
            }

            return $messageDTO;

        } else {

            return $result;
        }
    }


    /**
     * This method saves ab Message to the database.
     *
     * Returns the id of the inserted row.
     * @param MesageDTO $messageDTO
     * @return int|string
     */
    public function saveMessage(MessageDTO $messageDTO) {

        $query = "INSERT INTO ".self::TABLE_MESSAGES." ("
                    .self::FIELD_EMAIL.", "
                    .self::FIELD_NAME.", "
                    .self::FIELD_PHONE.", "
                    .self::FIELD_COMPANY.", "
                    .self::FIELD_CONTENT.", "
                    .self::FIELD_SUBJECT.", "
                    .self::FIELD_CREATED_AT
                    .") VALUES (
                    :".self::FIELD_EMAIL.",
                    :".self::FIELD_NAME.",
                    :".self::FIELD_PHONE.",
                    :".self::FIELD_COMPANY.",
                    :".self::FIELD_CONTENT.",
                    :".self::FIELD_SUBJECT.",
                    null);";    // By setting the column FIELD_CREATED_AT to null
                                    // we make sure that both columns FIELD_CREATED_AT and FIELD_UPDATED_AT
                                    // will take the value CURRENT_TIMESTAMP.

        $query_params = array(':'.self::FIELD_EMAIL     => $messageDTO->getEmail(),
                              ':'.self::FIELD_NAME      => $messageDTO->getName(),
                              ':'.self::FIELD_PHONE     => $messageDTO->getPhone(),
                              ':'.self::FIELD_COMPANY   => $messageDTO->getCompany(),
                              ':'.self::FIELD_CONTENT   => $messageDTO->getContent(),
                              ':'.self::FIELD_SUBJECT   => $messageDTO->getSubject());

        // Execute the query. Third param to false indicates not to fetch results, as
        // an UPDATE or INSERT query wont return any rows.
        $result = $this->execute($query, $query_params, false);

        // If there was 1 role affected, that's there were no errors:
        if ($result == 1)   {
            // It will return the id of the new inserted row:
            $insertedMessageId = $this->_db->lastInsertId();
            return $insertedMessageId;
        // If there was an error we return -1.
        } else {
            return -1;
        }
    }



    public function getMessageByEmail($messageEmail)   {

        $query = "SELECT * " .
                 "FROM messages WHERE messages.email = '$messageEmail'";

        $arrayMessagesDTO = $this->execute($query, null, true);

        return $arrayMessagesDTO[0];
    }

    public function getMessageById($messageId)   {

        $query = "SELECT * " .
            "FROM messages WHERE messages.id = '$messageId'";

        $arrayMessagesDTO = $this->execute($query, null, true);

        return $arrayMessagesDTO[0];
    }


    public function getMessages() {

        $query = "SELECT * " .
            "FROM messages WHERE 1";

        $arrayMessagesDTO = $this->execute($query, null, true);

        return $arrayMessagesDTO;
    }

}?>