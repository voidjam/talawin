<?php

class VideoDAO {

    // DB Tables constants:
    const TABLE_VIDEOS      = "videos";

    // Constants for the form variables from app:
    const FIELD_ID          = 'id';
    const FIELD_YOUTUBE_ID  = 'youtube_id';
    const FIELD_PRINTER_ID  = "printer_id";

    const FIELD_CREATED_AT  = "created_at";
    const FIELD_UPDATED_AT  = "updated_at";


    // Variables to store the connexion from config.inc.php
    var $_db;

    public function __construct() {

        global $db;
        $this->_db = $db;
    }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert was made correctly or not.
     */
    protected function execute($query, $query_params = null, $fetchResults = true) {

        try {

            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute($query_params);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0 && $fetchResults) {

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                $videoDTO[$key] = new VideoDTO();

                $videoDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);
                $videoDTO[$key]->setYoutubeId(array_key_exists(self::FIELD_YOUTUBE_ID, $row) ? $row[self::FIELD_YOUTUBE_ID] : null);
                $videoDTO[$key]->setPrinterId(array_key_exists(self::FIELD_PRINTER_ID, $row) ? $row[self::FIELD_PRINTER_ID] : null);

                $videoDTO[$key]->setCreatedAt(array_key_exists(self::FIELD_CREATED_AT, $row) ? $row[self::FIELD_CREATED_AT] : null);
                $videoDTO[$key]->setUpdatedAt(array_key_exists(self::FIELD_UPDATED_AT, $row) ? $row[self::FIELD_UPDATED_AT] : null);

            }

            return $videoDTO;

        } else {

            return $result;
        }
    }


    /**
     * This method saves a video to the database.
     *
     * @param $youtubeId
     * @param $printerId

     * @return int Returns the id of the inserted row.
     */
    public function saveVideo($youtubeId, $printerId) {

        $query = "INSERT INTO ".self::TABLE_VIDEOS." ("
            .self::FIELD_YOUTUBE_ID.", "
            .self::FIELD_PRINTER_ID.", "
            .self::FIELD_CREATED_AT
            .") VALUES (
            :".self::FIELD_YOUTUBE_ID.",
            :".self::FIELD_PRINTER_ID
            .", null) ";

        $query_params = array(
            ':'.self::FIELD_YOUTUBE_ID    => $youtubeId,
            ':'.self::FIELD_PRINTER_ID    => $printerId);

        // Execute the query. Third param to false indicates not to fetch results, as
        // an UPDATE or INSERT query wont return any rows.
        $result = $this->execute($query, $query_params, false);

        // If there was 1 role affected, that's there were no errors:
        if ($result = 1)   {
            // It will return the id of the new inserted video:
            $insertedVideoId = $this->_db->lastInsertId();

            return $insertedVideoId;
            // If there was an error we return -1.
        } else {
            return -1;
        }
    }


    public function getVideosByPrinterId($printer_id)   {

        $query = "SELECT * " .
            "FROM videos WHERE printer_id = '$printer_id'";

        $videosArray = $this->execute($query, null, true);

        return (is_array($videosArray) ? $videosArray: 0);
    }





    public function getVideoById($video_id)   {

        $query = "SELECT * " .
            "FROM videos WHERE videos.id = '$video_id'";

        $arrayVideoDTO = $this->execute($query, null, true);

        return $arrayVideoDTO[0];
    }






    /**
     * This method delete the video row for the corresponding id column.
     *
     * @param $video_id
     * @return bool
     */
    public function deleteVideoById($video_id)   {

        $query = "DELETE " .
            "FROM videos WHERE videos.id = '$video_id'";

        return $this->execute($query, null, false);
    }

    /**
     * This method deletes all the videos table rows related to the corresponding printer id.
     *
     * @param $printer_id
     * @return bool
     */
    public function deleteVideosByPrinterId($printer_id)   {

        $query = "DELETE " .
            "FROM videos WHERE videos.printer_id = '$printer_id'";

        return $this->execute($query, null, false);
    }


    /**
     * This functions check if there are at least one uploaded video for the corresponding printer_id.
     *
     * @param $printer_id
     * @return string
     */
    public function hasVideo($printer_id)  {

        $query = "SELECT count(videos.id) from videos where videos.printer_id = $printer_id";

        $stmt = $this->_db->prepare($query);
        $stmt->execute();
        return (intval($stmt->fetchColumn()) > 0);
    }


}?>