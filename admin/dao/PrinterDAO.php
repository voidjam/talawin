<?php

class PrinterDAO {

    // DB Tables constants:
    const TABLE_PRINTERS                = "printers";

    // Constants for the form variables from app.
    const FIELD_ID                      = 'id';

    // General features.
    const FIELD_BRAND_ID                = "brand_id";
    const FIELD_SUB_BRAND_ID            = "sub_brand_id";
    const FIELD_DISPLAY_ORDER           = "display_order";
    const FIELD_MODEL                   = "model";
    const FIELD_IS_NEW                  = "is_new";
    const FIELD_IS_RELEASE              = "is_release";
    const FIELD_IS_PUBLISHED            = "is_published";

    // Private fields.
    const FIELD_REF                     = 'ref';
    const FIELD_PRICE_PVP               = "price_pvp";
    const FIELD_PRICE_COST              = "price_cost";
    const FIELD_COMMENTS                = 'comments';

    // Specific fields for outlet printers.
    const FIELD_PRODUCTION_DATE         = "production_date";
    const FIELD_WARRANTY                = "warranty";
    const FIELD_IS_SOLD                 = "is_sold";

    // Tech Specifications.
    const FIELD_FEAT_INK_ID             = "feat_ink_id";
    const FIELD_FEAT_WHITE_INK_ID       = "feat_white_ink_id";
    const FIELD_FEAT_MEDIA_TYPE_ID      = "feat_media_type_id";
    const FIELD_FEAT_MAX_PRINT_WIDTH    = "feat_max_print_width";
    const FIELD_FEAT_MAX_MEDIA_THICK    = "feat_max_media_thick";
    const FIELD_FEAT_DROP_SIZE          = "feat_drop_size";
    const FIELD_FEAT_RESOLUTION         = "feat_resolution";
    const FIELD_FEAT_MAX_SPEED          = "feat_max_speed";

    // Fields to build the printers html page.
    const FIELD_TITLE                   = "title";
    const FIELD_HEADLINE                = "headline";
    const FIELD_DESCRIPTION             = "description";
    const FIELD_HTML_OVERVIEW           = "html_overview";
    const FIELD_HTML_FEATURES           = "html_features";
    const FIELD_HTML_SPECS              = "html_specs";

    const FIELD_UPDATED_AT              = "updated_at";
    const FIELD_CREATED_AT              = "created_at";



    // Variables to store the connexion from config.inc.php
    var $_db;

    public function __construct() {

        // We make sure we are using the global $db and not a local one.
        global $db;
        $this->_db = $db;
    }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert/update was made correctly or not.
     */
    protected function execute($query, $query_params = null, $fetchResults = true) {

        try {

            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute($query_params);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0 && $fetchResults) {

            $applicationDAO = new ApplicationDAO();
            $printerTransDAO = new PrinterTransDAO();
            $fileDAO = new FileDAO();

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                $printerDTO[$key] = new PrinterDTO();

                $printerDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);

                $printerDTO[$key]->setBrandId(array_key_exists(self::FIELD_BRAND_ID, $row) ? $row[self::FIELD_BRAND_ID] : null);
                $printerDTO[$key]->setSubBrandId(array_key_exists(self::FIELD_SUB_BRAND_ID, $row) ? $row[self::FIELD_SUB_BRAND_ID] : null);
                $printerDTO[$key]->setDisplayOrder(array_key_exists(self::FIELD_DISPLAY_ORDER, $row) ? $row[self::FIELD_DISPLAY_ORDER] : null);
                $printerDTO[$key]->setModel(array_key_exists(self::FIELD_MODEL, $row) ? $row[self::FIELD_MODEL] : null);
                $printerDTO[$key]->setIsNew(array_key_exists(self::FIELD_IS_NEW, $row) ? $row[self::FIELD_IS_NEW] : null);
                $printerDTO[$key]->setIsPublished(array_key_exists(self::FIELD_IS_PUBLISHED, $row) ? $row[self::FIELD_IS_PUBLISHED] : null);
                $printerDTO[$key]->setIsRelease(array_key_exists(self::FIELD_IS_RELEASE, $row) ? $row[self::FIELD_IS_RELEASE] : null);


                $printerDTO[$key]->setRef(array_key_exists(self::FIELD_REF, $row) ? $row[self::FIELD_REF] : null);
                $printerDTO[$key]->setPricePvp(array_key_exists(self::FIELD_PRICE_PVP, $row) ? $row[self::FIELD_PRICE_PVP] : null);
                $printerDTO[$key]->setPriceCost(array_key_exists(self::FIELD_PRICE_COST, $row) ? $row[self::FIELD_PRICE_COST] : null);
                $printerDTO[$key]->setComments(array_key_exists(self::FIELD_COMMENTS, $row) ? $row[self::FIELD_COMMENTS] : null);

                $printerDTO[$key]->setProductionDate(array_key_exists(self::FIELD_PRODUCTION_DATE, $row) ? $row[self::FIELD_PRODUCTION_DATE] : null);
                $printerDTO[$key]->setWarranty(array_key_exists(self::FIELD_WARRANTY, $row) ? $row[self::FIELD_WARRANTY] : null);
                $printerDTO[$key]->setIsSold(array_key_exists(self::FIELD_IS_SOLD, $row) ? $row[self::FIELD_IS_SOLD] : null);

                $printerDTO[$key]->setFeatInkId(array_key_exists(self::FIELD_FEAT_INK_ID, $row) ? $row[self::FIELD_FEAT_INK_ID] : null);
                $printerDTO[$key]->setFeatWhiteInkId(array_key_exists(self::FIELD_FEAT_WHITE_INK_ID, $row) ? $row[self::FIELD_FEAT_WHITE_INK_ID] : null);
                $printerDTO[$key]->setFeatMediaTypeId(array_key_exists(self::FIELD_FEAT_MEDIA_TYPE_ID, $row) ? $row[self::FIELD_FEAT_MEDIA_TYPE_ID] : null);
                $printerDTO[$key]->setFeatMaxPrintWidth(array_key_exists(self::FIELD_FEAT_MAX_PRINT_WIDTH, $row) ? $row[self::FIELD_FEAT_MAX_PRINT_WIDTH] : null);
                $printerDTO[$key]->setFeatMaxMediaThick(array_key_exists(self::FIELD_FEAT_MAX_MEDIA_THICK, $row) ? $row[self::FIELD_FEAT_MAX_MEDIA_THICK] : null);
                $printerDTO[$key]->setFeatDropSize(array_key_exists(self::FIELD_FEAT_DROP_SIZE, $row) ? $row[self::FIELD_FEAT_DROP_SIZE] : null);
                $printerDTO[$key]->setFeatResolution(array_key_exists(self::FIELD_FEAT_RESOLUTION, $row) ? $row[self::FIELD_FEAT_RESOLUTION] : null);
                $printerDTO[$key]->setFeatMaxSpeed(array_key_exists(self::FIELD_FEAT_MAX_SPEED, $row) ? $row[self::FIELD_FEAT_MAX_SPEED] : null);

                $printerDTO[$key]->setTitle(array_key_exists(self::FIELD_TITLE, $row) ? $row[self::FIELD_TITLE] : null);
                $printerDTO[$key]->setHeadline(array_key_exists(self::FIELD_HEADLINE, $row) ? $row[self::FIELD_HEADLINE] : null);
                $printerDTO[$key]->setDescription(array_key_exists(self::FIELD_DESCRIPTION, $row) ? $row[self::FIELD_DESCRIPTION] : null);
                $printerDTO[$key]->setHtmlOverview(array_key_exists(self::FIELD_HTML_OVERVIEW, $row) ? $row[self::FIELD_HTML_OVERVIEW] : null);
                $printerDTO[$key]->setHtmlFeatures(array_key_exists(self::FIELD_HTML_FEATURES, $row) ? $row[self::FIELD_HTML_FEATURES] : null);
                $printerDTO[$key]->setHtmlSpecs(array_key_exists(self::FIELD_HTML_SPECS, $row) ? $row[self::FIELD_HTML_SPECS] : null);

                // Get applications array for each PrinterDTO object.
                $printerDTO[$key]->setAppsArray($applicationDAO->getAppsArrayByPrinterId($printerDTO[$key]->getId()));
                // Get files array for each PrinterDTO object.
                $printerDTO[$key]->setFilesArray($fileDAO->getFilesByPrinterId($printerDTO[$key]->getId()));
                // Get the translation for this printer on a PrinterTransDTO object.
                $printerDTO[$key]->setPrinterTransDTO($printerTransDAO->getPrintersTransByPrinterId($printerDTO[$key]->getId()));
            }

            return $printerDTO;

        } else {

            return $result;
        }
    }

    /**
     * This method saves or updates a printer to the database.
     *
     * Returns the id of the inserted row.
     * @param PrinterDTO $printerDTO
     * @return int|string
     */
    public function savePrinter(PrinterDTO $printerDTO) {


        $currPrinterDTO = null;

        if($printerDTO->getId() != "") {
            $currPrinterDTO = $this->getPrinterById($printerDTO->getId());
        }

        // If the query returned a row then update,
        // otherwise insert a new user.
        if(sizeof($currPrinterDTO) > 0) {

            $sql = "UPDATE ".self::TABLE_PRINTERS
                ." SET "

                .self::FIELD_BRAND_ID." = :".self::FIELD_BRAND_ID.", "
                .self::FIELD_SUB_BRAND_ID." = :".self::FIELD_SUB_BRAND_ID.", "
                .self::FIELD_MODEL." = :".self::FIELD_MODEL.", "
                .self::FIELD_IS_NEW." = :".self::FIELD_IS_NEW.", "
                .self::FIELD_IS_PUBLISHED." = :".self::FIELD_IS_PUBLISHED.", "
                .self::FIELD_IS_RELEASE." = :".self::FIELD_IS_RELEASE.", "

                .self::FIELD_REF." = :".self::FIELD_REF.", "
                .self::FIELD_PRICE_PVP." = :".self::FIELD_PRICE_PVP.", "
                .self::FIELD_PRICE_COST." = :".self::FIELD_PRICE_COST.", "
                .self::FIELD_COMMENTS." = :".self::FIELD_COMMENTS.", "

                .self::FIELD_PRODUCTION_DATE." = :".self::FIELD_PRODUCTION_DATE.", "
                .self::FIELD_WARRANTY." = :".self::FIELD_WARRANTY.", "
                .self::FIELD_IS_SOLD." = :".self::FIELD_IS_SOLD.", "

                .self::FIELD_FEAT_INK_ID." = :".self::FIELD_FEAT_INK_ID.", "
                .self::FIELD_FEAT_WHITE_INK_ID." = :".self::FIELD_FEAT_WHITE_INK_ID.", "
                .self::FIELD_FEAT_MEDIA_TYPE_ID." = :".self::FIELD_FEAT_MEDIA_TYPE_ID.", "
                .self::FIELD_FEAT_MAX_PRINT_WIDTH." = :".self::FIELD_FEAT_MAX_PRINT_WIDTH.", "
                .self::FIELD_FEAT_MAX_MEDIA_THICK." = :".self::FIELD_FEAT_MAX_MEDIA_THICK.", "
                .self::FIELD_FEAT_DROP_SIZE." = :".self::FIELD_FEAT_DROP_SIZE.", "
                .self::FIELD_FEAT_RESOLUTION." = :".self::FIELD_FEAT_RESOLUTION.", "
                .self::FIELD_FEAT_MAX_SPEED." = :".self::FIELD_FEAT_MAX_SPEED.", "

                .self::FIELD_TITLE." = :".self::FIELD_TITLE.", "
                .self::FIELD_HEADLINE." = :".self::FIELD_HEADLINE.", "
                .self::FIELD_DESCRIPTION." = :".self::FIELD_DESCRIPTION.", "
                .self::FIELD_HTML_OVERVIEW." = :".self::FIELD_HTML_OVERVIEW.", "
                .self::FIELD_HTML_FEATURES." = :".self::FIELD_HTML_FEATURES.", "
                .self::FIELD_HTML_SPECS." = :".self::FIELD_HTML_SPECS

                ." WHERE "
                .self::FIELD_ID." = :".self::FIELD_ID;


            $query_params = array(

                ':'.self::FIELD_BRAND_ID              =>    $printerDTO->getBrandId(),
                ':'.self::FIELD_SUB_BRAND_ID          =>    $printerDTO->getSubBrandId(),
                ':'.self::FIELD_MODEL                 =>    $printerDTO->getModel(),
                ':'.self::FIELD_IS_NEW                =>    $printerDTO->getIsNew(),
                ':'.self::FIELD_IS_PUBLISHED          =>    $printerDTO->getIsPublished(),
                ':'.self::FIELD_IS_RELEASE            =>    $printerDTO->getIsRelease(),

                ':'.self::FIELD_REF                   =>    $printerDTO->getRef(),
                ':'.self::FIELD_PRICE_PVP             =>    $printerDTO->getPricePvp(),
                ':'.self::FIELD_PRICE_COST            =>    $printerDTO->getPriceCost(),
                ':'.self::FIELD_COMMENTS              =>    $printerDTO->getComments(),

                ':'.self::FIELD_PRODUCTION_DATE       =>    $printerDTO->getProductionDate(),
                ':'.self::FIELD_WARRANTY              =>    $printerDTO->getWarranty(),
                ':'.self::FIELD_IS_SOLD               =>    $printerDTO->isSold(),

                ':'.self::FIELD_FEAT_INK_ID              =>    $printerDTO->getFeatInkId(),
                ':'.self::FIELD_FEAT_WHITE_INK_ID        =>    $printerDTO->getFeatWhiteInkId(),
                ':'.self::FIELD_FEAT_MEDIA_TYPE_ID       =>    $printerDTO->getFeatMediaTypeId(),
                ':'.self::FIELD_FEAT_MAX_PRINT_WIDTH  =>    $printerDTO->getFeatMaxPrintWidth(),
                ':'.self::FIELD_FEAT_MAX_MEDIA_THICK  =>    $printerDTO->getFeatMaxMediaThick(),
                ':'.self::FIELD_FEAT_DROP_SIZE        =>    $printerDTO->getFeatDropSize(),
                ':'.self::FIELD_FEAT_RESOLUTION       =>    $printerDTO->getFeatResolution(),
                ':'.self::FIELD_FEAT_MAX_SPEED        =>    $printerDTO->getFeatMaxSpeed(),

                ':'.self::FIELD_TITLE                 =>    $printerDTO->getTitle(),
                ':'.self::FIELD_HEADLINE              =>    $printerDTO->getHeadline(),
                ':'.self::FIELD_DESCRIPTION           =>    $printerDTO->getDescription(),
                ':'.self::FIELD_HTML_OVERVIEW         =>    $printerDTO->getHtmlOverview(),
                ':'.self::FIELD_HTML_FEATURES         =>    $printerDTO->getHtmlFeatures(),
                ':'.self::FIELD_HTML_SPECS            =>    $printerDTO->getHtmlSpecs(),

                ':'.self::FIELD_ID                    =>    $printerDTO->getId()
            );


            // IF IT IS AN UPDATE: Execute the query. Third param to false indicates not to fetch results, as
            // an UPDATE or INSERT query wont return any rows.
            $result = $this->execute($sql,$query_params, false);

            // If there was 1 role affected, that's there were no errors:
            if ($result == 1)   {
                // It will return the id of the updated row:
                return $printerDTO->getId();
                // If there was an error we return -1.
            } else {
                return -1;
            }


        } else {


            $sql = "INSERT INTO ".self::TABLE_PRINTERS." ("

                .self::FIELD_BRAND_ID.", "
                .self::FIELD_SUB_BRAND_ID.", "
                .self::FIELD_MODEL.", "
                .self::FIELD_IS_NEW.", "
                .self::FIELD_IS_PUBLISHED.", "
                .self::FIELD_IS_RELEASE.", "

                .self::FIELD_REF.", "
                .self::FIELD_PRICE_PVP.", "
                .self::FIELD_PRICE_COST.", "
                .self::FIELD_COMMENTS.", "

                .self::FIELD_PRODUCTION_DATE.", "
                .self::FIELD_WARRANTY.", "
                .self::FIELD_IS_SOLD.", "

                .self::FIELD_FEAT_INK_ID.", "
                .self::FIELD_FEAT_WHITE_INK_ID.", "
                .self::FIELD_FEAT_MEDIA_TYPE_ID.", "
                .self::FIELD_FEAT_MAX_PRINT_WIDTH.", "
                .self::FIELD_FEAT_MAX_MEDIA_THICK.", "
                .self::FIELD_FEAT_DROP_SIZE.", "
                .self::FIELD_FEAT_RESOLUTION.", "
                .self::FIELD_FEAT_MAX_SPEED.", "

                .self::FIELD_TITLE.", "
                .self::FIELD_HEADLINE.", "
                .self::FIELD_DESCRIPTION.", "
                .self::FIELD_HTML_OVERVIEW.", "
                .self::FIELD_HTML_FEATURES.", "
                .self::FIELD_HTML_SPECS.", "

                .self::FIELD_CREATED_AT

                .") VALUES (
   
                        :".self::FIELD_BRAND_ID.",              
                        :".self::FIELD_SUB_BRAND_ID.",              
                        :".self::FIELD_MODEL.",                 
                        :".self::FIELD_IS_NEW.",  
                        :".self::FIELD_IS_PUBLISHED.",  
                        :".self::FIELD_IS_RELEASE.",  
                                      
                        :".self::FIELD_REF.",
                        :".self::FIELD_PRICE_PVP.",        
                        :".self::FIELD_PRICE_COST.",
                        :".self::FIELD_COMMENTS.",        
                        
                        :".self::FIELD_PRODUCTION_DATE.",                 
                        :".self::FIELD_WARRANTY.",              
                        :".self::FIELD_IS_SOLD.",           
                        
                        :".self::FIELD_FEAT_INK_ID.",              
                        :".self::FIELD_FEAT_WHITE_INK_ID.",        
                        :".self::FIELD_FEAT_MEDIA_TYPE_ID.",       
                        :".self::FIELD_FEAT_MAX_PRINT_WIDTH.",  
                        :".self::FIELD_FEAT_MAX_MEDIA_THICK.",  
                        :".self::FIELD_FEAT_DROP_SIZE.",        
                        :".self::FIELD_FEAT_RESOLUTION.",       
                        :".self::FIELD_FEAT_MAX_SPEED.",        
                        
                        :".self::FIELD_TITLE.",                 
                        :".self::FIELD_HEADLINE.",              
                        :".self::FIELD_DESCRIPTION.", 
                        :".self::FIELD_HTML_OVERVIEW.",         
                        :".self::FIELD_HTML_FEATURES.",         
                        :".self::FIELD_HTML_SPECS.",            
                        
                        null);";    // By setting the column FIELD_CREATED_AT to null
                                    // we make sure that both columns FIELD_CREATED_AT and FIELD_UPDATED_AT
                                    // will take the value CURRENT_TIMESTAMP.


            $query_params = array(

                ':'.self::FIELD_BRAND_ID              =>    $printerDTO->getBrandId(),
                ':'.self::FIELD_SUB_BRAND_ID          =>    $printerDTO->getSubBrandId(),
                ':'.self::FIELD_MODEL                 =>    $printerDTO->getModel(),
                ':'.self::FIELD_IS_NEW                =>    $printerDTO->getIsNew(),
                ':'.self::FIELD_IS_PUBLISHED          =>    $printerDTO->getIsPublished(),
                ':'.self::FIELD_IS_RELEASE            =>    $printerDTO->getIsRelease(),

                ':'.self::FIELD_REF                   =>    $printerDTO->getRef(),
                ':'.self::FIELD_PRICE_PVP             =>    $printerDTO->getPricePvp(),
                ':'.self::FIELD_PRICE_COST            =>    $printerDTO->getPriceCost(),
                ':'.self::FIELD_COMMENTS              =>    $printerDTO->getComments(),

                ':'.self::FIELD_PRODUCTION_DATE       =>    $printerDTO->getProductionDate(),
                ':'.self::FIELD_WARRANTY              =>    $printerDTO->getWarranty(),
                ':'.self::FIELD_IS_SOLD               =>    $printerDTO->isSold(),

                ':'.self::FIELD_FEAT_INK_ID              =>    $printerDTO->getFeatInkId(),
                ':'.self::FIELD_FEAT_WHITE_INK_ID        =>    $printerDTO->getFeatWhiteInkId(),
                ':'.self::FIELD_FEAT_MEDIA_TYPE_ID       =>    $printerDTO->getFeatMediaTypeId(),
                ':'.self::FIELD_FEAT_MAX_PRINT_WIDTH  =>    $printerDTO->getFeatMaxPrintWidth(),
                ':'.self::FIELD_FEAT_MAX_MEDIA_THICK  =>    $printerDTO->getFeatMaxMediaThick(),
                ':'.self::FIELD_FEAT_DROP_SIZE        =>    $printerDTO->getFeatDropSize(),
                ':'.self::FIELD_FEAT_RESOLUTION       =>    $printerDTO->getFeatResolution(),
                ':'.self::FIELD_FEAT_MAX_SPEED        =>    $printerDTO->getFeatMaxSpeed(),

                ':'.self::FIELD_TITLE                 =>    $printerDTO->getTitle(),
                ':'.self::FIELD_HEADLINE              =>    $printerDTO->getHeadline(),
                ':'.self::FIELD_DESCRIPTION           =>    $printerDTO->getDescription(),
                ':'.self::FIELD_HTML_OVERVIEW         =>    $printerDTO->getHtmlOverview(),
                ':'.self::FIELD_HTML_FEATURES         =>    $printerDTO->getHtmlFeatures(),
                ':'.self::FIELD_HTML_SPECS            =>    $printerDTO->getHtmlSpecs()
            );

            // Execute the query. Third param to false indicates not to fetch results, as
            // an UPDATE or INSERT query wont return any rows.
            $result = $this->execute($sql, $query_params, false);

            // If there was 1 role affected, that's there were no errors:
            if ($result == 1)   {
                // It will return the id of the new inserted file:
                $insertedWatchId = $this->_db->lastInsertId();
                return $insertedWatchId;
                // If there was an error we return -1.
            } else {
                return -1;
            }
        }
    }


    /**
     * Retrieves all rows from table printers fetched as PrinterDTO instances
     * ordered by column "display_order".
     *
     * @return bool
     */
    public function getPrinters()   {

        $query = "SELECT * FROM ".self::TABLE_PRINTERS
            ." WHERE 1 ORDER BY ".self::FIELD_DISPLAY_ORDER;
        $arrayPrinterDTO = $this->execute($query, null, true);

        return $arrayPrinterDTO;
    }

    /**
     * This method returns all new releases from printers table with is_published set to true.
     *
     * @return bool || array
     */
    public function getReleasedPrinters()   {

        $query = "SELECT * FROM ".self::TABLE_PRINTERS
            ." WHERE " . self::FIELD_IS_RELEASE . " = '1' AND ". self::FIELD_IS_PUBLISHED." = '1' ORDER BY ".self::FIELD_DISPLAY_ORDER;
        $arrayFileDTO = $this->execute($query, null, true);

        return $arrayFileDTO;
    }

    public function getPrinterById($printerId)   {

        $query = "SELECT * " .
            "FROM printers WHERE printers.id = '$printerId'";
        $arrayFileDTO = $this->execute($query, null, true);

        return $arrayFileDTO[0];
    }

    /**
     * This method sets the field is_publish according to the parameter.
     *
     * @param $is_published
     * @param $printer_id
     * @return bool
     */
    public function setIsPublish($is_published, $printer_id)    {

        $query =    "UPDATE ".TABLE_PRINTERS
                    ." SET `is_published` = $is_published"
                    ." WHERE `id` = $printer_id";

        return $this->execute($query, null, false);
    }

    /**
     * This method associates an application with the corresponding printer on database.
     *
     * @param $app_id
     * @param $printer_id
     * @return bool
     */
    public function addApplicationToPrinter($app_id, $printer_id) {

        $query = "INSERT INTO " .TABLE_PRINTERS_APPLICATIONS
            ." ( `printer_id`, `application_id`) VALUES ($printer_id, $app_id)";


        return $this->execute($query, null, false);


    }

    /**
     * This method deletes all rows from printers_applications table related
     * to the corresponding printer id.
     *
     * @param $printer_id
     * @return bool
     */
    public function resetApplicationsForPrinter($printer_id)    {

        $query = "DELETE FROM " .TABLE_PRINTERS_APPLICATIONS
            ." WHERE `printer_id` = $printer_id";

        return $this->execute($query, null, false);

    }

    /**
     * This method deletes a printer from database and its related applications
     * on printers_applications table.
     *
     * @param $printer_id
     * @return bool
     */
    public function deletePrinterById($printer_id)  {

        // Delete the corresponding row on printers table.
        $query = "DELETE FROM " .TABLE_PRINTERS."  WHERE `id` = $printer_id";

        return $this->execute($query, null, false);
    }


    /**
     * This method receives a pair printer_id, new_display_order and updates de new display_order
     * value on the corresponding printer.
     *
     * @param $printer_id
     * @param $new_display__order
     * @return null
     */
    public function updateDisplayOrder($printer_id, $new_display__order)    {

        $query = "UPDATE ".self::TABLE_PRINTERS
                ." SET ".self::FIELD_DISPLAY_ORDER." = ". $new_display__order
                ." WHERE ".self::FIELD_ID." = ".$printer_id;

        return $this->execute($query, null, false);

    }


    /**
     * This method check whether a printer has a translation record on printers_translation table
     * and returns true or false.
     *
     * @param $printer_id
     * @return bool
     */
    public function hasTranslation($printer_id) {

        $printerTransDAO = new PrinterTransDAO();
        return (!empty($printerTransDAO->getPrintersTransByPrinterId($printer_id)));

    }


    // STATIC TABLES //

    /**
     * This method access to printer_brand table returning every stored brand.
     */
    public function getPrinterBrands($onlyLabels = true) {

        $brands = Array();

        $query = $onlyLabels
            ? "SELECT label FROM printer_brand WHERE 1 ORDER BY id ASC"
            : "SELECT id, label FROM printer_brand WHERE 1 ORDER BY id ASC";

        try {
            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute(null);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0) {
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {
                if (!$onlyLabels) {
                    $brands[$key]["id"] = (array_key_exists("id", $row) ? $row["id"] : null);
                    $brands[$key]["label"] = (array_key_exists("label", $row) ? $row["label"] : null);
                } else {
                    $brands[$key] = (array_key_exists("label", $row) ? $row["label"] : null);
                }
            }
            return $brands;
        } else {
            return $result;
        }
    }


    public function getPrintersNumberByBrand($branId) {
        $query = "select COUNT(*) FROM ".self::TABLE_PRINTERS." WHERE brand_id = $branId";
        try {
            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute(null);
        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {
            $count = $row['COUNT(*)'];
        }
        return $count;
    }

    /**
     * This method access to printer_brand table returning every stored brand.
     */
    public function getPrinterSubBrands() {

        $brands = Array();

        $query = "SELECT label FROM printer_sub_brand WHERE 1 ORDER BY id ASC";

        try {
            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute(null);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0) {
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {
                $brands[$key] = (array_key_exists("label", $row) ? $row["label"] : null);
            }
            return $brands;
        } else {
            return $result;
        }
    }


    /**
     * This method access to ink table returning every stored ink.
     */
    public function getPrinterInks() {

        $inks = Array();

        $query = "SELECT label_es, label_en FROM ink WHERE 1 ORDER BY id ASC";

        try {
            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute(null);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0) {
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {
                $inks[$key]["label_es"] = (array_key_exists("label_es", $row) ? $row["label_es"] : null);
                $inks[$key]["label_en"] = (array_key_exists("label_en", $row) ? $row["label_en"] : null);
            }
            return $inks;
        } else {
            return $result;
        }
    }


    /**
     * This method access to white_ink table returning every stored white ink.
     */
    public function getPrinterWhiteInks() {

        $white_inks = Array();

        $query = "SELECT label_es, label_en FROM white_ink WHERE 1 ORDER BY id ASC";

        try {
            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute(null);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0) {
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {
                $white_inks[$key]["label_es"] = (array_key_exists("label_es", $row) ? $row["label_es"] : null);
                $white_inks[$key]["label_en"] = (array_key_exists("label_en", $row) ? $row["label_en"] : null);
            }
            return $white_inks;
        } else {
            return $result;
        }
    }


    /**
     * This method access to media_type table returning every stored media types.
     */
    public function getPrinterMediaTypes() {

        $media_types = Array();

        $query = "SELECT label_es, label_en FROM media_type WHERE 1 ORDER BY id ASC";

        try {
            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute(null);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0) {
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {
                $media_types[$key]["label_es"] = (array_key_exists("label_es", $row) ? $row["label_es"] : null);
                $media_types[$key]["label_en"] = (array_key_exists("label_en", $row) ? $row["label_en"] : null);
            }
            return $media_types;
        } else {
            return $result;
        }
    }


}?>