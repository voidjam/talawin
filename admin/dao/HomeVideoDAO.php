<?php

class HomeVideoDAO {

    // DB Tables constants:
    const TABLE_HOME_VIDEOS = "home_videos";

    // Constants for the form variables from app:
    const FIELD_ID                      = 'id';
    const FIELD_IS_SHOWN                = 'is_shown';
    const FIELD_DISPLAY_ORDER           = 'display_order';
    const FIELD_YOUTUBE_ID              = 'youtube_id';
    const FIELD_YOUTUBE_TITLE           = 'youtube_title';

    const FIELD_CREATED_AT              = "created_at";
    const FIELD_UPDATED_AT              = "updated_at";


    // Variable to store the connexion from config.inc.php
    var $_db;

    public function __construct() {

        global $db;
        $this->_db = $db;
    }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert was made correctly or not.
     *
     * @param $query
     * @param null $query_params
     * @param bool $fetchResults
     * @return bool|array
     */
    protected function execute($query, $query_params = null, $fetchResults = true) {

        try {

            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute($query_params);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0 && $fetchResults) {

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                $arrayHomeVideoDTO[$key] = new HomeVideoDTO();

                $arrayHomeVideoDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);
                $arrayHomeVideoDTO[$key]->setIsShown(array_key_exists(self::FIELD_IS_SHOWN, $row) ? $row[self::FIELD_IS_SHOWN] : null);
                $arrayHomeVideoDTO[$key]->setDisplayOrder(array_key_exists(self::FIELD_DISPLAY_ORDER, $row) ? $row[self::FIELD_DISPLAY_ORDER] : null);
                $arrayHomeVideoDTO[$key]->setYoutubeId(array_key_exists(self::FIELD_YOUTUBE_ID, $row) ? $row[self::FIELD_YOUTUBE_ID] : null);
                $arrayHomeVideoDTO[$key]->setYoutubeTitle(array_key_exists(self::FIELD_YOUTUBE_TITLE, $row) ? $row[self::FIELD_YOUTUBE_TITLE] : null);

                $arrayHomeVideoDTO[$key]->setCreatedAt(array_key_exists(self::FIELD_CREATED_AT, $row) ? $row[self::FIELD_CREATED_AT] : null);

            }

            return $arrayHomeVideoDTO;

        } else {
            return $result;
        }
    }


    /**
     * This method saves or updates a HomeBannerDTO object to the database.
     *
     * Returns the id of the inserted row.
     * @param HomeVideoDTO $homeVideoDTO
     * @return int|string
     */
    public function saveHomeVideo(HomeVideoDTO $homeVideoDTO) {


        $currHomeVideoDTO = null;

        if($homeVideoDTO->getId() != "") {
            $currHomeVideoDTO = $this->getHomeVideoById($homeVideoDTO->getId());
        }

        // If the query returned a row then update,
        // otherwise insert a new user.
        if(sizeof($currHomeVideoDTO) > 0) {

            $query = "UPDATE ".self::TABLE_HOME_VIDEOS
                ." SET "

                .self::FIELD_IS_SHOWN." = :".self::FIELD_IS_SHOWN.", "
                .self::FIELD_DISPLAY_ORDER." = :".self::FIELD_DISPLAY_ORDER.", "
                .self::FIELD_YOUTUBE_ID." = :".self::FIELD_YOUTUBE_ID.", "
                .self::FIELD_YOUTUBE_TITLE." = :".self::FIELD_YOUTUBE_TITLE

                ." WHERE "
                .self::FIELD_ID." = :".self::FIELD_ID;

            $query_params = array(

                ':'.self::FIELD_IS_SHOWN            =>    $homeVideoDTO->isShown(),
                ':'.self::FIELD_DISPLAY_ORDER       =>    $homeVideoDTO->getDisplayOrder(),
                ':'.self::FIELD_YOUTUBE_ID          =>    $homeVideoDTO->getYoutubeId(),
                ':'.self::FIELD_YOUTUBE_TITLE       =>    $homeVideoDTO->getYoutubeTitle(),

                ':'.self::FIELD_ID                  =>    $homeVideoDTO->getId()
            );


            // IF IT IS AN UPDATE: Execute the query. Third param to false indicates not to fetch results, as
            // an UPDATE or INSERT query wont return any rows.
            $result = $this->execute($query,$query_params, false);

            // If there was 1 role affected, that's there were no errors:
            if ($result == 1)   {
                // It will return the id of the updated row:
                return $homeVideoDTO->getId();
                // If there was an error we return -1.
            } else {
                return -1;
            }


        } else {

            $query = "INSERT INTO " . self::TABLE_HOME_VIDEOS . " ("
                . self::FIELD_IS_SHOWN . ", "
                . self::FIELD_DISPLAY_ORDER . ", "
                . self::FIELD_YOUTUBE_ID . ", "
                . self::FIELD_YOUTUBE_TITLE .", "

                . self::FIELD_CREATED_AT . ") VALUES (
                    :" . self::FIELD_IS_SHOWN . ",
                    :" . self::FIELD_DISPLAY_ORDER . ",
                    :" . self::FIELD_YOUTUBE_ID . ",
                    :" . self::FIELD_YOUTUBE_TITLE . ",

                    NULL);";    // By setting the column FIELD_CREATED_AT to null
            // we make sure that both columns FIELD_CREATED_AT and FIELD_UPDATED_AT
            // will take the value CURRENT_TIMESTAMP.

            $query_params = array(

                ':' . self::FIELD_DISPLAY_ORDER => $homeVideoDTO->getDisplayOrder(),
                ':' . self::FIELD_IS_SHOWN => $homeVideoDTO->isShown(),
                ':' . self::FIELD_YOUTUBE_ID => $homeVideoDTO->getYoutubeId(),
                ':' . self::FIELD_YOUTUBE_TITLE => $homeVideoDTO->getYoutubeTitle());

            // Execute the query. Third param to false indicates not to fetch results, as
            // an UPDATE or INSERT query wont return any rows.
            $result = $this->execute($query, $query_params, false);

            // If there was 1 role affected, that's there were no errors:
            if ($result == 1) {
                // It will return the id of the new inserted row:
                $insertedHomeVideoId = $this->_db->lastInsertId();
                return $insertedHomeVideoId;
                // If there was an error we return -1.
            } else {
                return -1;
            }
        }
    }


    /**
     * This method returns an array of HomeVideoDTO containing all rows stored in database
     * on home_videos table and sorted by display_order column value.
     *
     * @return array
     */
    public function getHomeVideos() {

        $query = "SELECT * FROM ".self::TABLE_HOME_VIDEOS
            ." WHERE 1 ORDER BY ".self::FIELD_DISPLAY_ORDER." ASC".", ".self::FIELD_ID." DESC";
        $arrayHomeVideoDTO = $this->execute($query, null, true);
        return $arrayHomeVideoDTO;
    }


    /**
     * @param $home_video_id
     * @return mixed
     */
    public function getHomeVideoById($home_video_id)   {

        $query = "SELECT * FROM ". self::TABLE_HOME_VIDEOS ." WHERE ". self::TABLE_HOME_VIDEOS.".".self::FIELD_ID ."= '$home_video_id'";
        $arrayHomeVideoDTO = $this->execute($query, null, true);
        return $arrayHomeVideoDTO[0];
    }


    /**
     * This method sets the field is_publish according to the parameter.
     *
     * @param $home_video_id
     * @param $is_shown
     * @return bool
     */
    public function setIsShown($home_video_id, $is_shown)    {

        $query =    "UPDATE ".self::TABLE_HOME_VIDEOS
            ." SET `is_shown` = $is_shown"
            ." WHERE `id` = $home_video_id";

        return $this->execute($query, null, false);
    }


    /**
     * This method deletes a home banner from database and all associated files.
     *
     * @param $home_video_id
     * @return array|bool
     */
    public function deleteHomeVideoById($home_video_id)  {

        // Delete the corresponding row on home_banners table.
        $query = "DELETE FROM " .self::TABLE_HOME_VIDEOS."  WHERE `id` = $home_video_id";
        return $this->execute($query, null, false);
    }

    /**
     * This method updates the column display_order for the given client_id.
     *
     * @param $home_video_id
     * @param $new_display_order
     * @return array|bool
     */
    public function updateDisplayOrder($home_video_id, $new_display_order)    {

        $query =     "UPDATE ".self::TABLE_HOME_VIDEOS
            ." SET `".self::FIELD_DISPLAY_ORDER."` = $new_display_order"
            ." WHERE `".self::FIELD_ID."` = $home_video_id";

        return $this->execute($query, null, false);
    }

}?>