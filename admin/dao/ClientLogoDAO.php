<?php

class ClientLogoDAO {

    // DB Tables constants:
    const TABLE_CLIENT_LOGO = "client_logos";

    // Constants for the form variables from app:
    const FIELD_ID              = 'id';
    const FIELD_IS_SHOWN        = 'is_shown';
    const FIELD_DISPLAY_ORDER   = 'display_order';
    const FIELD_CLIENT_NAME     = "client_name";
    const FIELD_FILE_NAME       = "file_name";
    const FIELD_MIME_TYPE       = "mime_type";
    const FIELD_PATH            = "path";
    const FIELD_SIZE            = "size";

    const FIELD_CREATED_AT      = "created_at";
    const FIELD_UPDATED_AT      = "updated_at";


    // Variables to store the connexion from config.inc.php
    var $_db;
    // Variable rootpath taken from object creator.
    var $_rootpath;

    public function __construct() {

        global $db;
        global $rootpath;
        $this->_db = $db;
        $this->_rootpath = $rootpath;
    }

    public function __destruct() { }


    /**
     * Executes the specified query and returns an associative array of results
     * if query was a select, otherwise it will return true or false depending
     * if the insert was made correctly or not.
     *
     * @return bool|array
     */
    protected function execute($query, $query_params = null, $fetchResults = true) {

        try {

            $stmt   = $this->_db->prepare($query);
            $result = $stmt->execute($query_params);

        } catch (PDOException $ex) {
            // Connexion failed:
            $response["success"] = 0;
            $response["message"] = "PDOException: ".$ex->getMessage();
            die(json_encode($response));
        }

        $numRows = $stmt->rowCount();

        if($numRows > 0 && $fetchResults) {

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $key=>$row) {

                $arrayClientLogoDTO[$key] = new ClientLogoDTO();

                $arrayClientLogoDTO[$key]->setId(array_key_exists(self::FIELD_ID, $row) ? $row[self::FIELD_ID] : null);
                $arrayClientLogoDTO[$key]->setIsShown(array_key_exists(self::FIELD_IS_SHOWN, $row) ? $row[self::FIELD_IS_SHOWN] : null);
                $arrayClientLogoDTO[$key]->setDisplayOrder(array_key_exists(self::FIELD_DISPLAY_ORDER, $row) ? $row[self::FIELD_DISPLAY_ORDER] : null);
                $arrayClientLogoDTO[$key]->setClientName(array_key_exists(self::FIELD_CLIENT_NAME, $row) ? $row[self::FIELD_CLIENT_NAME] : null);
                $arrayClientLogoDTO[$key]->setFileName(array_key_exists(self::FIELD_FILE_NAME, $row) ? $row[self::FIELD_FILE_NAME] : null);
                $arrayClientLogoDTO[$key]->setMimeType(array_key_exists(self::FIELD_MIME_TYPE, $row) ? $row[self::FIELD_MIME_TYPE] : null);
                $arrayClientLogoDTO[$key]->setSize(array_key_exists(self::FIELD_SIZE, $row) ? $row[self::FIELD_SIZE] : null);
                $arrayClientLogoDTO[$key]->setPath(array_key_exists(self::FIELD_PATH, $row) ? $row[self::FIELD_PATH] : null);
                $arrayClientLogoDTO[$key]->setCreatedAt(array_key_exists(self::FIELD_CREATED_AT, $row) ? $row[self::FIELD_CREATED_AT] : null);
            }

            return $arrayClientLogoDTO;

        } else {

            return $result;
        }
    }


    /**
     * This method saves a new Client Logo object to the database.
     *
     * Returns the id of the inserted row.
     * @param ClientLogoDTO $clientLogoDTO
     * @return int|string
     */
    public function saveClientLogo(ClientLogoDTO $clientLogoDTO) {

        $query = "INSERT INTO ".self::TABLE_CLIENT_LOGO." ("
            .self::FIELD_IS_SHOWN.", "
            .self::FIELD_DISPLAY_ORDER.", "
            .self::FIELD_CLIENT_NAME.", "
            .self::FIELD_FILE_NAME.", "
            .self::FIELD_MIME_TYPE.", "
            .self::FIELD_PATH.", "
            .self::FIELD_SIZE.", "
            .self::FIELD_CREATED_AT
            .") VALUES (
                    :".self::FIELD_IS_SHOWN.",
                    :".self::FIELD_DISPLAY_ORDER.",
                    :".self::FIELD_CLIENT_NAME.",
                    :".self::FIELD_FILE_NAME.",
                    :".self::FIELD_MIME_TYPE.",
                    :".self::FIELD_PATH.",
                    :".self::FIELD_SIZE.",
                    null);";    // By setting the column FIELD_CREATED_AT to null
        // we make sure that both columns FIELD_CREATED_AT and FIELD_UPDATED_AT
        // will take the value CURRENT_TIMESTAMP.

        $query_params = array(
            ':'.self::FIELD_DISPLAY_ORDER               => $clientLogoDTO->getDisplayOrder(),
            ':'.self::FIELD_IS_SHOWN                    => $clientLogoDTO->isShown(),
            ':'.self::FIELD_CLIENT_NAME                 => $clientLogoDTO->getClientName(),
            ':'.self::FIELD_FILE_NAME                   => $clientLogoDTO->getFileName(),
            ':'.self::FIELD_MIME_TYPE                   => $clientLogoDTO->getMimeType(),
            ':'.self::FIELD_PATH                        => $clientLogoDTO->getPath(),
            ':'.self::FIELD_SIZE                        => $clientLogoDTO->getSize());

        // Execute the query. Third param to false indicates not to fetch results, as
        // an UPDATE or INSERT query wont return any rows.
        $result = $this->execute($query, $query_params, false);

        // If there was 1 role affected, that's there were no errors:
        if ($result == 1)   {
            // It will return the id of the new inserted row:
            $insertedClientLogoId = $this->_db->lastInsertId();
            return $insertedClientLogoId;
            // If there was an error we return -1.
        } else {
            return -1;
        }
    }


    /**
     * This method returns an array of ClientLogoDTO containing all rows stored in database
     * on client_logo table and sorted by display_order column value.
     *
     * @return array
     */
    public function getClientLogos() {

        $query = "SELECT * FROM ".self::TABLE_CLIENT_LOGO
            ." WHERE 1 ORDER BY ".self::FIELD_DISPLAY_ORDER." ASC".", ".self::FIELD_ID." DESC";
        $arrayClientLogoDTO = $this->execute($query, null, true);
        return $arrayClientLogoDTO;
    }


    public function getClientLogoById($client_id)   {

        $query = "SELECT * FROM ". TABLE_CLIENT_LOGO ." WHERE ". TABLE_CLIENT_LOGO.".".self::FIELD_ID ."= '$client_id'";
        $arrayFileDTO = $this->execute($query, null, true);
        return $arrayFileDTO[0];
    }


    /**
     * This method sets the field is_publish according to the parameter.
     *
     * @param $client_id
     * @param $is_shown
     * @return bool
     */
    public function setIsShown($client_id, $is_shown)    {

        $query =    "UPDATE ".TABLE_CLIENT_LOGO
            ." SET `is_shown` = $is_shown"
            ." WHERE `id` = $client_id";

        return $this->execute($query, null, false);
    }


    public function deleteClientById($client_id)  {

        // First we take the whole row to get the information:
        $clientLogoDTO = $this->getClientLogoById($client_id);
        // We have to delete the corresponding image from file system.
        unlink($this->_rootpath.$clientLogoDTO->getPath().$clientLogoDTO->getFileName());

        // Finnally delete the corresponding row on client_logos table.
        $query = "DELETE FROM " .TABLE_CLIENT_LOGO."  WHERE `id` = $client_id";
        return $this->execute($query, null, false);
    }

    /**
     * This method updates the column display_order for the given client_id.
     *
     * @param $client_id
     * @param $new_display_order
     * @return array|bool
     */
    public function updateDisplayOrder($client_id, $new_display_order)    {

        $query =     "UPDATE ".TABLE_CLIENT_LOGO
            ." SET `".self::FIELD_DISPLAY_ORDER."` = $new_display_order"
            ." WHERE `".self::FIELD_ID."` = $client_id";

        return $this->execute($query, null, false);
    }

}?>