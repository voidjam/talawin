<?php

$rootpath = "./../";
session_start();

// Include Header.
include $rootpath.'admin/inc/header.php';
?>



    <!-- ----------------------------------------------------------------------------------------- -->
    <!-- Empty NavBar -->
    <nav class="navbar navbar-custom-style fixed-top navbar-toggleable-md navbar-light bg-faded">
        <a class="navbar-brand" href="#">
            <img src="<?php echo $rootpath?>admin/img/logo-big.png" class="nav-logo" alt="Talawin, the power of quality"></a>
    </nav>
    <!-- ----------------------------------------------------------------------------------------- -->




    <!-- ----------------------------------------------------------------------------------------- -->
    <!-- Authentification Form -->
    <div class="login-div">

        <form class="" action="<?php echo $rootpath?>admin/scripts/authentification.php" method="post">

            <div class="form-group row">
                <label for="inputEmail" class="col-sm-3 col-form-label">Email</label>
                <div class="col-sm-9">
                    <input required name="inputEmail" type="email" class="form-control" id="inputEmail" placeholder="Email">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-3 col-form-label">Password</label>
                <div class="col-sm-9">
                    <input required name="inputPassword" type="password" class="form-control" id="inputPassword" placeholder="Password">
                </div>
            </div>

            <div class="form-group row">
                <div class="offset-sm-3 col-sm-9">
                    <button type="submit" class="btn btn-primary btn-block login-button">Login</button>
                </div>
            </div>
        </form>
    </div>
    <!-- ----------------------------------------------------------------------------------------- -->


<?php

// Check for errors and warnings:
if(isset($_SESSION['error']) && is_array($_SESSION['error']))   include $rootpath.'admin/inc/warning.php';
// Include Footer.
include $rootpath.'admin/inc/footer.php';

?>