<?php

// Retrieve all providers from database.
$providerLogoDAO = new ProviderLogoDAO();
$arrayProviderLogoDTO = $providerLogoDAO->getProvidersLogos();
// Printers info: Get brands and number of printers stored on DB for each brand to show/hide nav-item elements.
$printerDAO = new PrinterDAO();
$arrayBrands = $printerDAO->getPrinterBrands(false);
foreach ($arrayBrands as $key => &$brand) { // For each brand, add the number of printers stored with that brand.
    $brand["count"] = $printerDAO->getPrintersNumberByBrand($brand["id"]);
}

?>

<html lang="<?php echo $language; ?>">

<!-- --------------------------------------------------------------------------------------------------------------- -->
<!-- HEAD ---------------------------------------------------------------------------------------------------------- -->
<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>TALAWIN · <?php echo $page_title; ?></title>
    <meta name="description" content="<?php echo $strings[$language][201]; ?>" />

    <!-- CDN BOOTSTRAP 4: CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <!-- Swiper CSS -->
<!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/css/swiper.min.css">
<!--    <link rel="stylesheet" href="--><?php //echo $rootpath; ?><!--css/swiper.css">-->

    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="<?php echo $rootpath; ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $rootpath; ?>css/owl.theme.default.min.css">

    <!-- Font Awesome -->
    <script src="https://use.fontawesome.com/8184c088bf.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo $rootpath; ?>css/style.css">
    <link rel="stylesheet" href="<?php echo $rootpath; ?>css/style-xl.css">
    <link rel="stylesheet" href="<?php echo $rootpath; ?>css/style-lg.css">
    <link rel="stylesheet" href="<?php echo $rootpath; ?>css/style-md.css">
    <link rel="stylesheet" href="<?php echo $rootpath; ?>css/style-sm.css">
    <link rel="stylesheet" href="<?php echo $rootpath; ?>css/style-xs.css">

    <!-- FAVICONs -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $rootpath; ?>apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $rootpath; ?>favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $rootpath; ?>favicon-16x16.png">
    <link rel="manifest" href="<?php echo $rootpath; ?>manifest.json">
    <link rel="mask-icon" href="<?php echo $rootpath; ?>safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <!-- jQuery 3 -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

</head>
<!-- END OF HEAD --------------------------------------------------------------------------------------------------- -->
<!-- --------------------------------------------------------------------------------------------------------------- -->
<body>
<!-- --------------------------------------------------------------------------------------------------------------- -->
<!-- NAVBAR -------------------------------------------------------------------------------------------------------- -->
<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-faded p-lg-0" id="top">
    <div class="container-fluid container-navbar-lg">


        <div class="container container-navbar mx-0 mx-sm-auto d-flex">
            <!-- TOGGLER BUTTON -->
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="ml-auto ml-lg-0 d-flex align-items-center">
                <!-- LANGUAGE FLAG - Only mobile devices width > 576px -->
                <a class="d-sm-none mr-3"
                   href="<?php echo $language == 'es'? $rootpath.str_replace('/es/', 'en/',$_SERVER['REQUEST_URI']) : $rootpath.str_replace('/en/', 'es/',$_SERVER['REQUEST_URI']); ?>"
                   style="font-weight: bold !important;" title="<?php echo $language == 'es'? 'English' : 'Español'; ?>">
                    <img src="<?php echo $rootpath; ?>img/flag_<?php echo $language == 'es' ? 'en' : 'es'; ?>.png" height="26px" border="0">
                </a>
                <!-- TELEPHONE ICON - Only mobile devices width > 576px -->
                <a href="tel:+34919917143" class="d-sm-none mr-2">
                    <i class="fa fa-phone-square" aria-hidden="true" style="font-size: 30px;"></i></a>
                <!-- TALAWIN LOGO -->
                <a id="navbarLogo" class="navbar-brand pl-2 pl-lg-0" href="<?php echo $rootpath.$language; ?>/index.php">
                    <img src="<?php echo $rootpath; ?>img/logo-big.png" class="nav-logo" alt="Talawin, the power of quality"></a>
                <!-- PROVIDERS LOGO HOLDER -->
                <!-- Hidden for XS, SM screen sizes. Showing after md size (> 768px) -->
                <a id="providersLogo" target="_blank" class="navbar-brand pl-4 pl-lg-0 ml-3 d-none d-sm-none d-md-inline-block" href="" style="display: none">
                    <img class="nav-logo" alt="Talawin, the power of quality"></a>
            </div>

        </div>

        <!-- COLLAPSED ITEMS: Elements visible on width > 992px  -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <!-- HOME -->
                <li class="nav-item" id="menu-home">
                    <div class="container">
                        <a class="nav-link" href="<?php echo $rootpath . $language .'/'; ?>index.php"><span class="d-lg-none"><?php echo $strings[$language][100]; ?></span><i class="fa fa-home d-none d-lg-block" aria-hidden="true"></i></a>
                    </div>
                </li>
                <!-- PRINTERS: EFI -->
                <li class="nav-item efi" id="menu-printers">
                    <div class="container">
                        <a class="nav-link" href="<?php echo $rootpath . $language .'/'; ?>hardware/index.php?brand=efi"><?php echo $strings[$language][10100]; ?></a>
                    </div>
                </li>
                <!-- PRINTERS: LIYU -->
                <li class="nav-item liyu" id="menu-printers">
                    <div class="container">
                        <a class="nav-link" href="<?php echo $rootpath . $language .'/'; ?>hardware/index.php?brand=liyu"><?php echo $strings[$language][10101]; ?></a>
                    </div>
                </li>

                <!-- PRINTERS 3D -->
                <!-- <li class="nav-item" id="menu-printers3d">
                    <div class="container">
                        <a class="nav-link" href="<?php echo $rootpath . $language .'/'; ?>hardware/3d"><?php echo $strings[$language][105]; ?></a>
                    </div>
                </li> -->
                <!-- OUTLET -->
                <li class="nav-item" id="menu-outlet">
                    <div class="container">
                        <a class="nav-link" href="<?php echo $rootpath . $language .'/'; ?>hardware/?section=outlet"><?php echo $strings[$language][102]; ?></a>
                    </div>
                </li>
                <!-- ABOUT -->
                <li class="nav-item" id="menu-about">
                    <div class="container">
                        <a class="nav-link" href="<?php echo $rootpath . $language .'/'; ?>about"><?php echo $strings[$language][103]; ?></a>
                    </div>
                </li>
                <!-- CONTACT -->
                <li class="nav-item" id="menu-contact">
                    <div class="container">
                        <a class="nav-link" href="<?php echo $rootpath . $language .'/'; ?>contact"><?php echo $strings[$language][104]; ?></a>
                    </div>
                </li>
            </ul>
        </div>

        <!-- LANGUAGE FLAG: Large devices -->
        <a id="languageFlag" class="mr-2 ml-5 d-none d-sm-inline"
           href="<?php echo $language == 'es'? $rootpath.str_replace('/es/', 'en/',$_SERVER['REQUEST_URI']) : $rootpath.str_replace('/en/', 'es/',$_SERVER['REQUEST_URI']); ?>"
           style="font-weight: bold !important;"
           title="<?php echo $language == 'es'? 'English' : 'Español'; ?>">
            <img src="<?php echo $rootpath; ?>img/flag_<?php echo $language == 'es' ? 'en' : 'es'; ?>.png" height="24px" border="0">
        </a>

    </div>
</nav>
<!-- END OF NAVBAR ------------------------------------------------------------------------------------------------- -->
<!-- --------------------------------------------------------------------------------------------------------------- -->


<script type="text/javascript">

    // Loop throw all providers logos to show them on nav-bar
    let arrayProviderLogoDTO = JSON.parse(`<?php echo json_encode($arrayProviderLogoDTO)?>`);
    let currentProviderLogo = 0;
    (function switchProviderLogo() {
        $("#providersLogo").fadeOut(200, function() {
            $("#providersLogo > img").attr("src", "<?php echo $rootpath; ?>" + arrayProviderLogoDTO[currentProviderLogo].path + arrayProviderLogoDTO[currentProviderLogo].file_name );
            $("#providersLogo").fadeIn(200);
            // Set link to provider web if saved on database.
            arrayProviderLogoDTO[currentProviderLogo].provider_link != ''
                ? $("#providersLogo").attr("href", arrayProviderLogoDTO[currentProviderLogo].provider_link)
                : $("#providersLogo").removeAttr('href');
        });
        currentProviderLogo++;
        if (currentProviderLogo == arrayProviderLogoDTO.length) currentProviderLogo = 0;
        // Schedule a repeat
        setTimeout(switchProviderLogo, 5000);
    })();

</script>



<script>
    // This will hide nav-item elements for brands with 0 printers on database.
    let brands = <?php echo json_encode($arrayBrands); ?>;
    brands.forEach( function(brand) {
        if (brand.count == 0) $(".nav-item."+brand.label).hide();
    })
</script>


