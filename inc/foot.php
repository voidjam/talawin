<!-- FOOTER -->
    <div class="row bg-dark footer-container mt-5">

        <div class="container">

            <!-- XS -->
            <div class="row footer text-white d-sm-none">
                <div class="col-10 align-self-center text-right">
                    <img src="<?php echo $rootpath; ?>img/logo-allwhite.png" class="foot-logo" alt="Talawin, the power of quality"> &copy; 2017
                </div>
                <div class="col-2 align-self-center text-right">
                    <a class="gototop" href="#" title="Volver arriba"><div class="my-5"><div class='icon-scroll'></div></div><!-- <i class="fa fa-chevron-circle-up" aria-hidden="true" style="font-size: 2rem;"></i> --></a>
                </div>
            </div>

            <!-- SM and more -->
            <div class="row justify-content-between footer text-white d-none d-sm-flex">
                <!-- PRIVACY POLICY LINK -->
                <div class="col-4 align-self-center">
                    <a class="privacy_link" href="<?php echo $rootpath; ?>/docs/Política_de_Privacidad_TALAWIN_es.pdf" target="_blank"><?php echo $strings[$language][1000]?></a><br>
                    <a class="privacy_link" href="<?php echo $rootpath; ?>/docs/Aviso_legal&Cookies_TALAWIN.pdf" target="_blank"><?php echo $strings[$language][1001]?></a>
                </div>
                <!-- SCROLL UP BUTTON -->
                <div class="col-4 align-self-center"><a class="gototop" href="" title="Volver arriba"><div class="my-5"><div class='icon-scroll'></div></div></a></div>
                <!-- TALAWIN LOGO B&W -->
                <div class="col-4 align-self-center text-right"><img src="<?php echo $rootpath; ?>img/logo-allwhite.png" class="foot-logo" alt="Talawin, the power of quality"> &copy; 2017</div>
            </div>

        </div>

    </div>
<!-- FOOTER end -->

</div>
<!-- WRAPPER end -->


<!-- MODAL FOR PRIVACY ACCEPTANCE ERRORS -->
<div class="modal fade" tabindex="-1" role="dialog" id="privacy-error">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="far fa-newspaper"></i><?php echo $strings[$language][413]?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"><p><?php echo $strings[$language][707]?></p></div>
        </div>
    </div>
</div>
<!-- MODAL MAILCHIMP SIGNUP RESPONSE end -->


<!-- MODAL MAILCHIMP SIGNUP RESPONSE -->
<div class="modal fade" tabindex="-1" role="dialog" id="mailchimp-response">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="far fa-newspaper"></i> <?php echo $strings[$language][704]; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"><p><?php echo $strings[$language][705]; ?></p></div>
        </div>
    </div>
</div>
<!-- MODAL MAILCHIMP SIGNUP RESPONSE end -->

<!-- MAILCHIMP SIGNUP POPUP -->
<div class="mailchimp-popup pl-3 pt-md-1">

    <!-- Begin MailChimp Signup Form -->
    <div id="mc_embed_signup">
        <form id="susbsciption-form" action="https://talawin.us18.list-manage.com/subscribe/post?u=ccbb912f0816915f1809ece54&amp;id=d58696c51c" method="post" name="mc-embedded-subscribe-form" class="mc-embedded-subscribe-form validate form-inline mb-3" target="_blank" novalidate>
            <div class="d-inline" id="mc_embed_signup_scroll">

                <span class="mr-4" style="vertical-align: sub;"><i class="far fa-newspaper"></i> <?php echo $strings[$language][700]; ?></span> <a href="" class="close-mailchimppopup"><i class="float-right pt-2 pr-3 fas fa-times"></i></a>

                <br>

                <input type="text" value="" name="FNAME" class="form-control form-control-sm required mr-1 d-inline col-4" placeholder="<?php echo $strings[$language][701]; ?>">

                <input type="email" value="" name="EMAIL" class="form-control form-control-sm required email d-inline col-4" placeholder="<?php echo $strings[$language][702]; ?>">

                <div id="mce-responses" class="clear">
                    <div class="response" style="display:none"></div>
                    <div class="response" style="display:none"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->

                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_ccbb912f0816915f1809ece54_dd3bded374" tabindex="-1" value=""></div>

                <div class="clear mt-2 mt-md-none"><input id="subscribeBtn" type="submit" value="<?php echo $strings[$language][703]; ?>" name="subscribe" class="btn btn-sm btn-success button-mailchimp"></div>

                <!-- PRIVACY POLITIC ACCEPTANCE CHECKBOX -->
                <div id="privacyCheckbox"><input type="checkbox" id="cboxPrivacyFooter" value=""/> <label for="cboxPrivacyFooter"><?php echo $strings[$language][706]; ?></label></div>
            </div>



        </form>
    </div>
    <!--End mc_embed_signup-->

</div>
<!-- MAILCHIMP SIGNUP POPUP end -->


<!-- jQuery UI (CDN) -->
<!-- <script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script> -->

<!-- CDN BOOTSTRAP 4 : JS, Popper, and jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<!-- CDN jQuery + Bootstrap JS -->
<!-- jQuery first, then Tether, then Bootstrap JS. -->
<!--
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
-->

<!-- Owl Carousel JS -->
<script src="<?php echo $rootpath; ?>js/owl.carousel.min.js"></script>

<!-- Smooth Scroll: A script to animate scrolling to anchor links. -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/smoothscroll/1.4.10/SmoothScroll.js"></script>

<!-- jQuery AJAX -->
<script src="<?php echo $rootpath; ?>js/jquery.ajaxchimp.js"></script>


<script type="text/javascript">

    ///////////////////////////////////////////////////////////////
    // MAILCHIMP SIGNUP *//////////////////////////////////////////

    // Mail chimp submission success callback: Show success message.
    function callbackMailchimp (resp) {
        if (resp.result === 'success') {
            $('#mailchimp-response').find(".modal-body > p").html("<?php echo $strings[$language][705]; ?>");
            $('#mailchimp-response').modal('show'); // Modal con mensaje de confirmación
            // Take away mailchimp popup.
            if ($(".mailchimp-popup").is(":visible")) {$( ".mailchimp-popup" ).fadeOut(300)}
        }
    }

    // Avoid submission if privacy policy has not been accepted.
    $("#subscribeBtn").on("click", function(e) {
        if(!$("#privacyCheckbox > input").is(":checked")) {
            e.preventDefault();
            $('#mailchimp-response').find(".modal-body > p").html("<?php echo $strings[$language][707]; ?>");
            $('#mailchimp-response').modal('show');
        }
    });

    // Manage subscription from submission throw ajaxChimp plugin.
    $('#susbsciption-form').ajaxChimp({
        url: 'https://talawin.us18.list-manage.com/subscribe/post?u=ccbb912f0816915f1809ece54&amp;id=d58696c51c',
        callback: callbackMailchimp
    });
    /* MAILCHIMP SIGNUP end */
    ///////////////////////////////////////////////////////////////

    $(function () {

        // Show MAILCHIMP SIGN UP FORM after X seconds
        setTimeout(function() {$( ".mailchimp-popup" ).slideToggle(300)}, 5000);

        // Close MAILCHIMP SIGN UP button
        $('.close-mailchimppopup').on('click', function(e) {
            e.preventDefault();
            $( ".mailchimp-popup" ).fadeOut(300);
        });

        /* Fade Out Effect */
        $('body').removeClass('fade-out');

        /* SCROLL FADE IN EFFECT */

        /* Every time the window is scrolled ... */
        $(window).scroll( function(){

            /* Check the location of each desired element */
            $('.hideme').each( function(i){

                var bottom_of_object = $(this).offset().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                /* If the object is completely visible in the window, fade it it */
                if( bottom_of_window > bottom_of_object ){

                    $(this).animate({'opacity':'1'},750).css({'transform':'translateY(0)'});

                }

            });

        });
        /* SCROLL FADE IN EFFECT end */

        /* SMOOTH SCROLL */
        // handle links with @href started with '#' only
        $(document).on('click', 'a[href^="#"]', function(e) {
            // target element id
            var id = $(this).attr('href');

            // target element
            var $id = $(id);
            if ($id.length === 0 || $(this).hasClass('carousel-control-prev') || $(this).hasClass('carousel-control-next') || $(this).hasClass('myPill')) {
                return;
            }

            // prevent standard hash navigation (avoid blinking in IE)
            e.preventDefault();

            // top position relative to the document
            var pos = $id.offset().top;

            // animated top scrolling
            $('body, html').animate({scrollTop: pos});
        });
        /* SMOOTH SCROLL end */

        // GO TO TOP
        $('.gototop').on('click', function(e) {
            e.preventDefault();
            $('body, html').animate({scrollTop: 0});
        });

    });
</script>

</body>
</html>