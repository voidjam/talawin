<?php

  $rootpath = '../';
  require_once($rootpath.'lib/phpmailer/PHPMailerAutoload.php');

  include_once($rootpath . 'lib/lang.php');

    // Get connection to database.
  include $rootpath.'admin/scripts/connect_front.php';

  $mailStrings = array(

  ///////////////////////////////////////////////////////////////////////
  // SPANISH ////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////
    'es' => array( 
               119   => '<strong>¡Atención!</strong> Formato de email no válido.',
               120   => '<strong>¡Atención!</strong> No se ha podido enviar el mensaje. Por favor, intentelo de nuevo más tarde.',
               121   => '<strong>¡Atención!</strong> Por favor, rellene todos los campos.'
      ),
  ///////////////////////////////////////////////////////////////////////
  // ENGLISH ////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////
    'en' => array( 
           119   => '<strong>Warning!</strong> Email format not valid.',
           120   => '<strong>Warning!</strong> Your email could not be sent. Please try again later.',
           121   => '<strong>Warning!</strong> Please fill in the appropriate fields.'
      )
  );

if($_POST){

    $name = $_POST['form_name'];
    $company = $_POST['form_company'];
    $email = $_POST['form_email'];
    $phone = $_POST['form_phone'];
    $message = $_POST['form_message'];
    $subject = $_POST['form_subject'];

    if (!empty($name) && !empty($company) && !empty($email) && !empty($phone) && !empty($message)) { // all fields are required!!!

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) { // validating email...
            echo '<div class="alert alert-warning alert-dismissible form-notification" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  ' . $mailStrings[$language][119] . '</div>'; // <script>$(".form-notification").fadeTo(2000, 500).fadeOut(400, function(){$(".form-notification").alert("close");});</script>
            die();
        }
    
        // Precuación para Windows (Ref: http://php.net/manual/es/function.mail.php)
        //$message = str_replace("\n.", "\n..", $message);

        // Firstly we save the message on database.
        $messageDAO = new MessageDAO();
        $messageDTO = MessageDTO::createMessageDTO(null, $email, $name, $phone, $company, $message, $subject);
        $insertedMsgId = $messageDAO->saveMessage( $messageDTO);

        // Now we send an email notification with the client's message.
        $message = nl2br(htmlentities($message, ENT_QUOTES, 'UTF-8'));

        $email_msg = new PHPMailer();
        $email_msg->From      = 'info@talawin.com';
        $email_msg->FromName  = $name;
        $email_msg->Subject   = $subject;
        $email_msg->IsHTML(true);
        // Activo condificacción utf-8
        $email_msg->CharSet = 'UTF-8';
        $email_msg->Body = "Nombre: <b>$name</b><br>Empresa: <b>$company</b><br>Email: <b>$email</b><br>Teléfono: <b>$phone</b><br><br>$message";
        $email_msg->AddAddress( 'info@talawin.com' );
        $email_msg->AddAddress( 'chadypa@hotmail.com' );
        $email_msg->addReplyTo($email, $name);

        //send email@
          if ($email_msg->Send()){
            echo 'success';
            die();
        } else {
            echo '<div class="alert alert-warning alert-dismissible form-notification" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  ' . $mailStrings[$language][120] . '</div>'; // <script>$(".form-notification").fadeTo(2000, 500).fadeOut(400, function(){$(".form-notification").alert("close");});</script>
            die();
        }

    } else {
        echo '<div class="alert alert-warning alert-dismissible form-notification" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  ' . $mailStrings[$language][121] . '</div>'; // <script>$(".form-notification").fadeTo(2000, 500).fadeOut(400, function(){$(".form-notification").alert("close");});</script>
        die();
    }

} else {

    echo '<div class="alert alert-warning alert-dismissible form-notification" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          ' . $mailStrings[$language][120] . '</div>'; // <script>$(".form-notification").fadeTo(2000, 500).fadeOut(400, function(){$(".form-notification").alert("close");});</script>
    die();

}

?>