<?php


// The language will be set on the URL. This is the URL without the hots, that is for instance "/es/about/...."
$url            = $_SERVER['PHP_SELF'];

// Depending on the path, set by $rootpath, we will extract all components from URL.
$path           = '../'.$rootpath;  // File name will be treated as another element of the path (index.php)
$dirs           = array();
do  {
    $path       = substr($path, 3);
    $dirs[]     = substr($url, strrpos($url, '/') + 1);
    $url        = substr($url, 0, strrpos($url, '/'));
} while (substr($path, 0, 3) == '../');

// We reverse the resulting array so the language value is on item 0.
$dirs           = array_reverse($dirs);
// Set the language value from URL.
$language       = $dirs[0];
// Check if languages has a possible value, 'es' or 'en', for example when loading admin panel.
if ($language != 'es' && $language != 'en') $language = 'es';
// Set locale on server depending on language for dates and time formatting.
setlocale(LC_ALL,$language === 'en'  ?  'en_US.UTF-8'  : 'es_ES.UTF-8');



////////////////////////////////////////////////////////////////////////////////////////////////
// ARRAY WITH TEXTS ////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

$strings = array(

    ///////////////////////////////////////////////////////////////////////
    // SPANISH ////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    'es' => array(

        // Page Title & Description
        001    => 'TALAWIN',
        002    => 'Encuentra los equipos de impresión de gran formato que más se ajusten a las necesidades de tu negocio. Consiga con nosotros el asesoramiento técnico más especializado y el mejor apoyo comercial.',

        // Navbar
        100    => 'Inicio',
        101    => 'Impresoras',
        10100    => 'Impresoras EFI',
        10101    => 'Impresoras LIYU',
        102    => 'Ocasión',
        103    => 'About',
        104    => 'Contacto',
        105    => '3D',

        // Home
        200   => 'Bienvenido a Talawin',
        201   => 'Encuentra los equipos de impresión de gran formato que más se ajusten a las necesidades de tu negocio. Consiga con nosotros el asesoramiento técnico más especializado y el mejor apoyo comercial.',
        202   => 'The power of quality',
        203 => 'Impresoras nuevas',
        204 => 'Los más actualizados sistemas de impresión con la tecnología más puntera que mejor se adapta a las exigencias del mercado actual.',
        205 => 'Ocasión',
        206 => 'Unidades revisadas, certificadas y garantizadas que suponen una oportunidad de negocio de excelente viabilidad al tiempo que favorecen un retorno de inversión más rápido.',
        207 => 'Sobre nosotros',
        208 => 'Descubre quiénes somos, cómo trabajamos y cuál es nuestro objetivo. Además, a quién representamos y quiénes son nuestros clientes.',
        209 => 'Contáctanos',
        210 => 'Déjanos informarte personalmente de las mejores oportunidades para tu empresa o negocio.',
        211 => 'NUESTROS VÍDEOS',
        212 => 'ÚLTIMAS NOVEDADES',
        213 => 'Impresión 3D',
        214 => 'La impresión 3D a gran escala ya es una realidad. Descubre el futuro consultando nuestro catálogo exclusivo de impresoras 3D.',

        // Impresoras
        300 => 'Tinta',
        301 => 'Tinta blanca',
        302 => 'Tipo del medio',
        303 => 'Máx. Ancho Impresión',
        304 => 'Máx. Grosor Material',
        305 => 'Tamaño gota',
        306 => 'Resolución',
        307 => 'Máx. velocidad',
        308 => 'Garantía',
        309 => 'Antigüedad',
        310 => 'Más Info',
        311 => 'Presupuesto',
        312 => 'Vendida',
        313 => 'Ver más modelos de impresoras de gran volumen',
        314 => 'meses',

        // Impresoras 3D:
        315 => "Ver mas información sobre Massivit, impresoras 3D de gran formato",
        316 => "Impresoras 3D",
        317 => "cm/hora en el eje Z",

        // Solicitar presupuesto
        400 => 'Solicitar presupuesto para',
        401 => 'Solicitar presupuesto para máquina ocasión',
        402 => 'Solicitud presupuesto',
        403 => 'Solicitud presupuesto máquina ocasión',
        404 => 'Nombre y apellidos',
        405 => 'Empresa',
        406 => 'Email',
        407 => 'Teléfono',
        408 => 'Mensaje',
        409 => 'Cerrar',
        410 => 'Enviar',
        411 => '¡Solicitud enviada! Muchas gracias, le contactaremos lo antes posible.',
        412 => 'Ha ocurrido un error. Por favor, inténtelo de nuevo más tarde.',
        413 => 'Puede confiar en Talawin',

        // About
        500 => 'Sobre TALAWIN',
        501 => '<p>TALAWIN nace hace 18 años de la experiencia inicial en soporte técnico y post venta de diversos sectores industriales.</p>

						<p>Hoy en día, TALAWIN es una empresa líder en el sector de la impresión digital en gran formato y súper-gran formato, siendo distribuidores oficiales de grandes marcas como:</p>',
		513 => '<p>Nuestra experiencia en el mercado a través de los años nos demuestra que las empresas prefieren tratar con un proveedor que no sólo oferta el equipo adecuado para sus necesidades, sino que también ofrece una completa transparencia en lo que respecta a la entrega, instalación, formación y apoyo continuado.</p>

						<p>Desde TALAWIN, abarcamos todas las demandas del sector de la impresión digital, desde la consultoría para ayudar a definir cuál es el equipo más adecuado a las necesidades de cada cliente, hasta el soporte post venta continuado;  con formación, mantenimiento y soporte técnico especializado.</p>

						<p>Visítenos en nuestra sala demo para probar nuestros equipos y poder ejecutar sus propios trabajos de prueba, así evaluará las tecnologías líderes más actuales en un entorno adecuado para esta función, donde le será fácil tomar la decisión más acertada.</p>',
        502 => 'Nuestros clientes nos avalan',
        503 => '<p>En TALAWIN sabemos que la mejor carta de presentación son nuestros clientes. En todo momento ofrecemos un servicio personalizado y de calidad, resolviendo todas sus dudas y atendiendo todas sus consultas, satisfaciendo así todas las expectativas de nuestros clientes.</p>

						<p>Porque nuestros clientes son nuestro mejor aval.</p>',
        504 => 'Equipo TALAWIN',
        505 => 'Gerente',
        506 => 'Director Comercial',
        507 => 'Directora Financiera',
        508 => 'Resp. Sala Demo',
        509 => 'Coordinadora',
        510 => 'Coordinador Técnico',
//        511 => 'Fabricantes',
//        512 => 'Nuestros fabricantes son nuestra mejor garantía.',
        513 => '<p>Nuestra experiencia en el mercado a través de los años nos demuestra que las empresas prefieren tratar con un proveedor que no sólo oferta el equipo adecuado para sus necesidades, sino que también ofrece una completa transparencia en lo que respecta a la entrega, instalación, formación y apoyo continuado.</p>

						<p>Desde TALAWIN, abarcamos todas las demandas del sector de la impresión digital, desde la consultoría para ayudar a definir cuál es el equipo más adecuado a las necesidades de cada cliente, hasta el soporte post venta continuado;  con formación, mantenimiento y soporte técnico especializado.</p>

						<p>Visítenos en nuestra sala demo para probar nuestros equipos y poder ejecutar sus propios trabajos de prueba, así evaluará las tecnologías líderes más actuales en un entorno adecuado para esta función, donde le será fácil tomar la decisión más acertada.</p>',

        // Contacto
        600 => 'Sede y Sala Demo',
        601 => 'Calle Margarita 16<br>
                                    Polígono Industrial El Lomo<br>
                                    28970 Humanes, Madrid<br>
									ESPAÑA',
        602 => 'Envíanos un mensaje',
        603 => 'Nombre y apellidos',
        604 => 'Empresa',
        605 => 'Email',
        606 => 'Teléfono',
        607 => 'Mensaje',
        608 => 'Enviar',
        609 => 'Enviar otro mensaje',

        // MailChimp
        700 => 'Suscríbete a nuestro Newsletter',
        701 => 'Tu nombre',
        702 => 'Tu email',
        703 => 'Suscríbete',
        704 => 'Talawin Newsletter',
        705 => '¡Te has inscrito! :)',
        706 => 'Acepto la&nbsp;<a href=\'../../docs/Política_de_Privacidad_TALAWIN_es.pdf\' target=\'_blank\'>política de privacidad</a>',
        707 => '<strong style=\'color:red\'>Debes aceptar nuestra <a href=\'../../docs/Política_de_Privacidad_TALAWIN_es.pdf\' target=\'_blank\'>Política de Privacidad</a></strong>',

        // Detalle Impresoras
        800 => 'Tamaño gota',
        801 => 'Resolución',
        802 => 'Velocidad máxima',
        803 => 'Tinta UV LED',
        804 => 'Solicitar presupuesto',
        805 => 'Descripción de la impresora',
        806 => 'General',
        807 => 'Características',
        808 => 'Especificaciones',
        809 => 'Aplicaciones',
        810 => 'Documentación',
        811 => 'Pancartas',
        812 => 'Vallas publicitarias',
        813 => 'Marquesinas para paradas de autobús',
        814 => 'Retroiluminación día/noche',
        815 => 'Gráficos para expositores',
        816 => 'Gráficos para exposiciones',
        817 => 'Banderas publicitarias',
        818 => 'Gráficos para flotas/vehículos',
        819 => 'Señalización interior',
        820 => 'Gráficos para puntos de venta',
        821 => 'Punto de venta',
        822 => 'Pósteres',
        823 => 'Gráficos/carteles para escaparates',
        824 => 'Gráficos para suelos',
        825 => 'Gráficos en piezas de cerámica',
        826 => 'Etiquetas',
        827 => 'Ampliar',

        // Impresoras 3D
        900 => 'Impresoras 3D',
        901 => 'Nº de cabezales',
        902 => 'Máx. Vol. Impresión',
        903 => 'Calidad',
        904 => 'Software',
        905 => 'Número de cabezales',
        906 => 'Máximo volumen de impresión',
        907 => 'Velocidad',


        // Footer
        1000 => "política de privacidad",
        1001 => "aviso legal",



        // SATIC TABLES: We are going to try to set QUALITY static table on an array
        // insted of database. There will be no foreign key to its id but I think it
        // will work just fine.
        'qualities' => array(
            0 => "Alta Resolucion",
            1 => "Calidad",
            2 => "Normal",
            3 => "Alta Resolución/Calidad/Normal")

    ),
    // END OF SPANISH /////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////////////////
    // ENGLISH ////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    'en' => array(

        // Page Title & Description
        001 => 'TALAWIN',
        002 => 'Find the large format printing equipment that best suits your business needs. Get the most specialized technical advice and the best commercial support.',


        // Navbar
        100 => 'Home',
        101 => 'Printers',
        10100    => 'EFI Printers',
        10101    => 'LIYU Printers ',
        102 => 'Outlet',
        103 => 'About',
        104 => 'Contact',
        105 => '3D',


        // Home
        200 => 'Welcome to Talawin',
        201 => 'Find the large format printing equipment that best suits your business needs. Get the most specialized technical advice and the best commercial support.',
        202 => 'The power of quality',
        203 => 'New printers',
        204 => 'The most up-to-date printing systems with the most cutting-edge technology that best adapts to today\'s market demands',
        205 => 'Recertified printers',
        206 => 'Reviewed, certified and guaranteed units which are a business opportunity of excellent viability while favoring a faster return of investment.',
        207 => 'About us',
        208 => 'Discover who we are, how we work and what our objective is. Also whom we represent and who our customers are.',
        209 => 'Contact us',
        210 => 'Let us inform you personally about the best opportunities for your company or business.',
        211 => 'OUR VIDEOS',
        212 => 'LATEST NEWS',
        213 => '3D Printing',
        214 => 'Large Scale 3D printing is now a reality. Discover the future at our exclusive catalog 3D printer',


        // Impresoras
        300 => 'Ink',
        301 => 'White ink',
        302 => 'Media type',
        303 => 'Max. print width',
        304 => 'Max. material thickness',
        305 => 'Drop size',
        306 => 'Resolution',
        307 => 'Max. speed',
        308 => 'Guarantee',
        309 => 'Antiquity',
        310 => 'More Info',
        311 => 'Quote',
        312 => 'Sold',
        313 => 'Watch more wide format printer models',
        314 => 'months',

        // Impresoras 3D:
        315 => "More info about Massivit, Wide Format 3D Printers",
        316 => "3D Printers",
        317 => "cm/hour on Z axis",


        // Solicitar presupuesto
        400 => 'Request a budget for',
        401 => 'Request a budget for recertified machines',
        402 => 'Budget request',
        403 => 'recertified machines budget request',
        404 => 'Name and surname',
        405 => 'Company',
        406 => 'Email',
        407 => 'Telephone number',
        408 => 'Message',
        409 => 'Close',
        410 => 'Send',
        411 => 'Request sent! Thank you, we will contact you as soon as possible.',
        412 => 'An error has ocurred. Please, try again later.',
        413 => 'You can trust us',


        // About
        500 => 'About TALAWIN',
        501 => '<p>TALAWIN began 18 years ago from the initial experience about technical support and post sale from various industrial sectors.</p>
                
                        <p>Nowadays, TALAWIN is a leading company in the large and the super large format digital printing sector, as large brands official distributors like:</p>',
        502 => 'Our customers endorse us',
        503 => '<p>In TALAWIN we know that our customers are our best cover letter. At all times, we offer a personalized high quality service, solving all your doubts and attending all your questions, satisfying all our customer\'s expectations.</p>
                
                        <p>Because our customers are our best endorsement.</p>',
        504 => 'Staff TALAWIN',
        505 => 'General Manager',
        506 => 'Sales Director',
        507 => 'Financial Director',
        508 => 'Demo Room Resp.',
        509 => 'Coordinator',
        510 => 'Technical coordinator',
//        511 => 'Fabricantes',
//        512 => 'Nuestros fabricantes son nuestra mejor garantía.',
        513 => '<p>Our market experience through the years shows us that companies prefer to deal with a supplier that not only offers the right equipment for their needs, but that also offers a complete transparency about delivery, installation and training, and continued support.</p>
                
                        <p>From TALAWIN, we cover all the digital printing industry demands, from the consultancy, in order to help defining which is the most appropriate equipment for each customer need, to post-sale continued support;  with training, maintenance and specialized technical support.</p>
                
                        <p>Visit us in our demo room to test our equipment and run your own test jobs, so you will evaluate the most current leading technologies in a suitable environment, where it will be easy for you to make the best decision.</p>',


        // Contacto
        600 => 'Headquarters and Show Room',
        601 => 'Calle Margarita 16<br>
                                    Polígono Industrial El Lomo<br>
                                    28970 Humanes, Madrid<br>
                                    ESPAÑA',
        602 => 'Send us an email',
        603 => 'Name and surname',
        604 => 'Company',
        605 => 'Email',
        606 => 'Telephone number',
        607 => 'Message',
        608 => 'Send',
        609 => 'Send another message',


        // MailChimp
        700 => 'Subscribe to our Newsletter',
        701 => 'Your name',
        702 => 'Your email',
        703 => 'Subscribe',
        704 => 'Talawin Newsletter',
        705 => 'You have registered! :)',
        706 => 'I accept the&nbsp;<a href=\'../../docs/Política_de_Privacidad_TALAWIN_es.pdf\' target=\'_blank\'>Privacy Policy</a>',
        707 => '<strong style=\'color:red\'>You haven\'t accepted our <a href=\'../../docs/Política_de_Privacidad_TALAWIN_es.pdf\' target=\'_blank\'>Privacy Policy</a></strong>',


        // Detalle Impresoras
        800 => 'Drop size',
        801 => 'Resolution',
        802 => 'Maximum speed',
        803 => 'UV LED ink',
        804 => 'Request a quote',
        805 => 'Printer description',
        806 => 'Overview',
        807 => 'Features',
        808 => 'Specs',
        809 => 'Applications',
        810 => 'Documentation',
        811 => 'Banners',
        812 => 'Billboards',
        813 => 'Bus stop canopies',
        814 => 'Backlight day/night',
        815 => 'Graphics for exhibitors',
        816 => 'Exhibition graphics',
        817 => 'Advertising flags',
        818 => 'Vehicles/fleet graphics',
        819 => 'Interior signage',
        820 => 'Point of sale graphics',
        821 => 'Point of sale',
        822 => 'Posters',
        823 => 'Shop windows graphics/posters',
        824 => 'Floor graphics',
        825 => 'Graphics on ceramic pieces',
        826 => 'Labels',
        827 => 'Zoom',

        // Impresoras 3D
        900 => '3D Printers',
        901 => 'Nº de cabezales',
        902 => 'Máx. Vol. Impresión',
        903 => 'Quality',
        904 => 'Software',
        905 => 'Número de cabezales',
        906 => 'Máximo volumen de impresión',
        907 => 'Print Speed',


        // Footer
        1000 => "privacy policy",
        1001 => "legal notice",


        // SATIC TABLES: We are going to try to set QUALITY static table on an array
        // insted of database. There will be no foreign key to its id but I think it
        // will work just fine.
        'qualities' => array(
            0 => "High Resolution",
            1 => "Quality",
            2 => "Normal",
            3 => "High Resolution/Quality/Normal")
    )
    // END OF ENGLISH /////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
);

////////////////////////////////////////////////////////////////////////////////////////////////
// /ARRAY WITH TEXTS ///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////


?>