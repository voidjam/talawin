<?php

$title = 'Redirección';
$rootpath = './';

include_once($rootpath . 'lib/langSelect.php');

header("Location: ./{$language}/");

?>
<html>
<head>
	<title>TALAWIN · The power of quality</title>

	<link rel="alternate" href="https://www.talawin.com/es" hreflang="es" />
    <link rel="alternate" href="https://www.talawin.com/en" hreflang="en" />

    <meta name="description" content="Encuentra los equipos de impresión de gran formato que más se ajusten a las necesidades de tu negocio. Consiga con nosotros el asesoramiento técnico más especializado y el mejor apoyo comercial." />
</head>
</html>