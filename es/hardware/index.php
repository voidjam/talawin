<?php

// Display PHP Errors
// ini_set('display_errors', '1');
// ini_set('display_startup_errors', '1');
// error_reporting(E_ALL);

$rootpath = '../../';

// Get connection to database.
include $rootpath . 'admin/scripts/connect_front.php';

// GET var to know if we are accessing thrw OLD PRINTERS link or NEW PRINTERS link.
$filter_new = (isset($_GET["section"]) && $_GET["section"] === "outlet") ? false : true;
// GET var to know if we are accessing thrw EFI printers or LIYU printers.
$filter_brand = isset($_GET["brand"]) ? $_GET["brand"] : false;


// Include language related vars.
include_once $rootpath . 'lib/lang.php';

// Set page title.
$page_title = $filter_new
    ? $filter_brand == 'efi' ? $strings[$language][10100] : $strings[$language][10101]
    : $strings[$language][102];

include_once($rootpath . 'inc/head.php');

?>
    <!DOCTYPE html>
    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-86281163-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments)};
        gtag('js', new Date());
        gtag('config', 'UA-86281163-3');
    </script>

    <script>
        document.body.id = "printers"
        document.body.classList.add("fade-out");
        document.body.classList.add("<?php echo ($filter_new ? $filter_brand : 'used')?>");
    </script>

    <!-- WRAPPER -->
    <div class="wrapper">

        <!-- MAIN CONTENT -->
        <div class="container">

            <ol class="breadcrumb bg-light text-small">
                <li class="breadcrumb-item"><a href="<?php echo $rootpath . $language . '/'; ?>"><?php echo $strings[$language][100]; ?></a></li>
                <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
            </ol>

            <div class="page_title">
                <img src="<?php echo $rootpath; ?>admin/img/printer_brands/logo_<?php echo $filter_brand ?>.png"/>
                <h3 class="title mb-4"><?php echo $page_title; ?></h3>
            </div>


            <?php

            // Retrieve all printers from database.
            $printerDAO           = new PrinterDAO();
            $fileDAO              = new FileDAO();
            $arrayPrinterDTO      = (array)$printerDAO->getPrinters();

            // Filter printers by brand
            if ($filter_new) {
                $arrayPrinterDTO = $filter_brand == 'efi'
                    ? array_values (array_filter($arrayPrinterDTO, function ($item)   { return $item->getBrandId()==0;}))
                    : array_values (array_filter($arrayPrinterDTO, function ($item)   { return $item->getBrandId()==1;}));
            }

            // Getting static table labels from database.
            $brands               = $printerDAO->getPrinterBrands();
            $inks                 = $printerDAO->getPrinterInks();
            $white_inks           = $printerDAO->getPrinterWhiteInks();
            $media_types          = $printerDAO->getPrinterMediaTypes();
            ?>

            <div class="row">
                <?php
                // If there are any printers on database we start creating rows:
                if (is_array($arrayPrinterDTO) && sizeof($arrayPrinterDTO) > 0) {

                    // Filter array with all printers to take only the new ones or the old ones.
                    $filter_new ? $arrayPrinterDTO = array_values (array_filter($arrayPrinterDTO, function ($item)   { return $item->getIsNew(); }))
                        : $arrayPrinterDTO = array_values (array_filter($arrayPrinterDTO, function ($item)   { return !$item->getIsNew();})) ;
                    // Retrieve the main image for every printer object.
                    foreach ($arrayPrinterDTO as $index => &$printerDTO) {
                        $printerDTO->setFilesArray($fileDAO->getMainImageByPrinterId($printerDTO->getId()));
                        unset($printerDTO);
                    }

                    // We create a row for each PrinterDTO contained on the array.
                    foreach ($arrayPrinterDTO as $key => $printerDTO) {
                        if ($printerDTO->getIsPublished()) {

                            // Create an img element to show the main pic of the current printer.
                            $fileDTO = $printerDTO->getFilesArray();
                            if ($fileDTO instanceof FileDTO)    {
                                $main_pic = "<img   src='".$rootpath.$fileDTO->getPath()."thum_".$fileDTO->getName()."' 
                                    data-original='".$rootpath.$fileDTO->getPath()."thum_".$fileDTO->getName()."' 
                                    class='card-img-top img-fluid lazy grid-img main-pic-grid-img' alt='".$printerDTO->getTitle()."'
                                    title='".$printerDTO->getTitle()."'/>";
                            } else  {
                                $main_pic =  "<span><strong>No hay fotos</strong></span>";
                            } ?>

                            <!-- PRINTER CARD -->
                            <div class="col-md-6 col-lg-4 printers-grid">
                                <div class="card d-flex">
                                    <!-- CARD TITLE -->
                                    <div class="card-header bg-light text-info">
                                        <a href="<?php echo $rootpath . $language . '/' . 'hardware/details/?product='.$printerDTO->getTitle().'&id='.$printerDTO->getId(); ?>" style="text-decoration: none;"><span class="lead" style="font-weight: 500;"><?php echo $printerDTO->getTitle(); ?></span></a>
                                    </div>

                                    <!-- CARD IMAGE -->
                                    <a href="<?php echo $rootpath . $language .'/' .'hardware/details/?product='.$printerDTO->getTitle().'&id='.$printerDTO->getId(); ?>" style="text-decoration: none;">
                                    <div class="main-pic-grid align-middle d-flex justify-content-center align-items-center">
                                        <?php
                                        // If Printer is sold (only Outlet) we place "sold" image.
                                        if ($printerDTO->isSold()) { ?>
                                            <div class="printer-sold rounded"><img src="<?php echo $rootpath; ?>img/sold.gif" class="img-fluid lazy"></div>
                                        <?php }

                                        // If Printer is NEW (new release) we place "NEW" icon.
                                        if ($printerDTO->getIsRelease()) { ?>
                                            <div class="printer-release"><img src="<?php echo $rootpath; ?>img/printer-release.png" class="lazy" width="20%"></div>
                                        <?php } ?>
                                        <!-- MAIN PIC IMAGE -->
                                        <?php echo $main_pic; ?>
                                    </div></a>

                                    <!-- LIST OF FEATURES -->
                                    <ul class="list-group list-group-flush text-muted">
                                        <!-- FIELD INK -->
                                        <li class="list-group-item bg-light"  title="<?php echo $strings[$language][300]; ?>: <?php echo $inks[$printerDTO->getFeatInkId()]['label_'.$language]; ?>"><?php echo $strings[$language][300]; ?>: <span class="ml-auto"><strong><?php echo $inks[$printerDTO->getFeatInkId()]['label_'.$language]; ?></strong></span></li>
                                        <!-- FIELD WHITE INK -->
                                        <li class="list-group-item"           title="<?php echo $strings[$language][301]; ?>: <?php echo $white_inks[$printerDTO->getFeatWhiteInkId()]['label_'.$language]; ?>"><?php echo $strings[$language][301]; ?>: <span class="ml-auto"><strong><?php echo $white_inks[$printerDTO->getFeatWhiteInkId()]['label_'.$language]; ?></strong></span></li>
                                        <!-- FIELD MEDIA TYPE -->
                                        <li class="list-group-item bg-light"  title="<?php echo $strings[$language][302]; ?>: <?php echo $media_types[$printerDTO->getFeatMediaTypeId()]['label_'.$language]; ?>"><?php echo $strings[$language][302]; ?>: <span class="ml-auto"><strong><?php echo $media_types[$printerDTO->getFeatMediaTypeId()]['label_'.$language]; ?></strong></span></li>
                                        <!-- FIELD MAX PRINT WIDTH -->
                                        <li class="list-group-item"           title="<?php echo $strings[$language][303]; ?>: <?php echo number_format($printerDTO->getFeatMaxPrintWidth(), 0); ?>"><?php echo $strings[$language][303]; ?>: <span class="ml-auto"><strong><?php echo number_format($printerDTO->getFeatMaxPrintWidth(), 0); ?> cm</strong></span></li>
                                        <!-- FIELD MEDIA THICK -->
                                        <li class="list-group-item bg-light"  title="<?php echo $strings[$language][304]; ?>: <?php echo number_format($printerDTO->getFeatMaxMediaThick(), 0); ?> mm"><?php echo $strings[$language][304]; ?>: <span class="ml-auto"><strong><?php echo number_format($printerDTO->getFeatMaxMediaThick(), 0); ?> mm</strong></span></li>
                                        <!-- FIELD DROP SIZE -->
                                        <li class="list-group-item"           title="<?php echo $strings[$language][305]; ?>: <?php echo number_format($printerDTO->getFeatDropSize(), 0); ?> pl"><?php echo $strings[$language][305]; ?>: <span class="ml-auto"><strong><?php echo number_format($printerDTO->getFeatDropSize(), 0); ?> pl</strong></span></li>
                                        <!-- FIELD RESOLUTION -->
                                        <li class="list-group-item bg-light"  title="<?php echo $strings[$language][306]; ?>: <?php echo $printerDTO->getFeatResolution(); ?> dpi"><?php echo $strings[$language][306]; ?>: <span class="ml-auto"><strong><?php echo $printerDTO->getFeatResolution(); ?> dpi</strong></span></li>
                                        <!-- FIELD MAX SPEED -->
                                        <li class="list-group-item"           title="<?php echo $strings[$language][307]; ?>: <?php echo $printerDTO->getFeatMaxSpeed(); ?> m²/hr"><?php echo $strings[$language][307]; ?>: <span class="ml-auto"><strong><?php echo $printerDTO->getFeatMaxSpeed(); ?> m²/hr</strong></span></li>
                                        <!-- FIELD WARRANTY -->
                                        <li class="list-group-item bg-light"  title="<?php echo $strings[$language][308]; ?>: <?php echo $printerDTO->getWarranty(); ?> <?php echo $strings[$language][314]; ?>"><?php echo $strings[$language][308]; ?>: <span class="ml-auto"><strong><?php echo $printerDTO->getWarranty(); ?> <?php echo $strings[$language][314]; ?></strong></span></li>
                                        <!-- FIELD PRODUCTION DATE: ONLY OLD PRINTERS -->
                                        <?php  if (!$filter_new) {
                                            echo '<li class="list-group-item">'.$strings[$language][309].': <span class="ml-auto"><strong>';
                                            // Locale is already set from lang.php
                                            echo ucfirst(strftime("%B %Y",strtotime( $printerDTO->getProductionDate() ))); // ucfirst: first letter -> capital letter
                                            echo '</strong></span></li>';
                                        } ?>
                                    </ul>
                                    <!-- END OF LIST OF FEATURES -->

                                    <!-- BUTTONS -->
                                    <div class="text-right p-3 mt-auto">
                                        <!-- MORE INFO -->
                                        <a href="<?php echo $rootpath . $language .'/' .'hardware/details/?product='.$printerDTO->getTitle().'&id='.$printerDTO->getId(); ?>"><button type="button" class="btn btn-sm btn-outline-info float-left"><?php echo $strings[$language][310]; ?></button></a>
                                        <!-- QUOTATION BUTTON: IF NOT ALREADY SOLD -->
                                        <?php
                                        // If Printer is sold (only Outlet) we disable "quote" button change label
                                        if ($printerDTO->isSold()) { ?>
                                            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#quoteModal" data-product="<?php echo $printerDTO->getTitle(); ?>" disabled><i class="fa fa-eur" aria-hidden="true"></i> <?php echo $strings[$language][312]; ?></button>
                                        <?php } else { ?>
                                            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#quoteModal" data-product="<?php echo $printerDTO->getTitle(); ?>"><i class="fa fa-eur" aria-hidden="true"></i> <?php echo $strings[$language][311]; ?></button>
                                        <?php } ?>
                                    </div>
                                    <!-- END OF BUTTONS -->
                                </div>
                            </div>
                            <!-- END OF PRINTER 3D CARD -->
                        <?php }
                    }
                } ?>
            </div>
            <!-- MORE PRINTERS: LINK TO EFI -->
            <a href="http://www.efi.com/es-es/products/inkjet-printing-and-proofing/vutek-superwide-printers/high-volume/" target="_blank"><i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo $strings[$language][313]; ?></a>
        </div>
        <!-- MAIN CONTENT end -->

        <!-- ----------------------------------------- -->
        <!-- QUOTATION REQUEST MODAL ----------------- -->
        <div class="modal fade" id="quoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header border-info bg-light rounded-top">
                        <h5 class="modal-title text-info" id="exampleModalLabel"><?php echo $strings[$language][402]; ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="formResponse"></div>
                        <div id="formContainer">
                            <form id="quoteForm">
                                <div class="form-group">
                                    <label for="form_name"><?php echo $strings[$language][404]; ?></label>
                                    <input type="text" class="form-control form-control-sm" id="form_name" name="form_name" required>
                                </div>
                                <div class="form-group">
                                    <label for="form_company"><?php echo $strings[$language][405]; ?></label>
                                    <input type="text" class="form-control form-control-sm" id="form_company" name="form_company" required>
                                </div>
                                <div class="form-group">
                                    <label for="form_email"><?php echo $strings[$language][406]; ?></label>
                                    <input type="email" class="form-control form-control-sm" id="form_email" name="form_email" required>
                                </div>
                                <div class="form-group">
                                    <label for="form_phone"><?php echo $strings[$language][407]; ?></label>
                                    <input type="tel" class="form-control form-control-sm" id="form_phone" name="form_phone" required>
                                </div>
                                <div class="form-group">
                                    <label for="form_message"><?php echo $strings[$language][408]; ?></label>
                                    <textarea class="form-control form-control-sm" id="form_message" name="form_message" style="word-wrap: break-word; resize: vertical;"></textarea>
                                </div>
                                <input type="hidden" id="form_subject" name="form_subject">
                                <!-- PRIVACY POLITIC ACCEPTANCE CHECKBOX -->
                                <div class="privacy" style="padding-left: .8rem;"><input type="checkbox" id="cbox2" value=""/><label style="padding-left: .4rem;" for="cbox2"><?php echo $strings[$language][706]; ?></label></div>
                        </div>

                    </div>
                    <div class="modal-footer bg-light rounded-bottom">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $strings[$language][409]; ?></button>
                        <button type="submit" class="btn btn-success" id="quoteSubmit"><?php echo $strings[$language][410]; ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END OF QUOTATION REQUEST MODAL ---------- -->
        <!-- ----------------------------------------- -->

        <!-- MatchHeight jQuery Plugin -->
        <script src="<?php echo $rootpath; ?>js/jquery.matchHeight.js" type="text/javascript"></script>

        <!-- Lazy Load jQuery Plugin -->
        <script src="<?php echo $rootpath; ?>js/jquery.lazyload.js" type="text/javascript"></script>

        <script type="text/javascript" charset="utf-8">

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // DOCUMENT READY BLOCK ////////////////////////////////////////////////////////////////////////////////////
            $(function() {

                // Set active alement on nav-bar.
                let is_new = <?php if ($filter_new) echo 1; else echo 0; ?>;
                let is_efi = <?php if ($filter_brand == 'efi') echo 1; else echo 0; ?>;
                if (is_new == 1) {
                    is_efi ? $('#menu-printers.efi').addClass('active') : $('#menu-printers.liyu').addClass('active');
                } else $('#menu-outlet').addClass('active');

                // Make imgs inside cards, and cards itself to have the same height.
                setTimeout(function() {
                    $('.main-pic-grid').matchHeight();
                }, 200);

                setTimeout(function() {
                    $('.card').matchHeight();
                }, 200);

                // Fade in images when they are completely loaded.
                $("img.lazy").lazyload({
                    effect : "fadeIn"
                });
            });
            // END OF DOCUMENT READY BLOCK /////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////


            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // QUOTATION REQUEST MODAL ////////////////////////////////////////////////////////////////////////////
            // Product name for Quote Modal
            $('#quoteModal').on('show.bs.modal', function (event) {
                var is_new = <?php if ($filter_new) echo 1; else echo 0; ?>;
                $("#formResponse").html('');
                $("#form_message").val('');
                $("#formContainer").show();
                $("#quoteSubmit").show();
                if (is_new) {

                } else {

                }
                var button = $(event.relatedTarget) // Button that triggered the modal
                var product = button.data('product') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                if (is_new) {
                    modal.find('.modal-title').text('<?php echo $strings[$language][400];?> ' + product)
                    $("#form_subject").val('<?php echo $strings[$language][402]; ?>' + product);
                } else {
                    modal.find('.modal-title').text('<?php echo $strings[$language][401];?> ' + product)
                    $("#form_subject").val('<?php echo $strings[$language][403];?>' + product);
                }
                //modal.find('.modal-body input').val(product)
            })


            //////////////////////////////////////////////////////////////////////////////////
            // AJAX: SENDING EMAIL ///////////////////////////////////////////////////////////
            $("#quoteForm").on('submit', function(e) {

                // Prevent default submit action.
                e.preventDefault();

                // Check if privacy has been accepted.
                if(!$("#quoteModal .privacy > input").is(":checked")) {
                    $('#privacy-error').modal('show');
                    return;
                }

                // Disable submit button.
                $('#quoteSubmit').attr("disabled", true);

                // Send quotation requesto to server.
                $.ajax({
                    type: "POST",
                    url: "<?php echo $rootpath; ?>lib/email-contact.php",
                    data: $(this).serialize(),
                    success: function(response){
                        if (response != 'success') {
                            $('#quoteSubmit').attr("disabled", false);
                            //$("#formResponse").html(response);
                            $("#formResponse").fadeOut(0,function(){
                                $(this).html(response).fadeIn();
                            });
                        } else {
                            $("#formResponse").html('<div class="alert alert-success alert-dismissible form-notification" role="alert">'+
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                                '<?php echo $strings[$language][411]; ?></div>'
                                //+
                                //'<script>$(".form-notification").fadeTo(2000, 500).fadeOut(400, function(){$(".form-notification").alert("close");$("#quoteSubmit").removeAttr("disabled");});<\/script>'
                            );
                            $("#formContainer").fadeOut();
                            $('#quoteSubmit').attr("disabled", false);
                            $("#quoteSubmit").fadeOut();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#formResponse").html('<div class="alert alert-danger alert-dismissible form-notification" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php echo $strings[$language][412]; ?></div>'
                            //+
                            //'<script>$(".form-notification").fadeTo(2000, 500).fadeOut(400, function(){$(".form-notification").alert("close");});<\/script>'
                        );
                        $('#quoteSubmit').attr("disabled", false);
                        console.log(textStatus, errorThrown);
                    }
                });
            });
            // END OF AJAX: SENDING EMAIL ////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////

            // END OF QUOTATION REQUEST MODAL /////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
        </script>

        <!-- AUTOSIZE JQUERY PLUGIN FOR TEXTAREAS -->
        <script src="<?php echo $rootpath; ?>js/autosize.min.js"></script>
        <script>
            autosize(document.querySelectorAll('textarea'));
        </script>

<?php
include_once($rootpath . 'inc/foot.php');
?>