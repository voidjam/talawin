<?php

$rootpath = '../../../';

// This section has been removed. Send NOT FOUND 404
http_response_code(404);
die();

// Get connection to database.
include $rootpath . 'admin/scripts/connect_front.php';

// GET var to know if we are accessing thrw OLD PRINTERS link or NEW PRINTERS link.
$filter_new = (isset($_GET["section"]) && $_GET["section"] === "outlet") ? false : true;

include_once $rootpath . 'lib/lang.php';

// Set page title.
$page_title = $strings[$language][900];

include_once($rootpath . 'inc/head.php');

?>
    <!DOCTYPE html>
    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-86281163-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments)};
        gtag('js', new Date());

        gtag('config', 'UA-86281163-3');
    </script>

    <script>document.body.className += ' fade-out';</script>

    <!-- WRAPPER -->
    <div class="wrapper">

        <!-- MAIN CONTENT -->
        <div class="container">

            <ol class="breadcrumb bg-light text-small">
                <li class="breadcrumb-item"><a href="<?php echo $rootpath . $language . '/'; ?>"><?php echo $strings[$language][100]; ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo $rootpath . $language . '/'; ?>hardware"><?php echo $strings[$language][101]; ?></a></li>
                <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
            </ol>

            <h3 class="mb-4"><?php echo $page_title; ?></h3>

            <?php

            // Retrieve all printers from database.
            $printer3dDAO = new Printer3dDAO();
            $file3dDAO = new Printer3dFileDAO();
            $arrayPrinter3dDTO = $printer3dDAO->getPrinters();

            // Getting static table labels from database.
            $brands = $printer3dDAO->getPrinter3dBrands();
            $inks = $printer3dDAO->getPrinter3dInks();
            $media_types = $printer3dDAO->getPrinter3dMediaTypes();
            ?>

            <div class="row">

                <?php

                // If there are any printers on database we start creating rows:
                if (is_array($arrayPrinter3dDTO) && sizeof($arrayPrinter3dDTO) > 0) {

                    // Filter array with all printers to take only the new ones or the old ones.
                    $filter_new ? $arrayPrinter3dDTO = array_values (array_filter($arrayPrinter3dDTO, function ($item)   { return $item->isNew(); }))
                        : $arrayPrinter3dDTO = array_values (array_filter($arrayPrinter3dDTO, function ($item)   { return !$item->isNew();})) ;
                    // Retrieve the main image for every printer object.
                    foreach ($arrayPrinter3dDTO as $index => &$printer3dDTO) {
                        $printer3dDTO->setFilesArray($file3dDAO->getMainImageByPrinter3dId($printer3dDTO->getId()));
                        unset($printer3dDTO);
                    }

                    // We create a row for each Printer3dDTO contained on the array.
                    foreach ($arrayPrinter3dDTO as $key => $printer3dDTO) {
                        if ($printer3dDTO->isPublished()) {

                            // Create an img element to show the main pic of the current printer.
                            $fileDTO = $printer3dDTO->getFilesArray();
                            if ($fileDTO instanceof Printer3dFileDTO)    {
                                $main_pic = "<img   src='".$rootpath.$fileDTO->getPath()."thum_".$fileDTO->getName()."' 
                                    data-original='".$rootpath.$fileDTO->getPath()."thum_".$fileDTO->getName()."' 
                                    class='card-img-top img-fluid lazy grid-img main-pic-grid-img' alt='".$printer3dDTO->getTitle()."'
                                    title='".$brands[$printer3dDTO->getBrandId()]." ".$printer3dDTO->getModel()."'/>";
                            } else  {
                                $main_pic =  "<span><strong>No hay fotos</strong></span>";
                            } ?>

                            <!-- PRINTER 3D CARD -->
                            <div class="col-md-6 printers-grid">
                                <div class="card d-flex" id="printer-card">

                                    <!-- CARD TITLE -->
                                    <div class="card-header bg-light text-info">
                                        <a href="<?php echo $rootpath . $language . '/' . 'hardware/3d/details/?product='.$printer3dDTO->getTitle().'&id='.$printer3dDTO->getId(); ?>" style="text-decoration: none;"><span class="lead" style="font-weight: 500;"><?php echo $printer3dDTO->getTitle(); ?></span></a>
                                    </div>

                                    <!-- CARD IMAGE -->
                                    <a href="<?php echo $rootpath . $language .'/' .'hardware/3d/details/?product='.$printer3dDTO->getTitle().'&id='.$printer3dDTO->getId(); ?>" style="text-decoration: none;">
                                    <div class="main-pic-grid align-middle d-flex justify-content-center align-items-center">
                                        <?php
                                        // if Printer is sold (only Outlet) we place "sold" image
                                        if ($printer3dDTO->isSold()) { ?>
                                            <div class="printer-sold rounded"><img src="<?php echo $rootpath; ?>img/sold.gif" class="img-fluid lazy"></div>
                                        <?php }
                                        // if Printer is NEW (new release) we place "NEW" icon
                                        if ($printer3dDTO->isRelease()) { ?>
                                            <div class="printer-release"><img src="<?php echo $rootpath; ?>img/printer-release.png" class="lazy" width="18%"></div>
                                        <?php } ?>
                                        <!-- MAIN PIC IMAGE -->
                                        <?php echo $main_pic; ?>
                                    </div></a>

                                    <!-- LIST OF FEATURES -->
                                    <ul class="list-group list-group-flush text-muted">
                                        <!-- FIELD INK -->
                                        <li class="list-group-item bg-light" title="<?php echo $strings[$language][300]; ?>: <?php echo $inks[$printer3dDTO->getFeatInkId()]; ?>"><?php echo $strings[$language][300]; ?>: <span class="ml-auto"><strong><?php echo $inks[$printer3dDTO->getFeatInkId()]; ?></strong></span></li>
                                        <!-- FIELD MEDIA TYPE -->
                                        <li class="list-group-item" title="<?php echo $strings[$language][302]; ?>: <?php echo $media_types[$printer3dDTO->getFeatMediaTypeId()]; ?>"><?php echo $strings[$language][302]; ?>: <span class="ml-auto"><strong><?php echo $media_types[$printer3dDTO->getFeatMediaTypeId()]; ?></strong></span></li>
                                        <!-- FIELD NUM HEADERS -->
                                        <li class="list-group-item bg-light" title="<?php echo $strings[$language][901]; ?>: <?php echo $printer3dDTO->getFeatNumHeaders(); ?>"><?php echo $strings[$language][901]; ?>: <span class="ml-auto"><strong><?php echo $printer3dDTO->getFeatNumHeaders(); ?></strong></span></li>
                                        <!-- FIELD PRINT SPEED -->
                                        <li class="list-group-item" title="<?php echo $strings[$language][907]; ?>: <?php echo strval($printer3dDTO->getFeatPrintSpeed()); ?>"><?php echo $strings[$language][907]; ?>: <span class="ml-auto"><strong><?php echo strval($printer3dDTO->getFeatPrintSpeed()); ?></strong></span></li>
                                        <!-- FIELD MAX PRINT VOL -->
                                        <li class="list-group-item" title="<?php echo $strings[$language][902]; ?>: <?php echo strval($printer3dDTO->getFeatMaxPrintVol()); ?>"><?php echo $strings[$language][902]; ?>: <span class="ml-auto"><strong><?php echo strval($printer3dDTO->getFeatMaxPrintVol()); ?></strong></span></li>
                                        <!-- FIELD QUALITY -->
                                        <li class="list-group-item bg-light" title="<?php echo $strings[$language][903]; ?>: <?php echo $strings[$language]['qualities'][$printer3dDTO->getFeatQuality()]; ?>"><?php echo $strings[$language][903]; ?>: <span class="ml-auto"><strong><?php echo $strings[$language]['qualities'][$printer3dDTO->getFeatQuality()]; ?></strong></span></li>
                                        <!-- FIELD SOFTARE -->
                                        <li class="list-group-item" title="<?php echo $strings[$language][904]; ?>: <?php echo $printer3dDTO->getFeatSoftware(); ?>"><?php echo $strings[$language][904]; ?>: <span class="ml-auto"><strong><?php echo $printer3dDTO->getFeatSoftware(); ?></strong></span></li>
                                        <!-- FIELD WARRANTY -->
                                        <li class="list-group-item bg-light" title="<?php echo $strings[$language][308]; ?>: <?php echo $printer3dDTO->getWarranty(); ?> <?php echo $strings[$language][314]; ?>"><?php echo $strings[$language][308]; ?>: <span class="ml-auto"><strong><?php echo $printer3dDTO->getWarranty(); ?> <?php echo $strings[$language][314]; ?></strong></span></li>
                                        <!-- FIELD PRODUCTION DATE: ONLY OLD PRINTERS -->
                                        <?php if (!$filter_new) {
                                            echo '<li class="list-group-item">'.$strings[$language][309].': <span class="ml-auto"><strong>';
                                            // Locale is already set from lang.php
                                            echo ucfirst(strftime("%B %Y",strtotime( $printer3dDTO->getProductionDate() ))); // ucfirst: first letter -> capital letter
                                            echo '</strong></span></li>';
                                        } ?>
                                    </ul>
                                    <!-- END OF LIST OF FEATURES -->

                                    <!-- BUTTONS -->
                                    <div class="text-right p-3 mt-auto">
                                        <!-- MORE INFO BUTTON -->
                                        <a href="<?php echo $rootpath . $language .'/' .'hardware/3d/details/?product='.$printer3dDTO->getTitle().'&id='.$printer3dDTO->getId(); ?>"><button type="button" class="btn btn-sm btn-outline-info float-left"><?php echo $strings[$language][310]; ?></button></a>
                                        <!-- QUOTATION BUTTON: ONLY IF NOT ALREADY SOLD -->
                                        <?php
                                        // if Printer is sold (only Outlet) we disable "quote" button change label
                                        if ($printer3dDTO->isSold()) { ?>
                                            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#quoteModal" data-product="<?php echo $printer3dDTO->getTitle(); ?>" disabled><i class="fa fa-eur" aria-hidden="true"></i> <?php echo $strings[$language][312]; ?></button>
                                        <?php } else { ?>
                                            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#quoteModal" data-product="<?php echo $printer3dDTO->getTitle(); ?>"><i class="fa fa-eur" aria-hidden="true"></i> <?php echo $strings[$language][311]; ?></button>
                                        <?php } ?>
                                    </div>
                                    <!-- END OF BUTTONS -->
                                </div>
                            </div>
                            <!-- END OF PRINTER 3D CARD -->
                        <?php }
                    }
                } ?>
            </div>

            <!-- MORE PRINTERS 3D: LINK TO MASSIVIT -->
            <a href="https://massivit3d.com/" target="_blank"><i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo $strings[$language][315]; ?></a>
        </div>
        <!-- MAIN CONTENT end -->

        <!-- ----------------------------------------- -->
        <!-- QUOTATION REQUEST MODAL ----------------- -->
        <div class="modal fade" id="quoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header border-info bg-light rounded-top">
                        <h5 class="modal-title text-info" id="exampleModalLabel"><?php echo $strings[$language][402]; ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="formResponse"></div>
                        <div id="formContainer">
                            <form id="quoteForm">
                                <div class="form-group">
                                    <label for="form_name"><?php echo $strings[$language][404]; ?></label>
                                    <input type="text" class="form-control form-control-sm" id="form_name" name="form_name" required>
                                </div>
                                <div class="form-group">
                                    <label for="form_company"><?php echo $strings[$language][405]; ?></label>
                                    <input type="text" class="form-control form-control-sm" id="form_company" name="form_company" required>
                                </div>
                                <div class="form-group">
                                    <label for="form_email"><?php echo $strings[$language][406]; ?></label>
                                    <input type="email" class="form-control form-control-sm" id="form_email" name="form_email" required>
                                </div>
                                <div class="form-group">
                                    <label for="form_phone"><?php echo $strings[$language][407]; ?></label>
                                    <input type="tel" class="form-control form-control-sm" id="form_phone" name="form_phone" required>
                                </div>
                                <div class="form-group">
                                    <label for="form_message"><?php echo $strings[$language][408]; ?></label>
                                    <textarea class="form-control form-control-sm" id="form_message" name="form_message" style="word-wrap: break-word; resize: vertical;"></textarea>
                                </div>
                                <input type="hidden" id="form_subject" name="form_subject">
                                <!-- PRIVACY POLITIC ACCEPTANCE CHECKBOX -->
                                <div class="privacy" style="padding-left: .8rem;"><input type="checkbox" id="cbox2" value=""/><label style="padding-left: .4rem;" for="cbox2"><?php echo $strings[$language][706]; ?></label></div>
                        </div>
                    </div>
                    <div class="modal-footer bg-light rounded-bottom">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $strings[$language][409]; ?></button>
                        <button type="submit" class="btn btn-success" id="quoteSubmit"><?php echo $strings[$language][410]; ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END OF QUOTATION REQUEST MODAL ---------- -->
        <!-- ----------------------------------------- -->

        <!-- MatchHeight jQuery Plugin -->
        <script src="<?php echo $rootpath; ?>js/jquery.matchHeight.js" type="text/javascript"></script>

        <!-- Lazy Load jQuery Plugin -->
        <script src="<?php echo $rootpath; ?>js/jquery.lazyload.js" type="text/javascript"></script>




        <script type="text/javascript" charset="utf-8">

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // DOCUMENT READY BLOCK ////////////////////////////////////////////////////////////////////////////////////
            $(function() {

                // Set active alement on nav-bar.
                $('#menu-printers3d').addClass('active');

                // Make imgs inside cards, and cards itself to have the same height.
                setTimeout(function() {
                    $('.main-pic-grid').matchHeight();
                }, 200);

                setTimeout(function() {
                    $('.card').matchHeight();
                }, 200);


                // Fade in images when they are completely loaded.
                $("img.lazy").lazyload({
                    effect : "fadeIn"
                });

            });
            // END OF DOCUMENT READY BLOCK /////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////



            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // QUOTATION REQUEST MODAL ////////////////////////////////////////////////////////////////////////////

                // Product name for Quote Modal
                $('#quoteModal').on('show.bs.modal', function (event) {
                    let is_new = <?php if ($filter_new) echo 1; else echo 0; ?>;
                    $("#formResponse").html('');
                    $("#form_message").val('');
                    $("#formContainer").show();
                    $("#quoteSubmit").show();
                    if (is_new) {
                        $("#form_subject").val('<?php echo $strings[$language][402]; ?> <?php echo $printer3dDTO->getTitle(); ?>');
                    } else {
                        $("#form_subject").val('<?php echo $strings[$language][403]; ?> <?php echo $printer3dDTO->getTitle(); ?>');
                    }
                    let button = $(event.relatedTarget); // Button that triggered the modal
                    let product = button.data('product'); // Extract info from data-* attributes
                    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                    let modal = $(this)
                    if (is_new) {
                        modal.find('.modal-title').text('<?php echo $strings[$language][400]; ?> ' + product)
                    } else {
                        modal.find('.modal-title').text('<?php echo $strings[$language][401]; ?> ' + product)
                    }
                    //modal.find('.modal-body input').val(product)
                });

                //////////////////////////////////////////////////////////////////////////////////
                // AJAX: SENDING EMAIL ///////////////////////////////////////////////////////////
                $("#quoteForm").on('submit', function(e) {
                    // Prevent default submit action.
                    e.preventDefault();

                    // Check if privacy has been accepted.
                    if(!$("#quoteModal .privacy > input").is(":checked")) {
                        $('#privacy-error').modal('show');
                        return;
                    }

                    // Disable submit button.
                    $('#quoteSubmit').attr("disabled", true);
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $rootpath; ?>lib/email-contact.php",
                        data: $(this).serialize(),
                        success: function(response){
                            if (response != 'success') {
                                $('#quoteSubmit').attr("disabled", false);
                                //$("#formResponse").html(response);
                                $("#formResponse").fadeOut(0,function(){
                                    $(this).html(response).fadeIn();
                                });
                            } else {
                                $("#formResponse").html('<div class="alert alert-success alert-dismissible form-notification" role="alert">'+
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                                    '<?php echo $strings[$language][411]; ?></div>'
                                    //+
                                    //'<script>$(".form-notification").fadeTo(2000, 500).fadeOut(400, function(){$(".form-notification").alert("close");$("#quoteSubmit").removeAttr("disabled");});<\/script>'
                                );
                                $("#formContainer").fadeOut();
                                $('#quoteSubmit').attr("disabled", false);
                                $("#quoteSubmit").fadeOut();
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            $("#formResponse").html('<div class="alert alert-danger alert-dismissible form-notification" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php echo $strings[$language][412]; ?></div>'
                                //+
                                //'<script>$(".form-notification").fadeTo(2000, 500).fadeOut(400, function(){$(".form-notification").alert("close");});<\/script>'
                            );
                            $('#quoteSubmit').attr("disabled", false);
                            console.log(textStatus, errorThrown);
                        }
                    });
                });
                // END OF AJAX: SENDING EMAIL ////////////////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////

            // END OF QUOTATION REQUEST MODAL /////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
        </script>


        <!-- AUTOSIZE JQUERY PLUGIN FOR TEXTAREAS -->
        <script src="<?php echo $rootpath; ?>js/autosize.min.js"></script>
        <script>
            autosize(document.querySelectorAll('textarea'));
        </script>

<?php
include_once($rootpath . 'inc/foot.php');
?>