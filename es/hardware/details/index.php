<?php

$rootpath = '../../../';

// Get connection to database.
include $rootpath . 'admin/scripts/connect_front.php';

// Set page title.
$page_title         = (isset($_GET["product"]) && $_GET["product"] != "") ? $_GET["product"] : false;

include_once $rootpath . 'lib/lang.php';

include_once($rootpath . 'inc/head.php');

// Retrieve printer from database.
$printerDAO         = new PrinterDAO();
$fileDAO            = new FileDAO();
$applicationDAO     = new ApplicationDAO();
$videoDAO           = new VideoDAO();

$printerId          = (isset($_GET["id"]) && $_GET["id"] != "") ? $_GET["id"] : false;
$printerDTO         = $printerDAO->getPrinterById($printerId); // printer DTO
$videoDTO           = $videoDAO->getVideosByPrinterId($printerId)[0]; // printer video DTO (cogemos solo el 1º)
$is_new             = $printerDTO->getIsNew();

// Page title
$section_name       = $is_new
    ? $strings[$language][101]." ".($printerDTO->getBrandId() == 0 ? 'EFI' : 'LIYU')
    : $strings[$language][102];

// Getting needed info related to current printer.
$arrayImagesFileDTO         = $fileDAO->getImagesByPrinterId($printerId);
$arrayDocumentsFileDTO      = $fileDAO->getDocumentsByPrinterId($printerId);
$arrayApplicationDTO        = $applicationDAO->getAppsArrayByPrinterId($printerId);

// Getting Translations for Printer Info
$printerTransDTO            = $printerDTO->getPrinterTransDTO();

?>
    <!DOCTYPE html>
    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-86281163-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments)};
        gtag('js', new Date());

        gtag('config', 'UA-86281163-3');
    </script>


    <script>document.body.className += ' fade-out';</script>

    <!-- Fancybox 3 CSS -->
    <link rel="stylesheet" href="<?php echo $rootpath; ?>css/jquery.fancybox.min.css">

    <!-- WRAPPER -->
    <div class="wrapper">

        <div class="container">
            <ol class="breadcrumb bg-light text-small">
                <li class="breadcrumb-item"><a href="<?php echo $rootpath . $language . '/'; ?>"><?php echo $strings[$language][100]; ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo $rootpath . $language . '/'; ?>hardware<?php if (!$is_new) echo '/?section=outlet'; ?>"><?php echo $section_name; ?></a></li>
                <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
            </ol>
        </div>

        <div class="container">
            <!-- MAIN CONTENT -->
            <div class="main rounded-top border border-light">

                <div class="row">

                    <div class="col-lg-4 text-right">
                        <h2 class="mt-4 mb-0 mb-md-3 text-small-sm" style="font-weight:bold;"><?php if ($language != 'es' && !empty($printerTransDTO)) echo $printerTransDTO->getTitle(); else echo $printerDTO->getTitle(); ?><br>
                            <small class="text-muted"><?php if ($language != 'es' && !empty($printerTransDTO)) echo $printerTransDTO->getHeadline(); else echo $printerDTO->getHeadline(); ?></small></h2>

                        <!-- ONLY lg and more (TITLE, HEADLINE, ICONS and QUOTE BUTTON) -->
                        <div class="d-none d-lg-block">
                            <div class="product-icons text-secondary mt-4 d-flex justify-content-around align-items-center">
                                <div class="text-center" style="display: grid"><img src="<?php echo $rootpath; ?>img/icon_dropsize.png" alt="Tamaño gota" data-toggle="tooltip" data-placement="top" title="<?php echo $strings[$language][800]; ?>"><?php echo number_format($printerDTO->getFeatDropSize(), 0); ?>pl</div>
                                <div class="text-center"><img src="<?php echo $rootpath; ?>img/icon_resolution.png" alt="Resolución" data-toggle="tooltip" data-placement="top" title="<?php echo $strings[$language][801]; ?>"> <?php echo $printerDTO->getFeatResolution(); ?>dpi</div>
                                <div class="text-center"><img src="<?php echo $rootpath; ?>img/icon_maxspeed.png" alt="Velocidad máxima" data-toggle="tooltip" data-placement="top" title="<?php echo $strings[$language][802]; ?>"> <?php echo $printerDTO->getFeatMaxSpeed(); ?>m²/hr</div>
                                <?php if ($printerDTO->getFeatInkId()==0) { ?>
                                    <div class="text-center"><img src="<?php echo $rootpath; ?>img/icon_led.png" alt="Tinta UV Led" data-toggle="tooltip" data-placement="top" title="<?php echo $strings[$language][803]; ?>"></div>
                                <?php } ?>
                            </div>
                            <?php
                            // if Printer is sold (only Outlet) we disable "quote" button change label
                            if ($printerDTO->isSold()) {
                                ?>
                                <button type="button" class="btn btn-outline-success btn-lg my-4" data-toggle="modal" data-target="#quoteModal" data-product="<?php echo $printerDTO->getTitle(); ?>" disabled><?php echo $strings[$language][312]; ?></button>
                                <?php
                            } else {
                                ?>
                                <button type="button" class="btn btn-outline-success btn-lg my-4" data-toggle="modal" data-target="#quoteModal" data-product="<?php echo $printerDTO->getTitle(); ?>"><?php echo $strings[$language][804]; ?></button>
                                <?php
                            }
                            ?>
                        </div>
                        <!-- ONLY lg and more END -->

                    </div>

                    <div class="col-lg-8">

                        <div class="card details-slider border-white my-3 my-sm-0">
                            <!-- Swiper -->
                            <div class="swiper-container swiper-printer-details" id="pics">
                                <div class="swiper-wrapper">
                                    <?php
                                    if (!empty($arrayImagesFileDTO)) {
                                        foreach ($arrayImagesFileDTO as $key => $imageFileDTO) {
                                            ?>
                                            <div class="swiper-slide swiper-printer" style="background:url('<?php echo $rootpath.$imageFileDTO->getPath().$imageFileDTO->getName(); ?>');"><a href="<?php echo $rootpath.$imageFileDTO->getPath().$imageFileDTO->getName(); ?>" data-fancybox="group" class="fancybox badge badge-info float-right d-none"><i class="fa fa-search-plus" aria-hidden="true"></i> <?php echo $strings[$language][827]; ?></a></div> <!-- data-toggle="lightbox" data-gallery="printer-gallery" -->
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <!-- Add Pagination -->
                                <div class="swiper-pagination"></div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                        </div>

                        <!-- ONLY md and less (TITLE, HEADLINE, ICONS and QUOTE BUTTON) -->
                        <div class="d-lg-none text-right">
                            <div class="product-icons text-secondary mt-sm-4 mt-lg-0 d-flex justify-content-around">
                                <div><img src="<?php echo $rootpath; ?>img/icon_dropsize.png" alt="Tamaño gota" data-toggle="tooltip" data-placement="top" title="<?php echo $strings[$language][800]; ?>"> <b><?php echo number_format($printerDTO->getFeatDropSize(), 0); ?> pl</b></div>
                                <div><img src="<?php echo $rootpath; ?>img/icon_resolution.png" alt="Resolución" data-toggle="tooltip" data-placement="top" title="<?php echo $strings[$language][801]; ?>"> <b><?php echo $printerDTO->getFeatResolution(); ?> dpi</b></div>
                                <div><img src="<?php echo $rootpath; ?>img/icon_maxspeed.png" alt="Velocidad máxima" data-toggle="tooltip" data-placement="top" title="<?php echo $strings[$language][802]; ?>"> <b><?php echo $printerDTO->getFeatMaxSpeed(); ?> m²/hr</b></div>
                                <?php if ($printerDTO->getFeatInkId()==0) { ?> <!-- LED ICON (MOBILE AND TABLET) -->
                                    <div class="d-lg-none"><img src="<?php echo $rootpath; ?>img/icon_led.png" alt="Tinta UV Led" data-toggle="tooltip" data-placement="top" title="<?php echo $strings[$language][803]; ?>"></div>
                                <?php } ?>
                            </div>
                            <?php
                            // if Printer is sold (only Outlet) we disable "quote" button change label
                            if ($printerDTO->isSold()) {
                                ?>
                                <button type="button" class="btn btn-outline-success btn-lg my-4" data-toggle="modal" data-target="#quoteModal" data-product="<?php echo $printerDTO->getTitle(); ?>" disabled><?php echo $strings[$language][312]; ?></button>
                                <?php
                            } else {
                                ?>
                                <button type="button" class="btn btn-outline-success btn-lg my-4" data-toggle="modal" data-target="#quoteModal" data-product="<?php echo $printerDTO->getTitle(); ?>"><?php echo $strings[$language][804]; ?></button>
                                <?php
                            }
                            ?>
                        </div>
                        <!-- ONLY md and less END -->

                    </div>

                </div>
            </div>
            <div class="main bg-light">
                <div class="row my-2">

                    <?php
                    if (!empty($videoDTO)) {
                    ?>

                    <!-- VIDEO COLUMN -->
                    <div class="col-12 col-lg-4 mb-3 mb-md-0 align-self-center">

                        <!-- videoWrapper -->
                        <div class="videoWrapper" data-v="<?php echo $videoDTO->getYoutubeId(); ?>">
                            <iframe id="youtube-video-0" width="560" height="315"
                                    src="https://www.youtube.com/embed/<?php echo $videoDTO->getYoutubeId(); ?>?enablejsapi=1&version=3&playerapiid=ytplayer&origin=https://www.talawin.com&autoplay=1&showinfo=0&rel=0&cc_load_policy=0"
                                    frameborder="0" allowscriptaccess="always" allowfullscreen="true" allow="autoplay" autoplay="1"></iframe>
                        </div>

                    </div>

                    <div class="col-12 col-lg-8">

                        <?php
                        } else {
                        ?>

                        <div class="col-12">

                            <?php
                            }
                            ?>


                            <p class="text-muted"><b><?php echo $strings[$language][805]; ?></b></p>

                            <?php if ($language != 'es' && !empty($printerTransDTO)) echo $printerTransDTO->getDescription(); else echo $printerDTO->getDescription(); ?></div>

                        <!--       <div class="col-12 col-md-4 mt-3 mt-md-0">

        <p class="text-muted"><b>Documentación</b></p>

        <div class="list-group list-documents">
          <?php
                        if (!empty($arrayDocumentsFileDTO)) {
                            foreach ($arrayDocumentsFileDTO as $key => $documentFileDTO) {
                                ?>
            <a href="<?php echo $rootpath.$documentFileDTO->getPath().$documentFileDTO->getName(); ?>" target="_blank" class="list-group-item list-group-item-action list-group-item-light"><small><i class="fa fa-file-pdf-o pr-1" aria-hidden="true"></i> <?php echo $documentFileDTO->getName(); ?></small></a>
          <?php
                            }
                        }
                        ?>
        </div>

      </div> -->

                    </div>
                </div>
                <div class="main border border-light rounded-bottom">
                    <div class="row my-2"> <!-- no-gutters -->

                        <div class="col-12"> <!-- col-lg-3 -->
                            <div class="nav flex-row nav-pills nav-justified border border-info border-top-0 border-left-0 border-right-0" id="v-pills-tab" role="tablist"> <!-- flex-lg-column -->
                                <a class="nav-link nav-item active myPill" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-expanded="true"><i class="fa fa-info" aria-hidden="true"></i> <span class="d-none d-md-block d-lg-inline"><?php echo $strings[$language][806]; ?></span></a>
                                <a class="nav-link nav-item myPill" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-expanded="true"><i class="fa fa-cogs" aria-hidden="true"></i> <span class="d-none d-md-block d-lg-inline"><?php echo $strings[$language][807]; ?></span></a>
                                <a class="nav-link nav-item myPill" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-expanded="true"><i class="fa fa-tint" aria-hidden="true"></i> <span class="d-none d-md-block d-lg-inline"><?php echo $strings[$language][808]; ?></span></a>
                                <a class="nav-link nav-item myPill" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-expanded="true"><i class="fa fa-building" aria-hidden="true"></i> <span class="d-none d-md-block d-lg-inline"><?php echo $strings[$language][809]; ?></span></a>
                                <a class="nav-link nav-item myPill" id="v-pills-documentation-tab" data-toggle="pill" href="#v-pills-documentation" role="tab" aria-controls="v-pills-documentation" aria-expanded="true"><i class="fa fa-book" aria-hidden="true"></i> <span class="d-none d-md-block d-lg-inline"><?php echo $strings[$language][810]; ?></span></a>
                            </div>
                        </div>
                        <div class="col-12 mt-3"> <!-- col-lg-9 mt-lg-0 -->
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                    <h4 class="d-md-none text-info" style="font-weight:500;"><?php echo $strings[$language][806]; ?></h4>
                                    <?php if ($language != 'es' && !empty($printerTransDTO)) echo $printerTransDTO->getHtmlOverview(); else echo $printerDTO->getHtmlOverview(); ?>
                                </div>
                                <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                    <h4 class="d-md-none text-info" style="font-weight:500;"><?php echo $strings[$language][807]; ?></h4>
                                    <?php if ($language != 'es' && !empty($printerTransDTO)) echo $printerTransDTO->getHtmlFeatures(); else echo $printerDTO->getHtmlFeatures(); ?>
                                </div>
                                <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                    <h4 class="d-md-none text-info" style="font-weight:500;"><?php echo $strings[$language][808]; ?></h4>
                                    <?php if ($language != 'es' && !empty($printerTransDTO)) echo $printerTransDTO->getHtmlSpecs(); else echo $printerDTO->getHtmlSpecs(); ?>
                                </div>
                                <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                    <h4 class="d-md-none text-info" style="font-weight:500;"><?php echo $strings[$language][809]; ?></h4>
                                    <?php
                                    if (!empty($arrayApplicationDTO)) {
                                        ?>
                                        <div class="row">
                                            <?php
                                            foreach ($arrayApplicationDTO as $key => $applicationDTO) {
                                                ?>
                                                <div class="col-6 col-lg-4 mb-4">
                                                    <div class="card">
                                                        <img class="card-img-top" src="<?php echo $rootpath.'admin/img/apps/'.$applicationDTO->getId().'/'.$applicationDTO->getFileNamePrefix().'00.jpg'; ?>" alt="<?php echo $applicationDTO->getLabelEs(); ?>">
                                                        <div class="py-1">
                                                            <p class="card-text text-center"><small style="font-weight:500;"><?php echo call_user_func(array($applicationDTO, 'getLabel'.ucwords($language))); ?></small></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="tab-pane fade" id="v-pills-documentation" role="tabpanel" aria-labelledby="v-pills-documentation-tab">
                                    <h4 class="d-md-none text-info" style="font-weight:500;"><?php echo $strings[$language][810]; ?></h4>
                                    <div class="list-group list-documents">
                                        <?php
                                        if (!empty($arrayDocumentsFileDTO)) {
                                            foreach ($arrayDocumentsFileDTO as $key => $documentFileDTO) {
                                                ?>
                                                <a href="<?php echo $rootpath.$documentFileDTO->getPath().$documentFileDTO->getName(); ?>" target="_blank" class="list-group-item list-group-item-action list-group-item-light"><i class="fa fa-file-pdf-o pr-1" aria-hidden="true"></i> <?php echo $documentFileDTO->getName(); ?></a>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- MAIN CONTENT end -->
            </div>

            <!-- Quote Modal -->
            <div class="modal fade" id="quoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header border-info bg-light rounded-top">
                            <h5 class="modal-title text-info" id="exampleModalLabel"><?php echo $strings[$language][804]; ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="formResponse"></div>
                            <div id="formContainer">
                                <form id="quoteForm">
                                    <div class="form-group">
                                        <label for="form_name"><?php echo $strings[$language][404]; ?></label>
                                        <input type="text" class="form-control form-control-sm" id="form_name" name="form_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="form_company"><?php echo $strings[$language][405]; ?></label>
                                        <input type="text" class="form-control form-control-sm" id="form_company" name="form_company" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="form_email"><?php echo $strings[$language][406]; ?></label>
                                        <input type="email" class="form-control form-control-sm" id="form_email" name="form_email" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="form_phone"><?php echo $strings[$language][407]; ?></label>
                                        <input type="tel" class="form-control form-control-sm" id="form_phone" name="form_phone" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="form_message"><?php echo $strings[$language][408]; ?></label>
                                        <textarea class="form-control form-control-sm" id="form_message" name="form_message" style="word-wrap: break-word; resize: vertical;"></textarea>
                                    </div>
                                    <input type="hidden" id="form_subject" name="form_subject">
                                    <!-- PRIVACY POLITIC ACCEPTANCE CHECKBOX -->
                                    <div class="privacy" style="padding-left: .8rem;"><input type="checkbox" id="cbox2" value=""/><label style="padding-left: .4rem;" for="cbox2"><?php echo $strings[$language][706]; ?></label></div>
                            </div>
                        </div>
                        <div class="modal-footer bg-light rounded-bottom">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $strings[$language][409]; ?></button>
                            <button type="submit" class="btn btn-success" id="quoteSubmit"><?php echo $strings[$language][410]; ?></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Quote Modal END -->

            <!-- MatchHeight jQuery Plugin -->
            <script src="<?php echo $rootpath; ?>js/jquery.matchHeight.js" type="text/javascript"></script>

            <!-- Fancybox 3 JS -->
            <script src="<?php echo $rootpath; ?>js/jquery.fancybox.min.js" type="text/javascript"></script>

            <!-- Swiper JS -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js"></script>
            <!-- Initialize Swiper (SLIDER HOME) -->
            <script type="text/javascript">
                var $links = $('.fancybox');
                var swiper = new Swiper('#pics', {
                    pagination: '.swiper-pagination',
                    paginationClickable: true,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    spaceBetween: 30,
                    observer: true,
                    observeParents: true,
                    autoplay: 6000,
                    autoplayDisableOnInteraction: false,
                    onClick: (swiper, event) => {
                        let element = event.target;
                        //specific element that was clicked i.e.: p or img tag
                        classString = element.className;
                        if (!(classString.includes('button')) && !(classString.includes('pagination'))) {
                            $.fancybox.open( $links, {protect : false}, swiper.activeIndex );
                        }
                    },
                    onSlideChangeEnd: (swiper) => {
                        var img_url = $('.swiper-slide-active').css('background-image');
                        img_url = img_url.replace('url(','').replace(')','');
                        $('#button-zoom').attr('href',img_url);
                    },
                });
            </script>

            <script type="text/javascript">
                // Document Ready Block.
                $(function() {

                    // ACTIVE MENU
                    var is_new = <?php echo $is_new; ?>;
                    if (is_new) {
                        $('#menu-printers').addClass('active');
                    } else {
                        $('#menu-outlet').addClass('active');
                    }

                    // Fade In + movement animation effect for images slider
                    // $('.details-slider').css({"transform": 'translate(0,-200px)'});
                    $('.details-slider').addClass('details-anim');

                    // ???
                    setTimeout(function() {
                        $('.tab-pane').matchHeight();
                    }, 200);

                    // $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                    //     event.preventDefault();
                    //     $(this).ekkoLightbox();
                    // });

                    // INIT TOOLTIPS
                    $('[data-toggle="tooltip"]').tooltip();

                });

                // Product name for Quote Modal
                $('#quoteModal').on('show.bs.modal', function (event) {
                    var is_new = <?php echo $is_new; ?>;
                    $("#formResponse").html('');
                    $("#form_message").val('');
                    $("#formContainer").show();
                    $("#quoteSubmit").show();
                    if (is_new) {
                        $("#form_subject").val('<?php echo $strings[$language][402]; ?> <?php echo $printerDTO->getTitle(); ?>');
                    } else {
                        $("#form_subject").val('<?php echo $strings[$language][403]; ?> <?php echo $printerDTO->getTitle(); ?>');
                    }
                    var button = $(event.relatedTarget) // Button that triggered the modal
                    var product = button.data('product') // Extract info from data-* attributes
                    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                    var modal = $(this)
                    if (is_new) {
                        modal.find('.modal-title').text('<?php echo $strings[$language][400]; ?> ' + product)
                    } else {
                        modal.find('.modal-title').text('<?php echo $strings[$language][401]; ?> ' + product)
                    }
                    //modal.find('.modal-body input').val(product)
                })

                // AJAX: SENDING EMAIL
                $("#quoteForm").on('submit', function(e) {
                    // Prevent default submit action.
                    e.preventDefault();

                    // Check if privacy has been accepted.
                    if(!$("#quoteModal .privacy > input").is(":checked")) {
                        $('#privacy-error').modal('show');
                        return;
                    }

                    // Disable submit button.
                    $('#quoteSubmit').attr("disabled", true);
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $rootpath; ?>lib/email-contact.php",
                        data: $(this).serialize(),
                        success: function(response){
                            if (response != 'success') {
                                $('#quoteSubmit').attr("disabled", false);
                                //$("#formResponse").html(response);
                                $("#formResponse").fadeOut(0,function(){
                                    $(this).html(response).fadeIn();
                                });
                            } else {
                                $("#formResponse").html('<div class="alert alert-success alert-dismissible form-notification" role="alert">'+
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                                    '<?php echo $strings[$language][411]; ?></div>'
                                    //+
                                    //'<script>$(".form-notification").fadeTo(2000, 500).fadeOut(400, function(){$(".form-notification").alert("close");$("#quoteSubmit").removeAttr("disabled");});<\/script>'
                                );
                                $("#formContainer").fadeOut();
                                $('#quoteSubmit').attr("disabled", false);
                                $("#quoteSubmit").fadeOut();
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            $("#formResponse").html('<div class="alert alert-danger alert-dismissible form-notification" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php echo $strings[$language][412]; ?></div>'
                                //+
                                //'<script>$(".form-notification").fadeTo(2000, 500).fadeOut(400, function(){$(".form-notification").alert("close");});<\/script>'
                            );
                            $('#quoteSubmit').attr("disabled", false);
                            console.log(textStatus, errorThrown);
                        }
                    });

                });
            </script>

            <!-- Autosize jQuery Plugin -->
            <script src="<?php echo $rootpath; ?>js/autosize.min.js"></script>
            <script>
                autosize(document.querySelectorAll('textarea'));
            </script>

<?php
include_once($rootpath . 'inc/foot.php');
?>