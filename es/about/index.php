<?php
	$rootpath = '../../';

    // Get connection to database.
    include $rootpath . 'admin/scripts/connect_front.php';

    include_once($rootpath . 'lib/lang.php');

    $page_title = $strings[$language][103];


    // Retrieve all clients from database.
    $clientLogoDAO = new ClientLogoDAO();
    $arrayClientLogoDTO = $clientLogoDAO->getClientLogos();

    // Retrieve all team members from database.
    $teamMemberDAO = new TeamMemberDAO();
    $arrayTeamMemberDTO = $teamMemberDAO->getTeamMembers();

    // Get properties: To applly custom properties saved on DDBB.
    $propertyDAO = new PropertyDAO();

    // Html header and nav-bar (providers logos array is init here).
	include_once($rootpath . 'inc/head.php');
?>
<!DOCTYPE html>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-86281163-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-86281163-3');
</script>

<!--<script>document.body.className += ' fade-out';</script>-->

	    <!-- WRAPPER -->
		<div class="wrapper">

			<!-- MAIN CONTENT -->
			<div class="container">

				<div class="row">
					<div class="col text-justify">

                        <!-- TITLE -->
						<h3><?php echo $strings[$language][500]; ?></h3>
                        <!-- INTRO -->
						<div class="container mt-4">
                            <?php echo $strings[$language][501]; ?>

                            <!-- PROVIDERS -->
                            <div class="row mb-2 mb-md-3 w-md-50 mx-auto justify-content-around align-items-center logos-fabricantes text-center">
                                <!-- PROVIDER LOGO HTML TEMPLATE
                                <div class="col m-1"><img src="logos/efi.png" class="lazy" alt="EFI"></div> -->
                                <!-- Build an html logo for each provider on database -->
                                <?php if (is_array($arrayProviderLogoDTO) && sizeof($arrayProviderLogoDTO) > 0) {
                                    // For each team member on the array we will build a card.
                                    foreach ($arrayProviderLogoDTO as $key => $providerLogoDTO) {
                                        if ($providerLogoDTO->isShown()) { ?>
                                            <div class="col m-1 mb-2" title="<?php echo $providerLogoDTO->getProviderName()?>">
                                                <?php if ($providerLogoDTO->getProviderLink() != "") { ?> <a href="<?php echo $providerLogoDTO->getProviderLink()?>" target="_blank"> <?php } ?>
                                                    <img src="<?php echo $rootpath . $providerLogoDTO->getPath() . $providerLogoDTO->getFileName(); ?>" class="lazy" alt="<?php echo $providerLogoDTO->getProviderName()?>">
                                                <?php if ($providerLogoDTO->getProviderLink() != "") { ?> </a> <?php }?></div>
                                        <?php }}
                                } ?>
                            </div>

                            <?php echo $strings[$language][513]; ?>

                        <h4 class="pt-4"><?php echo $strings[$language][504]; ?></h4>

                            <!-- TEAM MEMBERS -->
                            <div id="teamMembersGrid" class="">

                                <?php

                                // If there are any team members on database we start cards:
                                if (is_array($arrayTeamMemberDTO) && sizeof($arrayTeamMemberDTO) > 0) {
                                    // For each team member on the array we will build a card.
                                    foreach ($arrayTeamMemberDTO as $key => $teamMemberDTO) {
                                        if ($teamMemberDTO->isShown()) { ?>
                                        <div class="teamMemberItem">
                                            <img src="<?php echo $rootpath . $teamMemberDTO->getPath() . $teamMemberDTO->getFileName(); ?>" class="img-thumbnail">
                                            <br><b><?php echo $teamMemberDTO->getMemberName(); ?></b>
                                            <br><i><?php echo ($language == 'es' ? $teamMemberDTO->getMemberRole() : $teamMemberDTO->getMemberRoleEn()) ?></i>
                                        </div>
                                    <?php }}
                                } ?>


                                <!-- <div class="col-6 col-sm-4 col-lg-2 mb-3 mb-lg-0">
                                    <img src="team/chady.jpg" class="img-thumbnail">
                                    <br><b>Chady Peter</b>
                                    <br><i><?php echo $strings[$language][505]; ?></i>
                                </div>
                                <div class="col-6 col-sm-4 col-lg-2 mb-3 mb-lg-0">
                                    <img src="team/esmeralda.jpg" class="img-thumbnail">
                                    <br><b>Esmeralda del Rey</b>
                                    <br><i><?php echo $strings[$language][507]; ?></i>
                                </div>
                                <div class="col-6 col-sm-4 col-lg-2 mb-3 mb-lg-0">
                                    <img src="team/antonio.jpg" class="img-thumbnail">
                                    <br><b>Antonio Medina</b>
                                    <br><i><?php echo $strings[$language][506]; ?></i>
                                </div>
                                <div class="col-6 col-sm-4 col-lg-2 mb-3 mb-lg-0">
                                    <img src="team/florin.jpg" class="img-thumbnail">
                                    <br><b>Florin Oprea</b>
                                    <br><i><?php echo $strings[$language][508]; ?></i>
                                </div>
                                <div class="col-6 col-sm-4 col-lg-2 mb-3 mb-lg-0">
                                    <img src="team/lucia.jpg" class="img-thumbnail">
                                    <br><b>Lucía Cobos</b>
                                    <br><i><?php echo $strings[$language][509]; ?></i>
                                </div>
                                <div class="col-6 col-sm-4 col-lg-2 mb-3 mb-lg-0">
                                    <img src="team/jose.jpg" class="img-thumbnail">
                                    <br><b>Jose Manuel Fueyo</b>
                                    <br><i><?php echo $strings[$language][510]; ?></i>
                                </div> -->
                            </div>

						<h4 class="pt-4"><?php echo $strings[$language][502]; ?></h4>

                            <?php echo $strings[$language][503]; ?>


                            <!-- CLIENT LOGOS CAROUSEL -->
                            <?php

                            // If there are any client logos on database we start cards:
                            if (is_array($arrayClientLogoDTO) && sizeof($arrayClientLogoDTO) > 0) {

                                ?>

                                <div class="owl-carousel owl-theme">

                                    <?php

                                    // We create a row for each ClientLogoDTO contained on the array.
                                    foreach ($arrayClientLogoDTO as $key => $clientLogoDTO) {

                                        ?>
                                        <img
                                                src="<?php echo $rootpath . $clientLogoDTO->getPath() . $clientLogoDTO->getFileName(); ?>"
                                                data-original="<?php echo $rootpath . $clientLogoDTO->getPath() . $clientLogoDTO->getFileName(); ?>"
                                                class="lazy"
                                                alt="<?php echo $clientLogoDTO->getClientName(); ?>">
                                        <?php

                                    }

                                    ?>

                                </div>

                                <?php

                            }

                            ?>
                            <!-- Client Logos Carousel END -->


						</div> <!-- container end -->

                    </div> <!-- container end -->

				</div> <!-- row end -->

			</div> <!-- wrapper end -->
			<!-- MAIN CONTENT end -->

	<!-- MatchHeight jQuery Plugin -->
  	<script src="<?php echo $rootpath; ?>js/jquery.matchHeight.js" type="text/javascript"></script>

	<!-- Lazy Load jQuery Plugin -->
	<script src="<?php echo $rootpath; ?>js/jquery.lazyload.js" type="text/javascript"></script>

	<script type="text/javascript" charset="utf-8">
		$(function() {
		  
		 // Lazy Load for Logo Images
		 $("img.lazy").lazyload({
		   effect : "fadeIn"
		 });

		});
	</script>

    <script type="text/javascript">
	    $(function () {

	    	// About Menu Active
	    	$('#menu-about').addClass('active');

            // Equals all card heights.
	    	setTimeout(function() {$('.card').matchHeight()}, 200);

	    	// APPLY CUSTOM CSS PROPERTIES SAVED ON PROPERTIES TABLE ON DDBB
            $(".teamMemberItem").each(function(index) { this.style.setProperty('--member-width', "<?php echo bcdiv(100, intval($propertyDAO->getPropertyValueByName(PropertyDAO::LAPTOP_TEAM_MEMBERS_ITEM_PER_ROW)), 4) ?>%");})
            $(".teamMemberItem").each(function(index) { this.style.setProperty('--member-width-tab', "<?php echo bcdiv(100, intval($propertyDAO->getPropertyValueByName(PropertyDAO::TAB_TEAM_MEMBERS_ITEM_PER_ROW)), 4)?>%");})
            $(".teamMemberItem").each(function(index) { this.style.setProperty('--member-width-mob', "<?php echo bcdiv(100, intval($propertyDAO->getPropertyValueByName(PropertyDAO::MOBILE_TEAM_MEMBERS_ITEM_PER_ROW)), 4) ?>%");})

            // CLIENT LOGOS: Init Owl Carousel
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:40,
                autoplay:true,
                autoplayTimeout:7500,
                responsive:{
                    0:      {items:3},
                    576:    {items:5},
                    768:    {items:6},
                    992:    {items:7}
                }
            })
		});
    </script>

	<!-- Swiper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>

	<!-- Initialize Swiper (Logos slider) -->
            <script>
                var swiper = new Swiper ('.swiper-container', {
                    // Optional parameters
                    direction: 'vertical',
                    loop: true,

                    // If we need pagination
                    pagination: {
                        el: '.swiper-pagination',
                    },

                    // Navigation arrows
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },

                    // And if we need scrollbar
                    scrollbar: {
                        el: '.swiper-scrollbar',
                    },
                })
            </script>
<!--    <script>-->
<!--    var swiper = new Swiper('.swiper-container', {-->
<!--        pagination: '.swiper-pagination',-->
<!--        slidesPerView: 5,-->
<!--        slidesPerGroup: 1,-->
<!--        paginationClickable: true,-->
<!--        spaceBetween: 30,-->
<!--        autoplay: 2000,-->
<!--	    autoplayDisableOnInteraction: false,-->
<!--	    nextButton: '.swiper-button-next',-->
<!--	    prevButton: '.swiper-button-prev',-->
<!--	    preloadImages: false,-->
<!--	    lazyLoading: true,-->
<!--	    lazyLoadingInPrevNext: true,-->
<!--	    grabCursor: true-->
<!--    });-->
<!--    </script>-->

<?php
	include_once($rootpath . 'inc/foot.php');
?>