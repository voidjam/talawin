<!DOCTYPE html>
<?php
$rootpath = '../../';
$page_title = 'Contacto';

// Get connection to database.
include $rootpath . 'admin/scripts/connect_front.php';

include_once($rootpath . 'lib/lang.php');

$page_title = $strings[$language][104];

// Html header and nav-bar (providers logos array is init here).
include_once($rootpath . 'inc/head.php');

$mailStrings = array(
    ///////////////////////////////////////////////////////////////////////
    // SPANISH ////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    'es' => array(
        01   => '<strong>¡Hecho!</strong> Hemos enviado su email. Le contestaremos lo antes posible.',
        02   => '<strong>¡Error!</strong> Su email no ha podido ser enviado. Por favor, intentelo de nuevo más tarde.'
    ),
    ///////////////////////////////////////////////////////////////////////
    // ENGLISH ////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    'en' => array(
        01   => '<strong>Done!</strong> Your email has been sent. We will answer you as soon as possible.',
        02   => '<strong>Error!</strong> Your email could not be sent. Please try again later.'
    )
);
?>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-86281163-3"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments)};
    gtag('js', new Date());

    gtag('config', 'UA-86281163-3');
</script>


<script>document.body.className += ' fade-out';</script>

<!-- WRAPPER -->
<div class="wrapper">

    <!-- MAIN CONTENT -->
    <div class="container contact-section">

        <div class="row">
            <div class="col-12 col-lg-6 addresses">

                <div class="card bg-light border-info mb-4">
                    <div class="card-body d-flex flex-wrap justify-content-around p-3">
                        <div class="d-flex align-items-center pr-2 py-1"><i class="fa fa-phone pr-2" aria-hidden="true"></i> <a href="tel:+34919917143"><strong>+34 919 917 143</strong></a></div>
                        <div class="d-flex align-items-center py-1"><i class="far fa-envelope pr-2"></i> <a href="mailto:info@talawin.com"><strong>info@talawin.com</strong></a></div>
                    </div>
                </div>

                <h6><?php echo $strings[$language][600]; ?></h6>

                <div class="container mb-4">

                    <div class="row">
                        <div class="col-12">
                            <div class="row align-items-center">
                                <div class="col-1 text-center">
                                    <i class="fas fa-map-marker-alt"></i>
                                </div>
                                <div class="col">
                                    <adress><?php echo $strings[$language][601]; ?></adress>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="map" class="contact-map contact-map-big rounded mt-2"></div>

                </div>

            </div>
            <div class="col-12 col-lg-6 mt-5 mt-lg-0">

                <h3 class="text-center"><?php echo $strings[$language][602]; ?></h3>

                <div class="container mt-3">
                    <div id="formResponse"></div>
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- CONTACT FORM ------------------------------------------------------------------------------ -->
                    <form id="contactForm">
                        <div class="form-group">
                            <label for="form_name"><?php echo $strings[$language][603]; ?></label>
                            <input type="text" class="form-control" id="form_name" name="form_name" required>
                        </div>
                        <div class="form-group">
                            <label for="form_company"><?php echo $strings[$language][604]; ?></label>
                            <input type="text" class="form-control" id="form_company" name="form_company" required>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="form_email"><?php echo $strings[$language][605]; ?></label>
                                <input type="email" class="form-control" id="form_email" name="form_email" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="form_phone"><?php echo $strings[$language][606]; ?></label>
                                <input type="tel" class="form-control" id="form_phone" name="form_phone" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="form_message"><?php echo $strings[$language][607]; ?></label>
                            <textarea class="form-control" id="form_message" name="form_message" rows="6" style="word-wrap: break-word; resize: vertical;" required></textarea>
                        </div>
                        <input type="hidden" id="form_subject" name="form_subject">
                        <!-- PRIVACY POLICE ACCEPTANCE CHECKBOX -->
                        <div class="privacy"><label for="contactMailPrivacy"><?php echo $strings[$language][706]; ?></label><input type="checkbox" id="contactMailPrivacy" value=""/></div>
                        <div class="form-group">
                            <button type="submit" id="formSubmit" class="btn btn-info btn-block btn-lg"><i class="far fa-paper-plane"></i> <?php echo $strings[$language][608]; ?></button>
                        </div>
                    </form>
                    <!-- END OF CONTACT FORM ----------------------------------------------------------------------- -->
                    <!-- ------------------------------------------------------------------------------------------- -->

                    <button type="button" id="formNew" class="btn btn-outline-info btn-block btn-lg" style="display:none;"><?php echo $strings[$language][609]; ?></button>
                </div>

            </div>

        </div>

        <!-- MAILCHIMP SIGNUP FORM -->
        <div class="row">
            <div class="mx-auto mt-5 px-4">

                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup">
                    <form action="https://talawin.us18.list-manage.com/subscribe/post?u=ccbb912f0816915f1809ece54&amp;id=d58696c51c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="mc-embedded-subscribe-form validate form-inline" target="_blank" novalidate>
                        <div class="d-inline" id="mc_embed_signup_scroll">

                            <span class="h4 mr-4" style="vertical-align: sub;"><i class="far fa-newspaper"></i> <?php echo $strings[$language][700]; ?></span>

                            <br class="d-md-none">
                            <br class="d-md-none">

                            <input type="text" value="" name="FNAME" class="form-control required mr-1 d-inline col-5" id="mce-FNAME" placeholder="<?php echo $strings[$language][701]; ?>">

                            <input type="email" value="" name="EMAIL" class="form-control required email d-inline col-5" id="mce-EMAIL" placeholder="<?php echo $strings[$language][702]; ?>">

                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->

                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_ccbb912f0816915f1809ece54_dd3bded374" tabindex="-1" value=""></div>

                            <br class="d-md-none">

                            <div class="clear mt-2 mt-md-none"><input type="submit" value="<?php echo $strings[$language][703]; ?>" name="subscribe" id="mc-embedded-subscribe" class="btn btn-success button-mailchimp"></div>

                        </div>
                    </form>
                    <!-- PRIVACY POLICE ACCEPTANCE CHECKBOX -->
                    <div id="contactNewsPrivacy" class="privacy"><label for="cbox2"><?php echo $strings[$language][706]; ?></label><input type="checkbox" id="cbox2" value=""/></div>
                </div>
                <!--End mc_embed_signup-->

            </div>
        </div>
        <!-- MAILCHIMP SIGNUP FORM end -->

    </div>
    <!-- MAIN CONTENT end -->

    <script type="text/javascript">

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // GOOGLE MAPS /////////////////////////////////////////////////////////////////////////////////////////////////
        let styles = [
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry",
                    "stylers": [{"color": "#f7f1df"}]
                },
                {
                    "featureType": "landscape.natural",
                    "elementType": "geometry",
                    "stylers": [{"color": "#d0e3b4"}]
                },
                {
                    "featureType": "landscape.natural.terrain",
                    "elementType": "geometry",
                    "stylers": [{"visibility": "on"}]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels",
                    "stylers": [{"visibility": "on"}]
                },
                {
                    "featureType": "poi.business",
                    "elementType": "all",
                    "stylers": [{"visibility": "on"}]
                },
                {
                    "featureType": "poi.medical",
                    "elementType": "geometry",
                    "stylers": [{"color": "#fbd3da"}]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [{"color": "#bde6ab"}]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.stroke",
                    "stylers": [{"visibility": "off"}]
                },
                {
                    "featureType": "road",
                    "elementType": "labels",
                    "stylers": [{"visibility": "on"}]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [{"color": "#ffe15f"}]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [{"color": "#efd151"}]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.fill",
                    "stylers": [{"color": "#ffffff"}]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry.fill",
                    "stylers": [{"color": "black"}]
                },
                {
                    "featureType": "transit.station.airport",
                    "elementType": "geometry.fill",
                    "stylers": [{"color": "#cfb2db"}]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{"color": "#a2daf2"}]
                }
            ];

        let div = document.getElementById('map');

        window.initMap = function() {

            let myLatlng = {lat: 40.2616344, lng: -3.8389};
            let center = {lat: 40.2616344, lng: -3.8389};

            let map = new google.maps.Map(div, {
                center: center,
                zoom: 15,
                styles
            });

            let marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                animation: google.maps.Animation.DROP,
                title: 'Sede y Sala Demo TALAWIN',
                url: "https://goo.gl/maps/eVhdAdYs7762"
            });

            google.maps.event.addListener(map, 'click', function () {window.open(marker.url);});
        };
        // END OF GOOGLE MAPS //////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        // NEW CONTACT FORM:
        $("#formNew").on('click', function() {
            $("#formResponse").html('');
            $("#form_message").val('');
            $("#contactForm").fadeIn();
            $("#formNew").fadeOut();
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // AJAX: SENDING CONTACT FORM BY EMAIL /////////////////////////////////////////////////////////////////////////
        $("#contactForm").on('submit', function(e) {
            e.preventDefault();

            // Check if privacy police is accepted before sending MailChimp subscription.
            if(!$("#contactMailPrivacy").is(":checked")) {
                $('#privacy-error').modal('show');
                $("#contactForm > .privacy > label").css("color", "red");
                return;
            }

            $("#form_subject").val('Solicitud información vía Web Talawin');
            navbar_height = $('.navbar').outerHeight();

            $('#formSubmit').attr("disabled", true);
            $.ajax({
                type: "POST",
                url: "<?php echo $rootpath; ?>lib/email-contact.php",
                data: $(this).serialize(),
                success: function(response){
                    if (response != 'success') {
                        $('#formSubmit').attr("disabled", false);
                        //$("#formResponse").html(response);
                        $('html, body').animate({
                            scrollTop: $("#formResponse").offset().top-navbar_height
                        });
                        $("#formResponse").fadeOut(0,function(){
                            $(this).html(response).fadeIn();
                        });
                    } else {
                        $('html, body').animate({
                            scrollTop: $("#formResponse").offset().top-navbar_height
                        });
                        $("#formResponse").html('<div class="alert alert-success alert-dismissible form-notification" role="alert">'+
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                            '<?php echo $mailStrings[$language][01]; ?></div>'
                            //+
                            //'<script>$(".form-notification").fadeTo(2000, 500).fadeOut(400, function(){$(".form-notification").alert("close");$("#formSubmit").removeAttr("disabled");});<\/script>'
                        );
                        $("#contactForm").fadeOut();
                        $('#formSubmit').attr("disabled", false);
                        $("#formNew").fadeIn();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#formResponse").html('<div class="alert alert-danger alert-dismissible form-notification" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php echo $mailStrings[$language][02]; ?></div>'
                        //+
                        //'<script>$(".form-notification").fadeTo(2000, 500).fadeOut(400, function(){$(".form-notification").alert("close");});<\/script>'
                    );
                    $('#formSubmit').attr("disabled", false);
                    console.log(textStatus, errorThrown);
                }
            });

        });
        // END OF AJAX: SENDING CONTACT FORM BY EMAIL //////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxBf4lKZYyQEDa8WacoqaxYPnmwhrVP0k&callback=initMap"></script>

    <!-- Autosize jQuery Plugin -->
    <script src="<?php echo $rootpath; ?>js/autosize.min.js"></script>
    <script>
        autosize(document.querySelectorAll('textarea'));
    </script>

    <script type="text/javascript">

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // DOCUMENT READY BLOCK ////////////////////////////////////////////////////////////////////////////////////////
        $(function () {

            $('#menu-contact').addClass('active');

            // Check if privacy police is accepted before sending MailChimp subscription.
            $("#mc-embedded-subscribe-form").on("submit", function(e) {
                // Check if privacy has been accepted.
                if(!$("#mc_embed_signup #contactNewsPrivacy > input").is(":checked")) {
                    e.preventDefault();
                    $('#privacy-error').modal('show');
                }
            });
        });
        // END OF DOCUMENT READY BLOCK /////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    </script>

<?php
include_once($rootpath . 'inc/foot.php');
?>