<?php
$rootpath = '../';

// Get connection to database.
include $rootpath . 'admin/scripts/connect_front.php';

// Include language related vars.
include_once $rootpath.'lib/lang.php';
// Set page title.
$page_title = $strings[$language][100];

// Include headers and nav-bar.
include_once($rootpath.'inc/head.php');


// DATABASE QUERIES: Get all needed info from database to build the page.

// HOME BANNERS: Retrieve all Home Banners from database
$homeBannerDAO = new HomeBannerDAO();
$arrayHomeBannerDTO = $homeBannerDAO->getDisplayedHomeBanners();
// In case all banners has is_shown == false we get all of them.
if ($arrayHomeBannerDTO == 1) $arrayHomeBannerDTO = $homeBannerDAO->getHomeBanners();

// HOME VIDEOS:
// Retrieve all Home Videos from database.
$homeVideosDAO = new HomeVideoDAO();
$arrayHomeVideosDTO = $homeVideosDAO->getHomeVideos();

// RELEASES (NOVEDADES).
// Retrieve all NEW printers (IS RELEASED) from database.
$printerDAO = new PrinterDAO();
$printer3dDAO = new Printer3dDAO();
$fileDAO = new FileDAO();
//$arrayNewPrintersDTO = array_slice(array_merge($printer3dDAO->getReleasedPrinters3d(), $printerDAO->getReleasedPrinters()), 0);  // Removed 3D printers.
$arrayNewPrintersDTO = array_slice($printerDAO->getReleasedPrinters(), 0);

?>
    <!-- ----------------------------------------------------------------------------------------------------------- -->
    <!-- GOOGLE ANALYTICS: Global Site Tag (gtag.js) - Google Analytics -------------------------------------------- -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-86281163-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments)};
        gtag('js', new Date());
        gtag('config', 'UA-86281163-3');
    </script>
    <!-- ----------------------------------------------------------------------------------------------------------- -->


    <!-- A clean general fade-in effect. -->
    <script>document.body.className += ' fade-out';</script>



    <!-- ----------------------------------------------------------------------------------------------------------- -->
    <!-- HOME: BANNERS SLIDER -------------------------------------------------------------------------------------- -->
    <div class="homebanners">
        <?php
        // Here we create the home banners slider.
        if (is_array($arrayHomeBannerDTO) && sizeof($arrayHomeBannerDTO) > 0) {  ?>

            <div class="carousel-home swiper-container">
                <div class="swiper-wrapper swiper-wrapper-home">

                    <?php
                    // We create a Swiper Slide for each HomeBannerDTO contained on the array.
                    foreach ($arrayHomeBannerDTO as $key => $homeBannerDTO) { ?>

                        <div class="swiper-slide"
                             style="background:url(<?php echo $rootpath.$homeBannerDTO->getPath().$homeBannerDTO->getLapEsFileName(); ?>); background-size: 100% !important;">
                        </div>
                        <!-- DEPRECATED --><div class="swiper-slide d-none"
                             style="background:url(<?php echo $rootpath.$homeBannerDTO->getPath().$homeBannerDTO->getMobEsFileName(); ?>); background-size: 100% !important;">
                        </div>
                    <?php } ?>

                </div>
                <!-- Scroll Down Icon -->
                <div class="slider-arrow d-none d-sm-block"><a href="#wrapper" id="gotowrapper"><div class='icon-scroll'></div></a></div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>

            </div>
        <?php } ?>
    </div>
    <!-- END OF HOME: BANNERS SLIDER ------------------------------------------------------------------------------- -->
    <!-- ----------------------------------------------------------------------------------------------------------- -->


    <!-- WRAPPER -->
    <div id="wrapper" class="wrapper pt-0">

        <!-- ------------------------------------------------------------------------------------------------------- -->
        <!-- NOVEDADES (Impresoras) y VÍDEOS ----------------------------------------------------------------------- -->
        <div class="container-fluid pt-2 pt-md-5 pb-5">

            <div class="container">

                <!-- ---------------------------------------- -->
                <!-- TITLE AND SUBTITLE --------------------- -->
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="display-4 pt-3 mb-0"><?php echo $strings[$language][200]; ?></h1>
                        <h2 class="ml-2 mb-5 mt-0"><small class="text-muted"><?php echo $strings[$language][202]; ?></small></h2>
                    </div>
                </div>
                <!-- END OF TITLE AND SUBTITLE -------------- -->
                <!-- ---------------------------------------- -->



                <!-- ---------------------------------------- -->
                <!-- VIDEOS & RELEASES ROW ------------------ -->
                <div class="row align-items-center">

                    <!-- ---------------------------------------- -->
                    <!-- HOME VIDEOS ---------------------------- -->
                    <?php
                    // If there are any printers on database we start creating rows:
                    if (is_array($arrayHomeVideosDTO) && sizeof($arrayHomeVideosDTO) > 0) { ?>

                        <!-- VIDEOS SECTION: TITLE -->
                        <div class="col-12 mb-3"><div class="text-secondary text-left"><h4><?php echo $strings[$language][211]; ?></h4></div></div>

                        <div class="col-12 col-md-6">

                            <!-- VIDEO PLAYER (FIRST VIDEO FROM DB) -->
                            <div class="videoWrapper">
                                <iframe id="youtube-video-0" width="100%" height="100%"
                                    src="https://www.youtube.com/embed/<?php echo $arrayHomeVideosDTO[0]->getYoutubeId(); ?>?enablejsapi=1&version=3&playerapiid=ytplayer&origin=http://talawin.com&showinfo=0&rel=0&cc_load_policy=0"
                                    frameborder="0" allowscriptaccess="always" allowfullscreen="true" allow="0"></iframe>
                            </div>
                            <!-- VIDEO PLAYER end -->

                        </div>
                        <div class="col-12 col-md-6 mt-5 mt-md-0">

                            <!-- YouTube Videos Carousel -->
                            <div class="owl-carousel owl-theme">
                                <?php
                                // We create an indicator for each HomeVideoDTO contained on the array.
                                //array_shift($arrayHomeVideosDTO);
                                foreach ($arrayHomeVideosDTO as $key => $homeVideoDTO) {
                                    if ($homeVideoDTO->isShown()) { ?>
                                        <div class="video-thumb">
                                            <div class="overlay-video" data-YoutubeId="<?php echo $homeVideoDTO->getYoutubeId(); ?>">  <!-- <i class="fas fa-play-circle"></i> -->
                                                <a href="https://www.youtube.com/watch?v=<?php echo $homeVideoDTO->getYoutubeId(); ?>" target="_blank">
                                                    <img src="https://img.youtube.com/vi/<?php echo $homeVideoDTO->getYoutubeId(); ?>/mqdefault.jpg" class="mb-2">
                                                </a>
                                                <a href="https://www.youtube.com/watch?v=<?php echo $homeVideoDTO->getYoutubeId(); ?>" target="_blank" class="overlay-video-play"></a>
                                            </div>
                                            <strong><a href="https://www.youtube.com/watch?v=<?php echo $homeVideoDTO->getYoutubeId(); ?>" target="_blank"><?php echo $homeVideoDTO->getYoutubeTitle(); ?></a></strong>
                                        </div>
                                    <?php }
                                } ?>
                            </div>

                        </div>
                    <?php } ?>
                    <!-- END OF HOME VIDEOS --------------------- -->
                    <!-- ---------------------------------------- -->

                </div>
                <!-- END OF VIDEOS & RELEASES ROW ----------- -->
                <!-- ---------------------------------------- -->

            </div><!-- End of .container -->
        </div><!-- End of .container-fluid -->


        <!-- ------------------------------------------------------------------------------------------------------- -->
        <!-- NOVEDADES (Impresoras) y VÍDEOS ----------------------------------------------------------------------- -->
        <div class="container-fluid bg-light pt-3 pb-4">

            <div class="container">

            <!-- ---------------------------------------- -->
            <!-- RELEASES ROW --------------------------- -->
            <div class="row align-items-center">

            <!-- ---------------------------------------- -->
            <!-- RELEASES ------------------------------- -->
            <?php
            // If there are any printers on database we start creating rows:
            if (is_array($arrayNewPrintersDTO) && sizeof($arrayNewPrintersDTO) > 0) { ?>
                <div class="col-12 mt-5 mb-4">

                    <div class="row">

                        <!-- RELEASES SECTION: TITLE -->
                        <div class="col-12"><h4 class="text-secondary text-left mb-3"><?php echo $strings[$language][212]; ?></h4></div>


                        <div class="owl-carousel owl-carousel-newReleases owl-theme">
                            <?php



                            // We create a card for each GeneralPrinterDTO contained on the array.
                            foreach ($arrayNewPrintersDTO as $key => $generalPrinterDTO) {

                                $generalFileDTO = $generalPrinterDTO->getFilesArray()[0];
                                if ($generalFileDTO instanceof FileDTO || $generalFileDTO instanceof Printer3dFileDTO ) {
                                    $main_pic = "<img src='" . $rootpath . $generalFileDTO->getPath() . "thum_" . $generalFileDTO->getName() . "' data-original='" . $rootpath . $generalFileDTO->getPath() . "thum_" . $generalFileDTO->getName() . "' class='card-img-top img-fluid lazy grid-img main-pic-grid-img pb-0' alt='" . $generalPrinterDTO->getTitle() . "'>";
                                } else { $main_pic = "<span><strong>Picture unavailable</strong></span>";  } ?>

                                <!-- -------------------------- -->
                                <!-- CARD --------------------- -->
                                <div class="printers-grid"> <!-- col-md-6 (BEFORE CAROUSEL) -->

                                    <div class="card d-flex mb-0">

                                        <!-- CARD HEADER -->
                                        <div class="card-header bg-light text-info">
                                            <span class="lead" style="font-weight: 500;"><a href="<?php echo $rootpath . $language .'/' .'hardware/'.(($generalPrinterDTO instanceof Printer3dDTO) ? '3d/' : '').'details/?product='.$generalPrinterDTO->getTitle().'&id='.$generalPrinterDTO->getId(); ?>" style="text-decoration: none;"><?php echo $generalPrinterDTO->getTitle(); ?></a></span>
                                        </div>

                                        <!-- CARD IMAGE -->
                                        <div class="main-pic-grid align-middle d-flex justify-content-center align-items-center">
                                            <?php
                                            // if Printer is sold (only Outlet) we place "sold" image
                                            if ($generalPrinterDTO->isSold()) { ?>
                                                <div class="printer-sold rounded"><img src="<?php echo $rootpath; ?>img/sold.gif" class="img-fluid lazy"></div>
                                            <?php } ?>
                                            <a href="<?php echo $rootpath . $language .'/hardware/'.(($generalPrinterDTO instanceof Printer3dDTO) ? '3d/' : '').'details/?product='.$generalPrinterDTO->getTitle().'&id='.$generalPrinterDTO->getId(); ?>" style="text-decoration: none;"><?php echo $main_pic; ?></a>
                                        </div>

                                        <!-- CARD BUTTONS -->
                                        <div class="text-right pl-3 pr-3 pb-3 pt-0 mt-auto">
                                            <a href="<?php echo $rootpath . $language .'/hardware/'.(($generalPrinterDTO instanceof Printer3dDTO) ? '3d/' : '') .'details/?product='.$generalPrinterDTO->getTitle().'&id='.$generalPrinterDTO->getId(); ?>"><button type="button" class="btn btn-sm btn-outline-info float-left"><?php echo $strings[$language][310]; ?></button></a>
                                            <?php
                                            // If Printer is sold (only Outlet) we disable "quote" button change label
                                            if ($generalPrinterDTO->isSold()) { ?>
                                                <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#quoteModal" data-product="<?php echo $generalPrinterDTO->getTitle(); ?>" disabled><i class="fa fa-eur" aria-hidden="true"></i> <?php echo $strings[$language][312]; ?></button>
                                            <?php } else { ?>
                                                <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#quoteModal" data-product="<?php echo $generalPrinterDTO->getTitle(); ?>"><i class="fa fa-eur" aria-hidden="true"></i> <?php echo $strings[$language][311]; ?></button>
                                            <?php } ?>
                                        </div>
                                    </div>

                                </div>
                                <!-- END OF CARD -------------- -->
                                <!-- -------------------------- -->
                            <?php } ?>
                        </div> <!-- end owl-carousel -->

                        <!--                                </div>-->

                    </div> <!-- end row -->
                </div> <!-- end 2nd col-6 -->
            <?php } ?>
            <!-- END OF RELEASES ------------------------ -->
            <!-- ---------------------------------------- -->

            </div>
            <!-- END OF RELEASES ROW -------------------- -->
            <!-- ---------------------------------------- -->

            </div> <!-- End of .container -->
        </div> <!-- End of .container-fluid -->


        <!-- MAIN CONTENT -->
        <div id="sectionCards" class="container mt-2 pt-5">
            <div class="row">

                <div class="col">

                    <div class="card bg-light mb-4 mb-xl-0 hideme" onclick="location.href='<?php echo $rootpath . $language .'/'; ?>hardware'" style="cursor:pointer;"> <!-- mb-3 mb-md-0 mr-md-3 -->
                        <img class="card-img-top img-fluid" src="<?php echo $rootpath; ?>img/card-home1_th.jpg" alt="Impresoras nuevas">
                        <img src="<?php echo $rootpath; ?>img/printers-headline.gif" width="100%">
                        <div class="card-body">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-2"><h5 class="card-title text-dark font-weight-normal"><i class="fas fa-certificate"></i></h5></div>
                                <div class="col-10"><h5 class="card-title text-dark font-weight-normal"><?php echo $strings[$language][203]; ?></h5></div>
                            </div>
                            <p class="card-text"><?php echo $strings[$language][204]; ?></p>
                        </div>
                    </div>

                </div>
                <div class="w-100 d-sm-none"></div>

                <!-- 3D PRINTERS -->
                <!-- <div class="col">

                    <div class="card bg-light mb-4 mb-xl-0 hideme" onclick="location.href='<?php echo $rootpath . $language .'/'; ?>hardware/3d'" style="cursor:pointer;">
                        <img class="card-img-top img-fluid" src="<?php echo $rootpath; ?>img/card-home5_th.png" alt="Impresoras 3D">
                        <img src="<?php echo $rootpath; ?>img/printers-headline.gif" width="100%">
                        <div class="card-body">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-2"><h5 class="card-title text-dark font-weight-normal"><i class="fas fa-cube"></i></h5></div>
                                <div class="col-10"><h5 class="card-title text-dark font-weight-normal"><?php echo $strings[$language][213]; ?></h5></div>
                            </div>
                            <p class="card-text"><?php echo $strings[$language][214]; ?></p>
                        </div>
                    </div>
                </div>
                <div class="w-100 d-xl-none"></div> -->
                <div class="col">

                    <div class="card bg-light mb-4 mb-xl-0 hideme" onclick="location.href='<?php echo $rootpath . $language .'/'; ?>hardware/?section=outlet'" style="cursor:pointer;"> <!-- mb-3 mb-md-0 mx-sm-0 mr-md-3 -->
                        <img class="card-img-top img-fluid" src="<?php echo $rootpath; ?>img/card-home2_th.jpg" alt="Segunda mano">
                        <img src="<?php echo $rootpath; ?>img/printers-headline.gif" width="100%">
                        <div class="card-body">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-2"><h5 class="card-title text-dark font-weight-normal"><i class="fas fa-check-circle"></i></h5></div>
                                <div class="col-10"><h5 class="card-title text-dark font-weight-normal"><?php echo $strings[$language][205]; ?></h5></div>
                            </div>
                            <p class="card-text"><?php echo $strings[$language][206]; ?></p>
                        </div>
                    </div>

                </div>
                <div class="w-100 d-sm-none"></div>
                <div class="col">

                    <div class="card bg-light mb-4 mb-xl-0 hideme" onclick="location.href='<?php echo $rootpath . $language .'/'; ?>about'" style="cursor:pointer;"> <!-- mb-3 mb-md-0 mr-md-3 -->
                        <img class="card-img-top img-fluid" src="<?php echo $rootpath; ?>img/card-home3_th.jpg" alt="Sobre nosotros">
                        <img src="<?php echo $rootpath; ?>img/printers-headline.gif" width="100%">
                        <div class="card-body">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-2"><h5 class="card-title text-dark font-weight-normal"><i class="fas fa-user-tie"></i></h5></div>
                                <div class="col-10"><h5 class="card-title text-dark font-weight-normal"><?php echo $strings[$language][207]; ?></h5></div>
                            </div>
                            <p class="card-text"><?php echo $strings[$language][208]; ?></p>
                        </div>
                    </div>

                </div>
                <div class="w-100 d-xl-none"></div>
                <div class="col">

                    <div class="card bg-light mb-4 mb-xl-0 hideme" onclick="location.href='<?php echo $rootpath . $language .'/'; ?>contact'" style="cursor:pointer;"> <!-- mb-3 mb-sm-0 mx-sm-0 -->
                        <img class="card-img-top img-fluid" src="<?php echo $rootpath; ?>img/card-home4_th.jpg" alt="Contáctanos">
                        <img src="<?php echo $rootpath; ?>img/printers-headline.gif" width="100%">
                        <div class="card-body">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-2"><h5 class="card-title text-dark font-weight-normal"><i class="fas fa-comments"></i></h5></div>
                                <div class="col-10"><h5 class="card-title text-dark font-weight-normal"><?php echo $strings[$language][209]; ?></h5></div>
                            </div>
                            <p class="card-text"><?php echo $strings[$language][210]; ?></p>
                        </div>
                    </div>

                </div>

            </div> <!-- ROW end -->

            <!-- ------------------------------------------------------------------------------------------------------- -->
            <!-- MAILCHIMP SIGNUP FORM --------------------------------------------------------------------------------- -->
            <div class="row">
                <div class="mx-auto mt-5 px-4">







                    <!-- Begin MailChimp Signup Form -->
                    <div id="mc_embed_signup">
                        <form id="mc-embedded-subscribe-form" action="https://talawin.us18.list-manage.com/subscribe/post?u=ccbb912f0816915f1809ece54&amp;id=d58696c51c" method="post" name="mc-embedded-subscribe-form" class="mc-embedded-subscribe-form validate form-inline" target="_blank" novalidate>
                            <div class="d-inline" id="mc_embed_signup_scroll">

                                <span class="h4 mr-4" style="vertical-align: sub;"><i class="far fa-newspaper"></i> <?php echo $strings[$language][700]; ?></span>

                                <br class="d-md-none">
                                <br class="d-md-none">

                                <input type="text" id="mce-FNAME" value="" name="FNAME" class="form-control required mr-1 d-inline col-5" placeholder="<?php echo $strings[$language][701]; ?>">

                                <input type="email" id="mce-EMAIL" value="" name="EMAIL" class="form-control required email d-inline col-5" placeholder="<?php echo $strings[$language][702]; ?>">

                                <div id="mce-responses" class="clear">
                                    <div class="response" style="display:none"></div>
                                    <div class="response" style="display:none"></div>
                                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->

                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_ccbb912f0816915f1809ece54_dd3bded374" tabindex="-1" value=""></div>

                                <br class="d-md-none">

                                <div class="clear mt-2 mt-md-none"><input type="submit" value="<?php echo $strings[$language][703]; ?>" name="subscribe" class="btn btn-success button-mailchimp"></div>
                            </div>
                        </form>
                        <!-- PRIVACY POLITIC ACCEPTANCE CHECKBOX -->
                        <div class="privacy"><label for="cbox2"><?php echo $strings[$language][706]; ?></label><input type="checkbox" id="cbox2" value=""/></div>
                    </div>
                    <!--End mc_embed_signup-->

                </div>
            </div>
            <!-- MAILCHIMP SIGNUP FORM end ----------------------------------------------------------------------------- -->
            <!-- ------------------------------------------------------------------------------------------------------- -->


        </div>
        <!-- MAIN CONTENT end -->


        <!-- Quote Modal -->
        <div class="modal fade" id="quoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header border-info bg-light rounded-top">
                        <h5 class="modal-title text-info" id="exampleModalLabel"><?php echo $strings[$language][402]; ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="formResponse"></div>
                        <div id="formContainer">
                            <form id="quoteForm">
                                <div class="form-group">
                                    <label for="form_name"><?php echo $strings[$language][404]; ?></label>
                                    <input type="text" class="form-control form-control-sm" id="form_name" name="form_name" required>
                                </div>
                                <div class="form-group">
                                    <label for="form_company"><?php echo $strings[$language][405]; ?></label>
                                    <input type="text" class="form-control form-control-sm" id="form_company" name="form_company" required>
                                </div>
                                <div class="form-group">
                                    <label for="form_email"><?php echo $strings[$language][406]; ?></label>
                                    <input type="email" class="form-control form-control-sm" id="form_email" name="form_email" required>
                                </div>
                                <div class="form-group">
                                    <label for="form_phone"><?php echo $strings[$language][407]; ?></label>
                                    <input type="tel" class="form-control form-control-sm" id="form_phone" name="form_phone" required>
                                </div>
                                <div class="form-group">
                                    <label for="form_message"><?php echo $strings[$language][408]; ?></label>
                                    <textarea class="form-control form-control-sm" id="form_message" name="form_message" style="word-wrap: break-word; resize: vertical;"></textarea>
                                </div>
                                <input type="hidden" id="form_subject" name="form_subject">
                                <!-- PRIVACY POLITIC ACCEPTANCE CHECKBOX -->
                                <div class="privacy" style="padding-left: .8rem;"><input type="checkbox" id="cbox2" value=""/><label style="padding-left: .4rem;" for="cbox2"><?php echo $strings[$language][706]; ?></label></div>
                        </div>
                    </div>
                    <div class="modal-footer bg-light rounded-bottom">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $strings[$language][409]; ?></button>
                        <button type="submit" class="btn btn-success" id="quoteSubmit"><?php echo $strings[$language][410]; ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Quote Modal END -->


        <!-- MatchHeight jQuery Plugin -->
        <script src="<?php echo $rootpath; ?>js/jquery.matchHeight.js" type="text/javascript"></script>

        <!-- Lazy Load jQuery Plugin -->
        <script src="<?php echo $rootpath; ?>js/jquery.lazyload.js" type="text/javascript"></script>

        <!-- Swiper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js"></script>

        <!-- CallPlayer (Pure JS script for YouTube Videos Carousel -->
        <!--<script src="<?php echo $rootpath; ?>js/callplayer.js" type="text/javascript"></script>-->

        <script type="text/javascript">


            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // INITIALIZE SWIPER: HOME BANNERS SLIDER //////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            var swiper = new Swiper('.swiper-container', {
                pagination: '.swiper-pagination',
                paginationClickable: true,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
                spaceBetween: 30,
                preloadImages: false,
                lazyLoading: true,
                autoplay: 6000,
                autoplayDisableOnInteraction: false,
                lazyLoadingInPrevNext: true
            });
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // END OF INITIALIZE SWIPER: HOME BANNERS SLIDER ///////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////





            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // DOCUMENT READY BLOCK ////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $(function () {

                // Add active item to nav-bar.
                $('#menu-home').addClass('active');


                // Init Owl Carousel (New Releases)
                $('.owl-carousel-newReleases').owlCarousel({
                    loop:true,
                    margin:26,
                    stagePadding:5,
                    autoplay:true,
                    autoplayTimeout:7500,
                    responsive:{0:{items:1}, 576:{items:2}, 768:{items:3}, 992:{items:4}}})
                // Init Owl Carousel (YouTube Videos Carousel)
                var owlYouTube = $('.owl-carousel');
                owlYouTube.owlCarousel({
                    loop:true,
                    margin:10,
                    autoplay:true,
                    responsive:{0:{items:2}, 576:{items:2}, 768:{items:2}, 992:{items:2}}})
                owlYouTube.trigger('next.owl.carousel');



                // Video Thumnail to Player
                $('.overlay-video').on('click', function(e) {
                    e.preventDefault();
                    let youtubeId = $(this).attr("data-YoutubeId");
                    $("#youtube-video-0").attr("src",'https://www.youtube.com/embed/' + youtubeId + '?enablejsapi=1&version=3&playerapiid=ytplayer&origin=http://talawin.com&autoplay=1&showinfo=0&rel=0&cc_load_policy=0');
                });




                // Equal Height for Printer Cards (Releases) (plugin matchHeight)
                setTimeout(function() {$('.main-pic-grid').matchHeight()}, 200);
                // Equal Height for Section Cards (plugin matchHeight)
                setTimeout(function() {$('.card').matchHeight()}, 200);




                ////////////////////////////////////////////////////////////////////
                // PRINTER QUOTATION MODAL /////////////////////////////////////////
                $('#quoteModal').on('show.bs.modal', function (event) {
                    $("#formResponse").html('');
                    $("#form_message").val('');
                    $("#formContainer").show();
                    $("#quoteSubmit").show();
                    $("#form_subject").val('<?php echo $strings[$language][402]; ?> <?php echo $generalPrinterDTO->getTitle(); ?>');
                    var button = $(event.relatedTarget) // Button that triggered the modal
                    var product = button.data('product') // Extract info from data-* attributes
                    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                    var modal = $(this)
                    modal.find('.modal-title').text('<?php echo $strings[$language][400]; ?> ' + product)
                    //modal.find('.modal-body input').val(product)
                });

                // AJAX: QUOTATION SENDING EMAIL
                $("#quoteForm").on('submit', function(e) {
                    // Prevent default submit action.
                    e.preventDefault();

                    // Check if privacy has been accepted.
                    if(!$("#quoteModal .privacy > input").is(":checked")) {
                        $('#privacy-error').modal('show');
                        return;
                    }

                    // Disable submit button.
                    $('#quoteSubmit').attr("disabled", true);
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $rootpath; ?>lib/email-contact.php",
                        data: $(this).serialize(),
                        success: function(response){
                            if (response != 'success') {
                                $('#quoteSubmit').attr("disabled", false);
                                //$("#formResponse").html(response);
                                $("#formResponse").fadeOut(0,function(){
                                    $(this).html(response).fadeIn();
                                });
                            } else {
                                $("#formResponse").html('<div class="alert alert-success alert-dismissible form-notification" role="alert">'+
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                                    '<?php echo $strings[$language][411]; ?></div>'
                                    //+
                                    //'<script>$(".form-notification").fadeTo(2000, 500).fadeOut(400, function(){$(".form-notification").alert("close");$("#quoteSubmit").removeAttr("disabled");});<\/script>'
                                );
                                $("#formContainer").fadeOut();
                                $('#quoteSubmit').attr("disabled", false);
                                $("#quoteSubmit").fadeOut();
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            $("#formResponse").html('<div class="alert alert-danger alert-dismissible form-notification" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php echo $strings[$language][412]; ?></div>'
                                //+
                                //'<script>$(".form-notification").fadeTo(2000, 500).fadeOut(400, function(){$(".form-notification").alert("close");});<\/script>'
                            );
                            $('#quoteSubmit').attr("disabled", false);
                            console.log(textStatus, errorThrown);
                        }
                    });

                });
                // END OF PRINTER QUOTATION MODAL ////////////////////////////////////
                //////////////////////////////////////////////////////////////////////



                // Check if privacy police is accepted before sending MailChimp subscription.
                $("#mc-embedded-subscribe-form").on("submit", function(e) {
                    // Check if privacy has been accepted.
                    if(!$("#mc_embed_signup .privacy > input").is(":checked")) {
                        e.preventDefault();
                        $('#privacy-error').modal('show');
                    }
                });

            });
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // END OF DOCUMENT READY BLOCK /////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        </script>

<?php
include_once($rootpath.'inc/foot.php');
?>